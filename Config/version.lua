local version = {}

function version.onlyDigit()
	local list = string.split(version.string(), ".")
	local str = ""
	for i,v in ipairs(list) do
		str = str .. v
	end
	return str
end

function version.string()
	--return "15.04.12.01"
	return "15.05.29.04"
end

return version

-- VERSION = "14.04.15.01"--"15.04.11.01"

-- local M = {}
-- function M:create()
-- 	local o = {}
-- 	setmetatable(o , self)
-- 	self.__index = self
-- 	return o
-- end

-- function M:get_VERSION()
-- 	local str_version = string.split(VERSION , ".")
-- 	local str_url = ""
-- 	for i,v in pairs(str_version) do
-- 		str_url = str_url .. v
-- 	end
-- 	self.versions = str_url
-- 	return self.versions
-- end

-- function M:get_font()
-- 	return VERSION
-- end

-- return M