
local vip = {
	  {
		  lv = 1,
		  gift = {
			  {cid='16005', num=1},
			  {cid='16001', num=10},
			  {cid='16018', num=1},
			  {cid='silver', num=100000},
			  },
		  privilege = "1.可免费领取VIP1特权大礼包\n2.可购买VIP1礼包\n3.更多VIP惊喜特权敬请期待！",
		  gold = 100
	  },
	  {
		  lv = 2,
		  gift = {
			  {cid='16005', num=2},
			  {cid='5325', num=1},
			  {cid='5330', num=1},
			  {cid='silver', num=200000},
			  },
		  privilege = "1.可免费领取VIP1特权大礼包\n2.可购买VIP2及其以下礼包\n3.更多VIP惊喜特权敬请期待！",
		  gold = 500
	  },
	  {
		  lv = 3,
		  gift = {
			  {cid='5306', num=1},
			  {cid='5410', num=1},
			  {cid='5401', num=1},
			  {cid='silver', num=300000},
			  },
		  privilege = "1.可免费领取VIP1特权大礼包\n2.可购买VIP3及其以下礼包\n3.更多VIP惊喜特权敬请期待！",
		  gold = 2000
	  },
	  {
		  lv = 4,
		  gift = {
			  {cid='gold', num=100},
			  {cid='5417', num=1},
			  {cid='5421', num=1},
			  {cid='silver', num=400000},
			  },
		  privilege = "1.可免费领取VIP1特权大礼包\n2.可购买VIP4及其以下礼包\n3.更多VIP惊喜特权敬请期待！",
		  gold = 5000
	  },
	  {
		  lv = 5,
		  gift = {
			  {cid='16005', num=1},
			  {cid='1512', num=1},
			  {cid='5419', num=1},
			  {cid='silver', num=500000},
			  },
		  privilege = "1.可免费领取VIP1特权大礼包\n2.可购买VIP5及其以下礼包\n3.更多VIP惊喜特权敬请期待！",
		  gold = 10000
	  },
	  {
		  lv = 6,
		  gift = {
			  {cid='16005', num=1},
			  {cid='1506', num=1},
			  {cid='5507', num=1},
			  {cid='silver', num=500000},
			  },
		  privilege="1.可免费领取VIP1特权大礼包\n2.可购买VIP6及其以下礼包\n3.更多VIP惊喜特权敬请期待！",
		  gold=30000
	  },
	  {
		  lv=7,
		  gift = {
			  {cid='5514', num=1},
			  {cid='5516', num=1},
			  {cid='5508', num=1},
			  {cid='silver', num=600000},
			  },
		  privilege = "1.可免费领取VIP1特权大礼包\n2.可购买VIP7及其以下礼包\n3.更多VIP惊喜特权敬请期待！",
		  gold = 100000
	  },
	  {
		  lv = 8,
		  gift = {
			  {cid='5510', num=1},
			  {cid='5506', num=1},
			  {cid='1505', num=1},
			  {cid='silver', num=800000},
			  },
		  privilege = "1.可免费领取VIP1特权大礼包\n2.可购买VIP8及其以下礼包\n3.更多VIP惊喜特权敬请期待！",
		  gold = 180000
	  },
	  {
		  lv = 9,
		  gift = {
			  {cid='5509', num=1},
			  {cid='5510', num=10},
			  {cid='16005', num=5},
			  {cid='silver', num=1200000},
			  },
		  privilege = "1.可免费领取VIP1特权大礼包\n2.可购买VIP9及其以下礼包\n3.更多VIP惊喜特权敬请期待！",
		  gold = 300000
	  },
}
return vip


