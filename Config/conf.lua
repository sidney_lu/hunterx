
IMAGEBUTTON = 0 --图片按钮
TEXTBUTTON = 1  --文本按钮

FONT = "Arial"
COMMONPATH = IMG_PATH .. "image/common/"
SCENECOMMON = IMG_PATH .. "image/scene/common/"

DESCCOLOR = ccc3(0xa3, 0x1d, 0x0e)

local config_type = ""
--0.5625
CONFIG_SCREEN_WIDTH     = 	480 --540 --480 --675 --900
CONFIG_SCREEN_HEIGHT    = 	854 --960 --854 --1200 --1600
CONFIG_SCREEN_AUTOSCALE = "FIXED_WIDTH"

CCTOUCHBEGAN = "began"
CCTOUCHMOVED = "moved"
CCTOUCHENDED = "ended"

isSoundAllowed = true
isMusicAllowed = true

isDebug = true
--end