

-- 装备星级对应强化最高等级
local EquipmentStrengthenMaxLevel = {
		[1] = {
			['levmax'] = 30
			},
		[2] = {
			['levmax'] = 40
			},
		[3] = {
			['levmax'] = 60
			},
		[4] = {
			['levmax'] = 90
			},
		[5] = {
			['levmax'] = 100
			}
	}

return EquipmentStrengthenMaxLevel