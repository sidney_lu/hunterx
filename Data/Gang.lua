    --[[

		帮派数据

]]


DATA_Gang = {}


-- 私有变量
local _data = {}

function DATA_Gang:init()
	_data = {}
end

function DATA_Gang:update( ... )
    local params = {...}
    local count = table.nums(params)
    local data = params[count]
    local temp = data
    for i=count-1,1,-1 do
         temp[params[i]] = temp
    end

end
function DATA_Gang:update( key ,data)
    print("merge",key,data)
    dump(data)
    if key=="" then
        table.merge(_data,data)
    else
        table.merge(_data[key],data)
    end
end
function DATA_Gang:set(key,sub,data)
    if not _data[key] then
        _data[key] = {}
    end
	_data[key][sub] = data
---info属性
---         "chieftains_name" = "tyc"			帮主
---         "chieftains_uid"  = "12705"			帮主uid
---         "count"           = 1				帮派当前人数			
---         "count_max"       = 20				帮派最大数
---         "funds"           = 0				资金
---         "id"              = "51"			帮派ID
---         "lv"              = 1				帮派等级
---         "name"            = "cc"			帮派名称
---         "notice"          = ""				公告内容
---         "rtime"           = 1374202908		自己入帮时间
---         "state"           = 1				自动加入申请为1      需申核后加入的为0
---         "sum_ability"     = 797				帮派战斗力
---         "time"            = "1374202908"	建帮派时间
---         "tribute"         = 0				帮贡
---         "userstate"       = 100				100 => '帮主' , 95 => '副帮主' , 90 => '堂主' , 0 => '帮众'
---         "usertribute"     = 0				帮威
---         "usertribute_v"   = 0				可用帮威
--         
--      成员信息
---     "list" = {						
---         1 = {
---             "ability" = 797					成员战力
---             "name"    = "tyc"				成员名称
---             "sex"     = "1"					成员性别
---             "silver"  = 0					成员总共捐献的银两
---             "state"   = 100					成员权限
---             "index"   = 0					成员同权限索引
---             "time"    = 1374202908			员成入职时间
---             "lv"    = 12					员成入等级
---             "title"   = "帮主"				成员头衔
---             "tribute" = 0					成员帮贡(帮威)
---             "uid"     = "12705"				成员uid
---         }
end
function DATA_Gang:get(key)
	if key == nil then return _data end
	
	return _data[key]
end

function DATA_Gang:set_type( type , data)
	_data[ type .. "" ] = data
end

--是否加入帮派
function DATA_Gang:isJoinGang()
	_data.info = _data.info or {}
	return ( table.nums( _data.info ) > 0 ) 
end
--返回申请的帮派列表
function DATA_Gang:getApply()
	return _data.applyData or nil 
end

function DATA_Gang:newTempData()
    if _data == nil then
	   _data = {}
    end
	_data.info = {	chieftains_name = "tyc"			,--帮主
	    chieftains_uid  = "12705"		,--	帮主uid
         count           = 1		,	--	帮派当前人数			
         count_max      = 20		,	--	帮派最大数
         funds         = 0		,	--	资金
         id              = "51"	,	--	帮派ID
         lv              = 1		,	--	帮派等级
         name            = "cc"	,	--	帮派名称
         notice          = ""		,	--	公告内容
         rtime           = 1374202908,	--	自己入帮时间
         state           = 1		,	--	自动加入申请为1      需申核后加入的为0
         sum_ability     = 797	,	--		帮派战斗力
         time            = "1374202908",--	建帮派时间
         tribute         = 0		,		--帮贡
         userstate       = 100	,		--	100 => '帮主' , 95 => '副帮主' , 90 => '堂主' , 0 => '帮众'
         usertribute     = 0		,		--帮威
         usertribute_v   = 0				--可用帮威
     }

     _data.shop = {}
     _data.shop.gemconfig = {}
     _data.shop.gemconfig[1] = {id=1,uid=5468,cid=16024,tribute =20, name = "红宝石", bagdesc = "ffdf"}
     _data.shop.gemconfig[2] = {id=1,uid=5468,cid=16023,tribute =10, name = "红宝石", bagdesc = "ffdfsad"}
     _data.shop.gemconfig[3] = {id=1,uid=5468,cid=16022,tribute =5, name = "红宝石", bagdesc = "ffdf"}
     _data.shop.gemconfig[3] = {id=1,uid=5468,cid=16021,tribute =1, name = "红宝石", bagdesc = "ffdf"}
     --[[宝石
     	tempData.gemconfig = data["result"].gemconfig
		DATA_Gang:set_type( "shop" , tempData )
		
		DATA_Gang:set_type( "info" , data["result"].info )
		DATA_Gang:set_type( "list" , data["result"].list )
		]]

    _data.gang_rank = {}
    _data.gang_rank.ability = {}
    list = _data.gang_rank.ability
    list[1] = {characterId=1000068,characterLevel=1,ability = 35687,nickname ="nmb",top=1,count=1,count_max=50}
    list[2] = {characterId=1000069,characterLevel=3,ability = 1153,nickname ="nmb",top=2,count=1,count_max=50}
    list[3] = {characterId=1000067,characterLevel=4,ability = 356589,nickname ="nmb",top=3,count=1,count_max=50}
    list[4] = {characterId=1000066,characterLevel=1,ability = 598775,nickname ="nmb",top=4,count=1,count_max=50}
    list[5] = {characterId=1000069,characterLevel=1,ability = 105699,nickname ="nmb",top=5,count=1,count_max=50}
    _data.gang_rank.ability_top = 3

    --名望
    _data.gang_rank.tribute = {}
    list = _data.gang_rank.tribute
    list[1] = {characterId=1000066,characterLevel=1,contribution = 598775,sum_ability=500,nickname ="nmb",top=1,count=1,count_max=50}
    list[2] = {characterId=1000069,characterLevel=1,contribution = 105699,sum_ability=500,nickname ="nmb",top=2,count=1,count_max=50}
    _data.gang_rank.tribute_top = 2
    --总榜
    _data.gang_rank.rank = {}
    list = _data.gang_rank.rank
    list[1] = {level=1,chief_name="bb",ability = 35687,sum_ability=500,name ="nmb",top=1,count=1,count_max=50}
    list[2] = {level=3,chief_name="bb",ability = 1153,sum_ability=500,name ="nmb",top=2,count=1,count_max=50}
    list[3] = {level=4,chief_name="bb",ability = 356589,sum_ability=500,name ="nmb",top=3,count=1,count_max=50}
    list[4] = {level=1,chief_name="bb",ability = 598775,sum_ability=500,name ="nmb",top=4,count=1,count_max=50}
    list[5] = {level=1,chief_name="bb",ability = 105699,sum_ability=500,name ="nmb",top=5,count=1,count_max=50}
    list[6] = {level=1,chief_name="bb",ability = 598775,sum_ability=500,name ="nmb",top=6,count=1,count_max=50}
    list[7] = {level=1,chief_name="bb",ability = 105699,sum_ability=500,name ="nmb",top=7,count=1,count_max=50}
    _data.gang_rank.rank_top = 5
        -- body

    --[[
        总榜
    ]]

    
    -- _data.task = {}
    -- _data.task.task = {}

    -- item = _data.task.task
    -- item.info = {}
    -- list = item.info
    -- list[1] = {tribute=50,name ="采草莓",taskid=1,bagdesc = "no ho",state=1,type=1,desc="杨工路名路过杏子林，遇到一老太在磨针,原来铁杵真的可以磨针。"}
    -- list[1].award = {cid=6001,lv=1,num=5}
    -- list[2] = {tribute=50,name ="采草莓",taskid=2,bagdesc = "no ho",state=1,type=2,desc="baba"}
    -- list[2].award = {cid=6001,lv=1,num=6}
    -- list[3] = {tribute=50,name ="采草莓",taskid=3,bagdesc = "no ho",state=1,type=3,desc="palapala"}
    -- list[3].award = {cid=6001,lv=1,num=7}
    -- list[4] = {tribute=50,name ="采草莓",taskid=4,bagdesc = "no ho",state=1,type=4,desc="111"}
    -- list[4].award = {cid=6001,lv=1,num=5}
    -- list[5] = {tribute=50,name ="采草莓",taskid=5,bagdesc = "no ho",state=1,type=5,desc="22"}
    -- list[5].award = {cid=6001,lv=1,num=5}
    -- list[6] = {tribute=50,name ="采草莓",taskid=6,bagdesc = "no ho",state=1,type=6,desc="33"}
    -- list[6].award = {cid=6001,lv=1,num=5}
    -- list[7] = {tribute=50,name ="采草莓",taskid=7,bagdesc = "no ho",state=1,type=7,desc="44"}
    -- list[7].award = {cid=6001,lv=1,num=5}
    -- list[8] = {tribute=50,name ="采草莓",taskid=8,bagdesc = "no ho",state=1,type=8,desc="55"}
    -- list[8].award = {cid=6001,lv=1,num=5}
    --[[
        任务
    ]]

    -- _data.pray = {}
    -- list[1] = {award_gold=10,award_silver=250}
    -- list[2] = {award_gold=20,award_silver=250}
    -- list[3] = {award_gold=30,award_silver=250}
    -- list[4] = {award_gold=40,award_silver=250}
    -- list[5] = {award_gold=50,award_silver=250}
    -- list[6] = {award_gold=60,award_silver=250}

    -- _data.pray.clifford_award = list

    -- _data.pray.clifford_money = 50
    --[[
        祈福
    ]]

    --_data.task.donateinfo = {gold = {tribute=2,funds=50},silver={tribute=1,funds=25}}
    --_data.task.donate ={juangold=500,juansilver=235}
    
    --[[
        捐献
    ]]
    
end

return DATA_Gang