--[[

阵法数据

]]


DATA_Formation = {}
--当前选中哪个英雄
local cur
local max

local _pramp = {}
-- 私有变量
local _data = {}

function DATA_Formation:init()
	_data = {}
	_data.conf = {}
	_data.conf["1"] = 1
	_data.conf["2"] = 4
	_data.conf["3"] = 7
	_data.conf["4"] = 10
	_data.conf["5"] = 13
	_data.conf["6"] = 16
	_data.conf["7"] = 19
	_data.conf["8"] = 22
	_pramp = {}
	max = _data.conf
	local cur = nil
end


function DATA_Formation:setConf(maxConf)
	max = maxConf
end

function DATA_Formation:set(data)
	table.merge(_data, data)
end

function DATA_Formation:get_data()
	return _data
end

function DATA_Formation:get(...)
	local arg = {...}
	local result = _data
	for k, v in pairs(arg) do
		if not result then
--			print(arg[i],"字段未找到")
			break
		end
		if v then
			result = result[v..""]
		end
	end
	return result
end

function DATA_Formation:get_OnCount()
	--return #(_data.on)
	return table.nums(_data.on)
end
function DATA_Formation:get_OnList(  )
	return _data.on
end

function DATA_Formation:get_BackList(  )
	return _data.back
end

function DATA_Formation:get_ON(key)
	if key == nil then return _data end

	return _data[key]
end

function DATA_Formation:get_lenght()
	local count = 1
	local heros = self:get_OnList()
	for k,v in pairs(heros) do
		count = count + 1
	end

	--local on = table.getn(_data["on"])
	--local back = table.getn(_data["back"])\
	local backups = self:get_BackList()
	for k,v in pairs(backups) do
		count = count + 1
	end
	return count
end

function DATA_Formation:set_index(index)--这里取得的是位置，不是英雄id
	_pramp["index"] = index
end
function DATA_Formation:getHeroByIndex( pos )
	-- if pos > 4 then
	-- 	local backups = self:get_BackList()
	-- 	return backups[pos]
	-- else
		local heros = self:get_OnList()
		return heros[pos]
	--end
end
function DATA_Formation:get_index(i)--这个方法换替补？？
	if i then
		if i <= table.getn(_data["on"]) then
			return _data["on"][i]
		else
			return _data["back"][i-table.nums(_data["on"])]
		end
	else
		if _pramp["index"] <= table.getn(_data["on"]) then
			return _data["on"][(_pramp["index"])]
		else
			return _data["back"][(_pramp["index"]-table.getn(_data["on"]))]
		end
	end
end
--检查对应id是否上阵
function DATA_Formation:checkIsExist( _id )
	local isExist = false
	local pos
	for key , v in pairs(_data["on"]) do
		 if v.gid == _id then
			 pos = key
			 isExist = true
			 break
		 end
	end
	for key , v in pairs(_data["back"]) do
		 if v.gid == _id then
			pos = key + 4
			isExist = true
			break
		 end
	end
	return isExist,pos
end

--检查对应cid是否在阵
function DATA_Formation:checkOnByCid(cid)
	local exist
	for k, v in pairs(_data["on"]) do
		local general_data = DATA_Bag:get("general" , v.gid )
		--cid = DATA_General:get(hero["gid"] , "cid")
		--cid = general_data.cid
		if general_data.cid == tonumber(cid) then
			exist = true
		end
	end	
	
	if not exist then -- 应该这里都是空。。。后备貌似都在_data["on"]
		for k, v in pairs(_data["back"]) do
			if tonumber(v.cid) == tonumber(cid) then
				exist = true
			end	
		end
	end
	return exist 
end

--检查对应的装备(英雄??)是否上阵
--返回已佩戴该装备的英雄id
function DATA_Formation:checkEquip( _id )
	local isExist = false
	local pos
	local roleid
	for key , v in pairs(_data["on"]) do
		 if v.e1==_id or v.e2==_id or v.e3==_id or v.e4==_id then
			 pos = key
			 isExist = true
			 roleid=v.gid
			 break
		 end
	end
	for key , v in pairs(_data["back"]) do
		 if v.e1==_id or v.e2==_id or v.e3==_id or v.e4==_id then
			pos = key + 4
			isExist = true
			roleid=v.gid
			break
		 end
	end
	return roleid
end

--检查对应的技能是否上阵
function DATA_Formation:checkSkill( _id )
	-- print("----------DATA_Formation:checkSkill--------------_id:", _id)
	local isExist = false
	local pos
	local roleid
	for key , v in pairs(_data["on"]) do
		 if v.s2==_id or v.s3==_id then
			 pos = key
			 isExist = true
			 roleid=v.gid
			 break
		 end
	end
	for key , v in pairs(_data["back"]) do
		 if v.s2==_id or v.s3==_id then
			pos = key + 4
			isExist = true
			roleid=v.gid
			break
		 end
	end
	-- print("------------------------roleid:", roleid)
	return roleid
end

--TODO(后面有空再修改)获取id对应英雄是否穿戴eCid的装备
function DATA_Formation:isDress(id, eCid)
	-- if id then
	-- 	if _data[id..""] then
	-- 		for k, v in pairs(_data[id..""]) do
	-- 			if tonumber(v["cid"]) == tonumber(eCid) then
	-- 					return true
	-- 				end
	-- 		end
	-- 	end
	-- end
	return false
end


function DATA_Formation:setCur(id)
	cur = id
end

function DATA_Formation:getCur()
	return cur
end

function DATA_Formation:getMax()
	return max
end

--计算当前综合统帅值
function DATA_Formation:countLead()
	local leadValue = 0
	for key , v in pairs(_data["on"]) do
		local general_data = DATA_Bag:get("general" , v.gid )
		leadValue = leadValue + getConfig( "generallead" , getConfig( "general" , general_data.cid , "star" ) , "lead" )
	end
	for key , v in pairs(_data["back"]) do
		local general_data = DATA_Bag:get("general" , v.gid )
		leadValue = leadValue + getConfig( "generallead" , getConfig( "general" , general_data.cid , "star" ) , "lead" )
	end
	
	
	return leadValue
end
return DATA_Formation