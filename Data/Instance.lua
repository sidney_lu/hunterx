DATA_Instance = {

}
local _data


function DATA_Instance:init()
	_data = {}
end

function DATA_Instance:set(...)
	local arg = {...}
	local result = _data
	for i = 1, #arg do 
		if i == #arg - 1 then
			result[arg[i]] = arg[i + 1]
			break
		else
			if not result[arg[i]] then
				result[arg[i]] = {}
			end
			result = result[arg[i]]
		end 
	end
end

function DATA_Instance:clearMessage()
	_data["pet"]["message"] = nil
end

function DATA_Instance:get(...)
	local arg = {...}
	local result = _data
	for i = 1, #arg do
		result = result[arg[i]]
		if not result then
--			print(arg[i].."在table中未找到")
			break
		end
	end
	return result
end

local curEquipData
--存放当前忠义堂关卡位置
function DATA_Instance:setCurEquipData( params )
	curEquipData = params
end
function DATA_Instance:getCurEquipData()
	return curEquipData
end



return DATA_Instance