--[[

用户数据

]]


DATA_Rank = {}


-- 私有变量
local _data = {}

function DATA_Rank:init()
	_data = {}
end


function DATA_Rank:setKey(key , data)
	_data[key] = data
end


function DATA_Rank:get(...)
	local arg = {...}
	local result = _data
	for k, v in pairs(arg) do
		result = result[v]
	end
	return result
end

return DATA_Rank