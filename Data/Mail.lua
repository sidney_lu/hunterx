--[[

聊天

]]


DATA_Mail = {
	
-- 消息推送数字
system_num = 0,
battle_num = 0,
social_num = 0
}


-- 私有变量
local _data = {}

-- 最多存储条数
local _max = 20
local _num = 0
function DATA_Mail:init()
	_data = { system={}, battle={}, social={} }
end

function DATA_Mail:addData(data)
	table.insert(_data , data)

	if #_data > _max then
		table.remove(_data , 1)
	end
end

function DATA_Mail:getAll()
	return _data
end

function DATA_Mail:set_type(message_type, data)
	for k,v in pairs(data) do
		_num = _num + 1
		table.insert(_data[message_type] , v)
	end
	while _num>_max do
		table.remove(1)
		_num = _num - 1
	end
end

function DATA_Mail:get_type(message_type)
	return _data[message_type]
end

function DATA_Mail:getLast()
	if _num == 0 then return nil end

	return _data.system[_num]
end

function DATA_Mail:setNum(message_type, data)
	if message_type==nil then
		table.merge(DATA_Mail, data)
	else
		DATA_Mail[message_type.."_num"] = tonumber(data)
	end
end

function DATA_Mail:getNum(message_type)
	if message_type == nil then
		return DATA_Mail.battle_num+DATA_Mail.system_num+DATA_Mail.social_num
	end
	return DATA_Mail[message_type.."_num"]
end

return DATA_Mail