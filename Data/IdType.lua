--[[

		升阶配置

]]


DATA_IDTYPE = {}

-- 私有变量
local _data = {}
_data["1"] = "general"
_data["2"] = "pet"
_data["3"] = "skill"
_data["4"] = "petskill"
_data["5"] = "equip"
_data["6"] = "prop"
_data["7"] = "prop"
_data["8"] = "prop"
_data["9"] = "prop"
_data["16"] = "prop"
_data["17"] = "prop"
_data["18"] = "npc"

function DATA_IDTYPE:init()
end

function DATA_IDTYPE:set(data)
        -- print("---------------DATA_IDTYPE:set----------")
	table.merge(_data, data)
end


function DATA_IDTYPE:get(key)
	if key == nil then return _data end

	return _data[key]
end
--获取对应cid的类型
function DATA_IDTYPE:getType( _cid )
            -- print("---------------DATA_IDTYPE:getType:", _cid)
    local key = math.floor( _cid/1000 )
    if key>30 then
        key = '3'
    else
        key = key..""
    end
	return _data[key]
end

return DATA_IDTYPE