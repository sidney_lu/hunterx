DATA_Mission = {}

local _data
function DATA_Mission:init()
	--_data = nil 
	_data = {}
end

--index是当索引第几关卡，因为每次只返回当前选择关卡的数据
function DATA_Mission:set(index , data)
	if not _data then
		_data = {}
	end
	_data[index] = data
	_data["max"] = data["max"]           -- 已打过的最大关卡
	_data["cleanup"] = data["cleanup"]   -- 扫荡
	_data["cleanusetime"] = data["cleanusetime"]
	_data["current"] = {map_id = data["max"]["map_id"],mission_id = data["max"]["mission_id"]}
end

function DATA_Mission:setByKey(index , key , data)
	if _data and  _data[index] then
		if data then
			_data[index][key] = data
		else
			_data[index] = key
		end
	end
end

function DATA_Mission:setData(...)
	local arg = {...}
	local result = _data
	for i = 1, #arg do 
		if i == #arg - 1 then
			result[arg[i]] = arg[i + 1]
			break
		else
			echoLog("datamission ",arg[i])
			if not result[arg[i]] then
				result[arg[i]] = {}
			end
			result = result[arg[i]]
		end 
	end
end

function DATA_Mission:get(...)
	local arg = {...}
	local result = _data
	for k, v in pairs(arg) do
		if not result then
			-- dump(result)
			-- dump(arg)
			-- print(arg[i],"字段未找到")
			break
		end
		-- print("############## v:", v)
		
		result = result[v]
	end
	return result
end

 function DATA_Mission:haveData(index)
 	print("DATA_Mission",5)
 	dump(_data)
	if _data and _data[index] then
		return true
	end
	return false
end

local tempCurMissionData 
function DATA_Mission:getCurMissionData()
	return tempCurMissionData
end
--存放放前战斗的mapid 和  missionid	只为配合战斗中跳过按钮
function DATA_Mission:setCurMissionData( params )
	tempCurMissionData = params
end
return DATA_Mission