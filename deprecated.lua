


ccc3 = cc.c3b
ccc4 = cc.c4b
ccp = cc.p

CCNode = cc.Node
CCScene = cc.Scene
CCLayer = cc.Layer
CCSprite = cc.Sprite
CCLuaLog = print
CCSize = cc.size
CCMenu = cc.Menu
CCMenuItemImage = cc.MenuItemImage
CCRepeatForever = cc.RepeatForever
CCRotateBy = cc.RotateBy
CCLayerColor = cc.LayerColor
CCSizeMake = cc.size
CCOrbitCamera = cc.OrbitCamera
CCScaleTo = cc.ScaleTo
CCCallFunc = cc.CallFunc
CCSequence = cc.Sequence
CCProgressTimer = cc.ProgressTimer
CCPointMake = cc.p
CCRenderTexture = cc.RenderTexture
CCMoveTo = cc.MoveTo
CCEaseExponentialOut = cc.EaseExponentialOut
CCSpawn = cc.Spawn
CCAnimate = cc.Animate
CCFadeIn = cc.FadeIn
CCEaseBounceOut = cc.EaseBounceOut
CCDelayTime = cc.DelayTime
CCFadeTo = cc.FadeTo
CCTintTo = cc.TintTo
CCProgressTo = cc.ProgressTo
CCTextFieldTTF = cc.TextFieldTTF
CCEaseElasticOut = cc.EaseElasticOut
CCJumpTo = cc.JumpTo
CCEaseElasticIn = cc.EaseElasticIn
CCEaseExponentialInOut = cc.EaseExponentialInOut

UICutLayer = cc.Layer
echoLog = function ( ... )
	if 1==1 then
		print(...)
	end
end 
testlog = function ( ... )
	print(...)
end
CCC3 = ccc3(0xff,0xff,0xff)
--CCRectMake
local CRect = require("CRect")

function CCRectMake(_x,_y,_width,_height)
	return CRect.new(_x,_y,_width,_height)
end


--enum
kEditBoxInputFlagPassword = 0
kCCProgressTimerTypeBar = 1


--CCLabelTTF
kCCTextAlignmentLeft = cc.TEXT_ALIGNMENT_LEFT
kCCTextAlignmentRight = cc.TEXT_ALIGNMENT_RIGHT
kCCTextAlignmentCenter = cc.TEXT_ALIGNMENT_CENTER

kCCVerticalTextAlignmentTop = cc.VERTICAL_TEXT_ALIGNMENT_TOP
kCCVerticalTextAlignmentBottom = cc.VERTICAL_TEXT_ALIGNMENT_BOTTOM
kCCVerticalTextAlignmentCenter = cc.VERTICAL_TEXT_ALIGNMENT_CENTER

CCLabelTTF = {}
function CCLabelTTF:create(text, font, fontSize, hAlignment, vAlignment)
	hAlignment = hAlignment or cc.TEXT_ALIGNMENT_CENTER
	vAlignment = vAlignment or cc.VERTICAL_TEXT_ALIGNMENT_CENTER
	return display.newTTFLabel({
			text = text, 
			font = font,
			size = fontSize,
			color = display.COLOR_WHITE,
			align = hAlignment,
			valign = vAlignment
	})	
end

--CCDirector
CCDirector = {}
function CCDirector:sharedDirector()
	return cc.Director:getInstance()
end

