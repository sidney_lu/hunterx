--[[

所有 socket 通信发送前以及回调

]]


local M = {}

--[[注册验证]]
function M._10001( type , data , callback)
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		local result = data["result"]

		-- 存储数据!~
		if result ~= nil then
			DATA_Session:set({ uid = result["uid"] , sid = result["sid"] })
		end

		-- 执行成功回调
		callback(data)
		if result.servers == nil or #result.servers==0 then
			switchScene("login" , nil , function()
					KNMsg.getInstance():flashShow("当前没有服务器，请稍候登录。")
				end)
		else
			CHECK = nil
			switchScene("servers" , result)
		end
	end

	return true , data
end

--[[登录验证]]
function M._10002( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		local result = data["result"]
		print("[httpaction result,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,]"..result["sid"].."|uid="..result["uid"])
		-- 存储数据!~
		if result ~= nil then
			DATA_Session:set({ uid = result["uid"] , sid = result["sid"] })
		end

		-- 执行成功回调
		callback(data)
		if result.servers == nil then
			switchScene("login" , nil , function()
					KNMsg.getInstance():flashShow("当前没有服务器，请稍候登录。")
				end)
		else
			CHECK = nil
			switchScene("servers" , result)
		end
	end

	return true , data
end

--[[yd验证]]
function M._10004( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		local result = data["result"]

		-- 存储数据!~
		if result ~= nil then
			DATA_Session:set({ uid = result["uid"] , sid = result["sid"] })
		end

		-- 执行成功回调
		callback(data)
		if result.servers == nil then
			switchScene("login" , nil , function()
					KNMsg.getInstance():flashShow("当前没有服务器，请稍候登录。")
				end)
		else
			switchScene("servers" , result)
		end
	end

	return true , data
end


--[[选择登录到游戏服务器]]
function M._10003( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		local result = data["result"]

		-- 执行成功回调
		callback(data)
	end

	return true , data
end

--[[快速登录]]
function M.login_quick( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		return M.login_develop( type , data , callback )
	end

	return true , data
end

--[[登录游戏服务器]]
function M._20001( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		local result = data["result"]

		-- 判断是否是新号
		if result==nil or result._G_userinfo == nil then
			-- 跳转到注册页面
			switchScene("newguy" , {})
		else
			-- 登录成功后，尝试连接长连接服务器
			-- SOCKET:getInstance("battle"):call("log" , "in" , "login" , {} , {
			-- 	success_callback = function()
			-- 		-- KNGuide:setStep( 3500 )	-- 新手引导
			-- 		-- 登录成功后，跳转到首页
			-- 		switchScene("home")
			-- 	end
			-- })
			-- KNGuide:setStep( 3500 )	-- 新手引导
			switchScene("home")
			DATA_Session:set({ uid = result["_G_userinfo"]["uid"]})
		end
	end

	return true , data
end

--[[创建新角色]]
function M._20002( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		local result = data["result"]

		DATA_Guide:setGuide(100)	-- 新手引导
		switchScene("home")
		DATA_Session:set({ uid = result["_G_userinfo"]["uid"]})
	end

	return true , data
end

function M._20014( type, data, callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		callback(data)
	end
	return true , data
end

function M.pay_callback( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
	end

	return true , data
end


--[[wap]]
function M.wap_getid( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		switchScene("wapid")
	end

	return true , data
end


function M.wap_set( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		switchScene("home" , {} , function()
			KNMsg:getInstance():flashShow("保存wap游戏id成功")
		end)
	end

	return true , data
end

--[[定时获取信息]]
function M._40001(type , data , callback)
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		data = data["result"]
		--DATA_Chat:addData(data["add"])
		local scene = display.getRunningScene()
		if scene.name ~= "battle" and data["_G_mail_system"] and GLOBAL_INFOLAYER then		-- 战斗没法刷新
			print("-------GLOBAL_INFOLAYER:update-----------")
		 	GLOBAL_INFOLAYER:update()--message 和 info 均更新
		end
		
		--如果在消息界面，则刷新当前界面
		if scene.name == "chat" then
			if data["type"] == "talk" then
				DATA_Info:insert("_G_message_talk" , data["add"] )
			end
			scene:refreshChat(data["type"])
		end
		
		if DATA_Info:getIsOpen() then
			--聊天界面不存在时     消息按钮闪烁
			if data["type"] == "talk" then
				DATA_Info:msgSource( "world" )
				DATA_Info:setIsMsg( true )
			-- elseif DATA_Info:isCustomKey( data["type"] ) then
			-- 	DATA_Info:msgSource( "custom" )
			-- 	DATA_Info:setIsMsg( true )
			end
		else
			print("chat layer open",data["add"])
			--如果聊天界面打开时
			print(data["type"],TALK:getType())
			if (data["type"] == "talk" and TALK:getType() == "world") or (data["type"]=="gang" and TALK:getType() == "gang") then
				--DATA_Info:insert("_G_message_talk" , data["add"] )
				--TALK:addItem( data["add"] , "world" )
				for k,v in pairs(data["add"]) do
					DATA_Info:insert(data["type"] , v )
					TALK:addItem(data["add"],data["type"])
				end
				

			end
			--自定义聊天界面推送
			if DATA_Info:isCustomKey( data["type"] ) and TALK:getType() == "custom"  then
				DATA_Info:insert("custom" , data["add"] , DATA_Info:getCustomKey() )
				TALK:addItem( data["add"] , "custom" )
			end
		end
	end
	return true , data
end

--[[发送世界聊天]]
function M.chat_sendworld( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		if callback ~= nil then
			callback(data)
		end
	end
	return true , data
end

--[[获取关卡信息]]
function M._21001( type , data , callback )
	if type == 1 then
	else
		local result = data["result"]
		DATA_Mission:set(result["current"]["map_id"],result)
		callback()
	end
	return true , data
end

--[[关卡-战斗开始]]
function M._21002( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		local result = data["result"]
		DATA_Battle:setMod("mission")
		DATA_Battle:setAct("execute")
		DATA_Battle:set( result )
		if result.win == 1 then
			--存储数据
			DATA_Mission:set(result["current"]["map_id"],result)
			DATA_Mission:setByKey("current",result["jump"])
		end
		if result["max"] then
			DATA_Mission:setByKey("max",result["max"])
		end
		-- local dd = io.open("D:/x/fantasyhero/battle.txt","w")
		-- if dd then
		-- 	dd:write(serialize(data["result"].report))
		-- end
		-- io.close(dd)
		--结束画面用数据
		DATA_Result:set( result )

		switchScene("battle" , { intoAnimation = true , showInfo = "入场动画"})
	end

	return true , data
end
function serialize(obj)  
    local lua = ""  
    local t = type(obj)  
    if t == "number" then  
        lua = lua .. obj  
    elseif t == "boolean" then  
        lua = lua .. tostring(obj)  
    elseif t == "string" then  
        lua = lua .. string.format("%q", obj)  
    elseif t == "table" then  
        lua = lua .. "{\n"  
    for k, v in pairs(obj) do  
        lua = lua .. "" .. serialize(k) .. "=" .. serialize(v) .. ",\n"  
    end  
    local metatable = getmetatable(obj)  
        if metatable ~= nil and type(metatable.__index) == "table" then  
        for k, v in pairs(metatable.__index) do  
            lua = lua .. "" .. serialize(k) .. "=" .. serialize(v) .. ",\n"  
        end  
    end  
        lua = lua .. "}"  
    elseif t == "nil" then  
        return nil  
    else  
        error("can not serialize a " .. t .. " type.")  
    end  
    return lua  
end  
function unserialize(lua)  
    local t = type(lua)  
    if t == "nil" or lua == "" then  
        return nil  
    elseif t == "number" or t == "string" or t == "boolean" then  
        lua = tostring(lua)  
    else  
        error("can not unserialize a " .. t .. " type.")  
    end  
    lua = "return " .. lua  
    local func = loadstring(lua)  
    if func == nil then  
        return nil  
    end  
    return func()  
end  

function M.mission_cleanup_start(type, data, callback)
	if type == 1 then
	else
		DATA_Mission:setByKey("cleanup", data["result"]["cleanup"])
		callback()
	end
	return true, data
end

function M.mission_cleanup_cancel(type, data, callback)
	if type == 1 then
	else
		DATA_Mission:setByKey("cleanup", {})
		callback()
	end
	return true, data
end

function M.mission_cleanup_subtime(type, data, callback)
	if type == 1 then
	else
		DATA_Mission:setByKey("cleanup",data["result"]["cleanup"])
		callback()
	end
	return true, data
end

function M.mission_cleanup(type, data, callback)
	if type == 1 then
	else
		local result = data["result"]
		DATA_Mission:setData(result["current"]["map_id"], "missions", result["current"]["mission_id"], result["missions"][1])
		DATA_Mission:setByKey("cleanup", {})
		DATA_Result:set( result )
		callback(result["current"]["map_id"])
	end
	return true, data
end
function M._21008( type,data,callback )
	if type == 1 then
	else
		local result = data["result"]
		--DATA_Mission:setData(result["current"]["map_id"], "missions", result["current"]["mission_id"], result["missions"][1])
		DATA_Mission:setByKey("cleanup", {})
		DATA_Result:set( result )
		--callback(result["current"]["map_id"])
		callback()
	end
	return true, data
end

function M.inshero_get(type , data , callback)
	if type == 1 then
	else
		DATA_Instance:set("hero",data["result"])
		callback()
	end
	return true , data
end


function M.insheronewnew_get(type , data , callback)
	if type == 1 then
	else
		DATA_Instance:set("hero",data["result"])
		callback()
	end
	return true , data
end

--抽签
function M.insheronewnew_draw(type , data , callback)
	if type == 1 then
	else
		DATA_Instance:set("hero",data["result"]["get"])
		callback()
	end
	return true , data
end

--兑换英雄
function M.insheronewnew_exchange(type , data , callback)
	if type == 1 then
	else
		DATA_Instance:set("hero",data["result"]["get"])
		callback(data["result"]["awards"]["drop"][1])
	end
	return true , data
end

--更改签面
function M.insheronewnew_alter(type , data , callback)
	if type == 1 then
	else
		DATA_Instance:set("hero",data["result"]["get"])
		callback()
	end
	return true , data
end

function M.inshero_refresh(type , data , callback)
	if type == 1 then
	else
		DATA_Instance:set("hero","current_award", data["result"]["current_award"])
		callback()
	end
	return true , data
end

function M.insequip_get(type , data , callback)
	if type == 1 then
	else
		for k, v in pairs(data["result"]) do
			if k ~= "instance" then
				DATA_Instance:set("equip", k, v)
			else
				DATA_Instance:set("equip", tonumber(data["result"]["current_map"]), v)
			end
		end
		callback(data["result"])
	end
	return true , data
end

function M.insequip_dig(type, data, callback)
	if type == 1 then
	else
		local result = data["result"]
		DATA_Instance:set("equip", result["map_id"],25, result["boss_data"])
		callback(result["awards"])
	end
	return true, data
end

function M.insequip_openbox(type, data, callback)
	if type == 1 then
	else
		for k, v in pairs(data["result"]["get"]) do
			if k ~= "instance" then
				DATA_Instance:set("equip", k, v)
			else
				DATA_Instance:set("equip", data["result"]["get"]["current_map"], v)
			end
		end
		callback(data["result"]["awards"])
	end
	return true, data
end


function M.insequip_useshu(type, data, callback)
	if type == 1 then
	else
		DATA_Instance:set("equip", "point", data["result"]["get"]["point"])
		callback()
	end
	return true, data
end
-- 如意阁获取当前状态
function M._30003(type, data, callback)
	if type == 1 then
	else
		DATA_Instance:set("skill",data["result"])
		callback()
	end
	return true, data
end
-- 如意阁购买次数
function M._30005(type, data, callback)
	if type == 1 then
	else
		DATA_Instance:set("skill","times",data["result"]["times"])
		callback()
	end
	return true, data
end

-- 如意阁刷新列表
function M._30006(type, data, callback)
	if type == 1 then
	else
		DATA_Instance:set("skill","list",data["result"]["list"])
		callback()
	end
	return true, data
end

-- 如意阁赌博一次
function M._30004(type, data, callback)
	if type == 1 then
	else
		DATA_Instance:set("skill","times",data["result"]["times"])
		callback(data["result"])
	end
	return true, data
end

function M.formation_get(type , data , callback)
	if type == 1 then

	else
		local result = data["result"]

		DATA_Formation:set(result)
		callback()
	end
	return true , data
end

function M.formation_doset(type , data , callback )

	if type ==1 then

	else
		--if data["code"] == 0 then
			-- DATA_General:insert(data["result"]["formation_detail"])
			--DATA_Formation:set(data["result"]["_G_formation"])
		callback()
		--end
	end
	return true ,data
end
function M._update_head(type,data,callback)
	-- 改头像
	if type ==1 then
	else
		callback()
	end
	return true,data
end
function M._hero_changegods( type,data,callback )
	if type==1 then
	else
		callback()
	end
	return true, data
end
function M._hero_dropmatrix( type , data , callback )
	-- 下阵英雄
	if type==1 then
	else
		callback()
	end
	return true,data
end
function M._20012(type , data , callback)
	if type ==1 then

	else
		--if data["code"] == 0 then
			-- DATA_Formation:set(data["result"]["_G_formation"])
			-- DATA_General:insert(data["result"]["formation_detail"])
			--DATA_Formation:set(data["result"]["_G_formation"])
			callback()
		--end
	end
	return true ,data
end
function M._20013( type,data,callback )
	-- 下阵一个武将
	if type== 1 then

	else
		callback()
	end
	return true,data
end
function M.wash_get(type , data , callback)
	if type == 1 then

	else
			local result = data["result"]
			DATA_Wash:set(result)
			callback()
	end
	return true,data
end

function M.wash_wash(type , data , callback)
	if type == 1 then

	else
		local result = data["result"]
		DATA_Wash:set(result)
		callback()
	end
	return true,data
end

function M.wash_save(type , data , callback)
	if type == 1 then

	else
			--local result = data["result"]
			--DATA_Wash:set(result)
			callback()
	end
	return true,data
end

function M.general_get(type, data, callback)
	if type == 1 then
	else
		callback()
	end
	return true, data
end
--武将传功
function M._20015(type, data, callback)
	if type == 1 then
	else
		callback(data)
	end
	return true, data
end
--武将升阶
function M.general_upgrade(type, data, callback)
	if type == 1 then
	else
		callback()
	end
	return true, data
end

--[[商店列表]]
function M._20004(type, data, callback)
	if type == 1 then
	else
		DATA_Shop:set(data["result"])
		callback(data)
	end
	return true, data
end
--[[商店购买]]
function M._20005(type, data, callback)
	if type == 1 then
	else
		callback(data)
	end
	return true, data
end
-- 获取装备消息
function M.equip_get(type, data, callback)
	if type == 1 then
	else
--		DATA_Equip:insert(data["result"]["equip"])
		callback(data)
	end
	return true, data
end
--英雄换装
function M._20010(type, data, callback)
	if type == 1 then
	else
		--DATA_ROLE_SKILL_EQUIP:set(data["result"]["_G_general_dress"])
		--应该用g_general更新g_formation
		callback(data)
	end
	return true, data
end
--英雄换技能
function M._20011(type, data, callback)
	if type == 1 then
	else
		DATA_ROLE_SKILL_EQUIP:set(data["result"]["_G_general_dress"])
		callback(data)
	end
	return true, data
end
--技能合成
function M.skill_merge(type, data, callback)
	if type == 1 then
	else
		callback(data)
	end
	return true, data
end

function M.skill_exchange(type, data, callback)
	if type == 1 then
	else
		callback(data)
	end
	return true, data
end


function M.bag_get(type, data, callback)
	if type == 1 then
	else
		DATA_Bag:set(data["result"]["type"],data["result"]["data"])
		callback()
	end

	return true, data
end

function M._20009(type, data, callback)
	if  type == 1 then
	else
		callback(data)
	end	
	return true, data
end

function M.bag_usekit(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end
-- 强化
function M._20013(type, data, callback)
	if type == 1 then
	else
		callback(data)
	end
	return true, data
end


function M.general_martial(type, data, callback)

	if type == 1 then

	else
	
		callback(data)
		
	end

	return true, data
end

function M.pulse_get(type, data, callback)

	if type == 1 then

	else
		DATA_Pulse:set(data)
		callback(data)
		
	end

	return true, data
end

function M.pulse_initial(type, data, callback)

	if type == 1 then

	else
		
		DATA_Pulse:set(data)
		callback(data)
		
	end

	return true, data
end

function M.pulse_feed(type, data, callback)

	if type == 1 then

	else
		
		DATA_Pulse:set(data)
		callback()
		
	end

	return true, data
end

function M.pulse_dig(type, data, callback)

	if type == 1 then

	else
		DATA_Pulse:set(data)
		callback()
		
	end

	return true, data
end

-- 竞技场列表
function M._21003(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true,data
end

--[[ 竞技-战斗开始]]
function M._21004( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		DATA_Battle:setMod("athletics")
		DATA_Battle:setAct("execute")
		DATA_Battle:set( data["result"] )

		--结束画面用数据
		DATA_Result:set( data["result"] )

		switchScene("battle")
	end

	return true , data
end

-- 竞技场增加次数
function M._21005(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end

-- 竞技场领取奖励
function M._21006(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end

function M.status_get(type, data, callback)
	if type == 1 then
	else
		callback({rank = data["result"]["athletics_rank"], ability = data["result"]["ability"] , gang = data["result"]["alliance"] })
	end
	return true, data
end

function M._20006(type, data, callback)
	if type == 1 then
	else
		callback(data)
	end
	return true, data
end
-- 获取排行榜
function M._20003(type, data, callback)
	if type == 1 then
	else
		callback()
	end
	return true, data
end
--[[发送消息]]
function M.message_talk(type, data, callback)
	if type == 1 then
	else
		callback()
	end
	return true, data
end

function M.message_get(type, data, callback)
	if type == 1 then
	else
		DATA_Info:set(data["result"])
		callback()
	end
	return true, data
end
--在线礼包数据获取
function M.olgift_get(type , data , callback)
	if type == 1 then
	else
		callback()
	end
	return true,data
end
--在线礼包领取
function M.olgift_receive(type , data , callback)
	if type == 1 then
	else
		callback()
	end
	return true,data
end
--新手成就礼包数据获取
function M.achievegift_get(type , data , callback)
	if type == 1 then

	else
		DATA_Olgift:set_type("achievegift" , data["result"] )
		callback()
	end
	return true,data
end

--新手成就礼包领取
function M.achievegift_receive(type , data , callback)
	if type == 1 then

	else
		DATA_Olgift:set_type("achievegift" , data["result"]["get"] )
		callback()
	end
	return true,data
end
-- 获取碎片个数
function M._30007(type, data, callback)
	if type == 1 then
	else
		if data["result"]["_G_soul"] then
			callback(data["result"]["_G_soul"])
		else
			callback(data["result"]["_G_chip"])
		end
	end
	return true, data
end
-- 英雄分角为碎片
function M._30009(type, data, callback)
	if type == 1 then
	else
		callback(data["result"]["_G_soul"])
	end
	return true, data
end
-- 英雄合成
function M._30008(type, data, callback)
	if type == 1 then
	else
		local id
		for k, v in pairs(data["result"]["_G_bag_general"]) do
			id = k 
		end
		callback(data["result"]["_G_soul"],id)
	end
	return true, data
end

-- 英雄定向合成
function M._30012(type, data, callback)
	if type == 1 then
	else
		local id
		for k, v in pairs(data["result"]["_G_bag_general"]) do
			id = k
		end
		callback(data["result"]["_G_soul"],id)
	end
	return true, data
end

-- 装备分解为碎片
function M._30011(type, data, callback)
	if type == 1 then
	else
		callback(data["result"]["_G_chip"])
	end
	return true, data
end


--装备融合
function M._30010(type, data, callback)

	if type == 1 then
	else
		local id
		for k, v in pairs(data["result"]["_G_bag_equip"]) do
			id = k 
		end
		callback(data["result"]["_G_chip"],id)
	end
	return true, data
end

function M.pulse_merge(type, data, callback)
	if type == 1 then
	else
		callback()
	end
	return true, data
end

--查看其它玩家基本信息
function M._20007(type, data, callback)
	if type == 1 then
	else
		DATA_OTHER:set_type( "base" , data["result"] )
		callback()
	end
	return true, data
end
--查看其它玩家阵法信息
function M._20008(type, data, callback)
	if type == 1 then
	else
		DATA_OTHER:set_type( "formation" , data["result"] )
		callback()
	end
	return true, data
end

function M._20016(type, data, callback)
	if type == 1 then
	else
		callback( data["result"] )
	end
	return true, data
end
function M._boss_kill( type,data,callback )
	if type == 1 then
	else
		callback(data["result"])
	end
	return true,data
end
function M._boss_ranking_list( type,data,callback )
	if type==1 then
	else
		callback(data["result"])
	end
	return true,data
end
--boss战入口
function M._get_boss( type,data,callback )
	if type==1 then
	else

		callback(data["result"])
	end
	return true,data
end
--活动入口
function M._50001(type , data , callback)
	if type == 1 then

	else
		dump(data["result"])
		DATA_Activity:set_type( data["result"] )
		dump(DATA_Activity:get("boss"))
		callback()
	end
	return true,data
end
--连续登陆领奖
function M._50002(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end

--好汉目标活动
function M.activity_receive_achieve(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
--升级活动
function M._50005(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
--对酒活动
function M._50003(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
function M._61003( type , data , callback )
	if type == 1 then
	else
		DATA_Gang:set_type("mine_details",data["result"])
		callback()
	end
	return true,data
end
function M._61004( type,data,callback )
	if type == 1 then
	else
		if data["result"].report=="" then
			--win = 1
			callback(data["result"])
		else
			DATA_Battle:setMod("mining")
			DATA_Battle:setAct("execute")
			DATA_Battle:set( data["result"] )
			--结束画面用数据
			DATA_Result:set( data["result"] )

			switchScene("battle")
		end
		
	end
	return true,data
end
function M._61005( type,data,callback )
	-- 矿山领取奖励
	if type == 1 then

	else
		callback()
	end
	return true,data
end
function M._61006( type,data,callback )
	if type == 1 then
	else
		callback(data["result"])
	end
	return true,data
end
function M._61007( type,data,callback )--夺矿记录 领取矿
	if type == 1 then
	else
		callback(data["result"])
	end
	return true,data
end
function M._42001( type , data , callback )
	if type == 1 then
	else
		DATA_Gang:set_type("shop",data["result"])
		callback()
	end
	return true,data
end
--排行
function M._42011( type,data,callback )
	if type == 1 then
	else
		if data.code ==0 then
			DATA_Gang:set_type("gang_rank",data["result"])
			callback()
		else
			KNMsg.getInstance():flashShow("排行数据错误["..data.msg.."]")
		end
		
	end
	return true,data
end
--帮会任务
function M._42021( type,data,callback )
	if type == 1 then
	else
		DATA_Gang:set("task","task",data["result"].task)
		--处理award, 字串转table
		for k, v in pairs(DATA_Gang:get("task").task.info) do
			v.award = loadstring("return "..v.award)()
		end
		callback()
	end
	return true,data
end
function M._42022( type,data,callback )
	if type == 1 then
	else
		if data.code >0 then
			KNMsg.getInstance():flashShow(data.msg)
		else
			callback()
		end
	end
	return true,data
end
function M._42023( type,data,callback )
	--  刷新任务
	if type == 1 then
	else
		if data.code == 0 then 
			DATA_Gang:set_type("task",data["result"])
			--处理award, 字串转table
			for k, v in pairs(DATA_Gang:get("task").task.info) do
				v.award = loadstring("return "..v.award)()
			end
			callback()
		end
	end
	return true,data
end
function M._42024( type,data,callback )
	--  刷新任务
	if type == 1 then
	else
		if data.code == 1 then 
			KNMsg.getInstance():flashShow("接受任务错误["..data.msg.."]")
		elseif data.code==0 then
			callback()
		end
	end
	return true,data
end
function M._42060( type,data,callback )
	-- 帮会战斗
	if type == 1 then else
		callback()
	end
	return true,data
end
function M._insskillget( type,data,callback )
	if type == 1 then else
		callback()
	end
	return true,data
end
function M._inspet_new( type,data,callback )
	if type == 1 then else
		callback()
	end
	return true,data
end
--帮会祈福
function M._42031( type,data,callback )
	if type ==1 then
	else
		DATA_Gang:set_type("pray",data["result"])
		callback()
	end
	return true,data
end
function M._42032( type,data,callback )
	if type == 1 then
		
	else
		print("42032",data.code)
		--DATA_Gang:set("pray","rands",data["result"].rands)
		DATA_Gang:set("pray","rands",data["result"].rands)
		DATA_Gang:set("pray","awards",data["result"].awards)
		callback(data)
	end
	return true,data
end
function M._42033(type,data,callback)--祈福动态
	if type==1 then
	else
		callback(data["result"])
	end
	return true,data
end
--捐献
function M._42041( type,data,callback )
	if type==1 then
	else
		callback()
	end
	return true,data
end
function M._42042( type,data,callback )
	if type==1 then
	else
		callback()
	end
	return true,data
end
function M._42040( type,data,callback )
	-- 获取捐献信息
	if type == 1 then else
		if data.code==0 then

			DATA_Gang:set("task","donateinfo",data["result"].donateinfo)
			DATA_Gang:set("task","donate",data["result"].donate)
			callback()
		else
			KNMsg.getInstance():flashShow("获取捐献信息错误."..data.msg)
		end
	end
	return true,data
end
--充值奖励活动
function M.activity_receive_payment(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
--首充礼包
function M.activity_receive_firstpay(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
--五星英雄
function M.activity_receive_singlepaymax(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
--全民福利
function M.activity_receive_welfare(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end

--超值兑换
function M.activity_receive_xchange(type , data , callback)
	if type == 1 then

	else
		dump( data["result"] )
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end

function M.iapppay_token(type , data , callback)
	if type == 1 then

	else
		callback(data["result"])
	end
	return true,data
	
end

function M._rob_get(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end
function M._62003( type,data,callback )
	-- 抢夺记录
	if type == 1 then
	else
		callback(data["result"])
	end
	return true,data
end
function M._62002( type,data,callback )
	-- 抢夺
	if type == 1 then
	else
		callback(data)
	end
	return true,data
end
function M._62001(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])	
	end
	return true, data
end
--获取帮派数据
function M._40004(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set( data["result"] )
		callback()
	end
	return true,data
end
--获取其它帮派排行数据
function M._alliance_rank(type , data , callback)
	if type == 1 then
	else
		if data.code == 0 then
			-- if data["result"]["apply"] then
			-- 	DATA_Gang:set_type( "apply" , data["result"]["apply"] )
			-- 	data["result"]["apply"] = nil
			-- end
		
			DATA_Gang:set_type("rank" , data["result"] )
			callback()
		end
	end
	return true,data
end
--创建帮派
function M._alliance_create(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type("info",data["result"].info[1])
		callback()
	end
	return true,data
end
--申请加入帮派
function M._alliance_ApplicationApply(type , data , callback)
	if type == 1 then
	else
		if data["result"].info  then
			DATA_Gang:set_type( "info" , data["result"].info )
			DATA_Gang:set_type( "list" , data["result"].list )--成员信息
		else
			DATA_Gang:set_type( "applyData" , data["result"]["applyData"] )
		end
		callback()
	end
	return true,data
end
--取消帮派申请
function M._alliance_ApplicationCancel(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "applyData" , data["result"]["applyData"] )
		callback()
	end
	return true,data
end

--查看其它帮派详细信息
function M._alliance_getallianceinfo(type , data , callback)
	if type == 1 then
	else
		callback( data["result"] )
	end
	return true,data
end
--编辑公告
function M.alliance_notice(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set( data["result"] )
		callback( data["result"] )
	end
	return true,data
end
--踢人、退出
function M.alliance_excluding(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type("info" ,  data["result"].info )
		DATA_Gang:set_type("list" ,  data["result"].list )
		callback( data["result"] )
	end
	return true,data
end
--加入帮派后帮派列表
function M.alliance_totalrank(type , data , callback)
	if type == 1 then
	else
		--rank   总榜
		--rank_top   总榜排名
		--ability   战力
		--ability_top   战力排行数
		--tribute   帮威
		--tribute_top   帮威排行
		DATA_Gang:set_type( "gang_rank" , data["result"] )
		callback( )
	end
	return true,data
end
--加入帮派后帮派列表
function M._alliance_eventmovement(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "event_movement" , data["result"].event_movement )
		if data.result.list then
			DATA_Gang:set_type("list",data["result"].list)
		end
		callback( )
	end
	return true,data
end

--帮派祈福界面数据
function M.alliance_clifford(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "pray" , data["result"] )
		callback( )
	end
	return true,data
end
--帮派祈福界面数据
function M.alliance_sendclifford(type , data , callback)
	if type == 1 then
	else
		
		local tempData = DATA_Gang:get("pray")
		tempData = tempData or {}
		tempData.rands = data["result"].rands
		DATA_Gang:set_type( "pray" , tempData )
		
		callback(data["result"] )
	end
	return true,data
end
--祈福动态信息
function M.alliance_cliffordmovement(type , data , callback)
	if type == 1 then
	else
		callback(data["result"] )
	end
	return true,data
end

--帮派收人数据
function M._alliance_fresh(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "applylist" , data["result"].applylist )
		callback( )
	end
	return true,data
end
--帮派拒绝申请者加入
function M._alliance_refuse(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "applylist" , data["result"].applylist )
		callback( )
	end
	return true,data
end
--帮派一键拒绝申请者加入
function M.alliance_refuseonekey(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "applylist" , data["result"].applylist )
		callback( )
	end
	return true,data
end
--帮派 申请同意
function M._alliance_agree(type , data , callback)
	if type == 1 then
	else
		--DATA_Gang:set_type( "info" , data["result"].info )
		DATA_Gang:set_type( "list" , data["result"].list )
		DATA_Gang:set_type( "applylist" , data["result"].applylist )
		callback( )
	end
	return true,data
end
--帮派一键同意
function M.alliance_agreeonekey(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "info" , data["result"].info )
		DATA_Gang:set_type( "list" , data["result"].list )
		DATA_Gang:set_type( "applylist" , data["result"].applylist )
		callback( )
	end
	return true,data
end
function M._alliance_getmembers( type,data,callback )
	if type == 1 then
	else
		DATA_Gang:set_type("list",data["result"].list)
		callback()
	end
	return true,data
end
function M.alliance_converTitle( t )
	local GangTitle_config = requires("Config.Gang")
	-- local textConfig = {}
	-- textConfig[100] =  "帮主"
	-- textConfig[95] = "副帮主"
	-- textConfig[81] = "青龙堂堂主"
	-- textConfig[82] = "白虎堂堂主"
	-- textConfig[83] = "朱雀堂堂主"
	-- textConfig[84] = "玄武堂堂主"
	for k,v in pairs(t) do
		v.title = GangTitle_config[v.duty]
	end
	
	return t
end

--帮派任命
function M._alliance_manage(type , data , callback)
	if type == 1 then
	else
		--DATA_Gang:set_type( "info" , data["result"].info )
		DATA_Gang:set_type( "list" , M.alliance_converTitle(data["result"].list))
		callback( )
	end
	return true,data
end
--帮派升级查询
function M._alliance_escalate(type , data , callback)
	if type == 1 then
	else
		if data["result"].info then
			DATA_Gang:set_type( "info" , data["result"].info )
			DATA_Gang:set_type( "list" , data["result"].list )
		else
			print("_alliance_escalate gang up")
			DATA_Gang:set_type( "gangup" , data["result"] )
		end
		callback( )
	end
	return true,data
end
function M._alliance_upgrade( type, data, callback )
	if type ==1 then
	else
		print("_alliance_upgrade code",data.code)
		if data.code == 0 then
			callback()
		else
			print("_alliance_upgrade false")
			KNMsg.getInstance():flashShow( data["result"].msg )
		end
	end
	return true,data
end
--帮派任务捐献
function M.alliance_task(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "task" , data["result"] )
		callback( )
	end
	return true,data
end
--确认捐献
function M.alliance_senddonate(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "info" , data["result"].info )
		DATA_Gang:set_type( "list" , data["result"].list )
		if data["result"].donate then
			local tempData = DATA_Gang:get( "task" )
			tempData.donate = data["result"].donate
			DATA_Gang:set_type( "task" ,tempData )
		end
		
		if data["result"].task then
			local tempData = DATA_Gang:get("task") or {}
			tempData.task = data["result"].task
			DATA_Gang:set_type( "task" , tempData )
		end 
		
		callback( )
	end
	return true,data
end
--重置任务
function M.alliance_resettask(type , data , callback)
	if type == 1 then
	else
		local tempData = DATA_Gang:get("task") or {}
		tempData.task = data["result"].task
		DATA_Gang:set_type( "task" , tempData )
		callback( )
	end
	return true,data
end
--执行任务
function M.alliance_posttask(type , data , callback)
	if type == 1 then
	else
		local tempData = DATA_Gang:get("task") or {}
		tempData.task = data["result"].task
		
		DATA_Gang:set_type( "task" , tempData )
		DATA_Gang:set_type( "info" , data["result"].info )
		DATA_Gang:set_type( "list" , data["result"].list )
		
		callback( )
	end
	return true,data
end
--接受任务
function M.alliance_receivetask(type , data , callback)
	if type == 1 then
	else
		local tempData = DATA_Gang:get("task") or {}
		tempData.task = data["result"].task
		DATA_Gang:set_type( "task" , tempData )
		callback( )
	end
	return true,data
end
function M._alliance_mine( type,data ,callback )
	if type == 1 then
	else
		DATA_Gang:set_type("info",data["result"].info[1])
		DATA_Gang:set_type("applyData",data["result"].applyData)
		callback()
	end
	return true, data
end
--帮会自动加人与验证加人切换
function M.alliance_switchuser(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "applylist" , data["result"].applylist )
		DATA_Gang:set_type( "list" , data["result"].list )
		callback( )
	end
	return true,data
end
--帮会商城
function M.alliance_shop(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "shop" , data["result"] )
		callback( )
	end
	return true,data
end
--开启帮会宝石加成
function M._alliance_wakeprop(type , data , callback)
	if type == 1 then
	else
		
		local tempData = DATA_Gang:get("shop")
		tempData.gemconfig = data["result"].gemconfig
		DATA_Gang:set_type( "shop" , tempData )
		
		--DATA_Gang:set_type( "info" , data["result"].info )
		--DATA_Gang:set_type( "list" , data["result"].list )
		callback( )
	end
	return true,data
end
--开启帮会宝石购买
function M.alliance_shopprop(type , data , callback)
	if type == 1 then
	else
		DATA_Gang:set_type( "info" , data["result"].info )
		DATA_Gang:set_type( "list" , data["result"].list )
		callback( )
	end
	return true,data
end

function M.message_get_wajue(type , data , callback)
	if type == 1 then
	else
		local result = data["result"]["_G_message_wajue"]
		callback(result[#result])
	end
	return true,data
end

function M._message_gettalk( type, data, callback )
	if type == 1 then
	else
		local flag_local = DATA_Info:get_type("_G_message_talk_timeflag") or 0
		if flag_local==0 then
			DATA_Info:set_type("_G_message_talk",data["result"].item)

			--有新的数据，才需要callback()
			
		else
			DATA_Info:add_bytype("_G_message_talk",data["result"].item)
			--if data["result"].curflag ~= nil then
				--local ret = DATA_Info:add_bytype("_G_message_talk",data["result"].item)
				-- if ret==true then
				-- 	--有新的数据，才需要callback()
				-- 	callback()
				-- 	DATA_Info:set_type("_G_message_talk_timeflag",data["result"].curflag)	
				-- end
			--end
		end
		-- for k,v in pairs(data["result"].item) do
		-- 	if v.curflag>flag_local then
		-- 		DATA_Info:set_type("_G_message_talk_timeflag",v.curflag)	
		-- 		flag_local = v.curflag
		-- 	end
		-- end
		callback()
		
	end
	return true,data
end
function M._alliance_get( type,data,callback )
	if type == 1 then
	else
		callback()
	end
	return true,data
end
--读取帮派聊天数据
function M._alliance_getchat(type , data , callback)   --吕玮 2015.7.8
	if type == 1 then
	else
		--DATA_Info:set_fields(data["result"].item, "gang","chat")
		--DATA_Info:set_gang(data["result"])
		--DATA_Info:get("gang").chatflag = data["result"].chatflag
		--print("_alliance_getchat",table.nums(DATA_Info:get_type("gang").chat))
		-- if  not DATA_Info:get_type("gang") then
		-- 	DATA_Info:set("gang",{})
		-- end
		-- DATA_Info:set("gang","chat",data["result"])
		local flag_local = DATA_Info:get_type("message_gang_timeflag") or 0
		if flag_local==0 then
			DATA_Info:set_type("message_gang",data["result"].item)
		else
			DATA_Info:add_bytype("message_gang",data["result"].item)
		end
		-- DATA_Info:set_type("gang",data["result"].chat)
		-- if data["result"].chatflag ~= nil then
		-- 	DATA_Info:set_type("gang_flag",data["result"].chatflag)
		-- end
		callback()
	end
	return true,data
end
--发送帮派聊天
function M._alliance_sendchat(type , data , callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true,data
end
--发送邮件公告
function M._40003(type , data , callback)
	if type == 1 then
	else
		print("M._message_gettalk is msgflag",msgflag)
		callback()
	end
	return true,data
end
--读取好友聊天数据
function M._message_get_siliao(type , data , callback)
	if type == 1 then
	else
		local flag_local = DATA_Info:get_type("message_get_siliao_timeflag") or 0
		if flag_local==0 then
			DATA_Info:set_type("message_get_siliao",data["result"])
		else
			DATA_Info:add_bytype("message_get_siliao",data["result"])
		end
		callback()
	end
	return true,data
end
--发送好友聊天数据
function M._message_send_siliao(type , data , callback)
	if type == 1 then
	else
		callback()
	end
	return true,data
end
--获取世界聊天数据
--function M.message_gettalk(type, data, callback)
	--if type == 1 then
	--else
		--DATA_Info:set_type("_G_message_talk" , data["result"]._G_message_talk )
		--callback()
	--end
	--return true, data
--end
--获取vip配置信息
function M.vip_get(type , data , callback)
	if type == 1 then
	else
		DATA_Vip:set_type( "vip" , data["result"] )
		callback()
	end
	return true,data
end
function M.vip_receive(type , data , callback)
	if type == 1 then
	else
		DATA_Vip:set_type( "vip" , data["result"].get )
		callback()
		switchScene("vip" , {} , function()
			M.inform( data["result"] )
		end)
	end
	return true,data
end

function M.evolvepool_merge(type, data, callback)
	if type == 1 then
	else
		--传物品的id与cid
		callback(data["result"]["id"], data["result"]["awards"]["drop"][1])
	end
	
	return true, data
end

--使用vip礼包
function M.bag_usevipkit(type, data, callback)
	if type == 1 then
	else
		--传物品的id与cid
		callback(data["result"],M.inform)
	end
	
	return true, data
end

--使用vip礼包
function M.bag_usev3card(type, data, callback)
	if type == 1 then
	else
		dump(data)
		callback()
	end
	
	return true, data
end

--使用祝福礼包
function M.bag_usezhufukit(type, data, callback)
	if type == 1 then
	else
		--传物品的id与cid
		callback(data["result"],M.inform)
	end
	
	return true, data
end

-- 英雄殿获取英雄
function M._30002(type, data, callback)
	if type == 1 then
	else
		
		callback(data["result"])
	end
	
	return true, data
end

-- 英雄殿获取当前状态
function M._30001(type, data, callback)
	if type == 1 then
	else
		--传物品的id与cid
		callback(data["result"]["_G_penglai"])
	end
	
	return true, data
end
--大于20级玩家返还活动
function M.feedback_lvgt20(type, data, callback)
	if type == 1 then
	else
		M.inform( data["result"] )
		callback()
	end
	return true, data
end
--排名100玩家返还活动
function M.feedback_rank100(type, data, callback)
	if type == 1 then
	else
		M.inform( data["result"] )
		callback()
	end
	return true, data
end
--大于20级玩家返还活动
function M.ucactivation_lvgt20(type, data, callback)
	if type == 1 then
	else
		M.inform( data["result"] )
		callback()
	end
	return true, data
end
--排名100玩家返还活动
function M.ucactivation_rank100(type, data, callback)
	if type == 1 then
	else
		M.inform( data["result"] )
		callback()
	end
	return true, data
end
--获取充值返利数据
function M.feedback_get_pay(type, data, callback)
	if type == 1 then
	else
		callback( data["result"] )
	end
	return true, data
end

--获取武将附加属性加成信息获取
function M.general_get_attr(type, data, callback)
	if type == 1 then
	else
		callback( data["result"] )
	end
	return true, data
end

--领取充值返利数据
function M.feedback_pay(type, data, callback)
	if type == 1 then
	else
		M.inform( data["result"] )
		callback()
	end
	return true, data
end
--获取好友数据
function M._40004( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		DATA_Friend:set( data["result"] )
		callback( )
	end
	return true , data
end
--添加好友数据
function M._40005( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		KNMsg.getInstance():flashShow( "好友添加成功！" )
		DATA_Friend:set( data["result"] )
		callback( )
	end
	return true , data
end

--删除好友
function M._40006( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		KNMsg.getInstance():flashShow( "删除成功！" )
		DATA_Friend:set( data["result"] )  --将get去掉--吕玮
		callback( )
	end
	return true , data
end
--祝福好友
function M._40007( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		DATA_Friend:set( data["result"] )  --将get去掉--吕玮
		if data["result"].success then
			KNMsg.getInstance():flashShow( "祝福成功！" )
		end
		callback( )
	end
	return true , data
end
--充值列表
function M._40008( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		callback( data.result )
	end
	return true , data
end
--充值列表
function M._40009( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		callback( data )
	end
	return true , data
end

--查找好友
function M._friends_search( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		DATA_OTHER:set_type( "base" , data["result"] )
		callback()
	end
	return true , data
end
--删除仇人
function M.friends_delenermy( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		-- 回调处理
		DATA_Friend:set( data["result"].get )
		callback()
	end
	return true , data
end
--[[ 好友-战斗开始]]
function M._21007( type , data , callback )
	if type == 1 then
		-- 发送前数据处理
	elseif type == 2 then
		DATA_Battle:setMod("friends")
		DATA_Battle:setAct("execute")
		DATA_Battle:set( data["result"] )

		--结束画面用数据
		DATA_Result:set( data["result"] )

		switchScene("battle")
	end

	return true , data
end

function M.mining_get(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end


function M.mining_get_list(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end



function M.mining_receive(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end
--同意保护矿山
function M.mining_guard_accept(type, data, callback)
	if type == 1 then
	else
		callback( data["result"] )
	end
	return true, data
end

function M.mining_guard_request(type, data, callback)
	if type == 1 then
	else
		callback()
	end
	return true, data
end
function M._war_door( type,data,callback )
	if type == 1 then
	else
		callback(data["result"])
	end
	return true,data
end
function M._gangbattle_enter(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end
function M._gangbattle_move(type, data, callback)
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end
function M._war_get( type,data,callback )
	if type == 1 then
	else
		callback(data["result"])
	end
	return true, data
end
function M._war_action( type,data,callback )
	if type == 1 then
	else
		callback()
	end
	return true,data
end
function M._war_info( type,data,callback )
	if type == 1 then

	else
		callback()
	end
	return true,data
end
--累积冲值 累充大礼
function M.activity_receive_paycount(type , data , callback)
	if type == 1 then
	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
--单充大礼
function M.activity_receive_singlepay(type , data , callback)
	if type == 1 then
	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
--领取连续登陆礼包
function M._50004(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
--领取升级奖励
function M._50005(type , data , callback)
	if type == 1 then

	else
		M.inform( data["result"] )
		DATA_Activity:set_type( data["result"] )
		callback()
	end
	return true,data
end
--帮战排行榜数据获取
function M.bangzhan_get_rank(type , data , callback)
	if type == 1 then

	else
		dump(data)
		DATA_Gang:set_type( "warsRank" , data["result"] )
		callback()
	end
	return true,data
end



--奖励领取提示
function M.inform( data )
	dump( data )
	if data.awards then
		local str = ""
		local decollator = "    "	--分割符
		
		local function countDrop( dropData )
			local tempStr = ""
			for key , v in pairs( dropData ) do	
				local curName = getConfig( getCidType( key ) , key , "name" )
				tempStr = tempStr .. curName .. "  +" .. v .. decollator
			end
			return tempStr
		end
		local isBox = false
		for key , v in pairs( data.awards ) do
			if key == "power"		then str = str .. "体力  +" .. v
			elseif key == "gold"		then str = str .. "黄金  +" .. v
			elseif key == "silver"		then str = str .. "银币  +" .. v
			elseif key == "chip1"		then str = str .. "一星装备碎片  +" .. v
			elseif key == "chip2"		then str = str .. "二星装备碎片  +" .. v
			elseif key == "chip3"		then str = str .. "三星装备碎片  +" .. v
			elseif key == "chip4"		then str = str .. "四星装备碎片  +" .. v
			elseif key == "soul1"		then str = str .. "一星英雄将魂  +" .. v
			elseif key == "soul2"		then str = str .. "二星英雄将魂  +" .. v
			elseif key == "soul3"		then str = str .. "三星英雄将魂  +" .. v
			elseif key == "soul4"		then str = str .. "四星英雄将魂  +" .. v
			elseif key == "animal1"		then str = str .. "一星兽魂  +" .. v
			elseif key == "animal2"		then str = str .. "二星兽魂  +" .. v
			elseif key == "animal3"		then str = str .. "三星兽魂  +" .. v
			elseif key == "animal4"		then str = str .. "四星兽魂  +" .. v
			elseif key == "_tip_"		then str = str .. v isBox = true 
			elseif key == "drop"	then str = str .. countDrop( v )
			end
			str = str ..  decollator
		end
		if isBox then
			KNMsg.getInstance():boxShow( str ,{ 
												confirmText = COMMONPATH .. "confirm.png" , 
												confirmFun = function()  end , 
												} )
		else
			KNMsg.getInstance():flashShow( str )
		end
	end
end


return M


