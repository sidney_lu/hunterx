--[[

通用数据存储

]]

--[[包含所有 DATA]]
local _datas = {
	"Session",
	"User",
	"Mail",
	"General",
	"Formation",
	"Battle",
	"Power",
	"Energy",
	"Mission",
	"Instance",
	"Wash",
	"Bag",
	"Equip",
	-- "UpdataData",
	"Hatch",
	"Shop",
	"RoleSkillEquip",
	"Result",
	"Martial",
	"Pulse",
	"Uplevel",
	"StoneList",
	"ExpConfig",
	"Uplevel",
	"IdType",
	"Rank",
	"Info",
	"Olgift",
	"Guide",
	"OtherPlay",
	"Incubation",
	"pulseTisp",
	"Activity",
	"Notice",
	"Gang",
	"Vip",
	"Friend",
	"Pet"
}

for i = 1 , #_datas do
	requires("Data." .. _datas[i])
end


local M = {}

--[[初始化]]
function M.init()
	for i = 1 , #_datas do
		requires("Data." .. _datas[i]):init()
	end
end

--[[处理公用数据]]
function M.saveCommonData( data )
	local result = data["result"]
	
	if type(result) ~= "table" then return false end
	-- 存储数据
	if isset(result , "_G_userinfo")           then DATA_User:set( result["_G_userinfo"] )                                end --用户信息
	if isset(result , "_G_power")              then DATA_Power:set( result["_G_power"] )                                  end --精力
	if isset(result , "_G_energy")             then DATA_Energy:set( result["_G_energy"] )                                end --斗志
	if isset(result , "_G_formation")          then DATA_Formation:set(result["_G_formation"])                            end --阵型
	if isset(result , "_G_bag_equip")          then DATA_Bag:set("equip",result["_G_bag_equip"])                          end
	if isset(result , "_G_bag_skill")          then DATA_Bag:set("skill",result["_G_bag_skill"])                          end
	if isset(result , "_G_bag_prop")           then DATA_Bag:set("prop",result["_G_bag_prop"])                            end
	if isset(result , "_G_bag_general")        then DATA_Bag:set("general",result["_G_bag_general"])                      end
	if isset(result , "_G_fengshen")		   then DATA_Bag:set("pet",result["_G_fengshen"]) end
	if isset(result , "_G_soul")			   then DATA_Bag:set("soul",result["_G_soul"]) end
	if isset(result , "_G_chip")			   then DATA_Bag:set("chip",result["_G_chip"]) end
	if isset(result , "_G_martial")            then DATA_Martial:set(result["_G_martial"])                                end
	if isset(result , "_G_id_type")            then DATA_IDTYPE:set(result["_G_id_type"])                     			  end
	if isset(result , "_G_ranking_level")      then DATA_Rank:setKey("level",result["_G_ranking_level"]) end
	if isset(result , "_G_ranking_athletics")  then DATA_Rank:setKey("athletics",result["_G_ranking_athletics"]) end
	if isset(result , "_G_ranking_ability")    then DATA_Rank:setKey("ability",result["_G_ranking_ability"]) end
	
	if isset(result , "_G_mail_talk")          then DATA_Mail:set_type("talk",result["_G_mail_talk"]) end
	if isset(result , "_G_mail_system")        then DATA_Mail:set_type("system",result["_G_mail_system"]) end
	if isset(result , "_G_mail_battle")        then DATA_Mail:set_type("battle",result["_G_mail_battle"]) end
	if isset(result , "_G_mail_count")         then DATA_Mail:setNum(nil,result["_G_mail_count"]) end
	if isset(result , "_G_olgift")             then DATA_Olgift:set_type("olgift" , result["_G_olgift"] ) end
	if isset(result , "_G_fame")               then DATA_User:set_fame( result["_G_fame"] ) end
	if isset(result , "_G_wash_max_percent")   then DATA_User:set_percent( result["_G_wash_max_percent"] ) end
	if isset(result , "_G_mission_cur")        then DATA_Guide:setStep( result["_G_mission_cur"] ) end          -- 当前关卡数也就是当前的引导
	if isset(result , "_G_hatch_fee")          then DATA_Incubation:set( result["_G_hatch_fee"] ) end
	if isset(result , "_G_pulse_conf")         then DATA_PulseTisp:set( result["_G_pulse_conf"] ) end
	if isset(result , "_T_updatetip")      	   then DATA_Notice:set(result["_T_updatetip"]) end
	if isset(result , "_G_loginaward_tip")     then DATA_Notice:setGetNum( result["_G_loginaward_tip"] ) end	--设置当前可领取奖励个数
	if isset(result , "_G_viplv")			   then DATA_Vip:set_type( "viplv" , result["_G_viplv"] ) end		--只有一个vip等级
	if isset(result , "_G_vipinfo")			   then DATA_Vip:set_type( "vipinfo" , result["_G_vipinfo"] ) end		--只有一个vip等级
	if isset(result , "_G_guild")  			   then 
		for k,v in pairs(result["_G_guild"]) do
			DATA_Gang:update(k,v)
			print("________G___________guild",k,v)
			print(DATA_Gang:get("info").tribute)
		end
	end
	return true
end


return M
