--[[

http 通信接口

]]

-- 该接口是全局变量
HTTP = {}

local httpActions = requires("Network.httpActions")
local commonActions = requires("Network.commonActions")
local json = requires("Network.dkjson")
local KNLoading = requires("Common.KNLoading")
local KNMask = requires("Common.KNMask")
local network = require("framework.network")

require("Data.Session")

--require("Data/Session")


--[[

发送数据, 并获得返回的数据

param 参数列表
	success_callback  function  成功回调
	error_callback  function  失败回调
	sync  boolean  是否异步，默认为true

]]
local M = {
	loading
}

function HTTP:call(mid , data , param)
	local this = {}
	setmetatable(this , self)
	self.__index = self


	local func = "_"..mid
	local success = false
	this.loading = nil


	-- 数据容错
	if type(param) ~= "table" then param = {} end
	if type(param.success_callback) ~= "function" then param.success_callback = function() end end
	if type(param.error_callback)   ~= "function" then
		param.error_callback = function(err)
			if err.code>1 and err.code < 100 then
				switchScene("login" , nil , function()
  					KNMsg.getInstance():flashShow("[" .. err.code .. "]" .. err.msg)	-- 弹出错误文字提示
 				end)
 			elseif err.code==1 then
 				KNMsg.getInstance():flashShow(err.msg)	-- 弹出错误文字提示，不带序号 
			else
				KNMsg.getInstance():flashShow("[" .. err.code .. "]" .. err.msg)	-- 弹出错误文字提示
			end
		end
	end
	if type(data) ~= "table" then 
		
		KNMsg.getInstance():flashShow("数据请求第二个参数必须为table["..tostring(mid).. "]"..tostring(data))	-- 弹出错误文字提示
		data = {} 
		return
	end


	-- 判断 httpActions 里有没有该回调
	if type(httpActions[func]) ~= "function" then
		-- 错误处理
		print("no httpActions function [" .. func .. "]")
		return false
	end

	-- 发送数据前，执行 httpActions 回调
	success , data = httpActions[func](1 , data , param.error_callback)

	--[[错误处理]]
	if not success then
		param.error_callback( {code = -996 , msg = "网络请求出错"} )
		return false
	end

	-- 拼装数据
	local request_data = {
		mid = mid,
		_v = VERSION,
		check = CHECK,
	}

	--[[非登录模块，添加登陆态参数]]
	if type(mid)=="string" or mid > 10002 then
		request_data["sid"] = DATA_Session:get("sid")
		request_data["uid"] = DATA_Session:get("uid")
	else
		request_data["sid"] = ""
		request_data["uid"] = 0
	end

	for k , v in pairs(data) do
		request_data[k] = v
	end
	local signstr = mid .. (request_data["uid"] or 0) .. (request_data["sid"] or "")
	request_data["sign"] = MD5( signstr )
	-- print(signstr.." : "..request_data["sign"])

	--[[显示遮罩层]]
	if param.no_mask ~= true then
		local scene = display.getRunningScene()

		if param.no_loading ~= nil then
			this.loading = KNMask:new({opacity = 0 , priority = -140})
		else
			this.loading = KNLoading:new()
		end
		scene:addChild( this.loading:getLayer() )
	end



	--[[接收数据]]
	local function _callback(httpCode , response)
		--[[
		echoLog("HTTP" , "#####################")
		echoLog("HTTP" , "#####################")
		echoLog("HTTP" , "### " .. string.len(response))
		echoLog("HTTP" , "### " .. response)
		echoLog("HTTP" , "#####################")
		echoLog("HTTP" , "#####################")
		]]

		if not response or response == "" then
			if this.loading ~= nil then this.loading:remove() end 		-- 去掉 loading

			--[[错误处理]]
			param.error_callback( {code = -999 , msg = "网络请求出错."} )
			return false
		end

        --[[解包数据]]
		if device.platform == "mac" then
			io.writefile(device.writablePath .. "/http.txt" , response , "w+")
		end
		
		
		if string.sub(response , 0 , 1) ~= "{" then
			if this.loading ~= nil then this.loading:remove() end 		-- 去掉 loading
 
			param.error_callback( {code = httpCode , msg = response} )
			return false
		end

		print("#########RESPONSE:", response)
		print("[http]writable path="..cc.FileUtils:getInstance():getWritablePath())
		local path=cc.FileUtils:getInstance():getWritablePath()
		path = path .. "log.txt"
		file = io.open(path, "w");
    	file:write(response.."\n");          -- \n 一行一行写进去
    	file:close()
		response = json.decode( response )

		if response == nil then
			if this.loading ~= nil then this.loading:remove() end 		-- 去掉 loading

			--[[错误处理]]
			param.error_callback( {code = -998 , msg = "网络请求出错."} )
			return false
		end
  
		--[[处理 code 不为 0 的情况]]
		if response.code ~= 0 then
			if this.loading ~= nil then this.loading:remove() end 		-- 去掉 loading
			
			--[[错误处理]]
			param.error_callback( {code = response.code , msg = response.msg} )
			return false
		end


		if this.loading ~= nil then this.loading:remove() end 		-- 去掉 loading

		--[[接到数据后，执行 httpActions 回调]]
		commonActions.saveCommonData( response )
		success , response = httpActions[func](2 , response , param.success_callback)

		--[[错误处理]]
		if not success then
			param.error_callback( {code = -996 , msg = "网络请求出错."} )
			return false
		end
	end

	-- 生成 URL-encode 之后的请求字符串
	local function http_build_query(data)
		if type(data) ~= "table" then return "" end

		local str = ""
		for k , v in pairs(data) do
			str = str .. k .. "=" .. string.urlencode(v) .. "&"
		end

		return str
	end

	-- 一次http请求
	local function sendRequest(url , postdata , callback , params)
		-- print("@url params:", postdata)
		params = params or {}
		local timeout = params.timeout or 15

		-- echoLog("[HTTP]"..url .. "?" .. postdata)

		local request = network.createHTTPRequest(function(event)
			if event.name == "progress" then
				return
			end

			if event.name == "timeout" then
				CCLuaLog("===== error: timeout " .. timeout .. "s =====" )
				callback( -28 , "网络请求超时" )
				return
			end

			local request = event.request

			-- local error_code = request:getErrorCode()
			local error_code = request:getResponseStatusCode()
			if error_code ~= 200 then
				local error_msg = request:getErrorMessage()
				echoLog("HTTP" , "===== error: " .. error_code .. " , msg: " .. error_msg .. " =====" )

				if string.find(error_msg , "resolve host name") ~= nil or string.find(error_msg , "connect to server")
								 or string.find(error_msg , "Failed sending data") then
					error_msg = "网络异常，无法连接服务器"
				elseif string.find(error_msg , "Timeout") ~= nil then
					error_msg = "网络连接超时"
				end
				callback( error_code , error_msg )
				return
			end

			callback( request:getResponseStatusCode() , request:getResponseData() )
		end , url , "POST")

		-- request:setAcceptEncoding(kCCHTTPRequestAcceptEncodingDeflate)
		print("postdata:", postdata)
		request:setPOSTData(postdata)
		request:setTimeout(timeout)
		request:start()
	end



	-- 发送数据
	local url
	if param.requestUrl and param.requestUrl ~= "" then
		url = param.requestUrl
	elseif type(mid)~="string" and mid <20000 and CONFIG_LOGIN_HOST ~= nil then
		url = CONFIG_LOGIN_HOST .. "/root"
	else
		url = CONFIG_HOST .. "/root"
	end
	print("url:", url)
	sendRequest( url , http_build_query(request_data) , _callback , param)

	return this
end

function HTTP:showLoading()
	if self.loading then
		self.loading:remove()
		self.loading = nil
	end
	
	self.loading = KNLoading:new()

	local scene = display.getRunningScene()
	scene:addChild( self.loading:getLayer() )
end


return HTTP
