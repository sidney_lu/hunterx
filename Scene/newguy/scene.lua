--[[

新手注册

]]


collectgarbage("setpause"  , 100)
collectgarbage("setstepmul"  , 5000)


-- [[ 包含各种 Layer ]]
local newLayer = requires( "Scene.newguy.layer")


local M = {}

function M:create(args)
	local scene = display.newScene("newguy")
	args = args or {}
	local step = args.step  or 1

	---------------插入layer---------------------
	scene:addChild( newLayer:new( args.open_id , step ) )
	---------------------------------------------

	return scene
end

return M
