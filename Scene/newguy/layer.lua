local PATH = IMG_PATH.."image/scene/newguy/"
local SCENECOMMON = IMG_PATH.."image/scene/common/"
local KNBtn = requires("Common.KNBtn")--requires "Common/KNBtn"
local Config_General = requires("Config.Hero")
local KNRadioGroup = requires("Common.KNRadioGroup") 
-- local KNShowbylist = requires( "Common/KNShowbylist")
local KNInputText = requires("Common.KNInputText")
--[[
	首页
]]
local generals = {
	-- "1345",
	-- "1317",
	-- "1347",
	"1403",
	"1410",
	"1418"
}

local NewLayer= {
	layer,
	viewLayer = nil,
	selectLayer = nil,
	selectIndex = 1,
	open_id = nil,
	scroll,
	group
}
function NewLayer:new(open_id , step)
	local this = {}
	setmetatable(this , self)
	self.__index = self

	this.open_id = open_id
	
	this.sex = 1
	this.nickStr = ""
	this.general_select = 1
	
	this.layer = display.newLayer()

	this:step(step)

	return this.layer
end
function NewLayer:step(step)
	-- step = 5
	
	if self.viewLayer ~= nil then
		-- self.layer:removeChild(self.viewLayer , true)
		--self.viewLayer:removeNodeEventListenersByEvent(cc.)
		self.viewLayer:removeFromParent(false)--如果这里true，会爆事件异常
	end

	self.viewLayer = display.newLayer()
	local next_step = false

	-- -- 开始创建人物
	self:selectGeneral()		

	-- if step == 1 then
	-- 	self.viewLayer:addChild( display.newSprite(PATH .. "bg_2.jpg" , display.cx , display.cy ) )
		
	-- 	local text2 = display.newSprite(PATH .. "text01_02.png")
	-- 	setAnchPos( text2 , 230 , 222 , 0.5 , 0.5 )
	-- 	self.viewLayer:addChild( text2 )
	-- 	text2:setScale(0.01)
	-- 	transition.scaleTo( text2 , {delay = 0.5,  time = 0.3 , scale = 1 ,easing = "BACKOUT" })
		
		
	-- 	local text1 = display.newSprite(PATH .. "text01_01.png")
	-- 	setAnchPos( text1 , display.cx , -50 , 0.5 , 1  )
	-- 	self.viewLayer:addChild( text1 )
	-- 	transition.fadeIn( text1 , { time = 0.3 , delay = 0.5 } )
	-- 	transition.moveTo( text1 , { time = 0.8 , y = 90 , onComplete = 
	-- 		function() 
	-- 			echoLog("GUIDE","touch 1")
	-- 			self.viewLayer:addChild( display.strokeLabel("点击继续.." , display.cx - 55 , display.height - 50 , 24 , ccc3( 0xff , 0xff , 0xff ) ) )
	-- 			next_step = true
				
	-- 			self.viewLayer:setTouchSwallowEnabled(false)
	-- 			self.viewLayer:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
	-- 				if event.name == "began" then
	-- 					if next_step then
	-- 						next_step = false
	-- 						self:step(2)
	-- 					end
	-- 					return false				
	-- 				end
	-- 			end)				

	-- 			self.viewLayer:setTouchEnabled(true)
	-- 		end})
			
	-- 	self.layer:addChild( self.viewLayer )
	-- elseif step == 2 then

	-- 	self.viewLayer:addChild( display.newSprite(PATH .. "bg_3.jpg" , display.cx , display.cy ) )
	-- 	local text2 = display.newSprite(PATH .. "text02_02.png")
	-- 	setAnchPos( text2 , 170 , 560 , 0.5 , 0.5 )
	-- 	self.viewLayer:addChild( text2 )
		
	-- 	text2:setScale(0.01)
	-- 	transition.scaleTo( text2 , {delay = 0.3,  time = 0.3 , scale = 1 ,easing = "BACKOUT" ,onComplete = 
	-- 			function()
	-- 					local text3 = display.newSprite(PATH .. "text02_03.png")
	-- 					setAnchPos( text3 , 260 , 680 , 0.5 , 0.5 )
	-- 					self.viewLayer:addChild( text3 )
	-- 					text3:setScale(0.01)
	-- 					transition.scaleTo( text3 , { delay = 0.3,  time = 0.3 , scale = 1 ,easing = "BACKOUT" })
	-- 			end
	-- 			 })

	-- 	local text1 = display.newSprite(PATH .. "text02_01.png")
	-- 	setAnchPos( text1 , display.cx , -50 , 0.5 , 1  )
	-- 	self.viewLayer:addChild( text1 )
	-- 	transition.fadeIn( text1 , { time = 0.3 , delay = 0.5 } )
	-- 	transition.moveTo( text1 , { time = 0.8 , y = 132 , onComplete = 
	-- 		function() 
	-- 			echoLog("GUIDE","touch 2")
	-- 			self.viewLayer:addChild( display.strokeLabel("点击继续.." , display.cx - 55 , display.height - 50 , 24 , ccc3( 0xff , 0xff , 0xff ) ) )
	-- 			next_step = true

	-- 			self.viewLayer:setTouchSwallowEnabled(false)
	-- 			self.viewLayer:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
	-- 				if event.name == "began" then
	-- 					if next_step then
	-- 						echoLog("GUIDE" , "next 3")
	-- 						next_step = false
	-- 						self:step(3)
	-- 					end
	-- 					return false				
	-- 				end
	-- 			end)	

	-- 			self.viewLayer:setTouchEnabled(true)
	-- 		end})
	-- 	self.layer:addChild( self.viewLayer )
		
	-- elseif step == 3 then
	-- 	self.viewLayer:addChild( display.newSprite(PATH .. "bg_4.jpg" , display.cx , display.cy ) )
		
	-- 	local text2 = display.newSprite(PATH .. "text03_03.png")
	-- 	setAnchPos( text2 , 170 , 500 , 0.5 , 0.5 )
	-- 	self.viewLayer:addChild( text2 )
	-- 	text2:setScale(0.01)
	-- 	transition.scaleTo( text2 , {delay = 0.3,  time = 0.3 , scale = 1 ,easing = "BACKOUT" ,onComplete = 
	-- 			function()
	-- 					local text3 = display.newSprite(PATH .. "text03_02.png")
	-- 					setAnchPos( text3 , 160 , 680 , 0.5 , 0.5 )
	-- 					self.viewLayer:addChild( text3 )
	-- 					text3:setScale(0.01)
	-- 					transition.scaleTo( text3 , { delay = 0.3,  time = 0.3 , scale = 1 ,easing = "BACKOUT" })
	-- 			end
	-- 			 })
		
	-- 	local text1 = display.newSprite(PATH .. "text03_01.png")
	-- 	setAnchPos( text1 , display.cx , -50 , 0.5 , 1  )
	-- 	self.viewLayer:addChild( text1 )
	-- 	transition.fadeIn( text1 , { time = 0.3 , delay = 0.5 } )
	-- 	transition.moveTo( text1 , { time = 0.8 , y = 132 , onComplete = 
	-- 		function() 
	-- 			self.viewLayer:addChild( display.strokeLabel("点击继续.." , display.cx - 55 , display.height - 50 , 24 , ccc3( 0xff , 0xff , 0xff ) ) )
	-- 			next_step = true
	-- 			self.viewLayer:setTouchSwallowEnabled(false)
	-- 			self.viewLayer:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
	-- 				if event.name == "began" then
	-- 					if next_step then
	-- 						echoLog("GUIDE" , "next 4")
	-- 						next_step = false
	-- 						self:step(4)
	-- 					end
	-- 					return false				
	-- 				end
	-- 			end)	

	-- 			self.viewLayer:setTouchEnabled(true)
	-- 		end})
	-- 	self.layer:addChild( self.viewLayer )
	-- elseif step == 4 then
	-- 	self.viewLayer:addChild( display.newSprite(PATH .. "bg_5.jpg" , display.cx , display.cy ) )
		
	-- 	local text2 = display.newSprite(PATH .. "text04_02.png")
	-- 	setAnchPos( text2 , 150 , 650 , 0.5 , 0.5  )
	-- 	self.viewLayer:addChild( text2 )
	-- 	text2:setScale(0.01)
	-- 	transition.scaleTo( text2 , {delay = 0.3,  time = 0.3 , scale = 1 ,easing = "BACKOUT" ,onComplete = 
	-- 			function()
	-- 					local text3 = display.newSprite(PATH .. "text04_03.png")
	-- 					setAnchPos( text3 , 330 , 620 , 0.5 , 0.5  )
	-- 					self.viewLayer:addChild( text3 )
	-- 					text3:setScale(0.01)
	-- 					transition.scaleTo( text3 , { delay = 0.3,  time = 0.3 , scale = 1 ,easing = "BACKOUT" })
	-- 			end
	-- 			 })
		
	-- 	local text1 = display.newSprite(PATH .. "text04_01.png")
	-- 	setAnchPos( text1 , display.cx , -50 , 0.5 , 1  )
	-- 	self.viewLayer:addChild( text1 )
	-- 	transition.fadeIn( text1 , { time = 0.3 , delay = 0.5 } )
	-- 	transition.moveTo( text1 , { time = 0.8 , y = 132 , onComplete = 
	-- 		function() 
	-- 			self.viewLayer:addChild( display.strokeLabel("点击继续.." , display.cx - 55 , display.height - 50 , 24 , ccc3( 0xff , 0xff , 0xff ) ) )
	-- 			next_step = true
				

	-- 			self.viewLayer:setTouchSwallowEnabled(false)
	-- 			self.viewLayer:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
	-- 				if event.name == "began" then
	-- 					if next_step then
	-- 						echoLog("GUIDE" , "next 5")
	-- 						next_step = false
	-- 						self:step(5)
	-- 					end
	-- 					return false				
	-- 				end
	-- 			end)	

	-- 			self.viewLayer:setTouchEnabled(true)
	-- 		end})
	-- 	self.layer:addChild( self.viewLayer )
	-- elseif step == 5 then
	-- 	local text1 = display.newSprite(PATH .. "text05_01.png")
	-- 	setAnchPos( text1 , display.cx , -50 , 0.5 , 0.5  )
	-- 	self.viewLayer:addChild( text1 )
	-- 	transition.fadeIn( text1 , { time = 0.3 , delay = 0.5 } )
	-- 	transition.moveTo( text1 , { time = 3 , y = display.cy , onComplete = 
	-- 		function() 
	-- 			self.viewLayer:addChild( display.strokeLabel("点击继续.." , display.cx - 55 , display.height - 50 , 24 , ccc3( 0xff , 0xff , 0xff ) ) )
	-- 			next_step = true
	-- 			self.viewLayer:setTouchSwallowEnabled(false)
	-- 			self.viewLayer:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
	-- 				if event.name == "began" then
	-- 					if next_step then
	-- 						echoLog("GUIDE" , "next 6")
	-- 						next_step = false
	-- 						self:selectGeneral()
	-- 					end
	-- 					return false				
	-- 				end
	-- 			end)	

	-- 			self.viewLayer:setTouchEnabled(true)
	-- 		end})
	-- 	self.layer:addChild( self.viewLayer )
	-- else
	-- 	-- 开始创建人物
	-- 	self:selectGeneral()		
	-- end
	--[[
	批量备注
	--]]
end


function NewLayer:regName()
	if self.viewLayer ~= nil then
		self.layer:removeChild(self.viewLayer , true)
	end
	self.viewLayer = display.newLayer()

	local bg = display.newSprite(IMG_PATH .. "image/scene/login/bg.png")
	setAnchPos(bg , display.cx , display.cy , 0.5 , 0.5 )
	self.viewLayer:addChild(bg)
	local logo =display.newSprite(IMG_PATH .. "image/scene/login/logo.png")
	setAnchPos(logo,10,display.height - 87 - 10)
	self.viewLayer:addChild(logo)
	--昵称
	local nicknameBg = display.newSprite(PATH .. "nickname_bg.png")
	setAnchPos(nicknameBg , display.cx , display.cy -240, 0.5 , 0 )
	self.viewLayer:addChild(nicknameBg)
	
	--角色背景
	local roleBg = display.newSprite(PATH .. "role_bg.png")
	setAnchPos(roleBg , display.cx , display.cy-100 , 0.5 , 0 )
	self.viewLayer:addChild(roleBg)
	
	--性别选择
	local KNRadioGroup = requires("Common.KNRadioGroup")
	local group = KNRadioGroup:new()
	for i = 1 , 2 do
		local sexBtn = KNBtn:new( COMMONPATH , {"sex1.png" , "select1.png"} , 150 + (i-1)*105 , display.cy-63 , {
 		front = COMMONPATH .. "sex" .. i .. ".png" , 
		callback = function()
			self.sex = i
		end
		} , group):getLayer()
		self.viewLayer:addChild(sexBtn)
	end
	
	local nicknameText
	
	-- 用户名输入框
	nicknameText = ui.newEditBox({
        image = "image/common/editbox.png",
        size = CCSize(170, 35),
        x = 230,
        y = display.cy-165,
        listener = function(event, editbox)
            if event == "began" then
                printf("editBox1 event began : text = %s", editbox:getText())
            elseif event == "ended" then
                printf("editBox1 event ended : %s", editbox:getText())
            elseif event == "return" then
                printf("editBox1 event return : %s", editbox:getText())
            elseif event == "changed" then
                printf("editBox1 event changed : %s", editbox:getText())
            else
                printf("EditBox event %s", tostring(event))
            end
        end
        })
	self.viewLayer:addChild(nicknameText,10)
	nicknameText:setFontColor(ccc3(0, 0, 0))

	local okBtn = KNBtn:new( PATH , {"ok.png" , "ok_pre.png"} , 390 , display.cy-220 , {
		callback = function()
			local niceID = string.trim( nicknameText:getText())
			if niceID ~= "" and string.len( niceID ) <= 18 then
				self.nickStr = niceID

				-- 发请求
				local post_data = {
					general_select = generals[self.general_select] ,
					sex = self.sex == 2 and 0 or 1 , 
					name = self.nickStr,
					channel = CHANNEL_ID,
				}

				-- 发请求
				HTTP:call(20002 , post_data , {
					success_callback = function()
						
					end
				})
			else
				KNMsg.getInstance():flashShow("昵称输入不合法")
			end
		end
	}):getLayer()
	self.viewLayer:addChild( okBtn )
	
	self.layer:addChild( self.viewLayer )
end

function NewLayer:selectGeneral()
	if self.viewLayer ~= nil then
		--self.layer:removeChild(self.viewLayer , true)--如果这里是true，会luatouchmanager错误
		self.viewLayer:removeFromParent(false)
	end
	self.viewLayer = display.newLayer()
	
	local bg = display.newSprite(PATH .. "bg.png")
	setAnchPos(bg , 0 , 0)
	self.viewLayer:addChild(bg)

	local title_bg = display.newSprite(PATH .. "title_bg.png")
	setAnchPos(title_bg , display.cx , display.top-70 , 0.5)
	self.viewLayer:addChild(title_bg)

	local title = display.newSprite(PATH .. "title.png")
	setAnchPos(title , display.cx , display.top-60 , 0.5)
	self.viewLayer:addChild(title)

	-- 头像选择
	local name_bg = display.newSprite(PATH .. "name_bg.png")
	setAnchPos(name_bg , display.cx , display.top-550 , 0.5)
	self.viewLayer:addChild(name_bg)

	--  姓名-人物
	local head_bg = display.newSprite(PATH .. "head_bg.png")
	setAnchPos(head_bg , display.cx , display.top-580 , 0.5)
	self.viewLayer:addChild(head_bg)
	
	
	self.group = KNRadioGroup:new()
	for i = 1 , 3 do
		local reg_button = KNBtn:new(SCENECOMMON , {"box.png", "select1.png"} , 83 + (i - 1) * 123 , display.top-570 , {
			front = getImageByType(generals[i] , "s"),
			frontScale = {1 , -3 , 3},
			noHide = true,
			selectZOrder = 50,
			selectOffset = {-3,3},
			scale = true,
			callback = function()
--				self:selectOne(i)
				self.scroll:setIndex(i)
			end
		},self.group)
		self.viewLayer:addChild( reg_button:getLayer() )

		local name = display.newSprite(PATH .. "name_" .. i .. ".png")
		setAnchPos(name , 113 + (i - 1) * 123 , display.top-600 , 0.5)
		self.viewLayer:addChild(name)
	end

	local light = display.newSprite(PATH .. "light.png")
	setAnchPos(light , display.cx , 515 , 0.5 , 0.5)
	self.viewLayer:addChild(light)

	local light_action
	light_action = function(rotate)
		transition.rotateTo(light , {time = 10 , rotate = rotate , onComplete = function()
			if rotate == 180 then rotate = 360 else rotate = 180 end
			light_action(rotate)
		end})
	end
	light_action(180)
	

	--[[注册按钮]]
	local reg_callback = function()
		self.general_select = self.scroll:getCurIndex()

		self:regName()
	end


	local reg_button = KNBtn:new(COMMONPATH , {"btn_bg_red.png","btn_bg_red_pre.png"} , 165 , 60 , {
		front = COMMONPATH .. "confirm_big.png",
		scale = true,
		callback = reg_callback
	})
	self.viewLayer:addChild( reg_button:getLayer() )


	self.layer:addChild( self.viewLayer )


--	self:selectOne(1)
	
	self.scroll = KNScrollView:new(0, display.top-750, 480, 700, 0, true, 1, {
		page_callback = function()
			self.group:chooseByIndex(self.scroll:getCurIndex())
		end
	})
	for i = 1, 3 do
		local temp = self:selectOne(i)
		self.scroll:addChild(temp, temp)
	end
	self.scroll:alignCenter()
	self.viewLayer:addChild(self.scroll:getLayer())
	
end

function NewLayer:selectOne(index)
	index = tonumber(index)
	self.selectIndex = index

	local cid = generals[index]

	local layer = display.newLayer();

	local bg = display.newSprite(PATH .. "card_bg_" .. index .. ".png")
	setAnchPos(bg , display.cx , 315 , 0.5)
	layer:addChild(bg)

	local big_icon = display.newSprite(getImageByType(cid , "b"))
	setAnchPos(big_icon , display.cx , 385 , 0.5)
	layer:addChild(big_icon)
	
	local job = display.newSprite(COMMONPATH.."job"..getConfig("general", generals[index], "role")..".png")
	setAnchPos(job, 130, 610)
	layer:addChild(job)

	local name_bg = display.newSprite(PATH .. "banner_bg.png")
	setAnchPos(name_bg , 315 , 440)
	layer:addChild(name_bg)

	local name = display.newSprite(PATH .. "banner_" .. index .. ".png")
	setAnchPos(name , 315 , 445)
	layer:addChild(name)


	local star_num = Config_General[cid]["star"]
	local star_init_x = 175 + (5 - star_num) * 14
	for i = 1 , star_num do
		local star = display.newSprite(COMMONPATH .. "star.png")
		setAnchPos(star , star_init_x + (i - 1) * 28 , 655)
		layer:addChild(star)
	end

	return layer
end


return NewLayer

