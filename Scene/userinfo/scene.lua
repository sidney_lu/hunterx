collectgarbage("setpause"  ,  100)
collectgarbage("setstepmul"  ,  5000)


-- [[ 包含各种 Layer ]]
local UserInfoLayer = requires("Scene.userinfo.userinfolayer")--require "Scene/pet/petlayer"



local M = {}

function M:create(rank)
	local scene = display.newScene("userinfo")

	---------------插入layer---------------------
	local info = UserInfoLayer:new(rank)
	scene:addChild(info:getLayer())
	---------------------------------------------
	function scene:onExit()
		info:stopSchedule()
	end
	function scene:onEnter( ... )
		--print("userinfo scene on enter")
		info:updateHeader()
	end
	return scene
end

-- function onNodeEvent( event )
--         if (event == "enter") then
--             if temp.onEnter then
--                 temp:onEnter()    
--             end
--         elseif (event == "exit") then
--             if temp.onExit then
--                 temp:onExit()
--             end
--             temp:unregisterScriptHandler()
--             if temp.scheduleScriptEntry then
--                 cc.Director:getInstance():getScheduler():unscheduleScriptEntry(temp.scheduleScriptEntry)
--             end
--         end
--     end
--     temp:registerScriptHandler(onNodeEvent)

-- function M:onEnter( ... )
-- 	print("userinfo scene onEnter")
-- end

return M
