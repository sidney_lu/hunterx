local HeadScene = class("HeadScene", function()
    return cc.Scene:create()
end)
local HEADER_PATH = IMG_PATH.."image/scene/userinfo/header/"
local KNBtn = requires("Common.KNBtn")
function HeadScene.create( ... )
	local instance = HeadScene.new()
	instance:init()
	

	return instance
end
function HeadScene:init( ... )

	local bg = display.newSprite(COMMONPATH .. "dark_bg.png")
	bg:setPosition(cc.p(display.cx,display.cy))
	self:addChild(bg)
	bg:setScaleY(display.height/698)
	bg:setScaleX(display.width/480)

	local bg_big = display.newSprite(HEADER_PATH.."ss_bg.png")
	bg_big:setPosition(cc.p(display.cx,display.cy))
	self:addChild(bg_big)
	self.bg_big = bg_big

	local btnClose = KNBtn:new(COMMONPATH, {"close.png"}, 410 , 500, {
			callback = function()
				cc.Director:getInstance():popScene()
			end}):getLayer()
	bg_big:addChild(btnClose)

	

	local headerPath
	if DATA_User:get("sex")==0 then
	 	headerPath= IMG_PATH.."image/scene/userinfo/header/head_big/20006.png"
	 	self.defaultNum = 20006
	else
		headerPath= IMG_PATH.."image/scene/userinfo/header/head_big/19001.png"
		self.defaultNum = 19001
	end

	if DATA_User:get("head")~=0 then
		headerPath = IMG_PATH.."image/scene/userinfo/header/head_big/"..DATA_User:get("head")..".png"
		self.defaultNum = DATA_User:get("head")
	end
	self.finalNum = self.defaultNum

	self.headIcon = display.newSprite(headerPath)
	self.headIcon:setPosition(cc.p(141/2,141/2))
	--bg_head:addChild(self.headIcon)
	local clipping = display.newClippingRectangleNode(cc.rect(0, 0, 136, 136))
	--local clipping = cc.ClippingNode:create()s
	--local rect = cc.rect(0, 0, 135, 135)
	--clipping:setStencil(rect)
	--local  stencil = cc.Node:create()    --模板stencil节点Node
	--stencil:setContentSize(100,100)
	--stencil:addChild(bg_head)
	--clipping:setStencil(stencil)
	clipping:addChild(self.headIcon)
	-- clipping:setInvertedf
	-- clipping:setAlphaThreshold(0.5)
	setAnchPos(clipping , 45 , 328 , 0 , 0)
	bg_big:addChild(clipping)

	local bg_head = display.newSprite(HEADER_PATH.."zz.png")
	bg_head:setPosition(cc.p(112,396))
	bg_big:addChild(bg_head)


	local itemGold = display.newSprite(HEADER_PATH.."xxx.png")
	itemGold:setPosition(cc.p(310,438))
	bg_big:addChild(itemGold)
	local goldIcon = display.newSprite(COMMONPATH.."silver.png")
	goldIcon:setPosition(cc.p(18,20))
	itemGold:addChild( goldIcon)
	goldText = ui.newTTFLabel({
        text = "10000",
        size = 20,
        color = ccc3( 0xff , 0xff , 0xff ),
        x = 80,
        y = 20,
        align = ui.TEXT_ALIGN_CENTER
	})	
	itemGold:addChild(goldText)

	local itemFlag = display.newSprite(HEADER_PATH.."xxx.png")
	itemFlag:setPosition(cc.p(310,389))
	bg_big:addChild(itemFlag)	
	local flagIcon = display.newSprite(COMMONPATH.."tribute.png")
	flagIcon:setPosition(cc.p(18,20))
	itemFlag:addChild(flagIcon)
	flagText = ui.newTTFLabel({
        text = DATA_Bag:getTypeCount("prop",17001),
        size = 20,
        color = ccc3( 0xff , 0xff , 0xff ),
        x = 80,
        y = 20,
        align = ui.TEXT_ALIGN_CENTER
	})	
	itemFlag:addChild(flagText)

	local btnConfirm = KNBtn:new(HEADER_PATH, {"queding.png","queding_pre.png"}, 303 , 299, {
			callback = function ( ... )
				self:saveHeader()
			end }):getLayer()
	bg_big:addChild(btnConfirm)

	local btnReset = KNBtn:new(HEADER_PATH, {"huanyuan.png","huanyuan_pre.png"}, 195 , 299, {
			callback = function ( ... )
				self:resetHeader()
			end}):getLayer()
	bg_big:addChild(btnReset)	

	self:initList()


end

function HeadScene:initList( ... )
	self.icons = {}
	self.bg_big:addChild(self:createIcon(19001,43,215))
	self.bg_big:addChild(self:createIcon(19002,118,215))
	self.bg_big:addChild(self:createIcon(19003,191,215))
	self.bg_big:addChild(self:createIcon(19004,265,215))
	self.bg_big:addChild(self:createIcon(19005,339,215))

	self.bg_big:addChild(self:createIcon(19006,43,135))	
	self.bg_big:addChild(self:createIcon(19007,118,135))
	self.bg_big:addChild(self:createIcon(20000,191,135))
	self.bg_big:addChild(self:createIcon(20001,265,135))
	self.bg_big:addChild(self:createIcon(20002,339,135))

	self.bg_big:addChild(self:createIcon(20003,43,55))	
	self.bg_big:addChild(self:createIcon(20004,118,55))
	self.bg_big:addChild(self:createIcon(20005,191,55))
	self.bg_big:addChild(self:createIcon(20006,265,55))
	self.bg_big:addChild(self:createIcon(20007,339,55))
	self.icons[self.finalNum]:setFront(COMMONPATH.."select1.png")
	print(self.finalNum,"def",self.defaultNum)
end
function HeadScene:createIcon( num ,x,y)
	--local sp =display.newSprite(HEADER_PATH.. "head/"..num..".png")
	--sp:setPosition(cc.p(x,y))
	local sp
	sp = KNBtn:new(HEADER_PATH, {"head/"..num..".png"}, x , y, {
			front = COMMONPATH.."role_frame.png",
			callback = function()
				--cc.Director:getInstance():popScene()
				
				self.icons[self.finalNum]:setFront(COMMONPATH.."role_frame.png")
				sp:setFront(COMMONPATH.."select1.png")
				self:changeHeader(num)
			end})
	self.icons[num] = sp
	local frame = display.newSprite(COMMONPATH.."role_frame.png")
	frame:setPosition(cc.p(32,32))
	sp:getLayer():addChild(frame)

	return sp:getLayer()
end
function HeadScene:changeHeader( num )
	self.headIcon:setSpriteFrame(display.newSpriteFrame(HEADER_PATH.."head_big/"..num..".png"))
	self.finalNum = num
end
function HeadScene:saveHeader( ... )
	if self.finalNum == self.defaultNum then
		cc.Director:getInstance():popScene()
		return
	end
	HTTP:call("update_head",{ num = self.finalNum },{success_callback = 
		function()
			
			cc.Director:getInstance():popScene()
	end})	
end
function HeadScene:resetHeader( ... )
	--self.finalNum = self.defaultNum
	self.icons[self.finalNum]:setFront(COMMONPATH.."role_frame.png")
	self:changeHeader(self.defaultNum)
	self.icons[self.finalNum]:setFront(COMMONPATH.."select1.png")
end
function HeadScene:preLoadFrames( imageUri ,imageName)
	--local texture=CCTextureCache:sharedTextureCache():addImage(CCFileUtils:sharedFileUtils():getCachePath().."..\\res\\"..imageUri)
	local texture=cc.Director:getInstance():getTextureCache():addImage(imageUri)
    local texSize=texture:getContentSize()
    local texRect = cc.rect(0, 0, texSize.width, texSize.height);
    local frame=cc.SpriteFrame:createWithTexture(texture,texRect)
	cc.SpriteFrameCache:getInstance():addSpriteFrame(frame,imageName);
end
return HeadScene