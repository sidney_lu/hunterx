local InfoLayer = requires("Scene.common.infolayer")
local KNBtn = requires("Common.KNBtn")
local SelectList = requires("Scene.common.selectlist")
local KNMask = requires("Common.KNMask")
local PATH = IMG_PATH.."image/scene/userinfo/"
local SCENECOMMON = IMG_PATH.."image/scene/common/"

local scrollLayer,baseScroll
local UserInfoLayer = {
	baseLayer,
	layer,
	timeLayer,
	timeSchedule,
	timeItems
}

function UserInfoLayer:new(params)
	local this = {}
	setmetatable(this,self)
	self.__index = self
	--cache init------------------------------
	for i=1,7 do 
		local name =IMG_PATH.."image/scene/userinfo/header/head_big/1900"..i..".png"	
		this:preLoadFrames(name,name)
		name = IMG_PATH.."image/scene/userinfo/header/head/1900"..i..".png"
		this:preLoadFrames(name,name)
	end
	
	for i=1,8 do
		local name = IMG_PATH.."image/scene/userinfo/header/head_big/2000"..(i-1)..".png"
		this:preLoadFrames(name,name)
		name = IMG_PATH.."image/scene/userinfo/header/head/2000"..(i-1)..".png"
		this:preLoadFrames(name,name)
	end
	local name = COMMONPATH.."sex0.png"
	this:preLoadFrames(name,name)
	name = COMMONPATH.."sex1.png"
	this:preLoadFrames(name,name)
	--------------------------------------------

	baseScroll = KNScrollView:new(0, 90 , display.width, display.height - 120 - 60, 10 , false )
	--scrollLayer = cc.LayerColor:create(ccc4(255,123,123,0))
	--scrollLayer:setContentSize(display.width,500)
	

	this.baseLayer = display.newLayer()

	
	this.layer = display.newLayer()
	this.layer:setContentSize(display.width,726)
	baseScroll:addChild(this.layer)
	-- 背景
	local bg = display.newSprite(COMMONPATH .. "dark_bg.png")
	setAnchPos(bg , 0 , 70)						-- 70 是底部公用导航栏的高度(好像不是哦)

	local bg_big = display.newSprite(COMMONPATH .. "bg_big.png")
	setAnchPos(bg_big , 18 , 88)
	this.layer:addChild(bg_big)
	--玩家头像
	local infoBg = display.newSprite(PATH.."info_bg.png")	
	setAnchPos(infoBg,245,650,0.5,0.5)
	this.layer:addChild(infoBg)
	
	local headerPath = COMMONPATH.."sex" .. DATA_User:get("sex") .. ".png"
	if DATA_User:get("head")~=0 then
		headerPath = IMG_PATH.."image/scene/userinfo/header/head/"..DATA_User:get("head")..".png"
	end
	--infoBg = display.newSprite(COMMONPATH .."sex" .. DATA_User:get("sex") .. ".png")
	infoBg = KNBtn:new("", {headerPath}, 311 , 338, {
			front = headerPath,
			parent = baseScroll,
			callback = function()
				local scene = require("Scene.userinfo.headscene").create()
				cc.Director:getInstance():pushScene(scene)
			end})
	setAnchPos(infoBg:getLayer(),71,619)
	this.layer:addChild(infoBg:getLayer())
	self.infoBtn = infoBg

	local frame = display.newSprite(COMMONPATH.."role_frame.png")
	setAnchPos(frame,67,614)
	this.layer:addChild(frame)

	--名称显示
	local text = display.newSprite(PATH.."name.png")
	setAnchPos(text,150,660)
	this.layer:addChild(text)
	local curVipLv = DATA_Vip:get("viplv")
	
	if DATA_Vip:isVip() then
		this.layer:addChild( display.newSprite( IMG_PATH .. "image/scene/vip/v" .. curVipLv.. ".png" , 230 , 660 ):align(display.BOTTOM_LEFT) )
	end
	
	text = display.strokeLabel( DATA_User:get("name") , ( DATA_Vip:isVip() and 270 or 230 ) , 658 , 24 , ccc3(0x4a,0x08,0x08) )
	this.layer:addChild(text)
	
	--称号显示
	text = display.newSprite(PATH.."title.png")
	setAnchPos(text,150,615)
	this.layer:addChild(text)
	
	if params then
		local str ="v0.png"
		for i = 1, #getTitle() do
			if tonumber(params.rank) > 10 then
			elseif tonumber(params.rank) > getTitle()[i][1] then
				str = "v"..(i - 1)..".png"
				break
			end
		end
		text = display.newSprite(PATH..str)
		setAnchPos(text, 230, 615)
		this.layer:addChild(text)
	end
	this.layer:addChild( display.newSprite(PATH.."detail_bg.png" , display.cx , 280):align(display.BOTTOM_CENTER) )
	local curexp = DATA_User:get("exp")
	local LevelUpExperience = getConfig("user", DATA_User:get("lv")+1, "exp")
	local showElement = {
			--{ text = "id" 		, value = DATA_User:get("uid") 	} ,										--玩家id
			{ text = "level" 	, value = DATA_User:get("lv")	} ,		
			{ text = "id"		, value = "-" },								--玩家等级
			{ text = "exp"		, value = curexp.."/"..LevelUpExperience} ,		                                --玩家经验
			--{ text = "value"	, value = params.ability	} ,											--玩家战力
			{ text = ""			, value = 0	} ,															--空
			{ text = "commander", value = DATA_User:getLead()	} ,										--统帅
			--{ text = "gang"		, value = params.gang	} ,												--帮会
			
			{ text = "on"		, value = DATA_Formation:get_lenght().."/8"	} ,							--上阵武将
			--{ text = "vip_text"	, value = DATA_Vip:get("viplv")	} ,										--vip等级
			--{ text = ""			, value = 0	} ,															--空
			{ text = "gold"		, value = DATA_User:get("gold")	} ,									    --黄金
			{ text = "silver"	, value = DATA_User:get("silver")	} ,								    --银两
	}
	local addX , addY
	for i = 1 , #showElement do
		local curData = showElement[i]
		if curData.text ~= "" then
			addX = 60 + ( i - 1 ) % 2 * 212
			addY = 540 - math.floor( ( i - 1 ) / 2 ) * 50
			local pathStr = PATH .. curData.text .. ".png"
			if curData.text == "gold" or curData.text == "silver" then
				pathStr = COMMONPATH .. curData.text .. ".png"
			end
			
			this.layer:addChild( display.newSprite( pathStr , addX , addY):align(display.BOTTOM_LEFT) )
			addX = addX + 93
			if curData.text == "on" then
				addX = addX + 18
			elseif curData.text == "vip_text" then
				addX = addX + 10
			elseif curData.text == "gold" or curData.text == "silver" then
				addX = addX - 50
			end
			this.layer:addChild( display.strokeLabel(curData.value , addX  , addY , 24 , ccc3( 0xff , 0xfb , 0xd4 ) ) )
		end
	end
	
	
	local lookVipBtn = KNBtn:new(COMMONPATH, {"long_btn.png", "long_btn_pre.png"}, 311 , 334, {
			front = PATH.."look_privilege.png",
			callback = function()
				--switchScene("vip")
				pushScene("vip")
			end}):getLayer()
	this.layer:addChild(lookVipBtn)
	
	--体力信息，当前值下次恢复值，总时间 
	this.layer:addChild( display.newSprite(PATH.."data_bg.png" , display.cx , 114 ):align(display.BOTTOM_CENTER)	 )			--体力背景
	this.layer:addChild( display.newSprite( PATH .. "power.png"  		, display.cx , 240):align(display.BOTTOM_CENTER) )	--体力文字title
	this.layer:addChild( display.newSprite( PATH .. "power_value.png" 	, 60 , 200):align(display.BOTTOM_LEFT) )				--当前体力值
	this.layer:addChild( display.newSprite( PATH .. "next_power.png" 	, 60 , 160):align(display.BOTTOM_LEFT) )				--下个恢复时间
	this.layer:addChild( display.newSprite( PATH .. "all_power.png" 	, 60 , 120):align(display.BOTTOM_LEFT) )				--所有恢复时间
	this.layer:addChild( display.strokeLabel( DATA_Power:get("num").."/"..DATA_Power:get("max"),140,200,22,ccc3(0x4a,0x08,0x08) ) )	--当前体力值/最大体力值
	
	
--	--斗志信息，当前值下次恢复值，总时间 
--	dataBg = display.newSprite(PATH.."data_bg.png")	
--	setAnchPos(dataBg,245,180,0.5,0.5)
--	this.layer:addChild(dataBg)
--
--	text = display.newSprite(PATH.."energy.png")
--	setAnchPos(text,245,225,0.5)
--	this.layer:addChild(text)
--	
--	text = display.newSprite(PATH.."energy_value.png")
--	setAnchPos(text,60,205)
--	this.layer:addChild(text)
--	
--	text = display.strokeLabel(DATA_Energy:get("num").."/"..DATA_Energy:get("max"),140,205,22,ccc3(0x4a,0x08,0x08))
--	this.layer:addChild(text)
--	
--	text = display.newSprite(PATH.."next_energy.png")
--	setAnchPos(text,60,165)
--	this.layer:addChild(text)
--	
--	text = display.newSprite(PATH.."all_energy.png")
--	setAnchPos(text,60,125)
--	this.layer:addChild(text)
	
	--体力与斗志的更新时间
	this.timeItems = {
		DATA_Power:get("next_recover_time"),
		DATA_Power:get("all_recover_time"),
--		DATA_Energy:get("next_recover_time"),
--		DATA_Energy:get("all_recover_time"),
	}
	
	--生成倒计时信息
	--this:createTimeLayer()
	
	
	
	this.baseLayer:addChild(bg)
	--this.baseLayer:addChild(bg_big)
	--this.baseLayer:addChild(this.layer)
	this.baseLayer:addChild(baseScroll:getLayer())
	--导航信息
	local info = InfoLayer:new("userinfo" , 0 , {tail_hide = true , title_text = PATH .. "user_title.png"})
	this.baseLayer:addChild(info:getLayer() , 2)

	return this
end


function UserInfoLayer:getLayer()
	return self.baseLayer
end

function UserInfoLayer:createTimeLayer()
	if self.timeLayer then
		self.layer:removeChild(self.timeLayer,true)
	end
	
	
	
	self.timeLayer = display.newLayer()
	
	local time = display.strokeLabel( timeConvert(self.timeItems[1]) , 250 , 182 , 20 )
	self.timeLayer:addChild(time)
	
	time = display.strokeLabel(timeConvert(self.timeItems[2]) , 250 , 142 , 20 )
	self.timeLayer:addChild(time)
--	
--	time = display.strokeLabel(timeConvert(self.timeItems[3]),250,165,20)
--	self.timeLayer:addChild(time)
--	
--	time = display.strokeLabel(timeConvert(self.timeItems[3]),250,125,20)
--	self.timeLayer:addChild(time)
	
	
	if not self.timeSchedule then
		self.timeSchedule = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(
			function()
				local refresh = false
				for i = 1, #self.timeItems do
					if self.timeItems[i] > 0 then
						self.timeItems[i] = self.timeItems[i] - 1
						if self.timeItems[i] == 0 then  --计时到0时请求一次数据
							refresh = true
						end
					end
				end
				if refresh then
					HTTP:call("status","get",{},{success_callback=
						function()
							switchScene("userinfo")
						end
					})
				else
					self:createTimeLayer()
				end
			end,1,false)
	end
	self.layer:addChild(self.timeLayer)	
end
function UserInfoLayer:updateHeader(  )
	local headerPath = COMMONPATH.."sex" .. DATA_User:get("sex") .. ".png"
	if DATA_User:get("head")~=0 then
		headerPath = IMG_PATH.."image/scene/userinfo/header/head/"..DATA_User:get("head")..".png"
	end
	self.infoBtn:setFront(headerPath)
end
function UserInfoLayer:preLoadFrames( imageUri ,imageName)
	--local texture=CCTextureCache:sharedTextureCache():addImage(CCFileUtils:sharedFileUtils():getCachePath().."..\\res\\"..imageUri)
	local texture=cc.Director:getInstance():getTextureCache():addImage(imageUri)
    local texSize=texture:getContentSize()
    local texRect = cc.rect(0, 0, texSize.width, texSize.height);
    local frame=cc.SpriteFrame:createWithTexture(texture,texRect)
	cc.SpriteFrameCache:getInstance():addSpriteFrame(frame,imageName);
end

function UserInfoLayer:stopSchedule()
	if self.timeSchedule then
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(self.timeSchedule)
	end
end

				
return UserInfoLayer