local PATH = IMG_PATH.."image/scene/war/"
local MOVE, HOLD, HIDE = 1, 2, 3
local Role = {
	layer,
	moving,
	stateIcon,
	state,
	data,
	name,
	nameBg
}

function Role:new(x, y,state, params)
	local this = {}
	self.__index = self
	setmetatable(this, self)
	
	this.stateIcon = {}
	this.data = params or {}
	
	this.layer = display.newLayer()
	this.layer:ignoreAnchorPointForPosition(false)
	setAnchPos(this.layer, x, y, 0.5, 0.5)
	--testlog("role.lua new instance")
	--dump(params)
	if this.data.group == 1 then 
		--if tonumber(this.data.uid) == tonumber(DATA_User:get("uid")) then
		if tonumber(this.data.id) == tonumber(DATA_User:get("uid")) then
			this.stateIcon[MOVE] = display.newSprite(PATH.."sign_self.png")		
		else
			this.stateIcon[MOVE] = display.newSprite(PATH.."sign_our.png")		
		end
		this.stateIcon[HOLD] = display.newSprite(PATH.."flag_our.png")		
	else
		--if tonumber(this.data.uid) == tonumber(DATA_User:get("uid")) then
		if tonumber(this.data.id)== tonumber(DATA_User:get("uid")) then
			this.stateIcon[MOVE] = display.newSprite(PATH.."sign_self.png")		
		else
			this.stateIcon[MOVE] = display.newSprite(PATH.."sign_enemy.png")		
		end
		this.stateIcon[HOLD] = display.newSprite(PATH.."flag_enemy.png")		
	end
	
	this.stateIcon[MOVE]:setVisible(false)
	this.stateIcon[HOLD]:setVisible(false)
	
	this.layer:setContentSize(this.stateIcon[MOVE]:getContentSize())
	
	setAnchPos(this.stateIcon[MOVE])
	setAnchPos(this.stateIcon[HOLD], this.stateIcon[HOLD]:getContentSize().width / 4, this.stateIcon[HOLD]:getContentSize().height / 4)
	
	this.layer:addChild(this.stateIcon[MOVE])
	this.layer:addChild(this.stateIcon[HOLD], 1)
	
	this.nameBg = display.newSprite(PATH.."name_bg.png")
	setAnchPos(this.nameBg,this.layer:getContentSize().width / 2, -25, 0.5)
	this.layer:addChild(this.nameBg)
	
	this.name = createLabel({str = this.data.name, size = 18, color = ccc3(0xe2, 0xe2, 0xe2)})
	setAnchPos(this.name, this.layer:getContentSize().width / 2, -22, 0.5)
	this.layer:addChild(this.name)
	
	if state then
		this:setState(state)
	else
		this:setState(HIDE)
	end
	
	return this
end
-- function Role:( ... )
-- 	-- body
-- end
function Role:setState(state)
	if self.stateIcon[self.state] then
		self.stateIcon[self.state]:setVisible(false)
	end
	
	self.state = state
	if state == HOLD or state == HIDE then
		self.moving = false
		self.layer:setRotation(0)
		self.name:setVisible(false)
		self.nameBg:setVisible(false)
	else
		self.name:setVisible(true)
		self.nameBg:setVisible(true)
	end
	
	if self.stateIcon[state] then
		self.stateIcon[state]:setVisible(true)
	end
end

function Role:hold(x, y, hide)
	if hide then
		self:setState(HIDE)
	else
		self:setState(HOLD)
	end
	self.layer:stopAllActions()
	self.layer:setPosition(ccp(x, y ))
end

function Role:battle(x, y)
	self:setState(HIDE)
	self.layer:stopAllActions()
	self.layer:setPosition(ccp(x, y))
end
function Role:reset(  )
	testlog("role.lua reset reset")
	self:setState(HOLD)
end
function Role:backHome(desX, desY)
	self:setState(HOLD)	
	self.layer:stopAllActions()
	self.layer:runAction(getSequenceAction(CCMoveTo:create(1, ccp(desX, desY)), CCCallFunc:create(function()
		self:setState(HIDE)
	end)))
	
end
function Role:startRun(  )
	if not self.actions then 
		testlog("startRun role actions = nil")
		return
	end
	if  self.layer:getNumberOfRunningActions()>=1 then
		testlog("role is running ")
		return
	end

	if #self.actionData>0 then
		local acts = self:decodeActions(self.actionData)
		table.insert(acts,	cc.CallFunc:create(function()
			self.moving = false
			self:setState(HOLD)
		end))
		
	
		local seq =cc.Sequence:create(acts)
		self.layer:runAction(seq)
		testlog("startRun role actions running",#acts)
		self.actions = {}
		self.actionData = {}
	else
		testlog("startRun role actions = 0")
	end
end
function Role:decodeActions( data )
	local actions = {}
	for k,v in pairs(data) do
		if v["key"] == "degree" then
			table.insert(actions,cc.RotateTo:create(v["frm"],v["to"]))
		elseif v["key"]=="move" then
			table.insert(actions,cc.CallFunc:create(function ( ... )
				self:setState(MOVE)
			end))
			table.insert(actions,cc.MoveTo:create(v["time"],cc.p(v.x,v.y)))
		end	
	end
	return actions
end
function Role:setPosition( x,y )
	self.layer:setPosition(x,y)
end
function Role:getPosition( )
	return self.layer:getPosition()
end
function Role:addMove( x,y ,percentage,srcX,srcY)
	local curX = self.layer:getPositionX()
	local curY = self.layer:getPositionY()
	srcX = srcX or curX
	srcY = srcY or curY
	local degree, time
	if x == srcX then
		if y > srcY then
			degree = 0
		else
			degree = 180
		end
	elseif y == srcY then
		if x > srcX then
			degree = 90
		else
			degree = -90
		end
	else
		degree = math.deg(math.atan((y - srcY) / (x - srcX)))
		if y < srcY then
			degree = degree + 180
		end
	end
	self.moving = true
	self:setState(MOVE)
	time = 1
	if not self.actions then
		self.actions = {}
	end
	if not self.actionData then
		self.actionData = {}
	end
	
	--local len = math.sqrt(x*x + y*y)
	--x1 / len * percentage = x / len *
	table.insert(self.actions, cc.RotateTo:create(0,degree))
	table.insert(self.actionData,{key="degree",frm=0,to=degree})
	--table.insert(self.actions,cc.MoveTo:create(time,cc.p(x,y)))
	table.insert(self.actions,cc.MoveTo:create(time,cc.p((x - srcX)*percentage + srcX  ,(y - srcY)*percentage + srcY)))
	table.insert(self.actionData,{key="move",time=time,x=(x - srcX)*percentage + srcX,y=(y - srcY)*percentage + srcY})
	print("role move fm ",srcX,srcY)
	print("role move to ",x,y,(x - srcX)*percentage + srcX , (y - srcY)*percentage + srcY)
	-- self.layer:runAction(getSequenceAction(cc.RotateTo:create(0, degree),
	-- 	cc.MoveTo:create(time, ccp(x, y)), 
	-- 	cc.CallFunc:create(function()
	-- 		self.moving = false
	-- 		self:setState(HOLD)
	-- 	end)))
	print("role add move count ",#self.actions)
end
-- function Role:move(x, y, map)
-- 	if self.moving then
-- 		return
-- 	end
	
-- 	local curX = self.layer:getPositionX()
-- 	local curY = self.layer:getPositionY()
	
-- 	local degree, time
-- 	if x == curX then
-- 		if y > curY then
-- 			degree = 0
-- 		else
-- 			degree = 180
-- 		end
-- 	elseif y == curY then
-- 		if x > curX then
-- 			degree = 90
-- 		else
-- 			degree = -90
-- 		end
-- 	else
-- 		degree = math.deg(math.atan((y - curY) / (x - curX)))
-- 		if y < curY then
-- 			degree = degree + 180
-- 		end
-- 	end
-- 	self.moving = true
-- 	self:setState(MOVE)
-- 	time = 5 
-- 	self.layer:runAction(getSequenceAction(cc.RotateTo:create(0, degree),
-- 		cc.MoveTo:create(time, ccp(x, y)), 
-- 		cc.CallFunc:create(function()
-- 			self.moving = false
-- 			self:setState(HOLD)
-- 		end)))
-- end
function Role:getID( )
	return self.data.id
end
function Role:getPos( )
	return self.data.pos
end
function Role:setPos( pos )
	self.data.pos = pos		
end
-- function Role:setPosition( x,y )
-- 	self.layer:setPosition(x,y)
-- end
function Role:isMoving()
	return self.moving
end


function Role:getLayer()
	return self.layer
end

return Role

