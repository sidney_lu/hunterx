local KNBtn = requires("Common.KNBtn")
local PATH = IMG_PATH.."image/scene/war/"
local Mgr = requires("Scene.war.mapmgr")

local WarLayer = {
	baseLayer,
	layer,
	mgr,
	data
}
local WAR_ID = 1
function WarLayer:new(data)
	local this = {}
	self.__index = self
	setmetatable(this, self)
	
	this.baseLayer = display.newLayer()
	this.data = data
	
	local bg = display.newSprite(PATH.."bg.jpg")
	setAnchPos(bg)
	this.baseLayer:addChild(bg)
	
	bg = display.newSprite(PATH.."score_bg.png")
	setAnchPos(bg, 10, 10)
	this.baseLayer:addChild(bg)
	
	bg = display.newSprite(PATH.."score_bg.png")
	setAnchPos(bg, display.width - 10 - 148 , display.height - bg:getContentSize().height - 10)
	this.baseLayer:addChild(bg)
	
	local back = KNBtn:new(COMMONPATH, {"back_img.png", "back_img_press.png"}, 50, display.height - 50, {
		callback = function()
			--switchScene("home")
			--war_join 屏蔽掉了？？
			--gangbattle_enter 自然会加入人
			-- HTTP:call("war_join",{warid=WAR_ID},{success_callback=function ( ... )
			-- 	testlog("war_join callback")
			-- end})
			this.mgr:get()
			
		end
	})
	this.baseLayer:addChild(back:getLayer())
	
	local btnLocation = KNBtn:new(COMMONPATH, {"back_img.png", "back_img_press.png"}, 50, display.height - 120, {
		callback = function()
			-- HTTP:call("war_info",{warid=WAR_ID, uid=DATA_Session:get("uid")},{success_callback =function ( data )

			-- end})
			--uid被占用，所以不能用uid了
			HTTP:call("war_door",{key="join",wid=1,playerid=100099,group=1,pos=9},{success_callback=function ( data )
			 	testlog("测试加一人 100099 pos=3")
			 	dump(data)
			 	--之前的enter事件已经初始化人员
			 	--所以这里加了一个人做测试，界面也要加一个
			 	this.mgr:addPlayer({id="100099",group=1,pos=9,stat="hold"})
			end})
		end
	})
	this.baseLayer:addChild(btnLocation:getLayer())
	this:warMap()
	
	return this
end

function WarLayer:warMap()
	if self.layer then
		self.baseLayer:removeChild(self.layer, true)
	end
	self.layer = display.newLayer()
	echoLog("WarLayer init map")
	dump(self.data,nil,6)
	self.mgr = Mgr:new(self.data)
	self.layer:addChild(self.mgr:getLayer())
	
	self.baseLayer:addChild(self.layer)
end

function WarLayer:refresh(data)
	self.mgr:mapLogic(data)
end

function WarLayer:getLayer()
	return self.baseLayer
end


return WarLayer