local M = {}
local Path = IMG_PATH.."image/scene/war/"
function M:create( mapManager )
	local this = {}
	self.__index = self
	setmetatable(this, self)

	self.layer = display.newLayer()
	mapManager:getLayer():addChild(self.layer)
	self.mapManager = mapManager


	return this
end
--[[
这里尽量只处理视图（视图逻辑）而不处理业务逻辑
]]
-- function M:execute( data,timeflag )
-- 	for k,v in pairs(self.items[])
-- end
function M:execute( data)

	if data.group == 1 then
		self.mapManager.items[tostring(data.id)]:setPos(1)
		
	else
		self.mapManager.items[tostring(data.id)]:setPos(13)
	end
	--更新旗子位置
	self.mapManager:setPlayer(self.mapManager.items[tostring(data.id)])
	testlog("iconmanager exe group=",data.group)
end
--[[
M:update 		更新玩家棋子状态样式
params:
data.id         玩家ID
data.time 		当前时间
data.rebornflag 复活时间
]]
function M:update( data )
	-- self.lblCount = 
	-- self.layer:addChild()
	local role = self.mapManager.items[tostring(data.id)]
	if role.lblCount==nil then
		role.lblCount = cc.LabelTTF:create("5",FONT,25)
		--role.lblCount:setPosition(cc.p(137,170))
		role:getLayer():addChild(role.lblCount)
	end
	
	role.lblCount:setString(tostring(data.rebornflag - data.time))
end
-- 3秒后
function M:setBack( data,time )
	--group = nil
	testlog("iconmanager group=",data.group)
	dump(data)
	if data.group==1 then
		-- role entity
		self.mapManager.items[data.id]:setPosition(display.width-60,50)
	else
		-- role entity
		self.mapManager.items[data.id]:setPosition(10,display.height-50)

	end
	self.mapManager.items[data.id].rebornflag = time + 5
--	self.items[data.uid] = {timeflag=time + 3}
	self:update({id=data.id,time=time,rebornflag=self.mapManager.items[data.id].rebornflag})
end

return M