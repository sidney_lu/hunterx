local M = {}
function M:create( params )
	local this = {}
	self.__index = self
	setmetatable(this, self)

	this.layer = display.newLayer()
	local lblText = cc.LabelTTF:create("结束，胜利",FONT,25)
	--role.lblCount:setString(tostring(data.rebornflag - data.time))
	lblText:setPosition(display.width/2,display.height/2)
	this.layer:addChild(lblText)
	return this
end
function M:getLayer( )
	return self.layer
end
function M:show( data )
	self.layer:setVisible(true)
	
end
function M:hide( ... )
	self.layer:setVisible(false)
end
return M