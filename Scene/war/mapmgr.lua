local KNBtn = requires("Common.KNBtn")
local Role = requires("Scene.war.role")
local PATH = IMG_PATH.."image/scene/war/"

local L, M, R = 50, 212, 375
local base, space = 10, 130 
local mapPos = {
		{	
			--{M - 30, 20,{2}},                    --1
			{M, 20,{2}},                    --1
			{M, base + space,{3,4,5}},                  --2
			{L, base + space * 2,{4,6}},                --3
			{M, base + space * 2,{5,7}},               --4
			{R, base + space * 2,{8}},                  --5
			{L, base + space * 3,{9}},                  --6
			{M, base + space * 3,{10}},                 --7 
			{R, base + space * 3,{11}},                 --8
			{L, base + space * 4,{10,12}},               --9
			{M, base + space * 4,{11,12}},                --10
			{R, base + space * 4,{12}},                 --11
			{M, base + space * 5,{13}},                  --12
			--{M - 30, base + space * 5 + 70,{}},         --13
			{M, base + space * 5 + 70,{}},         --13
		}
	}

local MapMgr = {
	baseLayer,
	mapLayer,
	items = {},   --参加帮战成员.Role
	points,   --据点
	mapId,
	offsetX, -- 在据点的偏移
	offsetY,
	baseX,   --在大本营的偏移
	baseY,
	fightAni,
	warInitial,--[[
					{data,--pos
					group,
					state,--killed/hold/move
					}
				]]
}
-- local co = nil
-- function MapMgr.run( warfield )
-- 	for 1=1 then
-- 		HTTP:call("war_get",{},{success_callback =function ( data )
-- 			dump(data,"",6)
-- 			if data ~= "wait" then
-- 				self:mapLogic(data)
-- 			end
-- 		end})
-- 	end
-- end
--在ganglayer调用这个方法
--貌似应该在warlayer调用这个方法了现在
local runCount = 0
local timeflag
function MapMgr:get(  )
	-- if not co then
	-- 	co = coroutine.create(war.run)
	-- end
	Clock:addTimeFun("war_clock", function()
		runCount = runCount + 1
		local i,decimal = math.modf(runCount/3)
		if decimal==0 then
			print("Clock war_clock",os.time())	
			HTTP:call("war_get",{timeflag=timeflag},{success_callback =function ( data )
				--dump(data,"",6)
				if data ~= "wait" then
				 	timeflag = data.time
					self.warTime:setString(timeConvert(data.time))

					self:mapLogic(data,timeflag)
				
					for key,role in pairs(self.items) do
						if role:getPos()==0 then
							--testlog("mapmgr get pos =0 key=",key)
							--testlog("mapmgr get pos rebornflag",role.rebornflag,timeflag)
							--dump(role.data)
							if timeflag >=role.rebornflag then
								self.iconManager:execute(role.data,timeflag)
							else
								self.iconManager:update({id=role.data.id, time=timeflag,rebornflag=role.rebornflag})
							end


						end
					end
				
				end
				for k, v in pairs(self.items) do
					--dump(v)
					v:startRun() --没有action的不会跑是
				end	
			end,no_mask = true})	

			
		end
	end)
	
	
end
--[[
地图初始化，params参数马上会传入
]]
function MapMgr:new(params)
	local this = {}
	self.__index = self
	setmetatable(this, self)
	this.params = params or {}
	this.baseLayer = display.newLayer()

	this.points = {}
	this.fightAni = {}
	this.mapId = 1
	
	timeflag = params.time
	this:createMap()

	self.warTime = createLabel({str = "1999.07.01", size = 20, color = ccc3(0x2c, 0, 0), x = 260, y = 338, width = 400})
	this.baseLayer:addChild(self.warTime)
	self.warTime:setPosition(cc.p(10,display.height-50))

	this.iconManager = require("scene.war.layers.iconmanager"):create(this)

	-- this.resultLayer = require("scene.war.layers.result"):create()
	-- this.baseLayer:addChild(this.resultLayer:getLayer())
	-- this.resultLayer:show()
	return this
end
function MapMgr:genData(  )
	local data = {}
	data.change_stat = {}
	data.change_stat[1]={}
	data.change_stat[1].stat="move"
	data.change_stat[1].uid="100069"
	local moveData={}
	data.change_stat[1].data=moveData
	moveData.cur = 3 -- 当前节点（这个和服务器 cur不同）
	moveData.max = 3 -- 整个路径的最大节点
	moveData.from = 2
	moveData.to = 2
	return data 
end
function MapMgr:createMap()
	if self.mapLayer then
		self.baseLayer:removeChild(self.mapLayer, true)
	end
	self.mapLayer = display.newLayer()
	--dump(self.params)
	echoLog("MapMgr createMap")
	echoLog("MapMgr createMap",self,self.params.stat)
	local my = DATA_User:get("uid")..""
	self.instance = {}
	self.instance.data = self.params.init[my]
	--self.instance.logic = self:tempLogic()
	--call self.map1()??

	local map = self["map"..self.mapId](self, self.params)
	self.mapLayer:addChild(map)
	self.mapPoint = map
	
	self.baseLayer:addChild(self.mapLayer)
end

function MapMgr:map1(params)
	
	local warInitial = params.init
	self.warInitial = warInitial
	local my = DATA_User:get("uid")..""
	echoLog("MapMgr map1 params_ini_my_data",params,warInitial,my,warInitial[my])
	local layer = display.newLayer()
	
	local iMyGroup = warInitial[my].group
	local pos = mapPos[1] 

	--自己所在的点不能点
	for i = 1, #pos do
		local point
		if i == 1 or i == #pos then
			local str
			if i == 1 then
				str = "self_supreme.png"
			else
				str = "enemy_supreme.png"
			end
			--我方或对方大本营
			point = KNBtn:new(PATH, {str}, pos[i][1], pos[i][2], {
				callback = function()
					-- --print("MapMgr instance",self.instance.data)
					-- if self.instance.data.data ==i then return end
					-- if self.items[my]:isMoving()  then
					-- else
					-- 	-- if 1==1 then
					-- 	-- 	testlog("click i =",i)
					-- 	-- 	return
					-- 	-- end
					-- 	if (init[my].group == 1 and i == 1) or (init[my].group == 2 or i == #pos) then
			
							HTTP:call("war_action",{warid = 1 ,uid = DATA_Session:get("uid"),  to = i},
								{success_callback=function ( data )
									--print("war_action success_callback",init[my].group,i)
									print("war_action base success_callback")
									dump(data)
									--self:mapLogic(data)
								end})
					-- 	end
					-- end
				end})
			self.baseX = point:getWidth() / 2
			self.baseY = point:getHeight() / 2
		else
			local text 
			-- echoLog("mapmgr point ",i,self.params)
			-- echoLog("mapmgr point ",i,self.params.map)
			-- echoLog("mapmgr point ",i,self.params.map.point[i])
			-- echoLog("MapMgr,init",my)
			-- echoLog("MapMgr,init",warInitial)
			-- echoLog("MapMgr,init",warInitial[my].group)
			
			--echoLog(self.params.map.point[i].score_2)
			--echoLog(self.params.map.point[i]["score".."_"..warInitial[my].group])
			if self.params.map.point[i]["score".."_"..warInitial[my].group] ~= 0 then
				text = {self.params.map.point[i]["score".."_"..warInitial[my].group].."积分",16, ccc3(0xe2, 0xe2, 0xe2), ccp(0, -25)}
			end
			
			point = KNBtn:new(PATH, {"pt_empty.png"}, pos[i][1], pos[i][2], {
				text = text,
				callback = function()
					if self.instance.data.data ==i then return end
					-- if self.items[my]:isMoving()  then
					-- else
		
						testlog("click i =",i)
						HTTP:call("war_action",{warid = 1 ,uid = DATA_Session:get("uid"),  to = i},
							{success_callback=function ( data )
								print("war_action success_callback")
								--self:mapLogic(data)
						end})
		--			end
				end
			})
			self.offsetX = point:getWidth() / 2
			self.offsetY = point:getHeight() / 2
			
			--将据点按钮对象加入
			self.points[i] = point
		end	
		layer:addChild(point:getLayer())
		
		local data = pos[i][3]
		for j = 1, #data do
			local line = display.newSprite(PATH.."map_line.png")
			setAnchPos(line, pos[i][1] + point:getWidth() / 2 + line:getContentSize().height / 2, pos[i][2] + point:getHeight() / 2.5)
			layer:addChild(line, -1)
			if pos[data[j]][1] - pos[i][1] == 0 or i == 1 or i == 12 then  --垂直旋转90度
				line:setRotation(-90)
				if i == 1 then
					setAnchPos(line,pos[i][1] + 5 + point:getWidth() / 2 + line:getContentSize().height / 2, pos[i][2] + point:getHeight() / 2.5)
				elseif i == 12 then
					setAnchPos(line,pos[i][1] + point:getWidth() / 2 + line:getContentSize().height / 2, pos[i][2] + point:getHeight() / 2.5 - 20)
				end
			elseif pos[data[j]][2] == pos[i][2] then  --横向连接
			else  --根据位置旋转一定的角度
				local rotate = math.deg(math.atan((pos[data[j]][2] - pos[i][2]) / (pos[data[j]][1] - pos[i][1]))) - 90
				if pos[data[j]][1] > pos[i][1] then
					rotate = rotate + 10
				else
					rotate = rotate - 10
				end
				line:setRotation(rotate)
				line:setScaleX(1.2)
			end
		end
	end
	
--	dump(init)
	--dump(warInitial)
--	玩家初始化，一般是hold
	for k, v in pairs(warInitial) do
		--print(k,v.sta,v.data)
		v.id = k
		if v.stat == "hold" then
			if v.data == 1 or v.data == #pos then
				--self.items[k] = Role:new(pos[2][1] + self.offsetX, pos[v.data][2] + self.baseY, 3, v)	
				self.items[k] = Role:new(pos[2][1] + self.offsetX, pos[v.data][2] + self.baseY, 2, v)	
			else
				self.items[k] = Role:new(pos[v.data][1] + self.offsetX, pos[v.data][2] + self.offsetY, 2, v)	
			end
		elseif v.stat == "move" then
			self.items[k] = Role:new(pos[v.data[1]][1] + self.offsetX, pos[v.data[1]][2] + self.offsetY,1, v)
		

		end
		layer:addChild(self.items[k]:getLayer())
	end
	return layer
end
--这个方法，只能在地图一有用，组1，和组2的基地点是固定的
--因为没有统一坐标转换
function MapMgr:addPlayer( player )
	local pos = mapPos[1]
	--table.insert(warInitial,)
	self.warInitial[player.id] = {data=player.pos,group=player.group,stat="hold"}
	testlog("mapmgr addPlayer id=",player.id)
	if player.pos==1 or player.pos == #pos then

		self.items[player.id] = Role:new(pos[2][1] + self.offsetX, pos[player.pos][2] + self.baseY, 2, player)	
	else
		self.items[player.id] = Role:new(pos[player.pos][1] + self.offsetX, pos[player.pos][2] + self.offsetY, 2, player)	
	end
	self.mapPoint:addChild(self.items[player.id]:getLayer())
end
function MapMgr:setPlayer( role )
	local pos = mapPos[1]
	--这个方法，只能在地图一有用，组1，和组2的基地点是固定的
	--因为没有统一坐标转换
	--role.data as pos
	if role:getPos()==1 or role:getPos()== #pos then
		self.items[role:getID()]:getLayer():setPosition(pos[2][1] + self.offsetX, pos[role:getPos()][2] + self.baseY)
	else
		self.items[role:getID()]:getLayer():setPosition(pos[role:getPos()][1] + self.offsetX, pos[role:getPos()][2] + self.offsetY)
	end	
end
-- function MapMgr:getLogic( src,des )
-- 	local v,w,k,min
-- 	local final = {}
-- 	local Pathmatirx, ShortPathTable = {}
-- 	for i=1,13 do
-- 		final[i] = 0
-- 		ShortPathTable[i] = 0
-- 	end

-- 	for v=
-- end
function MapMgr:countWithoutNil(t)
    local count = 0
    for k, v in pairs(t) do
    	if v then
    		print(countWithoutNil,v)
        	count = count + 1
        end
    end
    return count
end
function MapMgr:contains( t, index )
	for k,v in pairs(t) do
		if v==index then return true end
	end
	return false
end
--无权最短路径(已放到服务器实现？？)
function MapMgr:unweighted( v0 ,v1)
	local vertex = {}
	vertex[1] = {name = "v1",_index = 1,adjacent={2}}
	vertex[2] = {name = "v2",_index = 2,adjacent={3,4,5,1}}
	vertex[3] = {name = "v3",_index = 3,adjacent={2,4,6}}
	vertex[4] = {name = "v4",_index = 4,adjacent={3,2,5,7}}
	vertex[5] = {name = "v5",_index = 5,adjacent={4,2,8}}
	vertex[6] = {name = "v6",_index = 6,adjacent={3,9}}
	vertex[7] = {name = "v7",_index = 7,adjacent={4,10}}
	vertex[8] = {name = "v8",_index = 8,adjacent={5,11}}
	vertex[9] = {name = "v9",_index = 9,adjacent={6,12,10}}
	vertex[10] = {name = "v10",_index = 10,adjacent={7,9,8,12}}
	vertex[11] = {name = "v11",_index = 11,adjacent={8,12,10}}
	vertex[12] = {name = "v12",_index = 12,adjacent={9,10,11,13}}
	vertex[13] = {name = "v13",_index = 13,adjacent={12}}
	for k,v in pairs(vertex) do
		v.dist = 100000
		v.know = false
	end
	-- local S = {}
	-- s[v0] = 0
	--vertex[v0].know = true 
	vertex[v0].dist = 0
	local f = io.open("D:/x/fantasyhero/src/Scene/war/game.log", 'w')
	for curDist = 0,12 do 
		for k, v in pairs(vertex) do
			print("vertex",k,v.know,v.dist)
			if v.know ==false and v.dist == curDist then
				print("valid",v.name,v.dist,v.adjacent)
				v.know = true
				for index,point in pairs(vertex) do
					if self:contains(v.adjacent,index)==true then
						if point.dist == 100000 then
							point.dist = curDist + 1
							point.path = v
						end
					end
				end
			end 
		end

		f:write("start i="..curDist.."\n")
		for k, v in pairs(vertex) do
			f:write(v.name.." |")
			f:write(v.dist.." |")
			if v.path then
				f:write(v.path.name.." |")
			else
				f:write("- |")
			end
			f:write("\n")
		end
		
		f:write("\n")
	end
	f:close()
	local item = vertex[v1]
	local temp = {}
	while item.path~=nil do
		table.insert(temp,item)
		item = item.path
	end
	return temp
	
end
function MapMgr:dijkstra( v0 ,v1)
	v1= v1 or 5
	local n = 5 
	local visit = {}
	local A = {}
	A[1] = {}
	A[1][2]  = 1
	A[2] = {}
	A[2][1] =1
	A[2][3] =1
	A[2][4] =1
	A[2][5] =1
	A[3] = {}
	A[3][2] = 1
	A[3][4] = 1
	A[3][6] = 1
	A[4]= {}
	A[4][3] = 1
	A[4][5] = 1 
	A[4][2] = 1
	A[4][7] = 1
	A[5] = {}
	A[5][2] = 1
	A[5][8] = 1
	A[5][4] = 1
	A[6] = {}
	A[6][3] = 1
	A[6][9] = 1
	A[7] = {}
	A[7][4] = 1
	A[7][10] = 1
	A[8] = {}
	A[8][5] = 1
	A[8][11] = 1
	A[9] = {}
	A[9][6] = 1
	A[9][12] = 1
	A[10] = {}
	A[10][7] = {}
	A[10][12] = {}
	A[11] = {}
	A[11][8] = 1
	A[11][12] = 1
	A[12] = {}
	A[12][10] = 1
	A[12][9] = 1
	A[12][11] = 1
	A[12][13] = 1
	local dist = {}
	local S = {}
	local prev = {}
	for i=1,n do
		if A[v0][i]~=nil then
			dist[i] = A[v0][i]
		else
			dist[i] = 100000
		end
		S[i] = false
		if dist[i] == 100000 then
			prev[i] = -1
		else
			prev[i] = v0
		end
	end
	dist[v0] = 0
	S[v0] = true
	local u = v0
	-----------
	local f = io.open("D:/x/fantasyhero/src/Scene/war/game.log", 'w')
	for i=2,n do
		-- local mindist = 100000
		
		-- for j=1,n do
		-- 	if S[j]~=true and dist[j]<mindist then
		-- 		u = j
		-- 		mindist = dist[j]
		-- 	end
		-- end

		-- S[u] =true
		-- for j=1,n do
		-- 	if S[j]~=true and A[u][j]<dist[j] then
		-- 		dist[j] = dist[u] + A[u][j]
		-- 		prev[j] = u
		-- 	end
		-- end
		f:write("start i="..i.."\n")
		f:write("d:")
		for k, v in pairs(S) do
			if v==true then
				f:write("1 |")
			else
				f:write("0 |")
			end
		end
		f:write("\nf:")
		for k,v in pairs(dist) do
			f:write(v.." |")
		end
		
		f:write("\n")
	end
	f:close()
end
function MapMgr:tempLogic()
	local data = {}
	data.change_stat = {}
	-- for i=1,4 do
	-- 	local item = {}
	-- 	item.data={i,i+1}
	-- 	item.stat="move"
	-- 	item.uid = DATA_User:get("uid")..""
	-- 	item.old_data = {}
	-- 	--item.old_data.battle = {}
	-- 	table.insert(data.change_stat,item)
	-- end
	local enemyArray = {1000077,1000076,1000075,1000069}
	for i = 1 ,3 do
		local enemy = self.items[enemyArray[i]..""]
		local item = {}
		item.data = {enemy.data.data ,math.random(1,13)}
		item.stat = "move"
		item.uid = enemyArray[i]
		item.old_data = {}
		table.insert(data.change_stat,item)
	end

	local item = {}
	item.stat = "battle"
	item.uid = 1000076
	item.battle = {}
	item.battle.on_way = 0
	item.battle.report_id = 1
	item.old_data = {data = 3}
	table.insert(data.change_stat,item)
	return data
end
function MapMgr:moveMe( index )
	local k = DATA_User:get("uid")..""
	local pos = mapPos[self.mapId]
	self.items[k]:addMove(pos[index][1] + self.offsetX, pos[index][2] + self.offsetY)
end
function MapMgr:moveChest( uid,  index ,cur,max,source)
	local k = uid..""
	local pos = mapPos[self.mapId]
	if not cur then
		self.items[k]:addMove(pos[index][1] + self.offsetX, pos[index][2] + self.offsetY)
		
	else
		-- print("move chest",cur/max)
		-- print("from ",source,"to ",index)
		-- print(pos[index][1],pos[index][2],self.offsetX,self.offsetY)
		-- print(pos[source][1] ,pos[source][2],self.offsetX,self.offsetY)
		-- --roles
		-- print("k=",k)
		--dump(self.items)
		self.items[k]:addMove(pos[index][1] + self.offsetX, pos[index][2] + self.offsetY,
			cur/max,pos[source][1] + self.offsetX,pos[source][2] + self.offsetY)
	end
end

function MapMgr:moveMeExecute(  )
	local k = DATA_User:get("uid")..""
	self.items[k]:startRun()
end
function MapMgr:mapLogic(data,time)
	local x, y
	local pos = mapPos[self.mapId]
	local activities = {}
	--对于所有改变的状态循环处理
	for n, v in pairs(data.change_stat) do
			
		if v.stat == "move" and v.data["from"]~=v.data["to"] then
			local k = v.uid..""
			-- 清除战斗
			self:removeBattle(v.uid)
			self:moveChest(k,v.data["to"], v.data["cur"],v.data["max"],v.data["from"])
			self.items[k].data.data = cur
			if not activities[k] then
				activities[k] = self.items[k]
			end
			if self.points[v.data["from"]] then
				self.points[v.data["from"]]:setBg(1, PATH.."pt_empty.png")
			end
		-- elseif v.stat == "hold" then
		-- 	if v.old_data.battle then   --发生过战斗
		-- 		if v.old_data.battle.win == 0 then  --战斗失败
		-- 			self.items[k]:backHome(pos[2][1] + self.offsetX, pos[v.data][2] + self.baseY)
		-- 		else
		-- 			if v.data == 1 or v.data == #pos then
		-- 				self.items[k]:hold(pos[v.data][1] + self.offsetX, pos[v.data][2] + self.baseY, true)
		-- 			else
		-- 				self.items[k]:hold(pos[v.data][1] + self.offsetX, pos[v.data][2] + self.offsetY)
		-- 			end
		-- 		end
		-- 	else
		-- 	end
			
		-- 	if v.data ~= 1 and v.data ~= #pos then
		-- 		if v.group == 1 then
		-- 			self.points[v.data]:setBg(1, PATH.."pt_our.png")
		-- 		else
		-- 			self.points[v.data]:setBg(1, PATH.."pt_enemy.png")
		-- 		end
		-- 	end
		-- elseif v.stat == "hold" then
		-- 	-- 清除战斗
		-- 	testlog("清除战斗")
		-- 	self:removeBattle(v.uid)
			--self.items[v.id]
		elseif v.stat == "battle" then
			local k = v.uid..""
			--这里获取战斗发生的位置,
			--print("uid"..tostring(v.uid))
			local role = self.items[tostring(v.uid)]
			local x,y =role:getPosition()
			
			self.items[k]:battle(x, y)
			--战斗动画
			if not self.fightAni[v.uid] then

				local frames = display.newFramesWithImage(PATH .. "fight_effect.png" , 6 )
				self.fightAni[v.uid] = display.playFrames(x, y, frames, 0.1, {
					forever = true
				})
				self.mapLayer:addChild(self.fightAni[v.uid])
				-- print("++++++++++++++++++++++++++++++++++++")
				-- dump(self.fightAni)
				-- print("++++++++++++++++++++++++++++++++++++")
			end
		else
			local handler = require("Scene.war.step."..v.stat)
			handler:execute(self,v,time)
		end

		

	end


end
function MapMgr:removeBattle( uid )
	--将战斗的特效移除
	dump(self.fightAni)
	print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@",uid)
	if self.fightAni[uid] then
		
		self.mapLayer:removeChild(self.fightAni[uid], true)
		self.fightAni[uid] = nil
		print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
	end
end

function MapMgr:getLayer()
	return self.baseLayer
end


return MapMgr