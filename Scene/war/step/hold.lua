local M = {}
function M:create(  )
	local this = {}
	self.__index = self
	setmetatable(this, self)
	return this
end
function M:execute( mapManager,data )
	testlog("execute exe hold")
	mapManager.items[tostring(data.uid)]:reset()
	mapManager:removeBattle(data.uid)
end
return M