local M = {}
function M:create()
	local this = {}
	self.__index = self
	setmetatable(this, self)
	return this
end
function M:execute( mapManager,data )
	mapManager.resultLayer = require("scene.war.layers.result"):create()
	mapManager.baseLayer:addChild(mapManager.resultLayer:getLayer())
	mapManager.resultLayer:show()
end
return M