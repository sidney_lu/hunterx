collectgarbage("setpause"  ,  100)
collectgarbage("setstepmul"  ,  5000)


-- [[ 包含各种 Layer ]]
local HeroLayer = requires("Scene.hero.herolayer")



local M = {}

function M:create(args)
	local scene = display.newScene("hero")
	
	---------------插入layer---------------------
	local instanceLayer = HeroLayer:new(args)
	scene:addChild(instanceLayer.baseLayer)
	--scene:addChild(HeroLayer:new(args))
	---------------------------------------------
	
	function scene:onEnter()
		
		HeroLayer:checkAdvancedEquip()
		--每次enter会设置封神栏，这里会触发一次
		--所以第一次进入会设置2次s3，但是应该问题不大
		instanceLayer:updateFengshen()
	end
	return scene
end

return M
