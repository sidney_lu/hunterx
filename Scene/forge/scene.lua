--[[

首页场景

]]


collectgarbage("setpause"  ,  100)
collectgarbage("setstepmul"  ,  5000)


-- [[ 包含各种 Layer ]]
local ForgeLayer = requires("Scene.forge.forgelayer")
local InfoLayer = requires("Scene.common.infolayer")



local M = {}
local PATH = IMG_PATH.."image/scene/forge/"
function M:create(params)
	params = params or {}
	local scene = display.newScene("forge")

	---------------插入layer---------------------
	local forgeLayer = ForgeLayer:new(params or {})
	scene:addChild(forgeLayer:getLayer())
	self.infoLayer=InfoLayer:new("forge", 0, { title_hide = false,title_text = PATH.."title.png", closeCallback = forgeLayer.onBack })
	--scene:addChild(InfoLayer:new("forge", 0, { title_hide = true }):getLayer())
	forgeLayer:getLayer():addChild(self.infoLayer:getLayer())
	---------------------------------------------

	return scene
end

return M
