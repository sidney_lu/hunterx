local PATH = IMG_PATH.."image/scene/chat/"
local KNBtn = requires("Common.KNBtn")
local KNRadioGroup = requires("Common.KNRadioGroup")
local contentLayer = requires("Scene.chat.contentLayer")
local infoLayer = requires("Scene.common.infolayer")

--[[
	首页
]]
local showLayer = {
	baseLayer,
	layer,
	infoLayer,
}

function showLayer:new()
	local this = {}
	setmetatable(this , self)
	self.__index = self
	this.back_data = {}
	this.is_show_dialog = false
	local handle = CCDirector:sharedDirector():getScheduler()
	this.dialog = nil

	this.baseLayer = display.newLayer()
	this.layer = display.newLayer()

	-- 背景
	local bg = display.newSprite(COMMONPATH .. "dark_bg.png")
	setAnchPos(bg , 0 , 86)
	this.layer:addChild(bg)
	
	
	-- 导航
	local tabConfig = { --系统
		{
			"system",
			"system.png",
			"system_big.png",
		},
		{--战报
			"battle",
			"battle.png",
			"battle_big.png",
		},
		-- {--消费
		-- 	"consume",
		-- 	"consume.png",
		-- 	"consume_big.png",
		-- },
		{--社交
			"social",
			"social.png",
			"social_big.png",
		},
	}


	local defaultTab = 1
	local msg_num = DATA_Mail:getNum("battle")
	-- 如果当前有战报，则战斗邮件设置为默认并清空战报数目
	-- if msg_num > 0 then
	-- 	defaultTab = 2

	-- 	DATA_Mail:setNum("battle",0)
	-- end

	local temp
	local startX , startY = 10 , display.top-150
	this.group = KNRadioGroup:new()
	for i = 1, #tabConfig do
		local num = tonumber( DATA_Mail:getNum(tabConfig[i][1]) or 0 )
		local showNum = tabConfig[i][1] ~= "system" and num > 0
		temp = KNBtn:new(COMMONPATH .. "tab" , { "tab_star_normal.png" , "tab_star_select.png" } , startX , startY , {
			front = { PATH .. tabConfig[i][2] , PATH .. tabConfig[i][3]},
			other = showNum and { { COMMONPATH .. "egg_num_bg.png", 72, 37 } } or nil,
			text = showNum and { tostring( num ), 18 , ccc3( 0xff , 0xff , 0xff ) , { x = 40 , y = 21	} , nil , 20 } or nil,
			disableWhenChoose = true,
			id = tabConfig[i][1],
			callback = function()
				if this.infoLayer ~= nil then
					this.layer:removeChild(this.infoLayer:getLayer() , true)
				end

				this.infoLayer = contentLayer:new(tabConfig[i][1] , this)
				this.layer:addChild(this.infoLayer:getLayer())
			end
		} , this.group)

		this.layer:addChild(temp:getLayer())
		startX = startX + temp:getWidth() + 4
	end
	
	--公告按钮
	--local noticeBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" ,"btn_bg_pre.png"}, startX , display.top-150 ,
	local noticeBtn = KNBtn:new( COMMONPATH , { "btn_bg_pre.png" ,"btn_bg_pre.png"}, startX , display.top-150 ,
	{
		priority = -149,
		front = COMMONPATH .. "notice.png" ,
		callback = 
		function()
			local noticeLayer = requires("Scene.common.notice")
			local curScene = display.getRunningScene()
			local temp = noticeLayer:new()
			if temp ~= "not notice" then
		
				curScene:addChild( temp:getLayer() )
			else
				KNMsg.getInstance():flashShow( "当前没有公告！" )
			end
		end
	}):getLayer()
	this.layer:addChild(noticeBtn)
	
	-- 线
	local line = display.newSprite(COMMONPATH .. "tab_line.png")
	setAnchPos(line , 6 , display.top-155)
	this.layer:addChild(line)

	-- 具体展示数据
	this.group:chooseByIndex( defaultTab , false )
	this.infoLayer = contentLayer:new( tabConfig[defaultTab][1] , this)
	this.layer:addChild(this.infoLayer:getLayer())

	this.baseLayer:addChild( this.layer )

	-- 显示标题栏和底部按钮
	local info = infoLayer:new("chat" , 0 , {tail_hide = true , title_text = PATH .. "title.png"}):getLayer()
	this.baseLayer:addChild( info )

    return this
end


function showLayer:getLayer()
	return self.baseLayer
end


--[[刷新所有聊天消息]]
function showLayer:refreshChatContent(message_type)
	if message_type == "talk" and self.infoLayer ~= nil and self.group:getId() == "talk" then
		self.infoLayer:refresh(message_type)
	end
end

return showLayer
