--[[

英雄动作 (受击)

]]


local M = {}

local logic = requires("Scene.battle.logicLayer")


--[[执行特效 -- 刀光]]
function M:normal( hero , param )
	if type(param) ~= "table" then param = {} end
	

	--[[特效开始]]
	local sprite
	local frames = display.newFramesWithImage(IMG_PATH.."image/scene/battle/heroAction/dao.png" , 3)
	sprite = display.playFrames(
		hero._cx,
		hero._cy,
		frames,
		0.12 * logic.speed,
		{
			onComplete = function()
				sprite:removeSelf()	-- 清除自己
				
				if param.onComplete then param.onComplete() end
			end
		}
	)
	sprite:setAnchorPoint( ccp(0.5 , 0.5) )

	-- 震动一下
	local shake_y = ( hero:getData("_group") == 2 and 30 ) or -30
	M:shake( hero , { time = 0.05 , y = shake_y } )

	-- 颜色变红一下
	M:tint( hero , { max_num = 3 , time = 0.06 } )


	-- 放到 特效层
	logic:getLayer("effect"):addChild( sprite )


	return true
end

--[[英雄卡牌稍微震动一下]]
function M:shake( hero , param )
	if type(param) ~= "table" then param = {} end

	transition.shake( hero , {
		time = param.time * logic.speed or 0.12 * logic.speed,
		x = param.x or 0,
		y = param.y or -30,
		no_scale = true,
		onComplete = function()
			if param.onComplete then param.onComplete() end
		end
	})
end

--[[英雄卡牌变红，再变回来]]
function M:tint( hero , param )
	if type(param) ~= "table" then param = {} end

	local cur_num = param.cur_num or 0
	local max_num = param.max_num or 1
	if cur_num >= max_num then return end

	local spend = param.time or 0.1
	spend = spend * logic.speed
	transition.playSprites( hero , "tintTo" , {
		time = spend,
		r = param.r or 200,
		g = param.g or 0,
		b = param.b or 0,
	})

	transition.playSprites( hero , "tintTo" , {
		delay = spend,
		time = param.time,
		r = 255,
		g = 255,
		b = 255,
		onComplete = function() 
			M:tint( hero , { max_num = max_num , cur_num = cur_num + 1 , time = param.time } )

			if param.onComplete then param.onComplete() end
		end
	})

end

return M
