--[[

战斗结果

]]


local _ResultLayer = {}
local HeroLayer = requires("Scene.hero.herolayer")
local logic = requires("Scene.battle.logicLayer")
local KNBtn = requires( "Common.KNBtn")
local Upgrade = requires("Scene.common.upgrade")
-- local KNShowbylist = requires( "Common/KNShowbylist")
-- local KNNumberroll = requires("Common/KNNumberroll")
local KNCardpopup = requires("Common.KNCardpopup")
local KNBar = requires( "Common.KNBar")
local award_handle = nil		-- 弹出奖励动画的定时器
local card_mask
local result_sprite
local confirmBtn		-- 确定按钮
local KNMask = requires("Common.KNMask")

local PATH = IMG_PATH.."image/scene/battle/fruit/"
local WIN_PATH = IMG_PATH.."image/scene/battle/fruit/win/"
local LOST_PATH = IMG_PATH.."image/scene/battle/fruit/lost/"
local TITLE_PATH = WIN_PATH .. "title_win.png"		
local girl		--女孩
local bgFrame	--公用
local addType 	--坐标类型
local resultData

-- test data --
local function testdata()
	local result = io.readfile( "socket_zhanduojieshu2.txt")
	result = json.decode( result )
	result = result["result"]
	--存储数据
	DATA_Mission:setByKey(result["current"]["map_id"],"missions",result["missions"])
	DATA_Mission:setByKey("current",result["jump"])
	DATA_Mission:setByKey("max",result["max"])

	--结束画面用数据
	DATA_Result:set( result )
	resultData = DATA_Result:get()
end
if TESTDATA then
	testdata()
end

function _ResultLayer:create( params )
	
	confirmBtn = nil
	addType = 0
	params = params or {}
	
	--强制清除宠物目标选择层
	local scene = display.getRunningScene()
	if scene:getChildByTag("899") then
		local selectMask = scene:getChildByTag("899")
		if selectMask then
			scene:removeChild(selectMask , true)
		end
	end
	local m_mod = DATA_Battle:getMod()
	if m_mod == "guide" then
		return _ResultLayer:guide_result()
	end
	
	local fbMode = {
		inshero  = "十字坡" ,		
		insequip = "藏宝楼" ,
		inspet   = "景阳岗" , 	
		insskill = "如意阁" , 	
		rob 	  = "夺宝" , 	
	}
	
	--战斗结果数据
	resultData = DATA_Result:get()
	
	--扫荡数据
	if params.type == "mopUp" then
		return _ResultLayer:mission_result( params )
	end
	
	
	-- 数据兼容
	resultData.awards = resultData.awards or {}

	if DATA_Battle:get("win") ~= 1 then
		_ResultLayer:warLost()
		testlog("_ResultLayer warLost win!=1")
	else
		testlog("_ResultLayer warLost win==1")
		if resultData.type == "rob" or  resultData.type == "mining" then
			if resultData.success == 0 then
				_ResultLayer:warLost()
				testlog("_ResultLayer warLost win")
			else
				audio.stopMusic()
				i7PlaySound( IMG_PATH .. "sound/win.mp3")
			end
		else
			audio.stopMusic()
			i7PlaySound( IMG_PATH .. "sound/win.mp3")
		end
	end
	testlog("_ResultLayer throught")
	if fbMode[ m_mod ] then	--磨练 //已经包含夺宝
		print("----------磨练")
		return _ResultLayer:instance_result()
	elseif m_mod == "athletics" then	--竞技
		print("----------竞技")
		return _ResultLayer:athletics_result()
	elseif m_mod == "friends" then	--好友切磋
		print("----------好友切磋")
		return _ResultLayer:friends_result()
	elseif m_mod == "mining" then		--抢夺矿山
		print("----------抢夺矿山")
		return _ResultLayer:mining_result()
	elseif m_mod == "boss" then
		return _ResultLayer:boss_result()--warLost()
	else
		print("----------任务")
		TITLE_PATH = WIN_PATH .. "mission_win.png"
		return _ResultLayer:mission_result()		--任务
	end

end

--战斗失败
function _ResultLayer:warLost()
	--数据兼容
	resultData = resultData or {}
	audio.stopMusic()
	i7PlaySound( IMG_PATH .. "sound/lose.mp3" )
	
	result_sprite = display.newSprite( LOST_PATH .. "lost_bg.png")
	local size = result_sprite:getContentSize()
	
	local baseHeight = 16
	local lostTipPath
	if resultData.type == "athletics" then	lostTipPath = "lost_tip_athletics.png"		--竞技
	elseif resultData.type == "insskill" then lostTipPath = "lost_tip_skill.png"		--如意阁
	elseif resultData.type == "mining" and resultData.success == 0  then lostTipPath = "tip_mine_text.png"		--矿山战斗胜利但已经被别人占领
	else lostTipPath = "lost_tip_text.png"	
	end
	local existMiss = {
		insequip = "insequip" , 
		mission = "mission" , 
		mining = "mining",
		boss = "boss",
		base = "base"
	}
	resultData.type = resultData.type or "base"
	
	local lostTitle = display.newSprite( LOST_PATH .. ( resultData.type == "rob" and "rob_lost.png" or "lost_title.png" ) )
	bgFrame = display.newSprite( LOST_PATH .. "lost_bg_frame.png")
	girl = display.newSprite( PATH .. "girl.png")
	local girlFace = display.newSprite( LOST_PATH .. "lost_face.png")
	local tipText = display.newSprite( LOST_PATH .. lostTipPath )
	local tipTitle = display.newSprite( LOST_PATH .. "tip_title.png") -- 小贴士
	
	result_sprite:setPosition(display.cx , display.cy)  
	lostTitle:setPosition(display.cx + 17, display.top)
	bgFrame:setPosition(20, display.cy - baseHeight)
	girl:setPosition(display.cx+80,  display.cy - 27)
	--girlFace:setPosition(188 + 131, display.cy - 27 + 72)
	tipText:setPosition(20, display.cy + 80)
	tipText:setAnchorPoint(cc.p(0,0.5))
	tipTitle:setPosition(20, display.cy + 40)
	tipTitle:setAnchorPoint(cc.p(0,0.5))
	result_sprite:addChild( lostTitle )
	--result_sprite:addChild( bgFrame )
	result_sprite:addChild( girl )
	--result_sprite:addChild( girlFace )
	if existMiss[ resultData.type .. "" ] then
		result_sprite:addChild( tipText )
	end
	result_sprite:addChild( tipTitle )
	
	--去强化
	local goStrengthen = KNBtn:new( COMMONPATH , { "btn_bg_red.png" ,"btn_bg_red_pre.png"} , 
		( existMiss[ resultData.type .. "" ] and 64 or 73), 120,
		{
			isIsolated = true,
			priority = -150,
			front = LOST_PATH .. ( DATA_User:get("lv") <= 8 and "go_strengthen2.png" or "go_strengthen.png" ) ,
			callback = 
			function()
				if DATA_User:get("lv") <= 8 then
					DATA_Formation:set_index(1)
					switchScene("hero",{gid = DATA_Formation:getCur()})
				else
					local homeLayer = requires("Scene.home.homelayer")
					switchScene( "home" , nil , function() homeLayer:createAide() end ) 
				end
			end
		}):getLayer()
	result_sprite:addChild( goStrengthen)
	result_sprite.goStrengthen = goStrengthen
	
	--去打造
	if existMiss[ resultData.type .. "" ]  then
		local goForge = KNBtn:new( COMMONPATH , 
			{ "btn_bg_red.png" ,"btn_bg_red_pre.png"} , 275 , 120 , {
				isIsolated = true,
				priority = -150,
				front = LOST_PATH ..  ( DATA_User:get("lv") <= 8 and "go_forge2.png" or "go_forge.png" )  ,
				callback = 
				function()
					if DATA_User:get("lv") <= 8 then
						-- 判断等级开放
						local check_result = checkOpened("forge")
						if check_result ~= true then
							KNMsg:getInstance():flashShow(check_result)
							return
						end
						switchScene( "fb" , { coming = "general" }) --equip crash
					else
						KNMsg.getInstance():boxShow( "1: 如果游戏前期就遇到了困难，提升玩家等级是提升实力最快的方式\n2: 关卡里面的BOSS关卡可以提供大量经验值哦" , {})
					end
				end
			}):getLayer()
		result_sprite:addChild( goForge )
		result_sprite.goForge = goForge
	end
	
	
	transition.moveTo( lostTitle , { time = 0.3 , y = display.top-128 , easing  = "BACKOUT" })
	
	if resultData.type == "athletics" then
		-- local tempText = display.strokeLabel( "+" .. resultData.awards.fame , 45 + 113 , display.cy - baseHeight   , 18 , ccc3( 0xff , 0xfb , 0xd4 ) , nil , nil , { dimensions_width = 200 , dimensions_height = 70 , align = 0 } )
		-- result_sprite:addChild( tempText )
	end
		local tipStr = ( DATA_User:get("lv") ) and ( getConfig("LevelBriefIntroduction")[ math.random( 1 , DATA_User:get("lv") ) .. "" ] or {} ) or {}
	if tipStr.describe then
		tipStr = tipStr.describe
	else
		tipStr = "召唤高星级仙人，\n可提升战队的整体实力！"
	end
	if resultData.type == "rob" then
		tipStr = "糟糕，被对手发现，带着物品跑路啦！好可惜，差点就抢夺成功了TAT~"
	end
	local tempText = display.strokeLabel( tipStr , 130 , display.cy-50   , 18 , resultData.type == "rob" and ccc3( 0x0c , 0xfc , 0xff ) or ccc3( 0xff , 0xfb , 0xd4 ) , nil , nil , { dimensions_width = 200 , dimensions_height = 90 , align = 0 } )
	result_sprite:addChild( tempText )
end
--胜利基础
function _ResultLayer:baseWin( params )	
	params = params or {}
	
	local showGirl = params.showGirl
	local showBgFrame = params.showBgFrame
	
	result_sprite = display.newLayer()
	
	local lightBg = display.newSprite(WIN_PATH.."eff.png")
	local centerSp = display.newSprite( WIN_PATH .. "center_bg.png")
	centerSp:setPosition(display.cx,  display.cy)
	lightBg:setPosition(display.cx ,  display.cy+200)

	result_sprite:addChild(lightBg)
	result_sprite:addChild( centerSp )
	
	lightBg:runAction(CCRepeatForever:create(CCRotateBy:create(1,20)))
	
	local baseHeight = 16
	local winTitle = display.newSprite( ( resultData.type == "rob" and WIN_PATH .. "rob_win.png" or  TITLE_PATH ) ) 	--胜利标题
	bgFrame = display.newSprite( WIN_PATH .. "win_bg_frame.png")
	girl = display.newSprite( PATH .. "girl.png")

	
	setAnchPos( winTitle , display.cx , display.cy+300 , 0.5 , 0 )
	setAnchPos( bgFrame , 20 , display.cy - baseHeight , 0 , 0.5 )
	setAnchPos( girl , 188 , display.cy - 27  , 0 , 0.5 )
	
	
	result_sprite:addChild( winTitle )
	result_sprite:addChild( bgFrame )
	result_sprite:addChild( girl )
	
	
	girl:setVisible( showGirl )
	bgFrame:setVisible( showBgFrame )
	
	-- TODO 理清这个函数
	local function actionBackFun()
		if resultData.missions then
			local startNum = ( params.type and params.type == "mopUp" ) and resultData.missions[1].star or resultData.missions[resultData.current.mission_id].star
			local i = 0
			local function showStar()
				i = i + 1
				if i > startNum then
					if params.actionFun then  params.actionFun() end 
				else
					local adornStar = display.newSprite( COMMONPATH .. "star.png" )
					adornStar:setPosition(( display.cx - startNum * 18 + i * 37 ) - 18  , display.cy+240)
					result_sprite:addChild( adornStar )
					
					adornStar:setScale(5)
					transition.scaleTo( adornStar , { time = 0.3 , scale = 1 , onComplete = showStar ,  easing  = "BACKOUT"})
				end
			end
			showStar()
		else
			if params.actionFun then  params.actionFun() end 
		end
	end
	
	transition.moveTo( winTitle , {time = 0.5 , y = display.cy+230 , easing  = "BACKOUT" , onComplete = actionBackFun })
end

--扫荡结算！任务结算？？
function _ResultLayer:mission_result( params )
--	local json = requires( "Network/dkjson")
--	local response = io.readfile("c:\\battle8.txt")
--	response = json.decode( response )
--	resultData = response.result
--	
--	resultData.through = {award = "100000体力" , firsttime = 1 ,  brilliant = 1 }		--award 通关奖励	--firsttime 第一次通关	--brilliant 全三星通关
--	resultData._T_lvup = { lv = 2 ,maxlv = 4 }	--主公升级
--	resultData._T_hero_lvup = { ["121"] = "121" ,["94"] = "94" ,["217"] = "217" , ["158"] = "158" , ["156"] = "156" ,["146"] = "146" }	--各个英雄生机
--	
--	resultData._D_bag = resultData._D_bag or {}
--	resultData._D_bag.general = resultData._D_bag.general or {}
--	resultData._D_bag.general[ "121" ] = { cid = "1106" , lv = "2" , exp = 500 ,  cur_exp = 50 , id = 121 }
--	resultData._D_bag.general[ "94" ] = { cid = "1109" , lv = "2" , exp = 500 ,  cur_exp = 50  , id = 94 }
--	resultData._D_bag.general[ "217" ] = { cid = "1117" , lv = "2" , exp = 500 ,  cur_exp = 50 , id = 217 }
--	resultData._D_bag.general[ "158" ] = { cid = "1115" , lv = "2" , exp = 500 ,  cur_exp = 50 , id = 158 }
--	resultData._D_bag.general[ "156" ] = { cid = "1120" , lv = "2" , exp = 500 ,  cur_exp = 50 , id = 156 }
--	resultData._D_bag.general[ "146" ] = { cid = "1110" , lv = "2" , exp = 500 ,  cur_exp = 50 , id = 146 }
--	
--	resultData.awards = resultData.awards or {}						 	
-- 	resultData.awards.exp = resultData.awards.exp or 300
-- 	
-- 	
--	resultData.T_hero_expadd = {
--								["121"] = 20 , 
--								["94"] =  20 , 
--								["217"] = 20 , 
--								["158"] = 20 , 
--								["156"] = 60 , 
--								["146"] = 60 , 
--							 	}
	
	params = params or {}
	local layer = display.newLayer()
	local mask
	
	--战斗输赢
	local win = DATA_Battle:get("win")
	
	--显示确定按钮
	local function showConfirmBtn()
		confirmBtn:getLayer():setVisible(true)
		confirmBtn:setEnable(true)
	end
	--返回事件处理
	local function backFun()
		if award_handle ~= nil then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(award_handle)
			award_handle = nil
		end
		
		--通关展示
		local function passShow( params )
			params = params or {}
			local showLayer = display.newLayer()
			
			local roleImage = display.newSprite( COMMONPATH .. "guide_logo.png" )
			setAnchPos(roleImage , 24 , display.cy + 30 )
			showLayer:addChild( roleImage )
			
			local bg = display.newSprite(  PATH .. "pass_bg.png" )
			setAnchPos( bg , display.cx , display.cy  , 0.5 , 0.5 )
			showLayer:addChild( bg )
			
			local missionConfig = requires("Config.Mission")
			local curMapName = missionConfig[ resultData["current"]["map_id"] .. "" ].map_name
			
			local tipConfig = {
								["10"] = "少侠好身手！通过“" .. curMapName .. "”的考验！感觉您还没到极限，挑战自己的极限吧！会有你意想不到的收获哦！" , 
								["01"] = "少侠你战斗力简直爆表！通过“" .. curMapName .. "”的所有评级都为三星！称雄世界已然不远！" , 
								["11"] = "少侠你已经超神了！首次通过“" .. curMapName .. "”就以全三星的姿态过关！少侠一出谁与争锋啊！" , 
							}
			local tipStr = tipConfig[ resultData.through.firsttime .. resultData.through.brilliant .. "" ]
			local tipText = display.strokeLabel(tipStr , 144, 190 + 150 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , 2 , nil , {
							dimensions_width = 280,
							dimensions_height = 120,
							align = 0,})
			showLayer:addChild( tipText )
			
			--奖励图标
			local awardIcon = KNBtn:new( IMG_PATH .. "image/scene/fb/"  , {"item_bg.png"} , 50  , display.cy - 45 , { front =getImageByType( "16005" , "s")  }):getLayer()
			showLayer:addChild( awardIcon )
			
			--奖励名称
			local awardName = display.strokeLabel( resultData.through.award , 43, display.cy - 75 , 16 , ccc3( 0x2c , 0x00 , 0x00 ) , 2 , nil , {
			dimensions_width = 100,
			dimensions_height = 30,
			align = 1,})
			showLayer:addChild( awardName )
			
			--是否第一次通过
			local tempDelay = 0.4
			if resultData.through.firsttime == 1 then
				local firstFlag = display.newSprite(  PATH .. "pass_flag.png" )
				setAnchPos( firstFlag , 368 , display.cy - 80 , 0.5 , 0.5 )
				showLayer:addChild( firstFlag )
				
				firstFlag:setOpacity(0.3)
				firstFlag:setScale(3)
				transition.fadeIn(firstFlag , { delay = tempDelay ,time = 0})
				transition.scaleTo(firstFlag , { delay = tempDelay , scale = 1, time = 0.4 , easing = "ELASTICOUT" })
				
				tempDelay = 0.8
			end		
			--是否全三星通过		
			if resultData.through.brilliant == 1 then
				local starFlag = display.newSprite(  PATH .. "star_flag.png" )
				setAnchPos( starFlag , 240 , display.cy - 80 , 0.5 , 0.5 )
				showLayer:addChild( starFlag )
				
				
				starFlag:setOpacity(0.3)
				starFlag:setScale(3)
				transition.fadeIn(starFlag , { delay = tempDelay ,time = 0})
				transition.scaleTo(starFlag , { delay = tempDelay , scale = 1, time = 0.4 , easing = "ELASTICOUT" })
			end	
			
			local passMask
			local function clear()
				if showLayer then
					showLayer:removeSelf()
				end
				
				passMask:remove()
				
				if params.passBackFun then params.passBackFun() end
				
			end
			
			passMask = KNMask:new({ item = showLayer , opacity = 200 , click = clear })		
			local scene = display.getRunningScene()
			scene:addChild( passMask:getLayer() )		
		end
		
		local function jumpOutFun()
			if params.backFun then
				params.backFun()	--磨练的藏宝楼同任务结算相同，但返不同
			else
				print("-----------switchScene:", resultData.type.."")
				switchScene(resultData.type.."",{kind = "mission", level = resultData["jump"]["map_id"]})
			end
		end
	
		
		local function passBackFun()
			if getGuideInfo() then
				print("--------Upgrade:mainHero")
				Upgrade:mainHero( resultData , {backFun = jumpOutFun } )
			else
				print("-----jumpOutFun")
				jumpOutFun()
			end 
		end
		
				
		if confirmBtn then
			confirmBtn:getLayer():removeSelf()
			confirmBtn = nil
		end

		-- mask:remove()
		if resultData.through and resultData.through.firsttime then--存在通关奖励时
			print("--------存在通关奖励时")
			passShow( { passBackFun = passBackFun })
		else
			print("--------不存在通关奖励")
			passBackFun()
		end
		
	end

	if params.type == "mopUp" then
		win = 1
	end
	 
	-- 添加战斗背景图片
	if win == 1 then
		local awards = resultData.awards or {}
		
		local function careatTitle( name )
			local titleBg = display.newSprite( WIN_PATH .. "title_bg.png")
			local nameSp = display.newSprite( WIN_PATH .. name .. ".png" )
			titleBg:addChild( nameSp )
			setAnchPos( nameSp , 6 , 0 , 0 , 0 )
			return titleBg
		end
		local actionTime = 0.2
		local function showOther()
			local function showAarad()
				--获得奖励信息
				local function showAwards()
					if awards.drop then
						local cids = {}
						for key , cid in pairs(resultData.awards.drop) do
							cids[#cids + 1] = cid
						end
						if #cids ~= 0 then
							-- 播放获得卡牌的动画
							if params.type == "mopUp" then
								--local scroll = KNScrollView:new( 40 , 80 , 400, 185 , 50 , true , nil , { turnBtn = IMG_PATH .. "image/scene/gang/next.png" , turnBtnPriority = -140  } )
								local scroll = KNScrollView:new( 40 , display.cy-275 , 400, 185 , 50 , true , nil , { turnBtn = IMG_PATH .. "image/scene/gang/next.png" , turnBtnPriority = -140  } )
								local function mopUpAward( cid )
									local curName = getConfig( getCidType( cid ) ,  cid , "name" ) 
									local star = getConfig( getCidType( cid ) ,  cid , "star" )
									local otherData = {}
									for j = 1 , star do
										otherData[ #otherData + 1 ] = { IMG_PATH .. "image/scene/home/star.png" ,  7 - star * 12 + j * 24  , -27 }
									end
									local isPetskill =( getCidType(cid) == "petskill" )
									local textData = {}
									if isPetskill then
										otherData[ #otherData + 1 ] = {IMG_PATH.."image/scene/bag/kind_bg.png", 0 , 2 }
										textData = { {"兽",16, ccc3(255,255,255), ccp(-20, 23),nil, 17} , { curName , 18 , ccc3( 0xfe , 0xfc , 0xd3 ) , { x = 1 , y = -70	} , nil , 20 } }
									else
										textData = { curName , 18 , ccc3( 0xfe , 0xfc , 0xd3 ) , { x = 1 , y = -70	} , nil , 20 }
									end
									local awardBtn = KNBtn:new( SCENECOMMON , { "skill_frame1.png" } , 0 , 0 ,
											{
												front = getImageByType( cid ) ,
												other = otherData , 
												text = textData ,
												callback = function()end,
											}):getLayer()
											
									return awardBtn
								end
								
								for k = 1 , #cids do
									local item = mopUpAward( cids[k] )
									scroll:addChild( item )
								end
								scroll:alignCenter()
								result_sprite:addChild(scroll:getLayer() )
				
								
								showConfirmBtn()
							else
								_ResultLayer:playGetCards(cids , showConfirmBtn )
							end
						else
							showConfirmBtn()
						end
					else
						showConfirmBtn()
					end
				end
			
				--获得道具奖励标题
				local getPropSp = careatTitle( "get_prop" )
				setAnchPos( getPropSp , -50 , display.cy-130 , 0 , 0.5 )
				result_sprite:addChild( getPropSp )
				transition.moveTo( getPropSp , { time = actionTime , x = 80 } )
				
				-- 触发定时器
				award_handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
					CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(award_handle)
					award_handle = nil
					showAwards()
				end , 0.2 , false)
			end
			
			--展示英雄信息
			local function showHero()
				local upStr = nil
				--英雄信息
				--if resultData._D_bag and resultData._D_bag.general then
					local onHero = DATA_Formation:get_ON( "on" )
					local backHero = DATA_Formation:get_ON( "back" )
					local allHero = {}	--所有上阵武将id
					--for i = 1 , table.nums(onHero) do
					--	allHero[ #allHero + 1 ] = onHero[i].gid
					for k,v in pairs(onHero) do
						allHero[ #allHero + 1 ] = onHero[k].gid
					end
					--for i = 1 , table.nums(backHero) do
					--	 allHero[ #allHero + 1 ] = backHero[i].gid
					for k,v in pairs(backHero) do
						allHero[ #allHero + 1 ] = backHero[k].gid
					end
					
					local addExp = {}	--英雄添加经验文字
					local showUpFlag = {}
				
					for i = 1 , 8 do
						local index = i - 1
						local addX , addY = 84 + index % 4 * 83 , display.cy+ 10 - math.floor( index / 4 ) * 80 
						if i > #allHero then
							local heroBtn = KNBtn:new( SCENECOMMON , { "skill_frame4.png" } , addX , addY ):getLayer()
							result_sprite:addChild( heroBtn )
						else
							--local curData = resultData._D_bag.general[ allHero[i] .. "" ]
							local curData = DATA_Bag:get("general" , allHero[i] )
							if curData then
								local isUp =  resultData._T_hero_lvup and resultData._T_hero_lvup[ allHero[i].."" ] --当前英雄是否升级
								-- if not upStr then
								-- 	upStr = HeroLayer:coutUpTip( curData.id , true )
								-- end
								local heroBtn = KNBtn:new( SCENECOMMON , { "skill_frame1.png" } , 
									addX , 
									addY ,
									{
										front = getImageByType( curData.cid ) ,
										other = isUp and ( { { COMMONPATH .. "select2.png" , -11 , -10 , -10 } , { COMMONPATH .. "egg_num_bg.png" , 42 , 42 } } ) or { COMMONPATH .. "egg_num_bg.png" , 42 , 42 } , 
										text = { curData.lv..'' , 18 , ccc3( 0xff , 0xff , 0xff ) , { x = 21 , y = 20	} , nil , 20 } ,
									})
								result_sprite:addChild( heroBtn:getLayer() )
								
								--升级提示
								showUpFlag[ i .. "" ] = function()
									if isUp then
										local upFlag = display.newSprite( WIN_PATH .. "up_flag.png" )
										setAnchPos( upFlag , heroBtn:getX() + 34 ,heroBtn:getY()  , 0.5 )
										result_sprite:addChild( upFlag )
										
										local function loopPlayUp()
											setAnchPos( upFlag , heroBtn:getX() + 34 , heroBtn:getY() , 0.5 )
											-- local upAction = CCArray:create()
											-- upAction:addObject( CCMoveTo:create( 1 , ccp( heroBtn:getX() + 34 , heroBtn:getY() + 40 ) ) )
											-- upAction:addObject( CCCallFunc:create( loopPlayUp ) )
											-- upFlag:runAction( CCSequence:create( upAction ) )


											local seq = transition.sequence({
												CCMoveTo:create( 1 , ccp( heroBtn:getX() + 34 , heroBtn:getY() + 40 ) ),
												CCCallFunc:create( loopPlayUp ),
											})		
											upFlag:runAction(seq)								
										end
										loopPlayUp()
									end
								end
					
								addExp[ i .. "" ] = display.strokeLabel( "+" .. resultData.awards.exp , heroBtn:getX() - 3 , heroBtn:getY() , 18 , ccc3( 0x53 , 0xff , 0x15 ) , nil , nil , {
									dimensions_width = 70 ,
									dimensions_height = 20,
									align = 1
								})
								-- 上阵英雄经验条
								local expBar = KNBar:new("exp1" , addX , display.cy + 10 - math.floor( index / 4 ) * 90 , { 
																			maxValue = curData.exp , 
																			curValue = 0 , 
																			color = ccc3(0x2c , 0x00 , 0x01 ) ,
																			actionTime = 0.5 , 
																			} )
								expBar:setIsShowText( false )
								expBar:setCurValue( curData.cur_exp , true )
								result_sprite:addChild( expBar )
							end
						end
					end
						local shadeMask = display.newSprite( PATH .. "exp_shade.png")
						setAnchPos( shadeMask , display.cx , display.cy - 33 , 0.5 , 0.5)
						result_sprite:addChild( shadeMask )
						
						local function clearShadeMask()
							shadeMask:removeSelf()
							
							local tipsText = display.strokeLabel( "每个英雄获得经验：+" .. resultData.awards.exp  , 0 ,display.cy-112 , 18 , ccc3( 0x07 , 0xff , 0xfc ) , nil , nil , {
									dimensions_width = 480 ,
									dimensions_height = 30,
									align = 1
								})
							result_sprite:addChild( tipsText )
							
							for key , v in pairs(showUpFlag) do
								v()
							end
							
							showAarad()
							
						end
						local tempCount = 0
						for key , v in pairs(addExp) do
							tempCount = tempCount + 1
							local isLast = tempCount >= table.nums( addExp )
							result_sprite:addChild( v )
							
							transition.moveTo( v , { time = 1.5 , y = 40 , onComplete = 
																		function()
																			v:removeSelf()
																			if isLast then
																				clearShadeMask()
																			end
																		end	})
						
						end
				--end
				-- if upStr then
				-- 	result_sprite:addChild(  display.strokeLabel( upStr , 40 , 10 , 18 , ccc3( 0x5c , 0xe3 , 0xe5 ) , nil , nil , {
				-- 					dimensions_width = 400,
				-- 					dimensions_height = 50,
				-- 					align = 0
				-- 				}) )
				-- end
			end
			
			--生成主公信息
			local function showMan()
				local function createMan()
					local manLayer = display.newLayer()
					
					local expBar = KNBar:new("exp" , 135 , display.cy+155 , { 
																	maxValue = DATA_User:get("lvup_exp") , 
																	curValue = 0 , 
																	color = ccc3(0x2c , 0x00 , 0x01 ) ,
																	actionTime = 0.5 , 
																	} )
					expBar:setCurValue( DATA_User:get("cur_exp") , true )
					manLayer:addChild( expBar )
					
					local isManUp = resultData._T_lvup and tonumber( resultData._T_lvup.lv ) ~= 0	--主公是否升级
					local tempTable 
					if isManUp  then
						tempTable = {  
										{ IMG_PATH.."image/scene/common/navigation/level_bg.png" , -17 , 54 } ,
										{ COMMONPATH .. "select2.png" , -11 , -10 , -10 } , 
									}
					else
						tempTable = { IMG_PATH.."image/scene/common/navigation/level_bg.png" , -17 , 54 }
					end
					
					local manImage = KNBtn:new( COMMONPATH , { "sex".. DATA_User:get("sex") .. ".png" } , 
						display.cx - 160 , 
						display.cy + 125 ,
						{
							front = COMMONPATH.."role_frame.png" ,
							other = tempTable , 
							text = { DATA_User:get("lv") , 18 , ccc3( 0xbf , 0x3a , 0x01 ) , { x = -33 , y = 34	} , nil , 20 } ,
							callback = function()
							end
						}):getLayer()
					manLayer:addChild( manImage )
					
					--主公升级动画
					if isManUp then
						local upFlag = display.newSprite( WIN_PATH .. "up_flag.png" )
						setAnchPos( upFlag , 34 , 0 , 0.5    )
						manImage:addChild( upFlag , 10)
						
						local function loopPlayUp()
							setAnchPos( upFlag , 34 , 0 , 0.5 )
							-- local upAction = CCArray:create()
							-- upAction:addObject( CCMoveTo:create( 1 , ccp( 30 , 40) ) )
							-- upAction:addObject( CCCallFunc:create( loopPlayUp ) )
							-- upFlag:runAction( CCSequence:create( upAction ) )

							local seq = transition.sequence({
								CCMoveTo:create( 1 , ccp( 30 , 40) ),
								CCCallFunc:create( loopPlayUp )
							})		
							upFlag:runAction(seq)				
						end
						loopPlayUp()
					end
					
					local manName= display.strokeLabel( DATA_User:get("name") , 167 , display.cy+166 , 20 , ccc3( 0xff , 0xfb , 0xd5 ) )
					manLayer:addChild( manName )
					
					return manLayer
				end
				
				result_sprite:addChild( createMan() )
			
				if awards.silver then
					--获得银两
					local silverSp = display.newSprite( COMMONPATH .. "silver.png" )
					setAnchPos( silverSp , 266 , display.cy+115 , 0 , 0 )
					result_sprite:addChild( silverSp )
					
					local silverNum = display.strokeLabel( "+" .. awards.silver , 345 , display.cy+120 , 18 , ccc3( 0x53 , 0xff , 0x15 ) , nil , nil , {
						dimensions_width = 100 ,
						dimensions_height = 20 ,
						align = 0
						} )
					result_sprite:addChild( silverNum )
					
					local prestigeNum = display.strokeLabel( "EXP:+" .. ( awards.prestige or 0 ), 200 , display.cy+120 , 18 , ccc3( 0x53 , 0xff , 0x15 ) , nil , nil , {
						dimensions_width = 100 ,
						dimensions_height = 20 ,
						align = 0
						} )
					result_sprite:addChild( prestigeNum )
				end
				
				--展示英雄数据
				local heroSp = careatTitle( "hero" )
				setAnchPos( heroSp , -50 , display.cy+85 , 0 , 0 )
				result_sprite:addChild( heroSp )
				transition.moveTo( heroSp , { delay = 0.2 , time = actionTime , x = 80 , onComplete = showHero } )
			end
			
			--主公标题
			local manSp = careatTitle( "man" )
			setAnchPos( manSp , -50 , display.cy+200 , 0 , 0 )
			result_sprite:addChild( manSp )
			transition.moveTo( manSp , { time = actionTime , x = 80 , onComplete =  showMan } )
		end
		
		--背景图
		_ResultLayer:baseWin({ type = params.type == "mopUp" and "mopUp" or nil , actionFun =  showOther , showGirl = false , showBgFrame = false })
		
		-- 确定按钮
		confirmBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" ,"btn_bg_red_pre.png"}, display.cx - 146 / 2 , display.cy - 330 , 
							{ 
								priority = -132 , 
								scale = true , 
								front = COMMONPATH.."confirm.png" , 
								callback = 
								function()
									backFun() 
								end 
							} )
		confirmBtn:getLayer():setVisible(false)
		confirmBtn:setEnable(false)
		result_sprite:addChild( confirmBtn:getLayer() )
		
		mask = KNMask:new( { item = result_sprite } )
	else
		-- 失败  点击返回首页
		print("------失败  点击返回首页")
		mask = KNMask:new( { item = result_sprite } )
		-- mask = KNMask:new()
		if params.backFun then
			mask:click( params.backFun )	--磨练的藏宝楼同任务结算相同，但返不同
		else
			-- mask:click(backFun)
		end
	end
	-- mask:getLayer():add(result_sprite)
	layer:addChild( mask:getLayer() )
	
	return layer
end

--副本结算
function _ResultLayer:instance_result()
	local fbLayer = requires("Scene.fb.fblayer")
	local fbMode = {
				 inshero  = "hero" ,		
				 inspet   = "pet", 	
				 insskill = "skill" , 	
				 insequip = "equip" ,
				 rob      = "rob" ,
				}
	local layer = display.newLayer()
	
	local mask
	
	--战斗输赢
	local win = DATA_Battle:get("win")
	if resultData.success == 0 then win = 0 end

	--返回事件处理
	local function backFun()
		if award_handle ~= nil then
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(award_handle)
			award_handle = nil
		end
		
		local function jumpOutFun()
			if resultData.type == "rob" then
					-- HTTP:call("rob", "get", {},
					-- 			{ 
					-- 				success_callback =
					-- 				 function(data)
						switchScene( "pvp" , { state = fbMode[resultData.type] , robData = data , star = resultData.star } )
								-- 	end
								-- })
			else
				local func, dig
				if resultData.type == "insequip" then
					if win == 1 then
						if resultData.get.ins_id == 25 and resultData.get.current_map == DATA_Instance:get("equip", "max", "map_id") then 
							dig = true
							if resultData.get.current_map == 6 then
								KNMsg.getInstance():flashShow("恭喜您已通关藏宝楼所有副本！~")
							else
								func  = function()
									KNMsg.getInstance():flashShow("恭喜通关藏宝楼第"..resultData.get.current_map.."层，第"..(resultData.get.current_map + 1).."层已开启")
								end
							end
						elseif resultData.get["ins_id"] == 25 then
							dig = true
						end
					end
				end	
				switchScene( "fb" , { state = fbMode[resultData.type], coming = true, map =  resultData.get.current_map, dig = dig } , func)
			end
		end
	
		
		if resultData._T_hero_lvup or ( resultData._T_lvup and tonumber( resultData._T_lvup.lv ) ~= 0 ) or  getGuideInfo() then
			if resultData.type ~= "insequip" then mask:remove() end
			Upgrade:mainHero( resultData , { backFun = jumpOutFun} )
		else
			if resultData.type ~= "insequip" and resultData.type ~= "rob" then 
				mask:remove() 
			end
			jumpOutFun()
		end 
	end
	
	--藏宝楼结算 同任务
	if resultData.type == "insequip" then
		return _ResultLayer:mission_result( { backFun = backFun } )
	end
	
	local baseY = 256
	local baseX = 42
	-- 添加战斗背景图片
	if win == 1 and ( resultData.type == "insskill" or resultData.type == "rob" )then
		addType = 1
		--不存在英雄锦囊
		--背景图
		_ResultLayer:baseWin({ actionFun = showOther , showGirl = true , showBgFrame = true })
		
		
		--奖励功勋
		local getSkillTitle = display.newSprite( WIN_PATH .. ( resultData.type ~= "rob" and "get_skill.png" or "get_text.png" ) )
		setAnchPos( getSkillTitle , 70 , 475 , 0 , 0.5 )
		result_sprite:addChild( getSkillTitle )

		--确定按钮
		confirmBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png","btn_bg_red_pre.png" }, display.cx - 146 / 2 , 50 , { priority = -132 , scale = true , front = COMMONPATH.."confirm.png" , callback = backFun } )
		layer:addChild( confirmBtn:getLayer() , 2 )

		confirmBtn:getLayer():setVisible(false)
		confirmBtn:setEnable(false)


		local awards = resultData.awards or {}
		--获得奖励信息
		local function showAwards()
			--显示确定按钮
			local function showConfirmBtn()
				confirmBtn:getLayer():setVisible(true)
				confirmBtn:setEnable(true)
			end
			if awards.drop and table.nums(awards.drop) ~= 0 then
				local function mopUpAward( cid , num )
					local robElement = {
										_11 = { name = "一星将魂" , star = 1 , path = IMG_PATH .. "image/scene/forge/general_icon_1.png" } ,
										_12 = { name = "二星将魂" , star = 2 , path = IMG_PATH .. "image/scene/forge/general_icon_2.png" } ,
										_1 = { name = "一星碎片" , star = 1 , path = IMG_PATH .. "image/scene/forge/equip_icon_1.png" } ,
										_2 = { name = "二星碎片" , star = 2 , path = IMG_PATH .. "image/scene/forge/equip_icon_2.png" } ,}
					
					local num = num or 0
					local curName , star
					local otherData , textData
					if robElement[ "_"..cid ] then
						curName = robElement[ "_"..cid ].name
						star = robElement[ "_"..cid ].star
						
						otherData = {}
						for j = 1 , star do
							otherData[ #otherData + 1 ] = { IMG_PATH .. "image/scene/home/star.png" ,  7 - star * 12 + j * 24  , -27 }
						end
						textData = { curName , 18 , ccc3( 0xfe , 0xfc , 0xd3 ) , { x = 1 , y = -70	} , nil , 20 }
					else
						print("result layer cid ",cid)
						curName = getConfig( getCidType( cid ) ,  cid , "name" ) 
						star = getConfig( getCidType( cid ) ,  cid , "star" )
					
						otherData = {}
						for j = 1 , star do
							otherData[ #otherData + 1 ] = { IMG_PATH .. "image/scene/home/star.png" ,  7 - star * 12 + j * 24  , -27 }
						end
						if num > 1 then
							otherData[ star+1 ] = { COMMONPATH .. "egg_num_bg.png" , 53 , 54 } 
						end
						
						local isPetskill =( getCidType(cid) == "petskill" )
						textData = {}
						if isPetskill then
							otherData[ #otherData + 1 ] = {IMG_PATH.."image/scene/bag/kind_bg.png", 0 , 2 }
							textData = { {"兽",16, ccc3(255,255,255), ccp(-20, 23),nil, 17} , { curName , 18 , ccc3( 0xfe , 0xfc , 0xd3 ) , { x = 1 , y = -70	} , nil , 20 } , ( (num > 1) and { num , 18 , ccc3( 0xff , 0xff , 0xff ) , { x = 32 , y = 32} , nil , 20 } or nil ) }
						else
							textData = ( num > 1) and {{ curName , 18 , ccc3( 0xfe , 0xfc , 0xd3 ) , { x = 1 , y = -70	} , nil , 20 } , { num , 18 , ccc3( 0xff , 0xff , 0xff ) , { x = 32 , y = 32} , nil , 20 } } or { curName , 18 , ccc3( 0xfe , 0xfc , 0xd3 ) , { x = 1 , y = -70	} , nil , 20 }
						end
					end
					
					local awardBtn = KNBtn:new( SCENECOMMON , { "skill_frame1.png" } , 0 , 0 ,
							{
								front =  ( robElement[ "_"..cid ] and robElement[ "_"..cid ].path or getImageByType( cid ) ) ,
								other = otherData , 
								text = textData ,
								callback = function()

								end,
							}):getLayer()
					return awardBtn
				end
				for key , v in pairs(awards.drop) do
					--local tempAward = resultData.type == "rob" and mopUpAward( v , 0 )  or mopUpAward( key , v ) 
					local tempAward = mopUpAward(v.cid,v.num)
					setAnchPos(tempAward , 110 , 378 )
					result_sprite:addChild( tempAward )
				end
				-- local tempAward = mopUpAward( awards.drop.cid , awards.drop.num )
				-- setAnchPos(tempAward , 110 , 378 )
				-- result_sprite:addChild( tempAward )

				showConfirmBtn()
			else
				showConfirmBtn()
			end
		end

		-- 触发定时器
		award_handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(award_handle)
			award_handle = nil
			showAwards()
		end , 0.2 , false)
	else
		-- 失败  点击返回首页
		mask = KNMask:new({ item = result_sprite })
		mask:click(backFun)
		layer:addChild( mask:getLayer() )
		return layer
	end
	

	
	mask = KNMask:new({ item = result_sprite })
	layer:addChild( mask:getLayer() )
	

	return layer
end


--竞技结算页面
function _ResultLayer:athletics_result()
	local layer = display.newLayer()
	local mask

	--战斗输赢
	local win = DATA_Battle:get("win")
	
	--返回事件处理
	local function backFun()
		if resultData._T_lvup and tonumber( resultData._T_lvup.lv ) ~= 0 then
			mask:remove()
			Upgrade:new( resultData._T_lvup , { backFun = function() switchScene( "athletics" , {data = resultData["data"], challenge = true } ) end } )
		else
			switchScene("athletics",{data = resultData["data"], challenge = true})
		end 
	end
	
	if win == 1 then
		_ResultLayer:baseWin({ showGirl = true , showBgFrame = true })
		
		--奖励功勋
		local athleticsAwardText = display.newSprite( WIN_PATH .. "get_silver.png" )
		setAnchPos( athleticsAwardText , 70 , display.cy , 0 , 0.5 )
		result_sprite:addChild( athleticsAwardText )
		--奖励功勋数量
		local fameT = display.strokeLabel("+" .. ( resultData.awards.silver or 0 )   , 0, 0 , 20 , ccc3( 0xff , 0xfb , 0xd5 ) , nil , nil , { dimensions_width = 85 , dimensions_height = 30 , align = 0 } )
		setAnchPos( fameT , 227 , display.cy-3 , 0.5 , 0.5 )
		result_sprite:addChild( fameT )
		
		--连胜次数
		local  winning_streak_text = display.newSprite( WIN_PATH .. "winning_streak.png" )
		setAnchPos( winning_streak_text , 70 , display.cy-70 , 0 , 0.5 )
		result_sprite:addChild( winning_streak_text )
		
		if resultData.my ~=nil then
			--连胜次数
			local winning_streak_num = display.strokeLabel( ( resultData.my.successionwin or 0 ) , 0, 0 , 20 , ccc3( 0xff , 0xfb , 0xd5 ) , nil , nil , { dimensions_width = 85 , dimensions_height = 30 , align = 0 } )
			setAnchPos( winning_streak_num , 180 , display.cy-75 , 0.5 , 0.5 )
			result_sprite:addChild( winning_streak_num )
		end
	
		
		mask = KNMask:new({ item = result_sprite })
		layer:addChild( mask:getLayer() )
		
		--确定按钮
		confirmBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" ,"btn_bg_red_pre.png"}, display.cx - 146 / 2 , 50  , { priority = -132 , scale = true , front = COMMONPATH.."confirm.png" , callback = backFun } )
		layer:addChild( confirmBtn:getLayer() , 2 )
	else
		mask = KNMask:new({ item = result_sprite })
		layer:addChild( mask:getLayer() )
		-- 失败  点击返回首页
		mask:click(backFun)
	end

	return layer
end
--好友切磋复仇结算页面
function _ResultLayer:friends_result()
	local layer = display.newLayer()
	local mask

	--战斗输赢
	local win = DATA_Battle:get("win")
	--返回事件处理
	local function backFun()
		switchScene( "friend" , { activity = resultData["data"] } )
	end
	
	if win == 1 then
		_ResultLayer:baseWin({ showGirl = true , showBgFrame = true })
		
		if resultData.action == 1 then			--切磋
			if resultData.awards and resultData.awards.silver then		--有奖励则提示
				local athleticsAwardText = display.newSprite( WIN_PATH .. "get_silver.png" )
				setAnchPos( athleticsAwardText , 70 , 445 , 0 , 0.5 )
				result_sprite:addChild( athleticsAwardText )
				--奖励功勋数量
				local fameT = display.strokeLabel("+" .. ( resultData.awards.silver or 0 )   , 0, 0 , 20 , ccc3( 0xff , 0xfb , 0xd5 ) , nil , nil , { dimensions_width = 85 , dimensions_height = 30 , align = 0 } )
				setAnchPos( fameT , 227 , 442 , 0.5 , 0.5 )
				result_sprite:addChild( fameT )
			else
				result_sprite:addChild( display.newSprite( WIN_PATH .. "compare_notes.png" ,  52 , 390):align(display.BOTTOM_LEFT) )
			end
		elseif resultData.action == 2 then		--复仇
			result_sprite:addChild( display.newSprite( WIN_PATH .. "revenge.png" ,  52 , 390):align(display.BOTTOM_LEFT) )
		end

		mask = KNMask:new({ item = result_sprite })
		layer:addChild( mask:getLayer() )
		
		--确定按钮
		confirmBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" ,"btn_bg_red_pre.png"}, display.cx - 146 / 2 , 50  , { priority = -132 , scale = true , front = COMMONPATH.."confirm.png" , callback = backFun } )
		layer:addChild( confirmBtn:getLayer() , 2 )
	else
		mask = KNMask:new({ item = result_sprite })
		layer:addChild( mask:getLayer() )
		-- 失败  点击返回首页
		mask:click(backFun)
	end

	return layer
end
function _ResultLayer:boss_result(  )
	local layer = display.newLayer()
	local mask
	
	--战斗输赢
	local win = DATA_Battle:get("win")
	if resultData.success == 0 then win = 0 end
	testlog("_ResultLayer boss win=",win)--win = nil
	local backFun = function ( ... )
		--mask:remove()
		HTTP:call("get_boss", { type = "boss" } , {success_callback = 
		function(data)
			dump(data)
			switchScene("pvp",{state = "bossbattle",data=data})
		end})
	end
	local back 
	if win == 1 then
		--实际上不会执行这里
		_ResultLayer:baseWin({ showGirl = true , showBgFrame = true })
		
		result_sprite:addChild( display.newSprite( WIN_PATH .. "mine.png" ,  52 , 390):align(display.BOTTOM_LEFT) )

		mask = KNMask:new({ item = result_sprite })
		layer:addChild( mask:getLayer() )
		
		--确定按钮
		confirmBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" ,"btn_bg_red_pre.png"}, display.cx - 146 / 2 , 50  , { priority = -132 , scale = true , front = COMMONPATH.."confirm.png" , callback = backFun } )
		layer:addChild( confirmBtn:getLayer() , 2 )
	else
		mask = KNMask:new({ item = result_sprite })
		layer:addChild( mask:getLayer() )
		-- 失败  点击返回首页
		mask:click(backFun)
	end
	
	result_sprite:removeChild(result_sprite.goForge)
	result_sprite:removeChild(result_sprite.goStrengthen)
	local goBack = KNBtn:new( COMMONPATH , 
		{ "btn_bg_red.png" ,"btn_bg_red_pre.png"} , 175 , 120 , {
			isIsolated = true,
			priority = -150,
			front = LOST_PATH .."go_forge.png",
			callback = 
			function()
				backFun()
			end
		}):getLayer()
	result_sprite:addChild(goBack)

	return layer
end
--矿山结算页面
function _ResultLayer:mining_result()
	local layer = display.newLayer()
	local mask

	--战斗输赢
	local win = DATA_Battle:get("win")
	if resultData.success == 0 then win = 0 end
	
	--返回事件处理
	local function backFun()
		HTTP:call(61006,{type=0},{success_callback =function ( data )
			--switchScene( "diggings" ,resultData.data)
			switchScene( "diggings" ,data)
		end})
		
	end
	
	if win == 1 then
		_ResultLayer:baseWin({ showGirl = true , showBgFrame = true })
		
		result_sprite:addChild( display.newSprite( WIN_PATH .. "mine.png" ,  52 , 390):align(display.BOTTOM_LEFT) )

		mask = KNMask:new({ item = result_sprite })
		layer:addChild( mask:getLayer() )
		
		--确定按钮
		confirmBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" ,"btn_bg_red_pre.png"}, display.cx - 146 / 2 , 50  , { priority = -132 , scale = true , front = COMMONPATH.."confirm.png" , callback = backFun } )
		layer:addChild( confirmBtn:getLayer() , 2 )
	else
		mask = KNMask:new({ item = result_sprite })
		layer:addChild( mask:getLayer() )
		-- 失败  点击返回首页
		mask:click(backFun)
	end

	return layer
end



--演示战斗
function _ResultLayer:guide_result()

	local layer = display.newLayer()
	local mask
	
	local function backFun()
		local logic = requires("Scene.battle.logicLayer")
		if logic:getParams().resultCallFun then
			logic:getParams().resultCallFun()
		end
	end
	
	--战斗失败
	_ResultLayer:warLost()
	
	mask = KNMask:new({ item = result_sprite })
	layer:addChild( mask:getLayer() )
	
	mask:click(backFun)

	return layer
end


--[[播放卡牌获得动画]]
function _ResultLayer:playGetCards(cids , backFun , params )
	params = params or {}
	local scene = display.getRunningScene()

	card_mask = KNMask:new({priority = -133})
	scene:addChild(card_mask:getLayer() , 100)

	local callback
	callback = function(index)
		local next_index = index + 1
		if cids[next_index] then
			_ResultLayer:playOneCard(cids[next_index] , next_index , callback , params )
		else
			scene:removeChild(card_mask:getLayer() , true)

			if backFun then
				backFun()
			end
		end
	end
	
	
	_ResultLayer:playOneCard(cids[1] , 1 , callback , params )
end

function _ResultLayer:playOneCard(cid , index , callback , params )
	local curLayer = params.parentLayer or result_sprite
	local tempData = DATA_Result:get().awards or {}
	if tempData.dropkit then
		_ResultLayer:popOneCard( cid , index , callback)
	else
		local baseX = 42
		local baseY = 256
	
		local card_x = baseX + 49 + (index - 1) * 74
		local card_y = baseY - 153
		
		
		
		local curName = getConfig( getCidType( cid ) ,  cid , "name" ) 
		local star = getConfig( getCidType( cid ) ,  cid , "star" )
		local otherData = {}
		for j = 1 , star do
			otherData[ #otherData + 1 ] = { IMG_PATH .. "image/scene/home/star.png" ,  7 - star * 12 + j * 24  , -27 }
		end
		local isPetskill =( getCidType(cid) == "petskill" )
		local textData = {}
		if isPetskill then
			otherData[ #otherData + 1 ] = {IMG_PATH.."image/scene/bag/kind_bg.png", 0 , 2 }
			textData = { {"兽",16, ccc3(255,255,255), ccp(-20, 23),nil, 17} , { curName , 18 , ccc3( 0xfe , 0xfc , 0xd3 ) , { x = 1 , y = -70	} , nil , 20 } }
		else
			textData = { curName , 18 , ccc3( 0xfe , 0xfc , 0xd3 ) , { x = 1 , y = -70	} , nil , 20 }
		end
		
		local addX
		local addY
		if addType == 0 	then addX = 80 + ( index - 1 ) * 130 addY = display.cy-217
		elseif addType == 1	then addX = 100 addY = 373
		elseif addType == 2	then addX = 70 + ( index - 1 ) % 4 * 90 addY = 245 - math.floor(( index - 1 )/4)*85
		end
		
		
		local awardBtn = KNBtn:new( SCENECOMMON , { "skill_frame1.png" } , addX , addY ,
				{
					front = getImageByType( cid ) ,
					other = addType ~= 2 and otherData or nil , 
					text = addType ~= 2 and textData or nil ,
					callback = function()end,
				}):getLayer()

		curLayer:addChild( awardBtn )
		
		
		local handle
		handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(handle)
			handle = nil
	
			_ResultLayer:popOneCard(cid , index , callback)
		end , 0.22 , false)
	end
end

function _ResultLayer:popOneCard(cid , index , callback)
	local baseX = 42
	local baseY = 189
	local card_x = ( addType == 0 and 80 or 100) + ( index - 1 ) * 130 
	local card_y = ( addType == 0 and 189 or 373)

	local top_tips = nil
	local tempData = DATA_Result:get().awards
	if tempData.dropkit then
		top_tips = display.newLayer()
		
		
		local roleImageBg = display.newSprite( IMG_PATH .. "image/scene/common/box.png")
		setAnchPos(roleImageBg , 50 , -14 )
		
		local roleImage = display.newSprite( getImageByType(logic:getfoeAgent() , "s"))
		setAnchPos(roleImage , 53 , -5 )
		
		--击败文字
		local talkText = display.newSprite( PATH .. "talk_text.png" )
		setAnchPos(talkText , 127, -15)
		
		--威望
		local prestigeText = display.newSprite( IMG_PATH .. "image/scene/battle/result/prestige.png" )
		setAnchPos(prestigeText , 53 , -400)
		
		--经验
		local expText = display.newSprite( IMG_PATH .. "image/scene/battle/result/exp.png" )
		setAnchPos(expText , 186 , -400)
		
		--银两
		local silverText = display.newSprite( IMG_PATH .. "image/scene/battle/result/silver.png" )
		setAnchPos(silverText , 53 , -430)
		
		
		
		local expT = display.strokeLabel("+" .. ( tempData.exp or 0 ) , 236, -410 , 16 , ccc3( 0xff , 0xff , 0xff ) , nil , nil , { dimensions_width = 85 , dimensions_height = 30 , align = 0 } )
		local prestigeT = display.strokeLabel( "+" .. ( tempData.prestige or 0 ) , 106 , -410 , 16 , ccc3( 0xff , 0xff , 0xff ) , nil , nil , { dimensions_width = 85 , dimensions_height = 30 , align = 0 } )
		local silverT = display.strokeLabel(  "+" ..  ( tempData.silver or 0 ) , 106, -430 , 16 , ccc3( 0xff , 0xff , 0xff ) , nil , nil , { dimensions_width = 85 , dimensions_height = 20 , align = 0 } )
		
		top_tips:addChild(expT)
		top_tips:addChild(prestigeT)
		top_tips:addChild(silverT)
		
--		local cid_type = getCidType(cid)
--		local config = getConfig(cid_type , cid)
--		local top_tips_label = CCLabelTTF:create(config["name"] , FONT , 30)
--		top_tips_label:setColor( ccc3( 0xff , 0xfb , 0xd4 ) )
--		setAnchPos(top_tips_label , 190 , 0 , 0.5)
--	
--		function top_tips:setOpacity(opacity)
--			top_tips_sprite:setOpacity(opacity)
--			top_tips_label:setOpacity(opacity)
--		end
--		top_tips:addChild(top_tips_label)
		setAnchPos(top_tips , 60 , 720)
		
		top_tips:addChild(roleImageBg)
		top_tips:addChild(roleImage)
		
		top_tips:addChild(talkText)
		top_tips:addChild(expText)
		
		top_tips:addChild(prestigeText)
		top_tips:addChild(silverText)
	end


	local card_popup = KNCardpopup:new(cid , function()
		callback(index)
	end , {
		init_x = card_x - 196,
		init_y = card_y - 211,
		end_x = card_x - 196,
		end_y = card_y - 211 - 230 ,
		top_tips = top_tips,
	})


	card_mask:getLayer():addChild( card_popup:play() )
end

return _ResultLayer
