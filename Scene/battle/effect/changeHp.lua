--[[

	血量改变  数字变化  (向上漂的数字)

]]


local M = {}

local logic = requires("Scene.battle.logicLayer")

--[[执行特效]]
function M:run( hero , num , param )
	if type(param) ~= "table" then param = {} end
	
	if tonumber( num ) == 0 then
		if param.onComplete then param.onComplete() end
		return
	end
	
	local group , group_width
	if param.isCrit then
		i7PlaySound(IMG_PATH .. "sound/crit.mp3")
		--暴击掉血
		group , group_width = getImageNum( math.abs( num ) , COMMONPATH .. "cirt.png" , { offset = -10 } )
	else
		--掉血或者加血
		group , group_width = getImageNum( math.abs( num ) , num>0 and COMMONPATH .. "hp_green.png" or  COMMONPATH .. "hp.png" )
	end
	
	
	setAnchPos( group , hero._cx  , hero._cy , 0.5 )


	--[[特效开始]]
	group:setScale(0.3)
	transition.scaleTo(group, {
		time = 0.1 * logic.speed,
		scale = 2.5,
	})
	transition.scaleTo(group, {
		delay = 0.2 * logic.speed,
		time = 0.2 * logic.speed,
		scale = 1,
	})
	param.delay = param.delay or 0.6
	transition.moveTo(group, { delay = param.delay , time = 0.4, x = hero._cx , y = hero._cy + 100  })
	
	transition.fadeOut(group, {
		delay = 0.7 * logic.speed,
		time = 0.3 * logic.speed,
		onComplete = function()
			group:removeSelf()	-- 清除自己
			if param.onComplete then param.onComplete() end
		end
	})
	
	-- 添加到 特效层
	logic:getLayer("effect"):addChild( group )

end
return M
