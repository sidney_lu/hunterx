local M = {}
local logic = requires("Scene.battle.logicLayer")
function M:create(  )
	local this = {}
	setmetatable(this , self)
	self.__index = self
	local layer = display.newLayer()

	this.tipText = ui.newTTFLabel({
				        text = "x"..(1/logic.speed),
				        size = 36,
				        color = ccc3( 0x2c , 0x00 , 0x00 ),
				        x = 0,
				        y = 0,
				        align = ui.TEXT_ALIGN_CENTER
    				})		
	this.tipText:setAnchorPoint(cc.p(0,0))		
	this.tipText.curSpeed = logic.speed
	layer:addChild(this.tipText)

	layer:addNodeEventListener(cc.NODE_TOUCH_EVENT, handler(this, this.onTouch_))
	return layer
end
function M:onTouch_(event)
	-- for k,v in pairs(event) do
	-- 	testlog("onTouch_ speedlayer",k,v)	
	-- end
	if event.name == "began" then
		if self.tipText:getCascadeBoundingBox():containsPoint(cc.p(event.x,event.y)) then
			return true
		end
	elseif event.name=="ended" then
		if self.tipText.curSpeed == 1 then
			self.tipText.curSpeed = 2
			logic.speed = 1/2
		elseif self.tipText.curSpeed == 2 then
			self.tipText.curSpeed  = 4
			logic.speed = 1/4
		else
			self.tipText.curSpeed = 1
			logic.speed = 1
		end
		self.tipText:setString("x"..self.tipText.curSpeed)
	end
	
end
return M