collectgarbage("setpause"  ,  100)
collectgarbage("setstepmul"  ,  5000)


-- [[ 包含各种 Layer ]]
local ShopLayer = requires("Scene.shop.shoplayer")

local M = {}

function M:create()
	local scene = display.newScene("shop")

	---------------插入layer---------------------
	scene:addChild(ShopLayer:new(0 , 0))
	---------------------------------------------

	return scene
end

return M
