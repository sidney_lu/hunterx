local InfoLayer = requires("Scene.common.infolayer")
local KNBtn = requires("Common.KNBtn")
local KNBar = requires( "Common.KNBar")
local KNTextField = requires("Common.KNTextField")
local vipinfo = requires("Config.vip")
local PATH = IMG_PATH.."image/scene/vip/"
local baseElement = requires( "Scene.common.baseElement")
local M = {
}

function M:new(params)
	params = params or {}
	local this = {}
	setmetatable(this,self)
	self.__index = self
	
	local data = DATA_Vip:get("vip")
	this.baseLayer = display.newLayer()
	local layer = display.newLayer()
	
	-- 背景
	local bg = display.newSprite(COMMONPATH .. "dark_bg.png")
	setAnchPos(bg , 0 , 88)						-- 70 是底部公用导航栏的高度
	this.baseLayer:addChild(bg)
	local highLv = 9	--定义最高vip等级
	local vipexp = DATA_User:get("vipexp")
	local curVipLv =  DATA_User:get("vip")
	local nextLv = curVipLv + 1
	nextLv = nextLv>highLv and highLv or nextLv
	local needexp = vipinfo[nextLv].gold
	local nextGold = needexp - vipexp--到下级还需要充值多少
	
	layer:addChild( display.newSprite( IMG_PATH .. "image/scene/gang/hall/notice_bg2.png" , display.cx ,  display.top-212 ):align(display.BOTTOM_CENTER)) --618
	
	local tempBar = KNBar:new("exp_general" , 131 , display.top-163 , { maxValue = needexp , curValue = vipexp , color = ccc3(255, 255, 255) })
	tempBar:setIsShowText( nextLv <= highLv )
	layer:addChild( tempBar )
	
	layer:addChild( display.newSprite( PATH .. "cur_vip.png" , 55 ,  display.top-176):align(display.BOTTOM_LEFT) )	-- 当前vip等级 654
	layer:addChild( display.newSprite( PATH .. "cur_vip_title.png" , 150 ,  display.top-136 ):align(display.BOTTOM_LEFT) )	-- 当前vip等级694
	if curVipLv <= ( highLv-1 )  then
		layer:addChild( display.newSprite( PATH .. "need_gold.png" , 55 ,  display.top-200):align(display.BOTTOM_LEFT))	--再充值多少黄金即可成为vipN 630
		layer:addChild( display.strokeLabel( nextLv , 330 , display.top-200 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) )) --633
		
		nextGold = nextGold<10000 and nextGold or math.floor(nextGold/10000) .. "万"
		layer:addChild( display.strokeLabel( nextGold , 126 , display.top-200 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , { -- 633
				dimensions_width = 53 ,
				dimensions_height = 24,
				align = 1
			}) )
	end 
	layer:addChild( display.strokeLabel( curVipLv , 275 , display.top-134 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , { --696
			dimensions_width = 100 ,
			dimensions_height = 24,
			align = 0
		}) )
	
	local configData = vipinfo
	local scroll = KNScrollView:new( 10 , 103 , 460 , display.top-330 , 5 )
	for i = 1 , #configData do
		local tempItem = this:itemCell( { data = configData[i] , parent = scroll , isShowLine = i ~= 6 } )
		scroll:addChild(tempItem , tempItem )
	end
	scroll:alignCenter()
	layer:addChild( scroll:getLayer() )
	
	scroll:setIndex( curVipLv , true )
	
	layer:addChild( display.newSprite( COMMONPATH.."tab_line.png"  , 6 , display.top-223 ):align(display.BOTTOM_LEFT)) --607
	this.baseLayer:addChild(layer)
	
	-- 充值按钮
	local charge_btn = KNBtn:new( SCENECOMMON .. "navigation" , {"na_charge.png" , "na_charge_pre.png"} , 363 , display.top-190 , { --640
		priority = -130,
		callback = function()
			if params.isPush == true then
				popScene()
			else
				switchScene("pay")
			end
		end,
		scale = true,
	})
	layer:addChild(charge_btn:getLayer())
	
	--导航信息
	local info = InfoLayer:new( "vip" , 0 , {tail_hide = true , title_text = PATH .. "vip_title.png",isPush = params.isPush})
	this.baseLayer:addChild(info:getLayer() , 2)
	return this.baseLayer
end

function M:itemCell( params )
	params = params or {}
	local data = params.data or {}
	local isShowLine = params.isShowLine
	
	local layer = display.newLayer()
	
	if isShowLine then
		layer:addChild( display.newSprite( IMG_PATH .. "image/scene/notice/line.png" 	, display.cx , 3 ):align(display.BOTTOM_CENTER))
	end
	
	local pathStr = "get.png"
	
	local getBtn = KNBtn:new( COMMONPATH , 
		{ "btn_bg_red.png" ,"btn_bg_red_pre.png" , "btn_bg_red2.png" } , 
		154 , 15 ,
		{
			parent = params.parent ,
--			priority = -130,
			front = COMMONPATH .. pathStr ,
			callback = 
			function()
				HTTP:call( 20014, { lv = data.lv },{success_callback = 
				function()
					KNMsg:getInstance():flashShow("领取奖励成功，请在背包查收。")
				end})
			end
		})
	--getBtn:setEnable( data.status == 2 )
	layer:addChild( getBtn:getLayer() )
	
	layer:addChild( display.newSprite( IMG_PATH .. "image/scene/activity/shade1.png" , display.cx , 68 ):align(display.BOTTOM_CENTER) )	--奖励背景
	layer:addChild( display.newSprite( PATH .. "vip_awards_title.png"	 	, 100 , 165):align(display.BOTTOM_LEFT))
	layer:addChild( display.strokeLabel( data.lv , 133 , 167 , 20 , ccc3( 0xff , 0xfb , 0xd5 ) ) )
	
	--奖励
	local awardData = data.gift
	for i = 1 , #awardData do
		local curProp = awardCell( awardData[i],{showType="info"} )
		setAnchPos( curProp , 100 + ( i - 1 ) * 84 , 90 )
		layer:addChild( curProp )
	end
	
	local addY = 204 
	local noticeText = KNTextField:create( { str = data.privilege , size = 18  , color = ccc3( 0xff , 0xfb , 0xd5 ) , width = 345 } )
	addY = addY + noticeText:getContentSize().height
	setAnchPos( noticeText ,  110 , addY , 0 , 1 )
	layer:addChild( noticeText )
	
	addY = addY + 20
	layer:addChild( display.newSprite( PATH .. "tip_title.png" 				, 100 , addY):align(display.BOTTOM_LEFT))
	layer:addChild( display.newSprite( PATH .. "vip" .. data.lv .. ".png" 	, 0 , addY + 25 ):align(display.BOTTOM_LEFT) )
	layer:addChild( display.strokeLabel( data.gold , 177 , addY + 35 , 20 , ccc3( 0xff , 0xfb , 0xd5 ) , nil , nil , {
		dimensions_width = 70 ,
		dimensions_height = 24,
		align = 1
	}) )
	
	addY = addY + 60 + 30
	

	layer:setContentSize( CCSize(  460 , addY  ) )
	return layer
end

				
return M