local MAIN, HERO, SKILL, ATHLETICS,
 HEROSPLIT, HEROMERGE, EQUIPSPLIT, EQUIPMERGE,
 HEROLIST,EQUIPLIST = 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
local LEFT, RIGHT, TOP, BOTTOM = 1, 2, 3, 4
local PATH = IMG_PATH.."image/scene/fb/"
local SCENECOMMON = IMG_PATH.."image/scene/common/"
local KNBtn = requires("Common.KNBtn")
local KNRadioGroup = requires("Common.KNRadioGroup")
local KNComboBox = requires("Common.KNComboBox")
local KNMask = requires("Common.KNMask")
local Progress = requires("Common.KNProgress")
local InfoLayer = requires("Scene.common.infolayer")
local CombatAttributes = requires("Config.CombatAttributes")
local KNCardpopup = requires("Common.KNCardpopup")
local Lot= requires("Scene.fb.lot")
local SelectList = requires("Scene.common.selectlist")
local HomeCard = requires("Scene.fb.herocard")
local ListItem = requires("Scene.common.listitem")
local direc
--[[
	副本模块，在首页点击副本按钮进入此模块
	函数:new()			--可能传入跳转参数，直接进入子界面
	函数:createmain()   --这个才是主界面
	函数:createcontent()
]]	
local FBLayer = {
	layer,
	mainLayer,
	contentLayer,
	state,
	wait,
	onePt,
	info,
	mask,
	needHeroList
	
}

function FBLayer:new(params)
	local this = {}
	setmetatable(this , self)
	self.__index = self
	params = params or {}
	this.layer = display.newLayer()
	this.state = params.state 

	local bg = display.newSprite(SCENECOMMON.."bg.png")
	setAnchPos(bg)
	this.layer:addChild(bg)
	
	local title = display.newSprite(COMMONPATH.."dark_bg.png")
	setAnchPos(title, 0, 425, 0, 0.5)
	this.layer:addChild(title)
	--其他页面调用
	if params.coming == "hero" then
		-- 进入英雄殿(招贤阁)
		HTTP:call(30001,{},{success_callback = function(data)
			this:createHeroFb(data)
		end})
	elseif params.coming == "skill" then
		-- 进入如意阁（银沙城、赌场）
		if DATA_Instance:get("skill") then
			this:createSkillFb()
		else
			HTTP:call(30003,{},{
				success_callback = function()
				this:createSkillFb()
			end})
		end
	elseif params.coming == "athletics" then
		--竞技场
		HTTP:call(21003,{},{success_callback = 
			function(data)
				switchScene("athletics",{data = data })	
			end})
	elseif params.coming == "equip" then
		--神匠
		HTTP:call(30007, {kind=params.coming}, {
					success_callback = function(data)
						this.curStar = 1
						this.data = data
						this:createSplitLayer(params.coming, params.type, this.curStar)
					end
				}) 
	elseif params.coming == "general" then--（generate??）
		--转生阁？？？
		HTTP:call(30007, {kind=params.coming}, {
					success_callback = function(data)
						this.data = data
						this:createMergeLayer(params.coming)
					end
				})
	else
	 	this:createMain(params and params.coming or 'hero')
	end	

	return this
end

function FBLayer:createInfo(title, func)
	if self.info then
		self.layer:removeChild(self.info:getLayer(), true)
	end
	func = func or function()
		testlog("FBLayer call back",self.state)
		if self.state == MAIN then
			switchScene("home")
		elseif self.state == HEROSPLIT and direc then
			self.scroll = nil
			self:createSplitLayer("general", "split",self.curStar or 1)
		elseif self.state == HEROLIST then
			self.scroll = nil
			self:createSplitLayer("general", "split",self.curStar or 1)
		elseif self.state == EQUIPSPLIT and direc then
			self.scroll = nil
			self:createSplitLayer("equip", "split", self.curStar or 1)
		elseif self.state == HEROMERGE and direc then
			self.scroll = nil
			self:createMergeLayer("general")
		elseif self.state == EQUIPMERGE and direc then
			self.scroll = nil
			self:createMergeLayer("equip")
		else
			self.tempMerge = nil
			self.selectItems = nil
			self:createMain(self.state)
		end
		direc = nil
	end
	self.info = InfoLayer:new("fb", 0, {tail_hide = true, title_text = title, closeCallback = func})
	self.layer:addChild(self.info:getLayer(),10)
end
--主页
function FBLayer:createMain(name)
	if self.mainLayer then
		self.layer:removeChild(self.mainLayer,true)
	end
	self.selectItems = nil
	self.mainLayer = display.newLayer()

	-- 上面小标题背景
	local bg = display.newSprite(SCENECOMMON.."level_bg.png")
	setAnchPos(bg, 240, display.top-260, 0.5)
	self.mainLayer:addChild(bg)
	
	-- 中间大标题背景
	local bigbg_height = display.top-380
	-- bg = display.newScale9Sprite(COMMONPATH.."desc_bg.png", 30, 110, CCSize(417, bigbg_height))
	-- 	:align(display.BOTTOM_LEFT)
	bg = display.newSprite(COMMONPATH.."desc_bg.png")
	local height = display.height - 262 - 94
	local srcHeight = height
	if height > 445 then
		height = 445
	end
	local scaleY = height / 445
	bg:setScaleY(scaleY)
	--bg:setPosition(display.cx,display.cy - 85 )
	bg:setPosition(display.cx,display.top - 262 - srcHeight/2)
	--setAnchPos(bg, 30, 90 / scaleY)
	self.mainLayer:addChild(bg)
	
	local group = KNRadioGroup:new()

	local scroll = KNScrollView:new(35, display.top-255, 410, 130, 2, true)
	local des_scroll 
	--des_scroll =  KNScrollView:new(35, 90, 405, bigbg_height+15, 0, true, 1, {
	print("FBLayer scaleY",scaleY)
	des_scroll =  KNScrollView:new(35,display.top - 262 - srcHeight + 8*scaleY, 405, height, 0, true, 1, {
		page_callback = function()
			group:chooseByIndex(des_scroll:getCurIndex())
		end
	})
	
	local level = {
		"hero",
		"skill",
		--"athletics",
		"equip",
		"general"
	}
	-- 大图切换按钮
	for i = 1, #level do
		local btn = KNBtn:new(PATH,{level[i].."_icon.png", "select.png"}, 0, 0, {
			id = level[i],
			scale = true,
			parent = scroll,
			noHide = true,
			upSelect = true,
			selectZOrder = 1,
			callback = function()
				des_scroll:setIndex(i)	
			end
		},group)
		scroll:addChild(btn:getLayer(), btn)
		
		local desc = self:createContent(level[i], height)
		des_scroll:addChild(desc)
	end
	scroll:alignCenter()
	--des_scroll:alignCenter()
	self.mainLayer:addChild(scroll:getLayer())
	self.mainLayer:addChild(des_scroll:getLayer())
		
	local num 
	for k, v in pairs(level) do
		if v == name then
			num = k
			break
		end
	end
	
	group:chooseByIndex(num or 1)
	self.group = group
	scroll:setIndex(num or 1 , true)
	des_scroll:setIndex(num or 1,true)
	self.state = MAIN

	-- 新手引导
	local guide_step = KNGuide:getStep()
	if guide_step == 801 or guide_step == 3001 or guide_step == 3101 or guide_step == 3201 or guide_step == 3301 then
		local btn = scroll:getItems(num or 1)
		local btn_range = btn:getRange()

		KNGuide:show( btn:getLayer() , {
			x = btn_range:getMinX(),
			y = btn_range:getMinY(),
			callback = function()
				KNGuide:showBtnw( coming:getLayer() )
			end
		})
	end
	

	self.layer:addChild(self.mainLayer)
	self:createInfo(PATH.."title.png",function()
	 		switchScene("home")
	 end)
end

function FBLayer:createContent(kind, height ,srcHeight)
	srcHeight = srcHeight or 445
--	if self.contentLayer then
--		self.mainLayer:removeChild(self.contentLayer, true)
--	end	
--	
--	self.contentLayer = display.newLayer()
	
	local layer = display.newLayer()
	
	
	local bg = display.newSprite(PATH..kind.."_desc.png")
	local scaleY = height / srcHeight
	bg:setScaleY(scaleY)
	setAnchPos(bg, 10, 149 * scaleY)
	layer:addChild(bg)
	
	--local text = display.newSprite(PATH..kind.."_text.png")
	--setAnchPos(text, 395, 390, 1)
	--layer:addChild(text)
	
	local desc = {
		["hero"] =  "每天逛逛英雄殿，就可以招募到心仪的英雄",
		["skill"] = "呼吸吐纳，气沉丹田！如意阁,好招式！",
		["athletics"]  =  "竞技场挑战众高手证明自己实力! ",
		["equip"] = "这人在江湖，哪能不装备好。合牛B装备！",
		["general"] = "炼魂中可炼成将魂，合卡中可合成武将。"
	 }

   	
	text = display.strokeLabel(desc[kind], 0, 60 * scaleY, 20,nil,nil,nil,{
		dimensions_width = 400,
		dimensions_height = 100,
	} )
	layer:addChild(text)
	
	layer:setContentSize(CCSizeMake(405, 450))


	local btnY = 11 * scaleY
	if kind == 'equip' or kind == 'general' then
		--炼魂
		local split = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, 40 , btnY, {
			front = PATH..(kind == "equip" and kind or "general").."_split.png",
			callback = function()
				HTTP:call(30007, {kind=kind}, {
					success_callback = function(data)
						self.curStar = 1
						self.data = data
						self:createSplitLayer(kind, "split", self.curStar)
					end
				})
			end
		})
		split:getLayer():setScaleY(scaleY)
		layer:addChild(split:getLayer())
		
		--合成
		local merge = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, 200, btnY, {
			front = PATH..(kind == "equip" and kind or "general").."_merge.png",
			callback = function()
				HTTP:call(30007, {kind=kind}, {
					success_callback = function(data)
						self.data = data
						self:createMergeLayer(kind)
					end
				})
			end
		})
		merge:getLayer():setScaleY(scaleY)
		layer:addChild(merge:getLayer())
	else
		-- 进入的按钮
		local coming = KNBtn:new(COMMONPATH,{"btn_bg_red.png", "btn_bg_red_pre.png"}, 125, btnY, {
		scale = true,
		front = COMMONPATH.."coming.png",
		callback = function()

			-- 判断等级开放
			-- local check_result = checkOpened("fb_"..group:getId())
			-- if check_result ~= true then
			-- 	KNMsg:getInstance():flashShow(check_result)
			-- 	return false
			-- end
			
			if self.group:getId() == "hero" then
				-- 进入英雄殿
				HTTP:call(30001,{},{success_callback = function(data)
					self:createHeroFb(data)
				end})
			elseif self.group:getId() == "skill" then
				-- 进入如意阁
				if DATA_Instance:get("skill") then
					self:createSkillFb()
				else
					HTTP:call(30003,{},{
						success_callback = function()
						self:createSkillFb()
					end})
				end	
			elseif self.group:getId() == "athletics" then
				-- 进入竞技场
				local check_result = checkOpened("athletics")
				if check_result ~= true then
					KNMsg:getInstance():flashShow(check_result)
					return
				end

				HTTP:call(21003,{},{success_callback = 
					function(data)
						InfoLayer.isEnterFromFb = true
						switchScene("athletics",{data = data })	
					end})
			end

		end
		})
		coming:getLayer():setScaleY(scaleY)
		layer:addChild(coming:getLayer())
	end
	
	return layer
end

-- 如意阁
function FBLayer:createSkillFb(index)
	if self.mainLayer then
		self.layer:removeChild(self.mainLayer,true)
	end
	self.mainLayer = display.newLayer()

	-- 开始按钮，必须放这，因为新手引导要用 - -
	local clickStart
	
	local bg = display.newSprite(PATH.."bg.jpg")
	setAnchPos(bg)
	self.mainLayer:addChild(bg)
	
	self.state = SKILL
	
	--压注选择
	local valueGroup = KNRadioGroup:new()
	for i = 1, 3 do
		local btn = KNBtn:new(PATH, {"v"..i.."_btn.png", "btn_select.png"}, 320 - (i - 1) * 150, 610, {
			id = i,
			noHide = true,
			selectZOrder = 10,
			callback = function()
			end
		}, valueGroup)
		self.mainLayer:addChild(btn:getLayer())

		-- 新手引导
		if i == 2 and KNGuide:getStep() == 3203 then
			KNGuide:show( btn:getLayer() , {
				callback = function()
					KNGuide:show( clickStart:getLayer() , {
						remove = true
					})
				end
			})
		end
	end
	
	local pos = {
		{260,515,RIGHT},
		{380,515,BOTTOM},
		{380,405,BOTTOM},
		{380,295,BOTTOM},
		{380,190,LEFT},
		{260,190,LEFT},
		{140,190,LEFT},
		{20,190,TOP},
		{20,295,TOP},
		{20,405,TOP},
		{20,515,RIGHT},
		{120,505,RIGHT},
	}
	
	local group = KNRadioGroup:new()
	for i = 1, #pos do
		local link = display.newSprite(PATH.."link.png")
		local ox, oy = 0, 0
		
		if pos[i][3] == LEFT then
			ox = -80
			oy = 30
		elseif pos[i][3] == RIGHT then
			oy = 30
			ox = 40
		elseif pos[i][3] == BOTTOM then
			ox = 30
			link:setRotation(90)
		else
			ox = 30
			oy = 120	
			link:setRotation(90)
		end
		
		if i == 12 then
			ox = 60
			oy = 40
		end
		setAnchPos(link,pos[i][1] + ox,pos[i][2] + oy)
		self.mainLayer:addChild(link)
		
		local front,other,text
		local bg = "item_bg.png"
		local data = DATA_Instance:get("skill","list")
		if i == 1 then
			front = PATH.."start.png"
		else
			if data[i]["type"] == "silver" or data[i]["type"] == "prestige" then
				front = PATH..data[i]["type"]..".png"
			elseif data[i]["type"] == "thief" then
				front = IMG_PATH.."image/hero/s_general1334.png"
			else
				local pre
				if data[i]["type"] ~= "skill" then
					pre = "/s_"	
				else
					pre = "/"
				end
				local skillid = math.modf(data[i]["effect"]/100)
				front = IMG_PATH.."image/"..data[i]["type"]..pre..skillid..".png"
			end
		end
		
		if i == 12 then
			bg = "final_item_bg.png"
		end
		local btn = KNBtn:new(PATH, {bg,"cur.png"},pos[i][1],pos[i][2],{
			front = front,
			other = other,
			text = text,
			selectZOrder = 20,
			upSelect = true,
			noHide = true,
			callback = function()
				print("---------tip!")
				if i > 1 then
					local k = DATA_Instance:get("skill", "list", i)
					local skillid = math.modf(k["effect"]/100)
					print("--skillid:", skillid)
					if k["type"] == "thief" then
						KNMsg.getInstance():flashShow("小偷：将被偷取"..skillid.."银两")
					elseif k["type"] == "silver" then
						KNMsg.getInstance():flashShow("银两:将获得银两"..skillid)
					else
						local str = "英雄技能"
						str = getConfig(getCidType(skillid), skillid,"star").."星"..str
						KNMsg.getInstance():flashShow(str.."【"..getConfig(getCidType(skillid), skillid,"name").."】")
					end
					return false
				end
			end
		},group)
		self.mainLayer:addChild(btn:getLayer())
		
		if i > 1 then
			btn = getImageNum(i, COMMONPATH.."small_num.png")
			setAnchPos(btn, pos[i][1] + (i == 12 and 110 or 80) ,pos[i][2] + 10, 1)
			self.mainLayer:addChild(btn)
		end
	end
	
	group:chooseByIndex(index or 1)
	
		--这里是将骰子的点数拆成2数和
	local m, n = math.random(1,6), math.random(1,6)
	if index then
		local max 
		if index > 6 then
			max = 6
		else
			max = index - 1			
		end
		
		n = index - max
		n = math.random(n,max)
		m = index - n
	end
	
	if index and self.onePt then
		m = self.onePt
		n = index - self.onePt
	end
	
	local dice1 = display.newSprite(PATH..m..".png")
	setAnchPos(dice1, 190, 400)
	self.mainLayer:addChild(dice1,1)
	
	local dice2 = display.newSprite(PATH..n..".png")
	setAnchPos(dice2, 230, 360)
	self.mainLayer:addChild(dice2,1)									
										
	
	local stop,pt, add, count, award
	local function playAni(x, y,reverse)
		local frames = display.newFramesWithImage(PATH .. "ani.png" , 5 )
		local sprite 
		sprite = display.playFrames(x, y , frames, 0.1,
			{
				reverse = reverse,
				onComplete = function()
					if stop then
						self.mainLayer:removeChild(sprite,true)
						
						if not add then
							add = true
							
							--这里是将骰子的点数拆成2数和
							local max 
							if pt > 6 then
								max = 6
							else
								max = pt - 1			
							end
							
							self.onePt = pt - max  --其中一个骰子的点数
							self.onePt = math.random(self.onePt, max)
							
							dice1 = display.newSprite(PATH..self.onePt..".png")
							setAnchPos(dice1, 190, 400)
							self.mainLayer:addChild(dice1,1)
							
							dice2 = display.newSprite(PATH..(pt - self.onePt)..".png")
							setAnchPos(dice2, 230, 360)
							self.mainLayer:addChild(dice2,1)									
							--stop = false
						end
					else
						self.mainLayer:removeChild(sprite,true)
						self.mainLayer:addChild(playAni(x ,y,reverse))
					end
				end
			}
		)		
--		sprite:runAction(CCJumpTo:create(2,ccp(x,y),50,3))
		return sprite
	end
	
	if DATA_Instance:get("skill", "times") > 0 then
		clickStart =  KNBtn:new(PATH,{"click_star.png", "click_star_press.png"}, 180, 345,{
			callback = function()
				print("-----start!")
				i7PlaySound(IMG_PATH .. "sound/abaca.mp3")
				local mask
				if not self.wait then
					if DATA_Instance:get("skill", "times") > 0 then
						if isBagFull() then
							return false
						end
						--Wolf：将开始按钮从不显示改为移除
						--clickStart:showBtn(false)
						self.mainLayer:removeChild(clickStart:getLayer())

						mask = KNMask:new({opacity = 0})
						self.mainLayer:addChild(mask:getLayer())						
						self.wait = true
						self.mainLayer:removeChild(dice1, true)
						self.mainLayer:removeChild(dice2, true)
						
						self.mainLayer:addChild(playAni(230, 455))
						self.mainLayer:addChild(playAni(270, 410,true))
						
						local index = 1
						local schedule = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
							index = index + 1
							if index > 12 then
								index = 1
							end	
							group:chooseByIndex(index)
						end,0.1,false)
						stop = false
						add = false
						HTTP:call(30004,{yazhu = valueGroup:getId()},{
							no_loading = true,	
							error_callback = function()
								print("--error_callback")
								CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(schedule)
								self.wait = false
								KNMsg.getInstance():flashShow("请求失败，请检查网络或重试")
								self:createSkillFb()
							end,
							success_callback = function(data)
								print("--success_callback")
								pt = data.point
								count = 0
								self.wait = false
								award = data["award"]
								CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(schedule)
								schedule =  CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
									xpcall(function()
										if count == 2 and pt == index then
											mask:remove()
											-- self.mainLayer:removeChild(mask, true)
											local resultImg
											if pt < 9 then
												resultImg = display.newSprite(PATH.."v1_text.png")
											elseif pt < 12 then
												resultImg = display.newSprite(PATH.."v2_text.png")
											else
												resultImg = display.newSprite(PATH.."v3_text.png")
											end
											setAnchPos(resultImg, 240, 425, 0.5, 0.5)
											self.mainLayer:addChild(resultImg,100)
--											
											CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(schedule)
											resultImg:setScale(0.1)	
											resultImg:runAction(getSequenceAction(CCEaseElasticOut:create(CCScaleTo:create(0.8,1.5)), CCCallFunc:create(function()
												if DATA_Instance:get("skill","list",index.."","is_battle") == 1 then
													SOCKET:getInstance("battle"):call("insskill" , "execute" , "execute",{})
												else
													self:createSkillFb(pt)
													for k, v in pairs(award) do
														if k == "thief" then
															KNMsg.getInstance():flashShow("运气不好，被小偷偷了钱包,损失银币:"..v)
														elseif k == "silver" then
															KNMsg.getInstance():flashShow("获得银两:"..v)
														elseif k == "drop" then
															KNMsg.getInstance():flashShow("获得技能书:【"..getConfig("skill", tonumber(v), "name").."】")
														else
															KNMsg.getInstance():flashShow("获得"..k.."/"..v)
														end
													end
												end
											end)))
										else
											if pt == index then
												count = count + 1 
											end
											index = index + 1
											if index > 12 then
												index = 1
											end
												
											local stopN = pt - 3
											if stopN <= 0 then
												stopN = 12 + stopN
											end
											
											if count == 1 and index == stopN then
												stop = true
											end
											
											group:chooseByIndex(index)
										end
									end,function()
										--异常时停止定时器
										self.wait = false
										stop = false
										
										CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(schedule)
									end)
								end,0.1,false)
							end})
					else
						KNMsg.getInstance():flashShow("摇点次数已不足，请点增加按钮购买摇点次数")
					end
				else
					print("等待中哦")
				end
			end
		})
		self.mainLayer:addChild(clickStart:getLayer(), 10)
	end
	
	local times = display.newSprite(PATH.."time_text.png")
	setAnchPos(times, 60, 140)
	self.mainLayer:addChild(times)
	
	times = display.strokeLabel(DATA_Instance:get("skill","times").."/"..DATA_Instance:get("skill","max_times"), 120, 145, 20, ccc3(255,255,255))
	setAnchPos(times, 135, 145, 0.5)
	self.mainLayer:addChild(times)
	
	-- 购买次数
	local addTimes = KNBtn:new(COMMONPATH,{"add.png", "add_press.png"}, 160, 135, {
		callback = function()
			print("-----buy")
			HTTP:call(30005,{},{
				success_callback = function()
					self:createSkillFb()
				end
			})
		end
	})
	self.mainLayer:addChild(addTimes:getLayer())
	
	local textBtn = KNBtn:new(PATH, {"luck_text.png"}, 160, 280, {
		callback = function()
			KNMsg.getInstance():flashShow("幸运值满30点时100%开出豹子，骰子大于六点+2点幸运值，其余则-1幸运值")
		end
	})
--	local text = display.newSprite(PATH.."luck_text.png")
--	setAnchPos(text, 160,280)
	self.mainLayer:addChild(textBtn:getLayer())
	
	local bar = Progress:new(PATH,{"luck_bg.png","luck_fro.png"}, 240, 280,{
		showText = true,
		cur = DATA_Instance:get("skill","luck"),
		max = DATA_Instance:get("skill","max_luck")
	})
	self.mainLayer:addChild(bar:getLayer())
	
	-- 刷新列表
	local refresh = KNBtn:new(COMMONPATH,{"btn_bg.png", "btn_bg_pre.png"}, 330, 140, {
		scale = true,
		front = COMMONPATH.."refresh.png",
		callback = function()
			HTTP:call(30006,{},{
				success_callback = function()
					self:createSkillFb()
				end
			})
		end
	})
	self.mainLayer:addChild(refresh:getLayer())
	
	--刷新金币
	text = display.newSprite(COMMONPATH.."gold.png")
	setAnchPos(text, 340, 110)
	self.mainLayer:addChild(text)
	
	text = display.strokeLabel(DATA_Instance:get("skill", "refresh_price"), 375, 110, 20, ccc3(0xff,0xe5,0xa6))	
	self.mainLayer:addChild(text)
	
	--增加次数金币
	text = display.newSprite(COMMONPATH.."gold.png")
	setAnchPos(text, 90, 110)
	self.mainLayer:addChild(text)
	
		text = display.strokeLabel(DATA_Instance:get("skill", "point_price"), 125, 110, 20, ccc3(0xff,0xe5,0xa6))	
	self.mainLayer:addChild(text)
	
	
	self.layer:addChild(self.mainLayer)
	self:createInfo(PATH.."skill_title.png",function()
			self:createMain("skill")
		end)
end

-- 英雄殿
function FBLayer:createHeroFb(data)
	if self.mainLayer then
		self.layer:removeChild(self.mainLayer,true)
	end
	
	self.mainLayer = display.newLayer()
	self.state = HERO
	
	local bg = display.newSprite(PATH.."hero_new_bg.jpg")
	setAnchPos(bg, 240, 460, 0.5, 0.5)
	self.mainLayer:addChild(bg)
	
	bg = display.newSprite(COMMONPATH.."have.png")
	setAnchPos(bg, 300, 700)
	self.mainLayer:addChild(bg)
	
	bg = display.newSprite(COMMONPATH.."gold.png")
	setAnchPos(bg, 350, 700)
	self.mainLayer:addChild(bg)
	
	self.mainLayer:addChild(createLabel({str = DATA_User:get("gold"), x = 380, y = 702, color = ccc3(0xff,0xd1, 0x39)}))
	
	
	local curStar = 5
	local select = {}
	local function rdHero()
		if not self.needHeroList then
			self.needHeroList = {}
			for k, v in pairs(getConfig("general")) do
				if v.star >= 3 and v.hidden == 0 then
					if not self.needHeroList[v.star] then
						self.needHeroList[v.star] = {}
					end
					table.insert(self.needHeroList[v.star], k)
				end
			end
		end
		
		if not select[curStar] then
			select[curStar] = {}	
		end
		
		if #select[curStar] == 3 then
			curStar = curStar - 1
			select[curStar] = {}
		end
		
		local rd = self.needHeroList[curStar][math.random(1, #self.needHeroList[curStar])]
		
		while table.hasValue(select[curStar], rd) do
			rd = self.needHeroList[curStar][math.random(1, #self.needHeroList[curStar])]
		end
		
		table.insert(select[curStar], rd)
		
		return rd
	end
	--生成主页滑动卡牌
	local items = {}
	for i = 1, 8 do
		items[i] = HomeCard:new(435 , i , rdHero())
		items[i]:addTo(i,self.mainLayer)
	end
	
	for i = 1, 8 do
		items[i]:move(true, self, 1.5)
	end
	
	for i = 1, 2 do
		local kind = i == 1 and "xian" or "shen"
		local btn = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, 20 + (i - 1) * 220, 115, {
		front = PATH..(i == 1 and "normal_text.png" or "advance_text.png"),
		callback = function()
			if data[kind].curtimes == data[kind].freetimes then
				if data[kind].gold > 0 and not countGold(data[kind].gold) then
					return false
				end
			end
			for i = 1, 8 do
				items[i]:stop()
				items[i]:move(true, self, 0.01)
			end
			HTTP:call(30002, {type = kind }, {
				no_loading = true,
				success_callback = function(data)
					for i = 1, 8 do
						items[i]:stop()
					end					
					self:popCardList(data["awards"]["drop"], data["_G_penglai"])
				end, error_callback = function(msg)
					KNMsg.getInstance():flashShow(msg.msg)
					self:createHeroFb(data)
				end
			})
			end
		})
		self.mainLayer:addChild(btn:getLayer())
		
		if data[kind].curtimes < data[kind].freetimes then
			self.mainLayer:addChild(createLabel({str = "免费", x = 175 + (i - 1) * 220,y = 138, color = ccc3(0xff,0xd1, 0x39)} ))
			self.mainLayer:addChild(createLabel({str = (data[kind].freetimes - data[kind].curtimes).."/"..data[kind].freetimes, x = 180 + (i - 1) * 220,y = 115, color =DESCCOLOR} ))
		else
			local gold = display.newSprite(COMMONPATH.."gold.png")
			setAnchPos(gold, 165 + (i - 1) * 220, 135 )
			self.mainLayer:addChild(gold)
			
			self.mainLayer:addChild(createLabel({str = data[kind].gold, x = 200 + (i - 1) * 220,y = 138, color = ccc3(0xff,0xd1, 0x39)} ))
			
			local cool = data[kind].cool
			local timeText = createLabel({str = timeConvert(data[kind].cool), x = 160 + (i - 1) * 220,y = 100, color = DESCCOLOR, size = 14})
			Clock:addTimeFun("hero_clock"..i, function()
					--if cool > 0 then
						cool = cool - 1
						timeText:setString(timeConvert(cool))
					-- else
					-- 	HTTP:call(30001, {}, {
					-- 		success_callback = function(data)
					-- 			Clock:removeTimeFun("hero_clock1")
					-- 			Clock:removeTimeFun("hero_clock2")
					-- 			self:createHeroFb(data) 
					-- 		end
					-- 	})
					-- end
				end)
			self.mainLayer:addChild(timeText)
		end

		if i == 1 and KNGuide:getStep() == 3003 then
			KNGuide:show( btn:getLayer() , {remove = true} )
		end

		if i == 2 and KNGuide:getStep() == 3004 then
			KNGuide:show( btn:getLayer() , {remove = true} )
		end
	end
	
	self.layer:addChild(self.mainLayer)
	
	self:createInfo(PATH.."hero_title.png",function()
		Clock:removeTimeFun("hero_clock1")
		Clock:removeTimeFun("hero_clock2")
		self:createMain("hero")
	end)
end

function FBLayer:getLayer()
	return self.layer
end

function FBLayer:createRobFb(data, star)
	if self.mainLayer then
		self.layer:removeChild(self.mainLayer,true)
	end
	local rob = RobLayer:new({data = data, star = star})
	
	self.mainLayer = rob:getLayer()
	self.layer:addChild(self.mainLayer)
	
	self:createInfo(PATH.."rob_title.png",function()
		if rob:getState() == 1 then
			self:createMain("rob")
		else
			self:createRobFb(data, rob:getStar())	
		end
	end)
end

function FBLayer:stopTimer(index)
	if index then
		if Clock:getKeyIsExist("inspet_clock"..index) then
			Clock:removeTimeFun("inspet_clock"..index)
		end
	else
		Clock:removeTimeFun("inspet_clock1")
		Clock:removeTimeFun("inspet_clock2")
		Clock:removeTimeFun("inspet_clock3")
	end
end
function FBLayer:popCardList(data, new)
	
	local index = 1
	local function pop(index)
		local card_popup = KNCardpopup:new(data[index] , function()
				if index == table.nums(data) then
					self:createHeroFb(new)
				else
					pop(index + 1)
				end
			end , {
				end_x = -80,
				end_y = -350,
			})
		display.getRunningScene():addChild(card_popup:play())						
	end
	pop(index)
end
--祈将弹出的列表
function FBLayer:popCardList1(data)
	if self.mask then
		self.layer:removeChild(self.mask:getLayer(), true)
	end
	
	local layer = display.newLayer()
	local bg = display.newSprite(PATH.."pop_bg.png")
	setAnchPos(bg, 240, 435, 0.5, 0.5)
	layer:addChild(bg)
	
	local btn = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, 170, 305, {
		front = COMMONPATH.."ok.png",
		priority = -131,
		callback = function()
			self.layer:removeChild(self.mask:getLayer(), true)
		end
	})
	layer:addChild(btn:getLayer())
	
	local num = display.newSprite(PATH.."hero_nums.png")
	setAnchPos(num, 20, 310)
	layer:addChild(num)
	
	local n = table.nums(data)
	num = createLabel({str = n, x = 120, y = 310, color =ccc3(255,255,255)})
	layer:addChild(num)
	
	local scroll = KNScrollView:new(22, 330, 435, 250,2,true, 0, {priority = -132})
	layer:addChild(scroll:getLayer())
	
	for i = n, 1, -1 do
		local layer = display.newLayer()
--		local cardBg = display.newSprite(IMG_PATH.."image/scene/uplevel/card_bg.png")
--		layer:setContentSize(cardBg:getContentSize())
		layer:ignoreAnchorPointForPosition(false)
--		
--		setAnchPos(cardBg)
--		layer:addChild(cardBg)
--		
--		scroll:addChild(layer)
		local heroInfo = getConfig("general", data[i])
		heroInfo["cid"] = data[i]
		local card = KNBtn:new(IMG_PATH.."image/scene/uplevel/", {"card_bg.png"}, 0, 0, {
			front = getImageByType(data[i], "b"),
			frontScale = {0.6, 0,10},
			priority = -131,
			parent = scroll,
			other = {PATH.."name_bg.png", 20, 20},
			text = {getConfig("general", data[i], "name"), 18, ccc3(0x2c, 0, 0), ccp(0,-82)},
			callback = function()
				pushScene("detail" , {
						detail = "general",
						data = heroInfo
						})
			end
		})
		layer:setContentSize(CCSizeMake(card:getWidth(), card:getHeight()))
		layer:addChild(card:getLayer())
		
		local star
		for j = 1, getConfig("general", data[i], "star") do
			star = display.newSprite(COMMONPATH .. "star.png")
			setAnchPos(star, (card:getWidth() / 2 - (getConfig("general", data[i], "star") * star:getContentSize().width) / 2) + star:getContentSize().width * (j - 1), 190)
			
			layer:addChild(star)
		end
		scroll:addChild(layer)
	end
	scroll:alignCenter()
	
	self.mask = KNMask:new({item = layer})
	self.layer:addChild(self.mask:getLayer())
	
end

-- 英雄/幻兽分解界面（炼魂）
function FBLayer:createSplitLayer(kind, func, default)
	if self.mainLayer then
		self.layer:removeChild(self.mainLayer,true)
	end
	func = func or "split"
	self.mainLayer = display.newLayer()
	local max = 3
	if kind == "general" then
		self.state = HEROSPLIT
	elseif kind == "equip" then
		self.state = EQUIPSPLIT
		max = 6
	end
	
	--if kind == "general" then
		--图鉴
		local info = KNBtn:new(COMMONPATH, {"btn_bg.png", "btn_bg_pre.png"}, 380, display.top-150, {
			front = PATH.."info_text.png",
			callback = function()
				self:directList(nil,kind)
			end
		})
		self.mainLayer:addChild(info:getLayer())
		--组合
		-- local combine = KNBtn:new(COMMONPATH, {"btn_bg.png", "btn_bg_pre.png"}, 380, 690, {
		-- 	front = PATH.."combine_text.png",
		-- 	callback = function()
		-- 		self:createCombine()
		-- 	end
		-- })
		-- self.mainLayer:addChild(combine:getLayer())
	--end
	
	--幻兽与英雄通用的图片
	local str = (kind == "pet" and "general" or kind)
	
		
	local title = display.newSprite(COMMONPATH.."dark_bg.png")
	setAnchPos(title, 240, 425, 0.5, 0.5)
	self.layer:addChild(title)
	
	title = display.newSprite(COMMONPATH.."title_bg.png")
	setAnchPos(title, 0, display.top - title:getContentSize().height)
	self.mainLayer:addChild(title) 
	
	title = display.newSprite(COMMONPATH.."title_tail.png")
	setAnchPos(title, 0, display.top - title:getContentSize().height * 2)
	self.mainLayer:addChild(title) 
	
		
	title = display.newSprite(PATH..str.."_"..func.."_text.png")
	setAnchPos(title, 245, 765, 0.5)
	--self.mainLayer:addChild(title)
	
	--底部生成元素栏
	local bottom = display.newSprite(PATH.."bottom.png")
	setAnchPos(bottom, 240, 120, 0.5)
	self.mainLayer:addChild(bottom)
	
	bottom = display.newSprite(PATH..kind.."_"..func.."_label.png")
	setAnchPos(bottom, 20, 220)
	self.mainLayer:addChild(bottom)
	
	--在分解界面去合成,重新拉取炼魂数据
	local splitBtn = KNBtn:new(COMMONPATH, {"btn_ver.png", "btn_ver_pre.png"}, 400, 135, {
		front = PATH..str.."_"..func.."_btn_text.png",
		callback = function()
			HTTP:call(30007, {kind=kind}, {
				success_callback = function(data)
					print("---去合成!")
					self.selectItems = nil 
					self.data = data
					self:createMergeLayer(kind)
				end
			})
		end
	})
	self.mainLayer:addChild(splitBtn:getLayer())

	local choose = display.newSprite(PATH.."choose_star.png")
	setAnchPos(choose,380, display.top-470)
	self.mainLayer:addChild(choose)
	
	local autoBtn  		-- 自动添加按钮
	local fenjie_btn	-- 分解按钮
	local comboBox
	local group = KNRadioGroup:new()
	local items = {}
	for i = 1, 4 do
		items[i] = KNBtn:new(PATH, {"star"..i..".png", "star_select.png"}, 0, 0, {
			id = i,
			noHide = true,
			callback = function()
				self.curStar = i
				comboBox:setText(i)
				comboBox:autoShow()

				if KNGuide:getStep() == 1514 then
					KNGuide:show( autoBtn:getLayer() , {
						callback = function()
							KNGuide:show( fenjie_btn:getLayer() )
						end
					})
				end
			end
		},group)
	end
	
	comboBox = KNComboBox:new(418, display.top-470, {
		dir = COMMONPATH,
		res = {"small_btn_bg.png", "small_btn_bg_pre.png"},
		front = COMMONPATH.."star.png",
		text = {default or 1, 20, ccc3(0x2c, 0, 0)},
		bg = PATH.."combo_bg.png",
		up = true,
		offset = 15,
		items = items,
		default = default,
		itemsGroup = group,
		popCallback = function()
			if KNGuide:getStep() == 1513 then
				local btn_range = items[2]:getRange()
				KNGuide:show( items[2]:getLayer() , {
					x = btn_range:getMinX(),
					y = btn_range:getMinY(),
				})
			end
		end
	})
	self.mainLayer:addChild(comboBox:getLayer(), 10)
	if KNGuide:getStep() == 1512 then
		KNGuide:show( comboBox:getLayer() )
	end
	
	--自动添加
	autoBtn = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, 170, display.top-470, {
		front = PATH.."auto_add.png",
		callback = function()
			local result = self:getItems(kind, comboBox:getCurItem():getId())
			
			--自定义按照等级排序
			local keyList =  getSortList(result, function(l, r)
				local l_lv = DATA_Bag:get(kind, l, "lv") or 1
				l_lv = tonumber(l_lv)

				local r_lv = DATA_Bag:get(kind, r, "lv") or 1
				r_lv = tonumber(r_lv)

				return (l_lv < r_lv)
			end)
			self.selectItems = {}	
			
			local needNum = max - table.nums(self.selectItems)
			for k, v in pairs(keyList) do
				if not table.hasValue(self.selectItems, v) then
					table.insert(self.selectItems, v)
					needNum = needNum - 1
				end
				
				if needNum == 0 then
					self:createSplitLayer(kind, func, comboBox:getCurItem():getId())
					break
				end
				
			end
			
			if needNum > 0 then
				self.selectItems = {}
				self:createSplitLayer(kind, func, comboBox:getCurItem():getId())
				local str
				if kind == "general" then
					str = "英雄"
				elseif kind == "equip" then
					str = "装备"
				end
				
				KNMsg.getInstance():flashShow("当前星级"..str.."卡牌不足！~")
			end
		end
	})
	self.mainLayer:addChild(autoBtn:getLayer())
	
	
	--xx打造元素按钮xx（一星将魂，二星将魂）
	for i = 1, #self.data do
		testlog("将魂",i,self.data[i]["num"])
		local other, front, text
	
		if self.data[i]["num"] > 0 then
			other = {{PATH..kind.."_"..func.."_"..i..".png", 0, -25},{COMMONPATH.."egg_num_bg.png", 50, 50}}
			front = PATH..kind.."_icon_"..i..".png"

			text = {self.data[i]["num"],16, ccc3(255,255,255), ccp(28,28),nil,17 }
		else
			other = {PATH..kind.."_"..func.."_"..i..".png", 0, -25}
		end
		
		local temp = KNBtn:new(SCENECOMMON,{"skill_frame2.png"}, 40 + (i - 1) * 90, 150, {
			other = other,
			front = front,
			text = text
		})
			
		self.mainLayer:addChild(temp:getLayer())
		if (i == 1 and KNGuide:getStep() == 1505) or ( i == 2 and KNGuide:getStep() == 1516 ) then
			KNGuide:show( temp:getLayer() , {
				callback = function()
					KNGuide:show( splitBtn:getLayer() )
				end
			}) 
		end
	end
	
	
	local circleBig = display.newSprite(PATH..kind.."_circle_big.png")
	setAnchPos(circleBig)
	
	local outLayer = display.newLayer()
	self.mainLayer:addChild(outLayer)
	
	--外层阵法
	outLayer:addChild(circleBig)
	outLayer:setContentSize(circleBig:getContentSize())
	outLayer:ignoreAnchorPointForPosition(false)
	setAnchPos(outLayer,240, display.top-270, 0.5, 0.5)	
	
	local circleSmall = display.newSprite(PATH..kind.."_circle_small.png")
	setAnchPos(circleSmall,240, display.top-270, 0.5, 0.5)
	self.mainLayer:addChild(circleSmall)
	
	
--	outLayer:runAction(CCRepeatForever:create(CCRotateBy:create(0.3,40)))
--	circleSmall:runAction(CCRepeatForever:create(CCRotateBy:create(0.1,-40)))
	
	local items = {}
	local addSoul
	local pos 
	local num
	if kind == "equip" then
		num = 6
		pos = {
			{55, display.top-305},
			{350, display.top-305},
			{125, display.top-170},
			{300, display.top-170},
			{300, display.top-440},
			{125, display.top-440},
		}
	else
		num = 3
		local posCircle = display.newSprite(PATH.."pos_circle.png")
		setAnchPos(posCircle,240, display.top-270, 0.5, 0.5)
		self.mainLayer:addChild(posCircle)
		pos = {
			{210, display.top - 190},
			{305, display.top - 360},
			{110, display.top - 360},
		}
	end
	
	for i = 1, #pos do
		--若 已存在选则的元素
		local front,frontScale, other,text
		if self.selectItems and i <= table.nums(self.selectItems) then 
			front = getImageByType(DATA_Bag:get(kind , self.selectItems[i].."", "cid"),"s")
			frontScale = nil
			if kind ~= "pet" then
				other = {COMMONPATH.."egg_num_bg.png", 50, -8}
				text = {DATA_Bag:get(kind, self.selectItems[i].."", "lv"), 18, ccc3(255,255,255), ccp(28,-30), nil, 17}
			end
		else
			front = SCENECOMMON.."add.png"
			frontScale = {1, 14, -13}
			
		end
			
		local item = KNBtn:new(SCENECOMMON, {"skill_frame1.png","skill_frame1_press.png"}, pos[i][1], pos[i][2], {
			front = front,
			frontScale = frontScale,
			other = other,
			text = text,
			callback =function()
				local star
				if self.selectItems and table.nums(self.selectItems) > 0 then
					 star =  getConfig(kind, DATA_Bag:get(kind, self.selectItems[1], "cid"), "star")
				else
					star = self.curStar or 1
				end
				-- self:createList(kind, func, comboBox:getCurItem():getId(), num)
				self:createList(kind, func, star , num)
			end
		})
		self.mainLayer:addChild(item:getLayer(),2)
		items[i] = item
	end
	
	-- 正中央 炼魂按钮
	fenjie_btn = KNBtn:new(PATH,{"btn.png", "btn_pre.png"}, 202, display.top-307, {
		front = PATH..str.."_"..func.."_mid.png",
		callback = function()
		
			if not self.selectItems or table.nums(self.selectItems) < num then
				KNMsg.getInstance():flashShow("请选择"..num.."张同星级的卡牌")
				return false
			end
			
			local function startSplit()
				local star
				star = getConfig(kind, DATA_Bag:get(kind, self.selectItems[1], "cid"), "star")
				if kind == "general" then
					HTTP:call(30009, {star = star , generalid= string.join(self.selectItems,",") }, {
						no_loading = true,
						success_callback = function(data, stone)
							self.data = data
						end
					})
				elseif kind == "equip" then
					HTTP:call(30011, {star = star , eid= string.join(self.selectItems,",") }, {
						no_loading = true,
						success_callback = function(data)
							self.data = data
						end
					})
				end
				
				self.mask = KNMask:new({r = 255, g = 255, b = 255, opacity = 0})
				self.mainLayer:addChild(self.mask:getLayer())
				
				circleSmall:runAction(CCEaseExponentialInOut:create(CCRotateBy:create(3,-2160)))
				outLayer:runAction(getSequenceAction(CCEaseExponentialInOut:create(CCRotateBy:create(3,2160)),CCCallFunc:create(
					function()
						for i = 1, #items do
							items[i]:getLayer():runAction(getSequenceAction(CCEaseElasticIn:create(CCMoveTo:create(0.8,ccp(205,display.top-302))),CCCallFunc:create(
								function()
									self.mainLayer:removeChild(items[i]:getLayer(),true)
									--这里是按钮聚集到中央后变化成武魂的逻辑
									if not addSoul then
										addSoul = true
										
										local frames = display.newFramesWithImage(IMG_PATH.."image/scene/battle/skillAction/3904.png", 4 )
										local ani 
										ani = display.playFrames(240, display.top-275, frames, 0.15, {
											onComplete = function()
												self.mainLayer:removeChild(ani, true)
												
												local soul = display.newSprite(PATH..kind.."_icon_"..star..".png")
												setAnchPos(soul, 240, display.top-270, 0.5, 0.5)
												self.mainLayer:addChild(soul)
												
												soul:runAction(getSequenceAction(CCJumpTo:create(0.5,ccp(205, display.top-302),50,2),CCMoveTo:create(0.3,ccp(73 + (star - 1) * 100,183)),CCCallFunc:create(
													function()
														self.selectItems = {}
														 self:createSplitLayer(kind, func, comboBox:getCurItem():getId())
	--													self:createSplitLayer(kind, func)
													end)))
											end
										})
										self.mainLayer:addChild(ani)
									end
								end)))
						end
					end)))
				end
				
				--判断是否有高等级的物品
				local ask = false
				for k, v in pairs(self.selectItems) do
					if tonumber(DATA_Bag:get(kind, v, "lv")) > 1 then
						ask = true
						break
					end
				end
				if ask then
					local str 
					if kind == "general" then
						str = "英雄"
					elseif kind == "equip" then
						str = "装备"
					end
					KNMsg.getInstance():boxShow("您选择的"..str.."中存在珍贵物品，是否确认分解？ ", {
						confirmFun = function()
							startSplit()	
						end,
						cancelFun = function()
							
						end
					})	
				else
					startSplit()
				end
		end
	})
	self.mainLayer:addChild(fenjie_btn:getLayer())
	
	-- 新手引导
	if KNGuide:getStep() == 1503 then
		KNGuide:show( autoBtn:getLayer() , {
			callback = function()
				KNGuide:show( fenjie_btn:getLayer()) 
			end
		}) 
	end
	
	
	--描述
	local desc = {
		["general"] = {
			"1.三张同星英雄可炼成一个同星将魂。",
			"2.上阵和五星英雄不可炼魂。",
			"3.将魂可融合成更高星级英雄。"
		},
		["equip"] = {
			"1.六张同星装备可炼成一个同星碎片。",
			"2.已穿戴装备和五星装备不可分解。",
			"3.碎片可融合成更高星级装备。"
		}
	}
	
	for i = 1, #desc[kind] do
			local label = display.strokeLabel(desc[kind][i], 130, 340 - 30 * i, 14, ccc3(0xff,0xfb,0xd4))
		--self.mainLayer:addChild(label)
	end
	
	self.layer:addChild(self.mainLayer)
	
	self:createInfo(PATH..kind.."_split_text.png")
end
--合成界面(合卡)
function FBLayer:createMergeLayer(kind, index)
	if self.mainLayer then
		self.layer:removeChild(self.mainLayer,true)
	end
	
	self.mainLayer = display.newLayer()
	if kind == "general" then
		self.state = HEROMERGE
	elseif kind == "equip" then
		self.state = EQUIPMERGE
	end
	
	local str = (kind == "pet" and "general" or kind)

	local title = display.newSprite(COMMONPATH.."dark_bg.png")
	setAnchPos(title, 240, 425, 0.5, 0.5)
	self.layer:addChild(title)
	
	title = display.newSprite(COMMONPATH.."title_bg.png")
	setAnchPos(title, 0, display.top - title:getContentSize().height)
	self.mainLayer:addChild(title) 
	
	title = display.newSprite(COMMONPATH.."title_tail.png")
	setAnchPos(title, 0, display.top - title:getContentSize().height * 2)
	self.mainLayer:addChild(title) 
	
		
	-- title = display.newSprite(PATH..str.."_merge_text.png")
	-- setAnchPos(title, 245, display.top-35, 0.5)
	-- self.mainLayer:addChild(title)
	
	--碎片分解文字和元素栏
	local bottom = display.newSprite(PATH.."bottom.png")
	setAnchPos(bottom, 240, display.top-250, 0.5)
	self.mainLayer:addChild(bottom)
	
	bottom = display.newSprite(PATH..kind.."_split_label.png")
	setAnchPos(bottom, 20, display.top-150)
	self.mainLayer:addChild(bottom)
	
	--在合成页面去炼魂，重新拉取数据
	local btn = KNBtn:new(COMMONPATH, {"btn_ver.png", "btn_ver_pre.png"}, 410, display.top-240, {
		front = PATH..str.."_merge_btn_text.png",
		callback = function()
			local m
			if kind == "general" then
				m = "soul"
			elseif kind == "equip" then
				m = "equippieces"
			else
				m = "animalsoul"
			end
			-- HTTP:call(m, "get", {}, {
					-- success_callback = function(data)
						self.curStar = 1
						-- self.data = data
						self.tempMerge = nil
						self.selectItems = {}
						self:createSplitLayer(kind,"split")
					-- end
				-- })
		end
	})
	self.mainLayer:addChild(btn:getLayer())
	
	
	local tempLayer
	local needLabel
	local function createSelect(i)
		if tempLayer then
			self.mainLayer:removeChild(tempLayer, true)
		end
		tempLayer = display.newLayer()
		
		local other = {{PATH..kind.."_split_"..i..".png", 0, -25}}
		local front = PATH..kind.."_icon_"..i..".png"
		local temp = KNBtn:new(SCENECOMMON,{"skill_frame2.png"}, 90, display.top-420, {
			id = i,
			other = other,
			noHide = true,
			front = front,
			priority = -131,
--			text = {6, 18, ccc3(255,255,255), ccp(30,28),nil,15 },
			callback = function()
			end
		})
		tempLayer:addChild(temp:getLayer())
		local result
		if self.tempMerge then
	
			result = KNBtn:new(nil, {getImageByType(DATA_Bag:get(kind, self.tempMerge, "cid"), "b")}, 0, 0, {
				callback = function()
						local data 
						data = DATA_Bag:get(kind, tonumber(self.tempMerge))
						
						data["id"] = self.tempMerge
--						
						pushScene("detail" , {
						detail = getCidType(data["cid"]),
						data = data
					})
				end
			})	
			if kind == "general" or kind == "pet" then
				setAnchPos(result:getLayer(), 240, display.top-560)
			else
				setAnchPos(result:getLayer(), 280, display.top-500)
			end
			tempLayer:addChild(result:getLayer())
		else
			local str
			if kind ~= "general" then
				if kind == "equip" then
					str = "六个相同星级的碎片可以生成更高一级的装备"
				else
					str = "三个相同星级的兽魂可以生成更高一级的幻兽"
				end
				result = display.strokeLabel(str , 310, display.top-450, 20, ccc3(0x2c, 0, 0), nil, nil, {
					dimensions_width = 100,
					dimensions_height = 120,
				})
				tempLayer:addChild(result)
			end
		end
		
		self.mainLayer:addChild(tempLayer)
	end
	
 	local optBtn   			-- 合成按钮
 	local chooseGroup = KNRadioGroup:new()
	local needMore
	for i = 1, #self.data do
		local other, front, text

		other = {{PATH..kind.."_split_"..i..".png", 0, -25},{COMMONPATH.."egg_num_bg.png", 50, 50}}
		front = PATH..kind.."_icon_"..i..".png"
		text = {self.data[i]["num"],18, ccc3(255,255,255), ccp(30,28),nil,17 }

		local temp = KNBtn:new(SCENECOMMON,{"skill_frame2.png", "select1.png"}, 55 + (i - 1) * 90, display.top-220, {
			id = i,
			other = other,
			noHide = true,
			front = front,
			priority = -131,
			text = text,
			callback = function()
				if kind == "general" then
					if needMore then
						self.mainLayer:removeChild(needMore, true)
					end
					needMore = display.newSprite(PATH..kind.."_need_6_text.png")
					setAnchPos(needMore, 300, display.top-625)
					self.mainLayer:addChild(needMore)
					if needLabel then
						self.mainLayer:removeChild(needLabel , true)
					end
					needLabel  = display.strokeLabel("三个相同星级的将魂可以生成更高一级的英雄~,定向合卡翻倍!", 310, display.top-470, 20, ccc3(0x2c, 0, 0), nil, nil, {
						dimensions_width = 100,
						dimensions_height = 150,
					})
					self.mainLayer:addChild(needLabel )
				end
				createSelect(i)
			end
		}, chooseGroup)
		self.mainLayer:addChild(temp:getLayer())
	end
	
	local smallBg = display.newSprite(IMG_PATH.."image/scene/byexp/text_bg.png")
	setAnchPos(smallBg, 30, display.top-550)
	self.mainLayer:addChild(smallBg)
	
	smallBg = display.newSprite(PATH..kind.."_split_label.png")
	setAnchPos(smallBg, 30, display.top-310)
	self.mainLayer:addChild(smallBg)
	
	
	smallBg = display.newSprite(COMMONPATH.."next.png")
	setAnchPos(smallBg, 230, display.top-450)
	self.mainLayer:addChild(smallBg)
	
	
	smallBg = display.newSprite(IMG_PATH.."image/scene/byexp/text_bg.png")
	setAnchPos(smallBg, 270, display.top-550)
	self.mainLayer:addChild(smallBg)
	
	smallBg = display.newSprite(PATH..kind.."_merge_label.png")
	setAnchPos(smallBg, 270, display.top-310)
	self.mainLayer:addChild(smallBg)
	
	--这里根据不同的类型生成合卡按钮
	local bx,by, front
	if kind == "general" then
		bx = 50	
		by = display.top-600
		front = PATH.."random_merge.png"
		smallBg = display.newSprite(PATH..kind.."_need_text.png")
		setAnchPos(smallBg, 70, display.top-590)
		self.mainLayer:addChild(smallBg)
	else
		bx = 170
		by = display.top-600
		front = PATH..str.."_merge.png"
		
		smallBg = display.newSprite(PATH..kind.."_need_text.png")
		setAnchPos(smallBg, 70, display.top-500)
		self.mainLayer:addChild(smallBg)
	end
	
	local max = 3
	if kind == "equip" then
		max = 6
	end
	
	optBtn = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, bx, by, {
		front = front,
		callback = function()
			if self.data[chooseGroup:getId()]["num"] < max then
				local tip
				if kind == "general" then
					tip = "对不起，将魂不足无法融合"
				else
					tip = "对不起，碎片不足无法融合"
				end
				KNMsg.getInstance():flashShow(tip)

				if KNGuide:getStep() == 1508 then
					GLOBAL_INFOLAYER:refreshBtn()
				end
							
				return false
			end

			if isBagFull() then
				return false
			end
			
			self.selectItems = chooseGroup:getId()
			local star = self.selectItems 
			local m
			if kind == "general" then
				m = 30008
			elseif kind == "equip" then
				m = 30010
			end
			
			HTTP:call(m, {id = star}, {
					success_callback = function(data, cid)
						self.data = data
						self.tempMerge = cid
						self:createMergeLayer(kind, star)
--						KNMsg.getInstance():flashShow("融合成功，获得"..(star + 1).."星级英雄！~")
						local card_popup = KNCardpopup:new(DATA_Bag:get(kind, cid, "cid") , function()
							if KNGuide:getStep() == 1508 then
								GLOBAL_INFOLAYER:refreshBtn()
							end
						end , {	
							init_x =  100,
							init_y = -50,
							end_x =  100,
							end_y =  -50,})
						display.getRunningScene():addChild(card_popup:play())
					end})
		end
	})
	self.mainLayer:addChild(optBtn:getLayer())
	
	if KNGuide:getStep() == 1507 then
		KNGuide:show( optBtn:getLayer() , {
			remove = true
		}) 
	end		
	
	--定向合卡，只有英雄有
	if kind == "general" then
	
		local directBtn = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, 285, display.top-600, {
			front = PATH.."direct_merge.png",
			callback = function()
				if isBagFull() then
					return false
				end
--				if self.data[chooseGroup:getId()]["num"] > 3 * (chooseGroup:getId() + 1) then
					self:directList(chooseGroup:getId() + 1, kind)
--				else
--					KNMsg.getInstance():flashShow("将魂数量不足"..(3 * (chooseGroup:getId() + 1)).."个，无法合成！")
--				end
			end
		})
		self.mainLayer:addChild(directBtn:getLayer())
	end
	
	self.layer:addChild(self.mainLayer)
	
	chooseGroup:chooseByIndex(index or 1, true)

	self:createInfo(PATH..kind.."_merge_text.png",function()
		self:createMain("hero")
	end)
end
--定向合卡
function FBLayer:directList(star, kind)
	if self.mainLayer then
		self.layer:removeChild(self.mainLayer,true)
	end
	direc = true
	self.mainLayer = display.newLayer()
	self.curStar = star
	
	-- if kind =='equip' then
	-- 	self.state = EQUIPLIST
	-- else
	-- 	self.state = HEROLIST
	-- end
	
	local title = display.newSprite(COMMONPATH.."title_bg.png")
	setAnchPos(title, 0, display.top - title:getContentSize().height)
	self.mainLayer:addChild(title, 1) 
	
	title = display.newSprite(COMMONPATH.."title_tail.png")
	setAnchPos(title, 0, display.top - title:getContentSize().height * 2)
	self.mainLayer:addChild(title, 1) 
	
		
	-- title = display.newSprite(PATH..(star and "direct_text.png" or "info_title.png"))
	-- setAnchPos(title, 245, display.top-135, 0.5)
	-- self.mainLayer:addChild(title, 1)
	
	title = display.newSprite(COMMONPATH.."tab_line.png")
	setAnchPos(title, 240, display.top-166, 0.5)
	self.mainLayer:addChild(title, 1)
	
	local group = KNRadioGroup:new()
	for i = 1, star and 1 or 5 do
		local curStar = star and star or i
		local btn = KNBtn:new(COMMONPATH.."tab/", {"tab_star_normal.png","tab_star_select.png"},10 + (i - 1) * 90 , display.top-165, {
			id = curStar,
			front =  {COMMONPATH.."tab/".."tab_star"..curStar..".png",COMMONPATH.."tab/".."tab_star"..curStar.."_select.png"},
			callback = function()
				if kind == "equip" then 
					self:equip_directScroll(curStar)
				else
					self:directScroll(curStar, star and true or false, star and true or false)
				end
			end
		}, group)
		self.mainLayer:addChild(btn:getLayer(), -1)
	end
--	btn:call()
	group:chooseByIndex(1, true)

	self.layer:addChild(self.mainLayer)	

	local titlestr
	if kind == "equip" then
		titlestr = IMG_PATH .. "image/scene/goldequip/equip_book.png"
	else
		titlestr = PATH..(star and "direct_text.png" or "info_title.png")
	end
	self:createInfo(titlestr)
end

function FBLayer:equip_directScroll(star) 
	if self.scroll then 
		self.mainLayer:removeChild(self.scroll:getLayer(),true)
	end 
	self.scroll = KNScrollView:new(0, 100, 480, display.top-270, 5) 
	local border_pos = { -13 , -10 } 
	if CHANNEL_GROUP == "tmsj" or CHANNEL_GROUP == "td" then 
		border_pos = { -13 , - 15 } 
	end 
	local function createItem(items)
		local layer = display.newLayer() 
		local bg = display.newSprite(COMMONPATH.."item_bg.png") 
		setAnchPos(bg) 
		layer:setContentSize(bg:getContentSize())
		layer:addChild(bg) 
		if items then 
			local n = 1 
			for k, v in pairs(getSortList(items)) do 
				k = v v = items[v] 
				local btn = KNBtn:new(SCENECOMMON, {"box.png"}, 10 + 90 * (n - 1), 30, 
				{ front = getImageByType(k,"s"), frontScale = {1, -3, 4}, scale = true, 
				parent = self.scroll, other = {COMMONPATH.."star_border/border_"..getConfig("equip", k,"star")..".png", 
				border_pos[1], border_pos[2]}, text = {v["name"], 16, ccc3(0x2c, 0, 0), ccp(0, -50)}, 
				callback = function() 
					local data = v 
					data["tujian"] = "1" 
					v["cid"] = k 
					pushScene("detail" , { detail = "equip", data = data}) 
				end }) 
				layer:addChild(btn:getLayer()) 
				n = n + 1 
			end 
			return layer 
		end 
	end 
	local data = getConfigTable("equip" , {star = star}) 
	local count = 0 
	local members = {} 
	local keyList = getSortList(data) 
	for k, v in pairs(keyList) do
		count = count + 1 
		members[v] = data[v] 
		if count % 5 == 0 or count == table.nums(data) then 
			self.scroll:addChild(createItem(members)) 
			members = {} 
		end 
	end 
	self.scroll:alignCenter() 
	self.mainLayer:addChild(self.scroll:getLayer()) 
end

--英雄定向合成
--star 当前星级
--之前有传入star noChoose = true
--之前有传入star except = true
function FBLayer:directScroll(star, noChoose, except)
	if self.scroll then
		self.mainLayer:removeChild(self.scroll:getLayer(),true)
	 end
	 self.scroll = KNScrollView:new(0, 100, 480, display.top-270, 5)
	 
	 local function createItem(items)
		 local layer = display.newLayer()
		 local bg = display.newSprite(COMMONPATH.."item_bg.png")
		 setAnchPos(bg)
		 layer:setContentSize(bg:getContentSize())
		 layer:addChild(bg)
		 if items then
			 local n = 1
			 for k, v in pairs(items) do
				 local btn = KNBtn:new(SCENECOMMON, {"box.png"}, 10 + 90 * (n - 1), 30, {
					 front = getImageByType(k,"s"),
					 frontScale = {1, -3, 4},
					 scale = true,
			   		parent = self.scroll,
					 text = {v["name"], 20, ccc3(0x2c, 0, 0), ccp(0, -50)},
					 callback = function()
						 local data = v
						 v["cid"] = k
					 	pushScene("detail" , {
							detail = "general",
							toChoose = noChoose,
							chooseCallback = function()
								if getConfig("general", k, "special") == 1 then
									KNMsg.getInstance():flashShow("该英雄为特殊英雄,不能定向合成,只可在VIP礼包或活动获得!")
								else
									HTTP:call(30012,{g_cid = k},{
										success_callback = function(data, id)
											self.data = data
											self.tempMerge = id
											popScene()
											self:createMergeLayer("general", star - 1)
											local card_popup = KNCardpopup:new(k, function() end , {	
												init_x =  100,
												init_y = -50,
												end_x =  100,
												end_y =  -50,})
												self.mainLayer:runAction(getSequenceAction(CCDelayTime:create(0.1),CCCallFunc:create(function()
												display.getRunningScene():addChild(card_popup:play())
											end)))
										end
									})
								end
							 end,
							data = data})
					 end
				 })
				 layer:addChild(btn:getLayer())
				 n = n + 1
			 end
		else  --特殊英雄提示
			local function colorName(str, x, y)
				return createLabel({str = str, color = ccc3(255, 0, 0), size = 14, x = x, y = y, width = 300})
			end
			
			layer:addChild(colorName("蚩尤、黄帝、广成子、文殊", 20, 80))
			layer:addChild(createLabel({str = "这四个超5星英雄不可通过定向合成:", size = 14, x =190, y = 80, width = 300}))
			
			layer:addChild(colorName("罗睺、计都", 20, 50))
			layer:addChild(createLabel({str = "分别可在VIP5、VIP6特权礼包中获得", size = 14, x = 100, y = 50, width = 300}))
			
			layer:addChild(colorName("云中子、杨戬", 20, 20))
			layer:addChild(createLabel({str = "可在更高级V特权礼包中获得", size = 14, x = 110, y = 20, width = 300}))
		
			
			local btn = KNBtn:new(COMMONPATH, {"long_btn.png", "long_btn_pre.png"}, 320, 8, {
				front = COMMONPATH.."vip_privilege.png",
				callback = function()
					--switchScene("vip")
					pushScene("vip")
				end
			})
			layer:addChild(btn:getLayer())
--			千仞雪、 阿呆、 唐三、雷翔这四个超5星英雄不可通过定向合成:


--			雷翔和唐三分别可在VIP5、VIP6特权礼包中获得

--			 千仞雪和阿呆可在更高级V特权礼包中获得 
		end
		 return layer
	 end
	 testlog("FBLayer 合卡 star_except",star,except)
	 local data = getConfigTable("hero", {star = star, special = (except and 0 or nil)})
	 
	 local count = 0
	 local members = {}
	 for k, v in pairs(data) do
		count = count + 1
		members[k] = v
			
		if count % 5 == 0 or count == table.nums(data) then
			self.scroll:addChild(createItem(members))
			members = {}
		end
	 end
	 
	 if except then
		 --创建特殊英雄提示信息
		 self.scroll:addChild(createItem())
	 end
	 self.scroll:alignCenter()
	 
	 self.mainLayer:addChild(self.scroll:getLayer())
end

function FBLayer:getItems(kind, star)
		 
	local general = DATA_Bag:get(kind) or {}
	local result = {}
	--筛选满足条件的卡牌，给定星级，且未上阵
	for k, v in pairs(general) do
		if kind == "pet" then
			if getConfig(kind, DATA_Bag:get(kind , k, "cid"), "star") ==  star and 
				DATA_Pet:getFighting() ~= tonumber(k) then
					result[k] = v
			end	
		else	
			local on
			if kind == "equip" then
				on = DATA_Formation:checkEquip(tonumber(k),kind)
			else
				on = DATA_Formation:checkIsExist(tonumber(k))
			end
			if getConfig(kind, DATA_Bag:get(kind , k, "cid"), "star") ==  star and 
				not on then 
					result[k] = v
			end
		end
	end
	return result
end
--选择英雄卡牌列表
function FBLayer:createList(kind, func, star, max)
	if self.mainLayer then
		self.layer:removeChild(self.mainLayer,true)
	end
	
	self.mainLayer = display.newLayer()

	if kind == "general" then	
		self.state = HEROLIST
	elseif kind == "equip" then
		self.state = EQUIPLIST
	else
	
	end
	
	local str = (kind == "pet" and "general" or kind)
	
	if not self.selectItems then
		self.selectItems = {}
	end
		
	local title = display.newSprite(COMMONPATH.."dark_bg.png")
	setAnchPos(title, 240, display.cy, 0.5, 0.5)
		self.layer:addChild(title)
	
	-- title = display.newSprite(COMMONPATH.."title_bg.png")
	-- setAnchPos(title, 0, display.top - title:getContentSize().height)
	-- self.mainLayer:addChild(title) 
	
	-- title = display.newSprite(COMMONPATH.."title_tail.png")
	-- setAnchPos(title, 0, display.top - title:getContentSize().height * 2)
	-- self.mainLayer:addChild(title) 
	
		
	-- title = display.newSprite(PATH..str.."_"..func.."_text.png")
	-- setAnchPos(title, 245, 765, 0.5)
	-- self.mainLayer:addChild(title)
	
	-- title = display.newSprite(COMMONPATH.."tab_line.png")
	-- setAnchPos(title, 240, 690, 0.5)
	-- self.mainLayer:addChild(title)
	
	local first = true
	local result 
	local group = KNRadioGroup:new()
	for i = 1, 4 do
		local btn = KNBtn:new(COMMONPATH.."tab/", {"tab_star_normal.png", "tab_star_select.png"},
			20 + (i - 1) * 90, display.height - 150, {
			id = i,
			front = {COMMONPATH.."tab/".."tab_star"..i..".png", COMMONPATH.."tab/".."tab_star"..i.."_select.png"},
			callback = function()
				self.curStar = i
				if first then
					first = false
				else
					self.selectItems = {}
				end
				result = self:createScroll(kind,i, max)
			end
			
		},group)
		self.mainLayer:addChild(btn:getLayer(), -1)
	end
	group:chooseByIndex(star or 1, true)
	
	local okBtn = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, 280, 105, {
		front = COMMONPATH.."ok.png",
		callback = function()
			if table.nums(self.selectItems) < max then
				KNMsg.getInstance():flashShow("请选则"..max.."张同星级的卡牌")
				return false
			end
			self.scroll = nil
			self:createSplitLayer(kind, func, self.curStar)
		end
	})
	
	local onKey = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, 70, 105, {
		front  = COMMONPATH.."onekey.png",
		callback  = function()
			local needNum = max - table.nums(self.selectItems)
			for k, v in pairs(result) do
				if not table.hasValue(self.selectItems,k) then
					table.insert(self.selectItems, k)
					needNum = needNum - 1
				end
				
				if needNum == 0 then
					self:createScroll(kind,group:getId(), max)
					break
				end
			end
			
			--当元素数量不够时
			if needNum > 0 then
				self:createScroll(kind, group:getId(), max)
			end
		end
	})
	
	self.mainLayer:addChild(okBtn:getLayer())
	self.mainLayer:addChild(onKey:getLayer())
	
		
	self.layer:addChild(self.mainLayer)
end

function FBLayer:createScroll(kind,level, num)
	if self.scroll then
		self.mainLayer:removeChild(self.scroll:getLayer(),true)
	 end

	--self.scroll = KNScrollView:new(0, 150, 480, display.top -250, 5)
	self.scroll = KNScrollView:new(0, 148, 480, display.height -295, 5)
	
	local result = self:getItems(kind, level)
	
	if table.nums(result) > 0 then
		if self.empty then
			self.mainLayer:removeChild(self.empty, true)
			self.empty = nil
		end
		for k, v in pairs(result) do
			--判断是否已选 则
			local checked
			if type(self.selectItems) == "table" and table.nums(self.selectItems) > 0 then
				for sk, sv in pairs(self.selectItems) do
					if sv == k then
						checked = true
						break
					end
				end
			end
			
			local item
			item = ListItem:new(kind, k,{
				parent = self.scroll,
				check = true,
				checked = checked,
				checkBoxOpt = function()
					if item:isSelect() then
						if table.nums(self.selectItems) < num then
							table.insert(self.selectItems, k)
						else
							KNMsg:getInstance():flashShow("最多选择"..num.."张卡牌融合")
							return false
						end
					else
						for sk, sv in pairs(self.selectItems) do
							if sv == k then
								table.remove(self.selectItems,sk)
								break
							end
						end
					end
				end
			})
			self.scroll:addChild(item:getLayer(), item)
		end	 
		self.scroll:alignCenter(true)
		self.mainLayer:addChild(self.scroll:getLayer())
	else
		self.empty = display.newSprite(COMMONPATH.."empty.png")
		setAnchPos(self.empty,240, 425, 0.5,0.5)
		self.mainLayer:addChild(self.empty)
	end
	 
	 return result
end

return FBLayer
