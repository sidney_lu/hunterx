local CREATE,ALL,TIME,STATE,GOLD = 0,1,2,3,4
local PATH = IMG_PATH.."image/scene/strengthen/"
local SCENECOMMON = IMG_PATH.."image/scene/common/"
--[[英雄模块，首页点击英雄图标进入]]
local InfoLayer = requires("Scene.common.infolayer")--requires "Scene/common/infolayer"
local KNBtn = requires("Common.KNBtn")--requires "Common/KNBtn"
local KNRadioGroup = requires("Common.KNRadioGroup")--requires "Common/KNRadioGroup"
local ProConfig = requires("Config.CombatAttributes")
local KNSlider = requires("Common.KNSlider")
local Money = requires("Config.EquipmentStrengthenMoneyCost")
local Value = requires("Config.EquipmentUpgradeValue")
local limit = requires("Config.EquipmentStrengthenMaxLevel")

local StrengthenLayer = {
	baseLayer,
	layer,
	infoLayer,
	tabGroup,  --标签栏组
	iconGroup, --栏目中子项按钮选择组
	iconScroll, --按钮滑动组件
	timeText,  --需要时间
	fireState, --火焰
	gold,         --升级需要金币
	strenInfo,  --强化的信息显示层
	furnaceLayer, -- 火炉层
	scheduleHandler,
	startTime,
	account,   --当前账户
	slider,
	num ,     --升级数
	money,  --钱数
	next,
	value
	
	
}

function StrengthenLayer:new(param)
	local this = {}
	setmetatable(this,self)
	self.__index  = self

	local targetID = param.targetID

	this.baseLayer = display.newLayer()
	this.layer = display.newLayer()
	this.num = 1
	local info_layer = InfoLayer:new("strength", 0, {tail_hide = true, title_text = PATH.."strong.png", closeCallback = function()
			popScene()
			if param.main then
				param.main[1]:createLayer(param.main[2], param.main[3])
			end
		 end})


	local bg = display.newSprite(SCENECOMMON.."bg.png")
	local title = display.newSprite(SCENECOMMON.."cbox.png")    -- 装备滑动栏背景框
	local tabBg = display.newSprite(COMMONPATH.."dark_bg.png")
	local titleBg = display.newSprite(PATH.."stren_title.png")
	local curBg = display.newSprite(PATH.."cur_bg.png")
	local upBg = display.newSprite(PATH.."up_bg.png")
	local next = display.newSprite(COMMONPATH.."next.png")
	
	setAnchPos(bg)
	setAnchPos(title,240,display.top-235,0.5) 
	setAnchPos(tabBg, 0,display.cy, 0,0.5)
	setAnchPos(titleBg,240,105,0.5)
	setAnchPos(curBg,355,260,0.5)
	setAnchPos(upBg,40,260)
	setAnchPos(next, 230, 310)

	this.layer:addChild(tabBg)
	--创建标签栏
	local tab = {
		{"weapon",10},
		{"defender",100},
		{"shoe",190},
		{"jewelry",280},
	}
	this.tabGroup = KNRadioGroup:new()
	for i,v in pairs(tab) do
		local temp = KNBtn:new(COMMONPATH.."tab/",{"tab_star_normal.png","tab_star_select.png"},v[2],display.top-150,{
				id = v[1],
				front = {COMMONPATH.."tab_strength/".."tab_"..v[1].."_normal.png",COMMONPATH.."tab_strength/".."tab_"..v[1]..".png"},
				callback=
				function()
					this.layer:removeChild(this.strenInfo,true)
					this:createTab(v[1])
				end
			},this.tabGroup)
		this.layer:addChild(temp:getLayer(), 1)
	end
	this.layer:addChild(titleBg)
	this.layer:addChild(title)
	this.layer:addChild(curBg)
	this.layer:addChild(upBg)
	this.layer:addChild(next)
	
	curBg = display.newSprite(PATH.."cur_text.png")
	setAnchPos(curBg, 80, 320)
	this.layer:addChild(curBg)
	
	curBg = display.newSprite(PATH.."up_text.png")
	setAnchPos(curBg, 320, 320)
	this.layer:addChild(curBg)
	-- 佩戴英雄
	curBg = display.newSprite(PATH.."dress_text.png")
	setAnchPos(curBg, 50,display.top-260)
	this.layer:addChild(curBg)
	
	local line = display.newSprite(COMMONPATH.."tab_line.png")
	setAnchPos(line, 5, display.top-155)
	this.layer:addChild(line)

	-- 强化按钮
	local strenBtn = KNBtn:new(COMMONPATH,{"btn_bg_red.png", "btn_bg_red_pre.png"},255,125,{
			scale = true,
			front = COMMONPATH.."strengthen.png",
			callback = function()
				if this.iconGroup:getChooseBtn() then
					--temp
					if tonumber(DATA_User:get("silver")) < this.money then
						KNMsg.getInstance():flashShow("对不起，您的银两不足，无法强化")
						return
					end
					local equip_id = this.iconGroup:getId()
					local lv_double = DATA_User:get("lv") * 2
					local equip_lv = DATA_Bag:get("equip", equip_id, "lv")
					if lv_double-equip_lv == 0 then
						KNMsg.getInstance():flashShow("装备强化等级最高为人物等级的2倍!")
						return
					end
					HTTP:call(20013,{id = equip_id, uplv = this.num} , {
						success_callback = function()
							KNMsg.getInstance():flashShow("强化成功")
							this.num = 1
							this:createInfo(equip_id)
						end
					})
				else
					KNMsg.getInstance():flashShow("请选择需要强化的装备")
				end
			end
		})
	this.layer:addChild(strenBtn:getLayer())

	-- 新手引导(3次)
	if KNGuide:getStep() == 205 then
		KNGuide:show( strenBtn:getLayer() , {
			callback = function()
				KNGuide:show( strenBtn:getLayer() , {
					callback = function()
						KNGuide:show( strenBtn:getLayer() , {
							callback = function()
								info_layer:refreshBtn()
							end
						})
					end
				})
			end
		})
	end
	

	local rest = display.newSprite(PATH.."rest.png")
	setAnchPos(rest,60,150)
	this.layer:addChild(rest)
	
	local icon = display.newSprite(COMMONPATH.."silver.png")
	setAnchPos(icon,115,150)
	this.layer:addChild(icon)
	
	this.account = display.strokeLabel(DATA_User:get("silver").."",170,153, 20, 
				ccc3( 0x2c , 0x00 , 0x00 ) , 2 ,
				ccc3( 0x40 , 0x1d , 0x0c ))
	this.layer:addChild(this.account)
	
	local coast = display.newSprite(PATH.."coast.png")
	setAnchPos(coast,60,120)
	this.layer:addChild(coast)
	
	icon = display.newSprite(COMMONPATH.."silver.png")
	setAnchPos(icon,115,118)
	this.layer:addChild(icon)
	
	-- 装备背景圆圈
	icon = display.newSprite(COMMONPATH.."circle.png")
	setAnchPos(icon,240,display.cy-35, 0.5)
	this.layer:addChild(icon)
	
	this.tabGroup:chooseById(DATA_Bag:get("equip",targetID,"type"),true)
	this.iconGroup:chooseById(targetID , true)
--	this:createFurnace(CREATE)
	
--划动强化条	
	--增减按钮
	local addBtn = KNBtn:new(COMMONPATH,{"next_big.png"} , 365 , 185, {
		scale = true,
		priority = -130,
		callback = function()
			if this.num < 10 then
				this.num = this.num + 1
				this.slider:setValue(this.num)
			end
		end
	})
	
	local minusBtn = KNBtn:new(COMMONPATH,{"next_big.png"} ,80 , 185 ,{
		scale = true,
		priority = -130,
		callback = function()
			if this.num > 1 then
				this.num = this.num - 1
				this.slider:setValue(this.num)
			end
		end
	})
	minusBtn:setFlip(true)
	
	addBtn:getLayer():setScale( 0.9 )
	minusBtn:getLayer():setScale( 0.9 )
	this.layer:addChild(addBtn:getLayer())
	this.layer:addChild(minusBtn:getLayer())
	
	this.baseLayer:addChild(bg)
	this.baseLayer:addChild(info_layer:getLayer(),1)
	this.baseLayer:addChild(this.layer)	
	return this
end

	--每一栏信息
 function StrengthenLayer:createTab(type)
	if self.infoLayer then
		self.layer:removeChild(self.infoLayer,true)
	end
	self.infoLayer = display.newLayer()
	self.iconGroup = KNRadioGroup:new()
	self.iconScroll = KNScrollView:new(60,display.top-243,360,100,10,true,0,{turnBtn=COMMONPATH.."next.png"})
	
	--装备排序规则
	local function equipRule(l, r)
		local lValue = DATA_Formation:checkEquip(tonumber(l),"equip")
		local rValue = DATA_Formation:checkEquip(tonumber(r),"equip")
		
		--通过武将的id获取其所在阵位加权处理，第一位权值最大
		_,lValue = DATA_Formation:checkIsExist(tonumber(lValue))
		_,rValue = DATA_Formation:checkIsExist(tonumber(rValue))
		
		lValue = (9 - (lValue or 9)) * 10000
		rValue = (9 - (rValue or 9)) * 10000
		local lcid = DATA_Bag:get("equip",l,"cid")
		local rcid = DATA_Bag:get("equip",r,"cid")
		--按照星级排序,同星级按等 级排序
		lValue = lValue + getConfig("equip", lcid, "star") * 500 + DATA_Bag:get("equip", l, "lv")
		rValue = rValue + getConfig("equip", rcid, "star") * 500 + DATA_Bag:get("equip", r, "lv")
		
		return lValue > rValue
	end
	
	local items = DATA_Bag:getTable("equip", type)
	local result = getSortList(items, equipRule)
	local pos = 0 --元素所在的位置
	local temp,state
	for i,v in pairs(result) do
		pos = pos + 1
		local p = pos --需要新定义一个变量，记录当前位置

		v = tonumber(v)
		temp = KNBtn:new(SCENECOMMON,{"skill_frame1.png", "select1.png"},0,0,
			{
				id = v,
				upSelect = true ,
				selectZOrder = 12,
				parent = self.iconScroll,
				front = getImageByType(DATA_Bag:get("equip", v, "cid"),"s"),
				noHide = true,
				callback = function()
					self.num = 1
					self:createInfo(v)
					self.iconScroll:scrollTo(v)
				end,
			},self.iconGroup)
		
		self.iconScroll:addChild(temp:getLayer(),temp)
	end

	if pos == 0 then
		self.iconGroup:cancelChoose()
	else
		self.iconGroup:chooseByIndex(1 , true)
	end	
	self.iconScroll:alignCenter()
	self.infoLayer:addChild(self.iconScroll:getLayer())
	self.layer:addChild(self.infoLayer)
end

--每件装备的强化信息
function StrengthenLayer:createInfo(id, setText)
	-- print("####createInfo->equip id:", id)
	local cid = DATA_Bag:get("equip",id,"cid")
	local equipdata = getConfig("equip", cid)
	local star = equipdata["star"]
	local lv = DATA_Bag:get("equip", id, "lv")
	local kind = equipdata["type"]
	local init = equipdata["initial"]
	
	if self.slider then
		self.layer:removeChild(self.slider, true)
	end


	if self.strenInfo then
		self.layer:removeChild(self.strenInfo,true)
	end
	self.strenInfo = display.newLayer()
	
	local btn = KNBtn:new(SCENECOMMON,{"skill_frame1.png"},210,display.cy+52,{
		text = {{equipdata["name"].."  Lv"..DATA_Bag:get("equip",id,"lv"),14,nil,ccp(0,-50)}},
		front = getImageByType(DATA_Bag:get("equip",id,"cid"),"s")
	})
	self.strenInfo:addChild(btn:getLayer())
	
	
	local num = equipdata["star"]
	for i = 1, num do
		local star = display.newSprite(COMMONPATH.."star.png")
		star:setScale(0.8)
		setAnchPos(star, 240 - (num * 30 * 0.8) / 2 + (i - 1) * 30 * 0.8 , display.cy+10)
		self.strenInfo:addChild(star)
	end
	
	
	local name_str = "无人装备"
	local roleid = DATA_Formation:checkEquip(id, "equip")
	if roleid then
		local rolecid = DATA_Bag:get("general",roleid,"cid")
		name_str = getConfig("general", rolecid, "name")
	end
	local dressHero = display.strokeLabel(name_str, 140, display.top-260,20)
	self.strenInfo:addChild(dressHero)
	
	-- 当前属性
	local cur = display.strokeLabel("Lv"..lv.." "..ProConfig[kind]..":+"..math.round((init + (lv - 1) * Value[star][kind])),60,310, 18, 
			ccc3( 0xff , 0xfb , 0xd4 ) , 2 ,
			ccc3( 0x40 , 0x1d , 0x0c ))
	setAnchPos(cur,130, 290, 0.5)
	self.strenInfo:addChild(cur)	
	-- 强化属性
	self.next = display.strokeLabel("Lv"..(lv + self.num).." "..ProConfig[kind]..":+"..math.round((init + (lv + self.num - 1) * Value[star][kind])) ,280,310, 18, 
			ccc3( 0xff , 0xfb , 0xd4 ) , 2, 
			ccc3( 0x40 , 0x1d , 0x0c ))
	setAnchPos(self.next,360, 290, 0.5)
	self.strenInfo:addChild(self.next)	

	self.money = Money[lv + self.num][star] - Money[tonumber(lv)][star]
	self.value = display.strokeLabel(self.money,170,121, 20, 
				ccc3( 0x2c , 0x00 , 0x00 ) , 2 ,
				ccc3( 0x40 , 0x1d , 0x0c ))
	self.strenInfo:addChild(self.value)
	
	--更新账户信息
	self.account:setString(DATA_User:get("silver").."")

	self.layer:addChild(self.strenInfo)
		
	--划动条
	local num1 = limit[star]["levmax"] - lv
	local num2 = DATA_User:get("lv") * 2 - DATA_Bag:get("equip", id, "lv")
	local num2a = DATA_User:get("lv") * 2
	local num2b = DATA_Bag:get("equip", id, "lv")
	local num3 = #Money - DATA_Bag:get("equip", id, "lv")
	-- print("####num1:", num1)
	-- print("####num2:", num2, "a:", num2a, "b:", num2b)
	-- print("####num3:", num3)

	self.slider = KNSlider:new( "buy" ,  {
		x = 136 , 
		y = 215, 
		minimum = 1 , 
		maximum = math.min(10,(limit[star]["levmax"] - lv), DATA_User:get("lv") * 2 - DATA_Bag:get("equip", id, "lv"), 
			#Money - DATA_Bag:get("equip", id, "lv") ), 
		value = 1 , 
		callback  = function( _curIndex )
			-- print("###################_curIndex:", _curIndex)
			self.num = _curIndex
			if self.num == 0 then
				self.num = 1
			end
			if self.next and self.value then
				self.next:setString("Lv"..(lv + self.num).." "..ProConfig[kind]..":+"..math.round((init + (lv + self.num - 1) * Value[star][kind])))
				self.money = Money[lv + self.num][star] - Money[tonumber(lv)][star]
				self.value:setString(self.money)		
			end
		end,
		priority = -140
	})
	
		
	self.layer:addChild( self.slider )
end

function StrengthenLayer:getLayer()
	return self.baseLayer
end

return StrengthenLayer