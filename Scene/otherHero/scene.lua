collectgarbage("setpause"  ,  100)
collectgarbage("setstepmul"  ,  5000)


-- [[ 包含各种 Layer ]]
local HeroLayer = requires("Scene.otherHero.otherherolayer")



local M = {}

function M:create(args)
	local scene = display.newScene("otherhero")

	---------------插入layer---------------------
	scene:addChild(HeroLayer:new(args))
	---------------------------------------------
	return scene
end

return M
