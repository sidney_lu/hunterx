-- 发送邮件界面
local KNBtn = requires("Common.KNBtn")
local KNMask = requires("Common.KNMask")
local KNClock = requires("Common.KNClock")
local KNRadioGroup = requires( "Common.KNRadioGroup")
local oneMessage = requires( "Scene.chat.oneMessage")

local PATH = IMG_PATH .. "image/scene/chat/"
local curType , data , textfield ,  haveHornNumSp , hornNum , group , scroll , input_bg ,  send_btn , empty_sprite , toText , findPlay
local closeback
local M = {

}
function M:new( params )
	local this = {}
	setmetatable(this , self)
	self.__index = self
	TALK = this
	params = params or {}
	
	this.baseLayer = display.newLayer()
	this.inputLayer =display.newLayer()
	
	local mask
	closeback = params.callback
	-- 背景
	local bg = display.newSprite(PATH .. "talk_bg.png" , display.cx , display.cy):align(display.BOTTOM_CENTER)
	bg:setScaleY(0.5)
	this.baseLayer:addChild(bg)
	this:showChatBox()
	--关闭
	local closeBtn = KNBtn:new( PATH , { "close.png" ,"close_press.png"}, 420 , display.cy+200 ,
	{
		priority = -130,
		callback = 
		function()
			--mask:remove()
			params.callback()
		end
	}):getLayer()
	this.baseLayer:addChild( closeBtn )
	this.baseLayer:setTouchEnabled(true)
	this.baseLayer:setTouchPriority(-129)
	setAnchPos( this.baseLayer , 0 , display.height )
	transition.moveTo( this.baseLayer , { delay = 0.5 , time = 0.5 , y = 0 , easing = "BACKOUT" })
	--mask = KNMask:new( { opacity = 0 , item = this.baseLayer , priority = -129 } )
	return this.baseLayer
end

function M:showChatBox( )
	textfield = ui.newEditBox({
        image = IMG_PATH.."image/common/editbox.png",
        size = CCSize(display.width-100, 35),
        x = display.cx,
        y = display.cy+150,
        listener = function(event, editbox)
            if event == "began" then
                printf("editBox1 event began : text = %s", editbox:getText())
            elseif event == "ended" then
                printf("editBox1 event ended : %s", editbox:getText())
            elseif event == "return" then
                printf("editBox1 event return : %s", editbox:getText())
            elseif event == "changed" then
                printf("editBox1 event changed : %s", editbox:getText())
            else
                printf("EditBox event %s", tostring(event))
            end
        end
        })
	textfield:setTouchPriority(-129)
	textfield:setFontColor(ccc3(0, 0, 0))
	self.inputLayer:addChild( textfield , 10 )

	--拥有喇叭数
	haveHornNumSp = display.newSprite(PATH .. "have_horn_num.png")
	setAnchPos( haveHornNumSp , 26 , display.cy+90 )			
	self.inputLayer:addChild( haveHornNumSp )
	
	--喇叭数
	local isHorn , hornTotleNum = DATA_Bag:getTypeNum( "prop" , "horn" )
	hornNum = display.strokeLabel( hornTotleNum or 0  ,  115  , display.cy+86 , 18 , ccc3(0xff , 0xfc , 0xd3 ) , nil , nil ,
				 {
				 	 dimensions_width = 80 , 
				 	 dimensions_height = 30 , 
				 	 align = 0 
				 })
	self.inputLayer:addChild( hornNum )
	
	-- 发送输入框
	send_btn = KNBtn:new(IMG_PATH .. "image/common", {"btn_bg.png" , "btn_bg_pre.png"} , 353 , display.cy+50 , {
		priority = -130 ,
		front = IMG_PATH .. "image/common/send.png",
		scale = true,
		noHide = true,
		callback = function()
			local textvalue = textfield:getText()
			if textvalue == "" then 
				KNMsg:getInstance():flashShow("没有输入内容")
			elseif string.len(textvalue) > 135 then	--45个汉字
				KNMsg:getInstance():flashShow("输入内容过长无法发送")
			else
				if not isHorn then
					KNMsg:getInstance():flashShow("亲，你的喇叭不足，请至商城购买道具")
					return
				end
				HTTP:call(40003 , {
					content = textfield:getText()
				} , {
					success_callback = function(data)
						--isHorn , hornTotleNum = DATA_Bag:getTypeNum( "prop" , "horn" )
						--hornNum:setString( hornTotleNum )
						--self:changeStauts(false)
						closeback()
					end}
				)
			end
		end
	}):getLayer()
	self.inputLayer:addChild( send_btn )
	self.inputLayer:setTouchEnabled(true)
	self.inputLayer:setTouchPriority(-129)
	
	-- self.inputLayer:registerScriptTouchHandler(function(event,x,y)
	-- 	return true 
	-- end , true , -129)
					
	self.baseLayer:addChild( self.inputLayer )
end

return M