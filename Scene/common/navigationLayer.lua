local NavigationLayer = {}


function NavigationLayer:new(layerName , view_type , params, nobutton)

	local this = {}
	setmetatable(this , self)
	self.__index = self

	-- 首页的元素拆分开
	this.layer = display.newLayer()
	this.layer:setTag(8888)
end

--刷新标题
function InfoLayer:refreshTitle( path )
	if self.name then
		self.name:removeSelf()
		self.name = nil
	end

	
	self.name = display.newSprite( path )
	setAnchPos( self.name , 240 , display.height - self.title:getContentSize().height / 2 + 4 , 0.5 , 0.7 )
	self.layer:addChild( self.name )	
end
return NavigationLayer