-- 设置界面
local KNBtn = requires("Common.KNBtn")
local KNMask = requires("Common.KNMask")
local KNClock = requires("Common.KNClock")
local KNInputText = requires("Common.KNInputText")
local KNRadioGroup = requires( "Common.KNRadioGroup")
local oneMessage = requires( "Scene.chat.oneMessage")

local PATH = IMG_PATH .. "image/scene/chat/"
local curType , data , textfieldInput ,  haveHornNumSp , hornNum , group  , input_bg ,  send_btn , empty_sprite , toText , findPlay
local friendData = nil
local isAll = true		--好友聊天中是否显示全部
local newAddY = 20		--输入框新调整坐标
local scroll_y = 420  + newAddY	 
--local scroll_height = 340 - newAddY
local scroll_height = display.height -135 - 340 - newAddY
local delayTime = 5
local M = {
	scroll
}
--设置聊天对象
function M:setFriendData( tempData )
	if curType == "friend" then
		friendData = tempData
		isAll = false
		delayTime = 0
		if isAll ==true then
			textfieldInput:setTipString("点击输入信息")
		else
			textfieldInput:setTipString("密["..friendData.name.."]:<请输入信息>")
		end
	end
end
function M:new( params )
	local this = {}
	setmetatable(this , self)
	self.__index = self
	TALK = this --这里不确定缓存什么欧诺个
	params = params or {}
	curType = params.type or "world"
	friendData = params.friendData
	
	this.baseLayer = display.newLayer()
	this.viewLayer = display.newLayer()
	this.tabLayer = display.newLayer()
	this.inputLayer =display.newLayer()
	
	DATA_Info:setIsMsg( false )	--消除消息状动画
	
	local mask , layer ,  tableElement 
	layer = display.newLayer()
	
	this.baseLayer:addChild( layer )
	this.baseLayer:addChild( this.tabLayer )
	
	-- 背景
	layer:addChild( display.newSprite(PATH .. "talk_bg.png" , display.cx , 327):align(display.BOTTOM_CENTER) )
	this:showChatBox()
	this:setFriendData(friendData)--在chatbox 后才可以更新数据

	local startX,startY = 20 , display.height - 47
	--关闭
	local closeBtn = KNBtn:new( PATH , { "close.png" ,"close_press.png"}, 420 , startY - 7,
	{
		priority = -130,
		callback = 
		function()
			DATA_Info:setIsMsg( false )
			DATA_Info:setIsOpen()
			--Clock:removeTimeFun("talk")
			mask:remove()

			self:clean()
		end
	}):getLayer()
	layer:addChild( closeBtn )

	
	tableElement = { "world" , "gang" , "friend" }
	--local startX,startY = 20 , 767
	group = KNRadioGroup:new()
	for i = 1, #tableElement do
		local temp = KNBtn:new( PATH , {"btn_gray.png" , "btn_" .. tableElement[i] .. ".png" }  , startX , startY , {
			disableWhenChoose = true,
			priority = -130 , 
			upSelect = true,
			id = tableElement[i],
			front = PATH..tableElement[i].."_text.png" ,
			callback = function()
				
				if curType~=tableElement[i] then
					this:changeStauts( false )--不确定有什么用
					curType = tableElement[i]
					this.scroll:removeAll() --没有删除掉？？
					this:newEmptyIcon()


					this:refreshData({needLoading = false})
					this:refreshData()
					-- if table.nums(data)>0 then
					-- 	empty_sprite:removeSelf(true)
					-- 	empty_sprite = nil
					-- end
					textfieldInput:setString("")
					if curType == "friend" then
						this:showFriendsAllButton(true)
						if isAll ==true then
							textfieldInput:setTipString("点击输入信息")
						else
							textfieldInput:setTipString("密["..friendData.name.."]:<请输入信息>")
						end

						
					else
						this:showFriendsAllButton(false)
						textfieldInput:setTipString("点击输入信息")
					end
				end
			end
		},group)
		this.tabLayer:addChild(temp:getLayer())
		startX = startX + temp:getWidth() + 18
	end
	group:chooseById( curType , false )	--激活的选项
	
	--this:refreshData()
	this.baseLayer:addChild( this.viewLayer )
	this.baseLayer:addChild( this.inputLayer , 50 )

	data = nil
	-- no data here
	--but maybe the second time come in, it should have data
	if curType == "world" then
		data = DATA_Info:get_type("_G_message_talk")
	elseif curType == "gang" then
		data =DATA_Info:get_type("message_gang")
	elseif curType == "friend" then
		this:filtreFriendData()
	end 
	this:createList()
	
	setAnchPos( this.baseLayer , 0 , display.height )
	transition.moveTo( this.baseLayer , { delay = 0.5 , time = 0.5 , y = 0 , easing = "BACKOUT" })

	mask = KNMask:new( { opacity = 0 , item = this.baseLayer , priority = -129 } )
	local time_talk_loop = 0
	--clock loop init数据
	KNClock:addTimeFun( "talk_loop" , function ( ... )
		time_talk_loop = time_talk_loop + 1
		if time_talk_loop>3 then
			time_talk_loop = 0
			this:refreshData()
		end
	end )
	return mask
end

--[[needloading]]
function M:refreshData(params)
	testlog("talk.lua refreshData")
	self:setState()
	params = params or {}

	if params.needLoading==nil then
		params.needLoading = true
	end
	
	if curType == "world" then
		if params.needLoading==true then	

			local msgflag_local = DATA_Info:get_type("_G_message_talk_timeflag") or 0
			HTTP:call("message_gettalk" , {msgflag = msgflag_local } , {no_mask=true,
				success_callback = function()
					data = DATA_Info:get_type("_G_message_talk")
					--self:createList()
					local ret = self:addToList(data,DATA_Info:get_type("_G_message_talk_timeflag"))
					DATA_Info:set_type("_G_message_talk_timeflag",ret)
				end
			})
		else

			data = DATA_Info:get_type("_G_message_talk")
			local ret = self:addToList(data)
			DATA_Info:set_type("_G_message_talk_timeflag",ret)
		end
	elseif curType == "gang" then
		if params.needLoading == true then
			local chatflag_local =  DATA_Info:get_type("message_gang_timeflag") or 0
			HTTP:call("alliance_getchat" , {curflag = chatflag_local }, {no_mask=true,
				success_callback = function()
					data = DATA_Info:get_type("message_gang")
					--dump(data)

					local ret = self:addToList(data,DATA_Info:get_type("message_gang_timeflag"))
					DATA_Info:set_type("message_gang_timeflag",ret)
				end
			})
		else
			testlog("gangdata")
			dump(data)
			--data =DATA_Info:get_type("gang")
			data = DATA_Info:get_type("message_gang")
			local ret = self:addToList(data,0)
			DATA_Info:set_type("gang_timeflag",ret)
		end
	elseif curType == "friend" then
		if params.needLoading == true then
			--print("fdfsdfdfdsfsdfsdfasd")
			local touid = nil

			if friendData ~= nil then
				touid = friendData.uid
			end 
			local msgflag_local = DATA_Info:get_type("message_get_siliao_timeflag") or 0
			HTTP:call("message_get_siliao" , {touid =  touid,curflag = msgflag_local} , {no_mask=true,
				success_callback = function()
					self:filtreFriendData()--will set up data(module area)
					--self:createList()
					print(DATA_Info:get_type("message_get_siliao_timeflag"))
					local ret =self:addToList(data,DATA_Info:get_type("message_get_siliao_timeflag"))
					DATA_Info:set_type("message_get_siliao_timeflag",ret)
				end
			})
			-- local str = friendData and "我对【" .. friendData.name .."】说:" or "点击输入信息"
			-- textfieldInput:setString(str)
		else
			--print("fdfsdfdfdsfsdfsdfasd friend loading")
			self:filtreFriendData()
			local ret =self:addToList(data,0)
			DATA_Info:set_type("message_get_siliao_timeflag",ret)
		end

	end


end
function M:gangDataCell( tempData )
	local tempItem = {}
	tempItem[ #tempItem + 1 ] = tempData.title
	tempItem[ #tempItem + 1 ] = tempData.name
	tempItem[ #tempItem + 1 ] = tempData.content
	tempItem[ #tempItem + 1 ] = tempData.time or 0
	tempItem[ #tempItem + 1 ] = tempData.uid
	
	local temp = {}
	
	temp[ "msg" ] = tempItem
	
	local str = ( tempData.title ~= "" ) and "【#s#】" or "#s#"
	temp[ "template" ] = str .. ( tempData.time and  "【#s#】#s#(#s#)" or "【#s#】#s#" )
	
	return temp
end
--格式化帮派聊天数据
function M:getGangData()
	data = DATA_Info:get_type("gang")
	--data = data.chat or {}
	-- local tempData = {}
	-- for i = 1 , #data do
	-- 	tempData[ #tempData + 1 ] = self:gangDataCell(data[i])
	-- end
	-- data = tempData
end

function M:getType()
	return curType
end
-- this function is no use now
-- function M:addItem( _data , type )
-- 	if type == "gang" then
-- 		self:getGangData()
-- 	elseif type == "friend" then
-- --		_data = DATA_Info:get_type("friend")
-- --		_data = _data[#_data]
-- --		self:setFriendData()
-- 		self:filtreFriendData()
-- 		self:createList()
-- 		return 
-- 	else
-- 		data = DATA_Info:get_type("_G_message_talk")
-- 	end
	
-- 	local i = #data
-- 	if i<=1 then
-- 		self:createList()
-- 	else
-- 		i = type == "friend" and table.nums( DATA_Info:get_type("friend") ) or  #data
-- 		local one_message = oneMessage:talknew( _data , nil , {
-- 			type = curType ,
-- 			index = i,
-- 			first_line = true,
-- 			parent = scroll ,
-- 			color = ccc3( 0xfe , 0xfb , 0xd2 ) ,
-- 			backFun = function( data ) self:setFriendData( data ) end ,
-- 		})
-- 		scroll:addChild( one_message:getLayer() , one_message )
	
-- 		--实时接收到的数据添加进来后 重置滚动条
-- 		if #data > 1 then
-- 			scroll:setIndex( i , true )
-- 		end
-- 	end
-- end
--过滤好友聊天数据
function M:filtreFriendData( tempData )
	data = DATA_Info:get_type( "message_get_siliao" ) or {}
	print("filter data,isall",isAll)
	local tempData = isAll and data or {}
	if friendData then
		for key , v in pairs( data ) do
			if not isAll then
				if tonumber( v.u ) == tonumber( friendData.uid ) 
				 or tonumber(v.t) == tonumber(friendData.uid) then
					tempData[ #tempData + 1 ] = v
				end
			end
		end
		data = tempData
	end

	return data
end

function M:addToList( data ,local_flag)
	local_flag = local_flag or 0
	if data==nil then 
		return local_flag
	end
	local count = 0
	for k,v in pairs(data) do
		--in here , we expect that the data are ordered by createtime
		--local local_flag = DATA_Info:get_type("_G_message_talk_timeflag") or 0
		local curflag 
		if curType == "friend" then
			curflag = v.c
		else
			curflag = v.curflag
		end
		if curflag>local_flag then
			count = count +1
			local one_message = oneMessage:talknew( v , nil , {
				type = curType ,
				index = i,
				first_line = true,
				parent = self.scroll ,
				color = ccc3( 0xfe , 0xfb , 0xd2 ) ,
				backFun = function( data ) self:setFriendData( data ) end ,
			})
			self.scroll:addChild(one_message:getLayer() , one_message)
			--DATA_Info:set_type("_G_message_talk_timeflag",v.curflag)
			local_flag=curflag
		end
	end
	--实时接收到的数据添加进来后 重置滚动条
	if count > 0 then
		print("talk  dfdf count",count,self.scroll:getChildrenCount())
		self.scroll:setIndex( self.scroll:getChildrenCount() , true ) -- i==nil，也能重置？？

		if empty_sprite ~= nil then
			--empty_sprite:setVisible(false)
			empty_sprite:removeSelf(true)
			empty_sprite=nil
		end
	end

	

	return local_flag
end
function M:showFriendsAllButton( bol )
	self.allSingleBtn:getLayer():setVisible(bol)
end

function M:createList()
	testlog("talk.lua createList self.viewLayer",self.viewLayer)
	--local layer = display.newLayer()
	local isCreate = false
	if self.viewLayer then
		self.viewLayer:removeSelf()
		empty_sprite = nil
		self.viewLayer = nil
		self.viewLayer = display.newLayer()
		self.baseLayer:addChild( self.viewLayer )
		--self.viewLayer:addChild( layer )
		isCreate = true
	end	
	--if curType == "friend" then
		--显示当前玩家聊天记录/所有聊天记录
		--self.allSingleBtn 
		self.allSingleBtn = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" }, 310 , 770 ,
		{
			priority = -130,
			front = PATH .. ( isAll ==false and "show_all.png" or  "show_single.png" ) , 
			callback = 
			function()
				if isAll and not friendData then
					KNMsg:getInstance():flashShow("请选择说话对像！")
					return
				end
					isAll = not isAll
					if isAll ==true then
						textfieldInput:setTipString("点击输入信息")
					else
						textfieldInput:setTipString("密["..friendData.name.."]:<请输入信息>")
					end
					self:switchFriendRecords()
					-- if isAll then
					-- 	self.allSingleBtn:setFront( PATH .. "show_all.png" )
					-- else
					-- 	self.allSingleBtn:setFront( PATH .. "show_single.png" )
					-- end
					DATA_Info:set_type("message_get_siliao_timeflag",0)
					--self:filtreFriendData()

					self.scroll:removeAll()
					self:refreshData({needLoading = false})

					--self:createList()
				end
			
		})
		--layer:addChild( self.allSingleBtn:getLayer() )
		self.viewLayer:addChild( self.allSingleBtn:getLayer() )
		self.allSingleBtn:getLayer():setVisible(false)
	--end
	-- 展示滚动消息
	self:changeScrollPosition()--重要，必须执行这个，先调整scroll的位置
	testlog("展示滚动消息 height=",scroll_height,newAddY,display.height)
	local scroll = KNScrollView:new(35 , scroll_y , 420 , scroll_height , 2 , false , 0 , {priority = -130})
	self.scroll = scroll
	--layer:addChild( scroll:getLayer() )
	self.viewLayer:addChild(scroll:getLayer())
	if data~=nil and #data > 0 then
		
		for i = 1 , #data do
			local one_message = oneMessage:talknew( data[i] , nil , {
				type = curType ,
				index = i,
				first_line = true,
				parent = scroll ,
				color = ccc3( 0xfe , 0xfb , 0xd2 ) ,
				backFun = function( data ) self:setFriendData( data ) end ,
			})
			scroll:addChild(one_message:getLayer() , one_message)
		end
	
		--生成后定位
		if #data > 1 then
			scroll:setIndex(#data , true)
		end
		
		
	else
		--没有数据的时候，显示为空
		empty_sprite = display.newSprite(IMG_PATH .. "image/common/empty.png")
		setAnchPos(empty_sprite , display.cx , display.cy + 120 , 0.5 , 0.5)
		--layer:addChild( empty_sprite )
		self.viewLayer:addChild(empty_sprite)
	end
end
function M:newEmptyIcon( ... )
	if empty_sprite ==nil then
		empty_sprite = display.newSprite(IMG_PATH .. "image/common/empty.png")
		setAnchPos(empty_sprite , display.cx , display.cy + 120 , 0.5 , 0.5)
		self.viewLayer:addChild( empty_sprite )
	end
end
function M:changeScrollPosition( )
	if curType == "world" then
		scroll_y = 420  + newAddY
		scroll_height = display.height -135 - 340 - newAddY
	elseif curType == "gang" then
		scroll_y = 420  + newAddY
		scroll_height = display.height -135 - 340 - newAddY
	elseif curType == "friend" then
		scroll_y = 420 + newAddY
		--scroll_height = 340 - newAddY
		scroll_height = display.height -135 - 340 - newAddY
	end
end
--scroll_y 和scroll_height都被改变了，这个问题严重啊
function M:changeStauts( status )
	local moveLength = 0
	local friendAddY = curType == "friend" and 30 or 0 
	if status == true then
		textfieldInput:startInput()
		--scroll_y = 520
		--scroll_height = 240
		moveLength = 80
	else
		textfieldInput:stopInput( true )
		
		--scroll_y = 420 + newAddY
		--scroll_height = 340 - newAddY
		
		moveLength = 0
	end
	setAnchPos( haveHornNumSp , 26 , 331 + moveLength + newAddY )	
	setAnchPos( hornNum , 165 , 329 + moveLength + newAddY)	
	setAnchPos(send_btn , 373 , 363 + moveLength + newAddY)
	setAnchPos(input_bg , 14 , 360 + moveLength + newAddY)
	setAnchPos( textfieldInput:getLayer() , 30 , 383 + moveLength + newAddY)
	--setAnchPos( toText , 130  , 315  + moveLength + newAddY  )
	setAnchPos( findPlay:getLayer() ,13 , 315 + moveLength + newAddY  )
	--self:createList()
end
function M:showChatBox( )
	-- 切换真假输入框的状态
	textfieldInput = KNInputText:new( { width = 365 , 
									height = 28 , 
									size = 20 , 
									defStr = "点击输入信息" , 
									existStr = nil ,
									defColor = ccc3( 0x4d , 0x15 , 0x15 ) , 
									inputColor = ccc3( 0x4d , 0x15 , 0x15 ) 
									} )
	setAnchPos( textfieldInput:getLayer() , 30  , 383 + newAddY )
	self.inputLayer:addChild( textfieldInput:getLayer() , 10 )

	-- 真假输入框的背景
	input_bg =  KNBtn:new(PATH, {"talk_input_bg.png"} , 14 , 360 + newAddY  , {
		priority = -130 ,  
		callback = function()
			self:changeStauts(true)
		end
		}):getLayer()
	self.inputLayer:addChild(input_bg)
	--说话提示文字  --吕玮注释
	--[[toText = display.strokeLabel( friendData and "我对【" .. friendData.name .."】说:" or ""  ,  130  , 315  + newAddY , 20 , ccc3(0xff , 0xfc , 0xd3 ) , nil , nil ,
				 {
					 dimensions_width = 200 , 
					 dimensions_height = 30 , 
					 align = 0 
				})]]--
	--self.inputLayer:addChild( toText )
	--查找玩家
	findPlay = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" } , 13 , 315 + newAddY , {
							priority = -131 , 
							front = IMG_PATH .. "image/scene/friend/find_player_text.png" ,
							callback = function()
								local friendLayer = requires( "Scene.friend.friendLayer")
								friendLayer:findFriend( { type = "talk" , 
									backFun = function( tempData ) 
										print("talk.lua","setFriendData")
										-- 获取到数据
										self:setFriendData( tempData )
									end ,
									packFun = function (  ) 
										print("talk.lua","refresh refresh refresh")
										isAll = false
										self:switchFriendRecords()
										self.scroll:removeAll()


										self:refreshData({needLoading=false})
										textfieldInput:setTipString("密["..friendData.name.."]:<请输入信息>")
										textfieldInput:setString("")
									end} )
							end
							})
	self.inputLayer:addChild( findPlay:getLayer() )

	--拥有喇叭数
	haveHornNumSp = display.newSprite(PATH .. "have_horn_num.png")
	setAnchPos( haveHornNumSp , 26 , 331 + newAddY )			
	self.inputLayer:addChild( haveHornNumSp )
	
	--喇叭数
	local isHorn , hornTotleNum = DATA_Bag:getTypeNum( "prop" , "horn" )
	hornNum = display.strokeLabel( hornTotleNum   or 0  ,  165  , 329  + newAddY , 18 , ccc3(0xff , 0xfc , 0xd3 ) , nil , nil ,
				 {
					 dimensions_width = 80 , 
					 dimensions_height = 30 , 
					 align = 0 
				 })

	self.inputLayer:addChild( hornNum )
	
	local function delayFun()
		delayTime = delayTime - 1
		if delayTime <= 0 then
			KNClock:removeTimeFun( "talk" )
			delayeTime = 0
		end
	end
	
	-- 发送输入框
	send_btn = KNBtn:new(IMG_PATH .. "image/common", {"btn_bg.png" , "btn_bg_pre.png"} , 373 , 363 + newAddY , {
		priority = -130 ,
		front = IMG_PATH .. "image/common/send.png",
		scale = true,
		noHide = true,
		callback = function()
			if KNClock:getKeyIsExist("talk") then
				if delayTime ~= 0 then
					KNMsg:getInstance():flashShow("亲，你说话太快了，请隔5秒再发送哦。")
					return
				end
			else
				delayTime = 1
				KNClock:addTimeFun( "talk" , delayFun )
			end
			if not friendData and curType == "friend" then 
				--KNMsg:getInstance():flashShow("请选择说话对象!")
				KNMsg:getInstance():flashShow("请先查找玩家!")
				return
			end
			if textfieldInput:getString() == "" or textfieldInput:getString() == "点击输入信息"  then 
				KNMsg:getInstance():flashShow("没有输入内容")
			--elseif textfieldInput:getCharCount() > 135 then	--45个汉字
			elseif string.len(textfieldInput:getString()) > 135 then	--45个汉字
				KNMsg:getInstance():flashShow("输入内容过长无法发送")
			else
				if curType == "world" then
					--[[if not isHorn then
						KNMsg:getInstance():flashShow("亲，你的喇叭不足，请至商城购买道具")
						return
					end]]--
					HTTP:call(40003 , {
						content = textfieldInput:getString()
					} , {
						success_callback = function(data)
							isHorn , hornTotleNum = DATA_Bag:getTypeNum( "prop" , "horn" )
							hornNum:setString( hornTotleNum )
							self:changeStauts(false)
							textfieldInput:setString("")
						end})  --向世界发话  吕玮2015.7.7
				elseif curType == "gang" then
					HTTP:call("alliance_sendchat" , {
						content = textfieldInput:getString()
					} , {
						success_callback = function(result)
							--print("talk.lua",result)
							self:changeStauts(false)
							textfieldInput:setString("")
						end
					})
					
				elseif curType == "friend" then
					HTTP:call("message_send_siliao" , {
						content = textfieldInput:getString() , touid = friendData.uid,name = friendData.name
					} , {
						success_callback = function()
							self:changeStauts(false)
							textfieldInput:setString("")
						end
					})

				end
				--self:refreshData()
			end
		end
	}):getLayer()
	self.inputLayer:addChild( send_btn )
	
	local beginY = 0
	self.inputLayer:setTouchEnabled(true)
	--[[
	self.inputLayer:registerScriptTouchHandler(function(event,x,y)
		if event == CCTOUCHBEGAN then
			beginY = y
		end
		
		if event == CCTOUCHENDED then
			if math.abs( beginY - y ) < 30 then	--如果是移动则输入入法不消失
				self:changeStauts(false)
			end
		end 
		return true 
	end , false , -130 )]]



	local listener = cc.EventListenerTouchOneByOne:create()
	listener:setSwallowTouches(false)
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		beginY = y
		return true
	end, cc.Handler.EVENT_TOUCH_BEGAN)
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y
	
	end,cc.Handler.EVENT_TOUCH_MOVED)	
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		if math.abs( beginY - y ) < 30 then	--如果是移动则输入入法不消失
			self:changeStauts(false)
		end	
	end,cc.Handler.EVENT_TOUCH_ENDED)
	self.inputLayer:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, self.inputLayer)

					
	self:setState()
end
function M:switchFriendRecords( ... )
	if isAll then
		self.allSingleBtn:setFront( PATH .. "show_single.png" )
	else
		self.allSingleBtn:setFront( PATH .. "show_all.png" )
		
	end
end
--输入框下信息切换
function M:setState()
	local isFriend = curType == "friend"
	findPlay:getLayer():setVisible( isFriend )
	findPlay:setEnable( isFriend )
	--toText:setVisible( isFriend )
	haveHornNumSp:setVisible( curType == "world" )
	hornNum:setVisible( curType == "world" )
	hornNum:setVisible( curType == "world" )
end

function M:clean(  )
	KNClock.removeTimeFun("talk_loop")
end
return M