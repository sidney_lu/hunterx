--[[

首页场景

]]


collectgarbage("setpause"  , 100)
collectgarbage("setstepmul"  , 5000)


-- [[ 包含各种 Layer ]]
local homeLayer = requires("Scene.home.homelayer")
local infoLayer = requires("Scene.common.infolayer")
local M = {}
function M:create()
--	local noticeData = DATA_Notice:get()
	
	local scene = display.newScene("home")
	
	---------------插入layer---------------------
	local mainlayer = homeLayer:new()
	scene:addChild( mainlayer )
	mainlayer:addChild( infoLayer:new("home" , 3):getLayer() )
	---------------------------------------------

	if KNFileManager.readfile("savefile.txt" , "sound" , "=") == "1" then
	else
		audio.stopMusic()
		-- audio.disable()
		isMusicAllowed = false
	end
	
	if KNFileManager.readfile("savefile.txt" , "audio" , "=") == "1" then
		-- audio.setIsEffect( true )
	else
		-- audio.setIsEffect( false )
		isSoundAllowed = false
	end
	
	-- audio.preloadMusic(IMG_PATH .. "sound/background.mp3")

	if not audio.isMusicPlaying() then
		audio.setMusicVolume(1.0)
		audio.stopMusic()
		i7PlayMusic( IMG_PATH .. "sound/background.mp3")		
	end

	
	
	-- 公告
	local handle
	local function createNotice()
		CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(handle)
		handle = nil
		if DATA_Notice:getIsFirst() then
			local noticeData = DATA_Notice:get()
			if noticeData.broadcast then
				local noticeLayer = requires("Scene.common.notice")
				local curScene = display.getRunningScene()
				curScene:addChild( noticeLayer:new():getLayer() )
			end
		end
	end
	-- 暂停0.01秒后弹出公告
	--handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc( createNotice , 0.01 , false)
	


	-- 动态心跳包
	if not Clock:getKeyIsExist("heartbeat") then
		CCLuaLog("create heart beat")

		local times = 0
		local heartbeat_function = function()
			times = times + 1
			
			if times > 10 then
				local cur_scene = display.getRunningScene()
				if cur_scene.name == "battle" then return end
				CCLuaLog("-------------------------heart-------------------------")
				HTTP:call(40001 , {} , {
					no_mask = true
				})

				times = 0
			end
		end
		Clock:addTimeFun( "heartbeat" , heartbeat_function )
	end

	function scene:onEnter()
		DATA_Info:setIsMsg()
	end
	function scene:onExit()
		DATA_Info:setIsOpen()
		DATA_Info:addActionBtn( "home" , nil )	--删主界面聊天动画标记
	end
	return scene
end

return M
