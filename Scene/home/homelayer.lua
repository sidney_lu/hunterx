local PATH = IMG_PATH.."image/scene/home/"
local KNBtn = requires("Common.KNBtn")
local HomeCard = requires("Scene.home.homecard")
local KNMask = requires("Common.KNMask")
local PlayerGuide = requires("Common.PlayerGuide")


--[[
	首页
]]
local HomeLayer= {
	layer,
	strengthItemLayer ,    --强化按钮所在层，控制强化的子项是否显示
	formationLayer,
	btnLayer,
	cardItems,    --英雄卡片元素
	allow,  	  --允许进行卡片移动的判断
	move,
}
local aideBtn
function HomeLayer:new()
	-- test data --
	function testdata()
		requires( "Network.commonActions"):init()
		local commonActions = requires("Network.commonActions")
		local response = io.readfile( "http_commdata.txt")
		response = json.decode( response )
		commonActions.saveCommonData( response )
	end
	if TESTDATA then
		testdata()
	end

	local this = {}
	setmetatable(this , self)
	self.__index = self
	
	this.layer = display.newLayer()
	this.allow = true
	-- 背景
	local bg = display.newSprite(PATH .. "main.png")
	setAnchPos(bg , 0 , 0)
	this.layer:addChild(bg)
	-- 闯关按钮背景
	bg = display.newSprite(PATH .. "cloud.png")
	setAnchPos(bg,240, 70, 0.5)
	this.layer:addChild(bg)
	
	-- 转动的光圈
	local lightBg = display.newSprite(PATH .. "light_bg.png")
	setAnchPos(lightBg,245,90,0.5,0.5)
	this.layer:addChild(lightBg)
	lightBg:runAction(CCRepeatForever:create(CCRotateBy:create(5,20)))
	
	local light_front = display.newSprite(PATH .. "light_front.png")
	setAnchPos(light_front,245,90,0.5,0)
	this.layer:addChild(light_front)

	-- 创建布阵列表
	this:createformation()
	
	-- 主页图标按钮初始化
	local function initBtn()
		if this.btnLayer then
			this.layer:removeChild(this.btnLayer , true)
		end

		this.btnLayer = display.newLayer()

		local guide_step = KNGuide:getStep()

		local light = display.newSprite(PATH.."btn_light.png")
		-- 磨练按钮
		local btn_images = {"fb.png" , "fb_press.png"}
		-- if checkOpened("fb_equip") ~= true then btn_images = {"fb_disable.png"} end
		local temp = KNBtn:new(PATH , btn_images , 107 , 210 , {
			callback = function()
				-- 判断等级开放
				-- local check_result = checkOpened("fb_equip")
				-- if check_result ~= true then
				-- 	KNMsg:getInstance():flashShow(check_result)
				-- 	return
				-- end

				-- local fb_type = "equip"
				-- if guide_step == 800 then fb_type = "equip"
				-- elseif guide_step == 3000 then fb_type = "hero"
				-- elseif guide_step == 3100 then fb_type = "pet"
				-- elseif guide_step == 3200 then fb_type = "skill"
				-- end
				-- switchScene("fb" , {coming = fb_type})
				switchScene("fb" , {})
			end
		})
		setAnchPos(light, 107 + temp:getWidth() / 2, 210 + temp:getWidth() / 2,0.5,0.5)
		this.btnLayer:addChild(light)
		
		local function createAction()
			local action
			action = getSequenceAction(CCScaleTo:create(1,0.8),CCScaleTo:create(1,1),CCCallFunc:create(
			function()
				light:runAction(createAction())
			end))	
			return action
		end
		light:runAction(createAction())	
		this.btnLayer:addChild(temp:getLayer())

		-- 新手引导
		if guide_step == 800 or guide_step == 3000 or guide_step == 3100 or guide_step == 3200 then KNGuide:show( temp:getLayer() ) end
		
-- 		-- 战场按钮
		local btn_images = {"compete.png" , "compete_press.png"}
		if checkOpened("athletics") ~= true then btn_images = {"compete_disable.png"} end
		local temp = KNBtn:new(PATH , btn_images , 355 , 100 , {
			callback = function()
				-- 判断等级开放
				-- local check_result = checkOpened("athletics")
				-- if check_result ~= true then
				-- 	KNMsg:getInstance():flashShow(check_result)
				-- 	return
				-- end
				--local fb_type = "equip"
				if guide_step == 3300 then pvp_type = "rob" end
				switchScene("pvp" , {coming = pvp_type})
			end
		})
		this.btnLayer:addChild(temp:getLayer())

		-- 新手引导
		if guide_step == 1300 or guide_step == 3300 then KNGuide:show( temp:getLayer() ) end

		-- 闯关按钮
		local btn_images = {"mission.png" , "mission_press.png"} --cloud.png
		local temp = KNBtn:new(PATH , btn_images , 190 , 110, {  --262,158
			callback = function()
				DATA_Mission:setByKey("current","map_id",DATA_Mission:get("max","map_id"))
				DATA_Mission:setByKey("current","mission_id",DATA_Mission:get("max","mission_id"))
				if DATA_Mission:haveData(DATA_Mission:get("max","map_id")) then
					switchScene("mission")
				else
					--here	
					HTTP:call(21001 , {},{success_callback = function()
						switchScene("mission")
					end })
				end
			end
		})
		this.btnLayer:addChild(temp:getLayer())
		--temp:getLayer():align(display.CENTER)
		
		-- mission light
		-- local aniMask = display.newClippingRectangleNode(cc.rect(0, 0, temp:getWidth() - 25, temp:getHeight() - 25))
		-- setAnchPos(aniMask, 202, 124)
		-- this.btnLayer:addChild(aniMask)
		
		-- local moveLight = display.newSprite(COMMONPATH.."move_light.png")
		-- setAnchPos(moveLight, -moveLight:getContentSize().width)
		-- aniMask:addChild(moveLight)
		-- local function moveFun()
		-- 	local action
		-- 	action = getSequenceAction(CCMoveTo:create(1, ccp(temp:getWidth(), 0)), CCDelayTime:create(2),CCCallFunc:create(function()
		-- 		setAnchPos(moveLight, -moveLight:getContentSize().width)
		-- 		moveLight:runAction(moveFun())
		-- 	end))	
		-- 	return action	
		-- end
		-- moveLight:runAction(moveFun())
			

		-- 新手引导
		if guide_step == 101 or guide_step == 209 or guide_step == 304 or guide_step == 505 or guide_step == 704 then KNGuide:show( temp:getLayer() ) end


		-- 活动按钮
		-- local btn_images = {"gift.png"}
		-- local temp = KNBtn:new(PATH , btn_images , 10 , 255 , {
		-- 	other = DATA_Notice:getGetNum() > 0  and { COMMONPATH .. "egg_num_bg.png" , 60 , 60  } or nil,
		-- 	text = DATA_Notice:getGetNum() > 0 and { 1 , 16 , ccc3( 0xff , 0xff , 0xff ) , { x = 32 , y = 31 } , nil , 17 }   or nil ,  
		-- 	callback = function()
		-- 		--活动
		-- 		HTTP:call("activity" , "get", {} , {
		-- 			success_callback = function()
		-- 				switchScene("activity")
		-- 			end
		-- 		})
		-- 	end
		-- })
		-- this.btnLayer:addChild(temp:getLayer())

		
		-- 打造按钮
		local btn_images = {"activity.png" , "activity_press.png","activity_grey.png"}
		if checkOpened("forge") ~= true then btn_images = {"activity_grey.png"} end
		local temp = KNBtn:new(PATH , btn_images , 270 , 210 , {
			callback = function()
				-- 判断等级开放
				local check_result = checkOpened("forge")
				if check_result ~= true then
					KNMsg:getInstance():flashShow(check_result)
					return
				end
			
				switchScene("forge")
			end
		})
		temp:setEnable(true)
		this.btnLayer:addChild(temp:getLayer())

		-- 新手引导
		if guide_step == 1500 or guide_step == 1509 or guide_step == 3400 then KNGuide:show( temp:getLayer() ) end


		-- 宠物按钮
		local btn_images = {"pet.png" , "pet_press.png","pet_disable.png"}
		if checkOpened("pet") ~= true then btn_images = {"pet_disable.png"} end
		local temp = KNBtn:new(PATH , btn_images , 35 , 100 , {
			callback = function()
				-- -- 判断等级开放
				-- local check_result = checkOpened("pet")
				-- if check_result ~= true then
				-- 	KNMsg:getInstance():flashShow(check_result)
				-- 	return
				-- end

				switchScene("pet")
			end
		})
		--temp:setEnable(false)

		this.btnLayer:addChild(temp:getLayer())

		this.layer:addChild(this.btnLayer)

		-- 新手引导
		--if guide_step == 400 or guide_step == 500 or guide_step == 1200 then KNGuide:show( temp:getLayer() ) end
	end

	-- 初始化按钮
	initBtn()

	local function guide_101()
		PlayerGuide:show(this.layer, {clickfun=function() 
		end, width=40, point=ccp(display.cx+27, 40) })
	end
	local function guide_110()
		PlayerGuide:show(this.layer, {clickfun=function() 
		end})
	end
	--进场的引导
	--DATA_Guide:setGuide(200)
	-- local guidestep = DATA_Guide:getGuide()
	local guidestep = 1
	if guidestep == 100 then
		PlayerGuide:show(this.layer, {clickfun=function() 
			DATA_Guide:setGuide(101) 
			guide_101()
		end})
	elseif guidestep == 109 then
		PlayerGuide:show(this.layer, {clickfun=function() 
			DATA_Guide:setGuide(110) 
			guide_110()
		end})
	elseif guidestep == 200 then
		PlayerGuide:show(this.layer, {clickfun=function() 
			DATA_Guide:setGuide(201)
		end, width=60, point=ccp(display.cx, display.top-340) })
	end


	--聊天入口
	local btn_images = {"aide.png" , "aide_press.png"}
	local curTalk
	if checkOpened("talk") ~= true then btn_images = {"aide_press.png"} end
	local temp = KNBtn:new( IMG_PATH .. "image/scene/chat/"  , {"talk_flag1.png"} , 380 , 240 , {
		priority = -150,
		swallow = true,
		scale = true ,
		front = IMG_PATH .. "image/scene/chat/talk_flag.png" , 
		callback = function()
			local tempType = DATA_Info:getSourceType()
			DATA_Info:setIsMsg( false )
			if DATA_Info:getIsOpen( ) then
				local talkLayer = requires("Scene.common.talk")
				curTalk = talkLayer:new( { type = tempType } )
				local curScene = display.getRunningScene()
				curScene:addChild( curTalk:getLayer()  )
			else
				curTalk:remove()
			end
			DATA_Info:setIsOpen( not DATA_Info:getIsOpen( ) )
		end
	})
	this.btnLayer:addChild(temp:getLayer())
	DATA_Info:addActionBtn( "home" , temp )
	
	-- if CHANNEL_ID == "tmsjIosAppStore" then
	-- 	temp:getLayer():setVisible( false )
	-- 	temp:setEnable(false)
	-- end
	
	local gangSp = display.newSprite( "image/scene/chat/talk_flag2.png" ,
	 temp:getLayer():getContentSize().width/2 , temp:getLayer():getContentSize().height/2):align(display.CENTER)
	gangSp:setVisible(false)
	temp:getLayer():addChild( gangSp , 5 )
	
	local worldSp = display.newSprite("image/scene/chat/talk_flag3.png" , 
		temp:getLayer():getContentSize().width/2 , temp:getLayer():getContentSize().height/2 ):align(display.CENTER)
	worldSp:setVisible(false)
	temp:getLayer():addChild( worldSp , 6 )
--	
--	DATA_Info:addActionBtn( "home" , { btn = temp , gang = gangSp , world = worldSp } )
	-- 小助手按钮
	-- local cur_mission_step = DATA_Guide:get()
	-- if KNGuide:getStep() == 4200 or
	--    (
	-- 		(cur_mission_step["map_id"] > 2  or (cur_mission_step["map_id"] == 2  and cur_mission_step["mission_id"] >= 6) ) and
	-- 		(cur_mission_step["map_id"] < 11 or (cur_mission_step["map_id"] == 11 and cur_mission_step["mission_id"] < 5) )
	-- 	)
	-- then
		aideBtn = KNBtn:new(PATH , {"aide.png","aide_press.png" } , 6 , 235 , {
			scale = true ,
			swallow = true,
			callback = function()
				if KNGuide:getStep() == 4201 then
					self:aideAction()
				else
					self:createAide()
				end
			end
		}):getLayer()
		this.layer:addChild( aideBtn )
		--小助手 背景光效	--------------------------------------------
		local light = display.newSprite(PATH.."aide_light.png")
		
		local function createAction()
			local action
			action = getSequenceAction(CCScaleTo:create(1,0.8),CCScaleTo:create(1,1),CCCallFunc:create(
			function()
				light:runAction(createAction())
			end))	
			return action
		end
		light:runAction(createAction())
		light:setPosition(aideBtn:getContentSize().width/2, aideBtn:getContentSize().height/2)
		aideBtn:addChild( light )
		------------------------------------------------------------------
		if KNGuide:getStep() == 4100 or KNGuide:getStep() == 4200 then
			KNGuide:show( aideBtn , {remove = true})
		end
	--end
	
	return this.layer
end
function HomeLayer:aideAction()
--[[
	local actionAry = CCArray:create()
	actionAry:addObject( CCCallFunc:create(function()
		if aideBtn then
			aideBtn:removeSelf() 
			aideBtn = nil
		end
		aideBtn = KNBtn:new(PATH , {"aide.png" , "aide_press.png"} , 15 , 620 , {
			callback = function()
				self:createAide()
			end
		}):getLayer()
		
		--打开设置界面
		local infoLayer = requires("Scene/common/infolayer")
		infoLayer:showMoreBtn()
		
		local SettingLayer = requires("Scene/common/setting")
		display.getRunningScene():addChild( SettingLayer:new():getLayer() )
		
		display.getRunningScene():addChild( aideBtn , 10 )
	end) )
	
	actionAry:addObject( CCCallFunc:create(function()
		transition.moveTo( aideBtn , { time = 0.5 , x = 50 , y = 350 , onComplete = function() 
			if aideBtn then
				aideBtn:removeSelf() 
				aideBtn = nil
			end
		end } )
	end ))
	actionAry:addObject( CCCallFunc:create(function()
		aideBtn:removeSelf() 
		aideBtn = nil 
	end ) )
  	aideBtn:runAction( CCSequence:create( actionAry ) )
--]]


	local seq = transition.sequence({
		 CCCallFunc:create(function()
			if aideBtn then
				aideBtn:removeSelf() 
				aideBtn = nil
			end
			aideBtn = KNBtn:new(PATH , {"aide.png" , "aide_press.png"} , 15 , 620 , {
				callback = function()
					self:createAide()
				end
			}):getLayer()
			
			--打开设置界面
			local infoLayer = requires("Scene.common.infolayer")
			infoLayer:showMoreBtn()
			
			local SettingLayer = requires("Scene.common.setting")
			display.getRunningScene():addChild( SettingLayer:new():getLayer() )
			
			display.getRunningScene():addChild( aideBtn , 10 )
		end),
		 CCCallFunc:create(function()
			transition.moveTo( aideBtn , { time = 0.5 , x = 50 , y = 350 , onComplete = function() 
				if aideBtn then
					aideBtn:removeSelf() 
					aideBtn = nil
				end
			end } )
		end ),
		CCCallFunc:create(function()
			aideBtn:removeSelf() 
			aideBtn = nil 
		end)
	})
	aideBtn:runAction(seq)

end

function HomeLayer:basePopup( titlePath )
	local bg = display.newSprite( IMG_PATH .. "image/scene/mission/wipe_bg.png") --扫荡背景
	local addX = 90
	local addY = 324
	
	local titleBg = display.newSprite( IMG_PATH .. "image/scene/mission/title_bg.png") -- 闯关背景
	setAnchPos(titleBg, addX , addY)
	bg:addChild(titleBg)
	
	local title = display.newSprite( titlePath )
	setAnchPos(title, addX - 24 , addY )
	bg:addChild(title)
	
	return bg
end

--小助手配置
function HomeLayer:createAide( params )
	params = params or {}
	local layer = display.newLayer()
	local mask
	local AIDEPATH = IMG_PATH .. "image/scene/aide/"
	
	local bg = self:basePopup( AIDEPATH .. "title.png" )
	setAnchPos( bg , display.cx , display.cy + 30 , 0.5 , 0.5 )
	layer:addChild(bg)
	local configElement = { "exp" , "hero" , "equip" , "silver" }
	
	if checkOpened("pet") == true then
		configElement[ #configElement + 1 ] = "pet"
	end	
	if checkOpened("skill") == true then
		configElement[ #configElement + 1 ] = "skill"
	end
	for i = 1 ,#configElement do
		local addX , addY = 79 +  ( ( i - 1 ) % 2 ) * 179 , display.cy+50 - math.floor(( i - 1 )/2) * 74
		local tempBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" , "btn_bg_red2.png" }, 
			addX , addY ,
			{
				front = AIDEPATH .. "need_" .. configElement[i] .. ".png" ,
				priority = -142,	
				callback=
				function()
					switchScene( "aide" , { type = configElement[i] , backFun = function() switchScene( "home" , nil , function() self:createAide() end ) end} )
				end
			})
		layer:addChild( tempBtn:getLayer() )
	end
	
	--返回按钮
	local cancelBtn = KNBtn:new(COMMONPATH,{"back_img.png","back_img_press.png"},35,display.cy+150,{
		scale = true,
		priority = -142,	
		callback = function()
			mask:remove()
		end
	})
	layer:addChild(cancelBtn:getLayer())
	
	setAnchPos( layer , -display.width , 0 )
	transition.moveTo( layer , {time = 0.5 , easing = "BACKOUT" , x = 0 })
	local scene = display.getRunningScene()
	mask = KNMask:new({ item = layer , priority = -141 })
	scene:addChild( mask:getLayer() )
end

function HomeLayer:createformation()
	self.formationLayer = display.newLayer()
	self.formationLayer:setTouchEnabled(true)
	self.cardItems = {}
	
	local bg = display.newSprite(PATH.."formation_view_bg.png")
	bg:pos(0, -100)
	setAnchPos(bg)
	self.formationLayer:addChild(bg)
	
	self.formationLayer:setContentSize(bg:getContentSize())
	setAnchPos(self.formationLayer , 10 , display.top-490)

	-- 新手引导
	local guide_step = KNGuide:getStep()
	if guide_step == 200 or guide_step == 300 or guide_step == 700 or guide_step == 1100 or guide_step == 2500 or guide_step == 4000 then KNGuide:show( self.formationLayer , {height = 275} ) end
	
	-- local title = display.newSprite(PATH.."my_formation.png")
	-- setAnchPos(title,bg:getContentSize().width / 2,bg:getContentSize().height - 50,0.5)
	-- self.formationLayer:addChild(title)
	
	--生成主页滑动卡牌
	--print("---length:", DATA_Formation:get_lenght())
	--dump(DATA_Formation:get_data())
	local lastIndex = 0
	local heros = DATA_Formation:get_OnList()
	for k,v in pairs(heros) do
		if k >lastIndex then lastIndex = k end
	end

	for i = 1, 8 do
		print(i,DATA_Formation:getHeroByIndex(i))
		--self.cardItems[i] = HomeCard:new(120 , i , i > DATA_Formation:get_lenght())
		self.cardItems[i] = HomeCard:new(120 , i , i > lastIndex) -- unlock
		self.cardItems[i]:addTo(i,self.formationLayer)
	end
	
	--默认将第一个武将显示在最前方
	for k, v in pairs(self.cardItems) do
		v:setState(2,self)	
	end
	if DATA_Formation:get_lenght() > 0 then
		local heros = DATA_Formation:get_OnList()
		local firstIndex = 0
		for k, v in pairs(heros) do
			if firstIndex==0 or k<firstIndex then
				firstIndex = k
			end
		end
		--DATA_Formation:setCur(DATA_Formation:get_index(1)["gid"])
		DATA_Formation:setCur(DATA_Formation:getHeroByIndex(firstIndex)["gid"])
	end
	
	local touchX, legal,lastX,time,speed

	local listener = cc.EventListenerTouchOneByOne:create()
	listener:setSwallowTouches(false)
	listener:registerScriptHandler(function(touch, event)
		--testlog("home layer...EVENT_TOUCH_BEGAN")
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		if not CCRectMake(10,display.top-550,bg:getContentSize().width,
				bg:getContentSize().height):containsPoint(ccp(x,y)) then
			return false
		end		
		touchX = x
		lastX = x
		legal = true
		time = os.clock()		
		return true
	end, cc.Handler.EVENT_TOUCH_BEGAN)
	listener:registerScriptHandler(function(touch, event)
		-- print("...EVENT_TOUCH_MOVED")
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		local dir = x < lastX
		if math.abs(touchX - x) > 10 then
			touchX = x
			speed = os.clock() - time
			time = os.clock()
			if self.allow then
				self.move = true
				self.allow = false
			end
			legal = false		
		end
		if self.move then
			self.move = false
			for i = 1, #self.cardItems do
				self.cardItems[i]:move(dir,self,speed)
			end
		end
		lastX = x
	end,cc.Handler.EVENT_TOUCH_MOVED)	
	listener:registerScriptHandler(function(touch, event)
		-- print("...EVENT_TOUCH_ENDED")
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		if legal and DATA_Formation:getCur() then
			DATA_Formation:set_index(1)
			switchScene("hero",{gid = DATA_Formation:getCur()})
		end
		touchX = 0
		lastX = 0
		legal = false		
	end,cc.Handler.EVENT_TOUCH_ENDED)
	listener:registerScriptHandler(function(touch, event)
		--print("...EVENT_TOUCH_CANCELLED")
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		if legal and DATA_Formation:getCur() then
			DATA_Formation:set_index(1)
			switchScene("hero",{gid = DATA_Formation:getCur()})
		end
		touchX = 0
		lastX = 0
		legal = false		
	end,cc.Handler.EVENT_TOUCH_CANCELLED)	

	self.formationLayer:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, self.formationLayer)


	self.layer:addChild(self.formationLayer)
end

return HomeLayer
