local PATH = IMG_PATH .. "image/scene/detail/"
local PROP_PATH = IMG_PATH .. "image/prop/"
local COMMONPATH = IMG_PATH .. "image/common/"
local KNBtn = requires( "Common.KNBtn")
local Config = requires( "Config.Prop")
local CombatAttributes = requires( "Config.CombatAttributes")
local Stone = requires("Config.StoneVariety")

--[[道具信息]]
local PropDetail = {
	layer,
	params
}

function PropDetail:new(params)
	local this = {}
	setmetatable(this,self)
	self.__index = self

	this.layer = display.newLayer()
	this.params = params or {}

	local pid = 0
	local _data = {}
	if params.id ~= nil then
		pid = params.id
		_data = DATA_Bag:get("prop" , pid)
	elseif params.data ~= nil then
		_data = params.data
		pid = _data["id"]
	end

	pid = tonumber(pid)

	local cid = tostring(_data["cid"])
	local config_data = Config[cid]

	-- 最后面的背景
	local bg_small = display.newSprite(COMMONPATH .. "bg_small.png")
	-- 道具的背景图
	local bg = display.newSprite(PATH .. "bg.png")
	bg:setScaleY(0.6)
	bg:setScaleX(0.9)
	setAnchPos(bg , 45 , display.top-165, 0, 1)
	local big_icon = display.newSprite(getImageByType(cid , "b"))
	local name_label = display.strokeLabel( config_data["name"] , 65 , display.top-157 , 24 , DESCCOLOR )
	local lv_label = nil
	if _data["lv"] then
		lv_label = display.strokeLabel( "Lv" .. _data["lv"] , 250 , display.top-157 , 18 ,DESCCOLOR )
		
	end
	local split_sprite = display.newSprite(PATH .. "spilt.png")

	local effect_label, effect_next = nil
	if _data["type"] == "stone" then
		if _data["lv"] then
			effect_label = display.strokeLabel( CombatAttributes[_data["effect"]] .. " +".. Stone[_data["cid"]..""][_data["lv"]..""], 35 , 250 , 16 , ccc3( 0x2c , 0x00 , 0x00 ) , 2 , ccc3( 0x00 , 0x00 , 0x00 ) , {
				dimensions_width = 300,
				dimensions_height = 30,
			})
			
			effect_next = display.strokeLabel( CombatAttributes[_data["effect"]] .. " +" ..(Stone[_data["cid"]..""][(_data["lv"] + 1)..""] or "max") , 35 , 190 , 16 , ccc3( 0x2c , 0x00 , 0x00 ) , 2 , ccc3( 0x00 , 0x00 , 0x00 ) , {
				dimensions_width = 300,
				dimensions_height = 30,
			})
		end
	end

	-- 道具信息
	local title = display.newSprite(PATH .. "prop_info.png")
	setAnchPos(title , 330 , display.top-130, 0, 1)
	-- 祥细
	-- local descText = display.strokeLabel( config_data["desc"] , 350 , display.top-500 , 18 , ccc3( 0x2c , 0x00 , 0x00 ) , 2 , ccc3( 0x00 , 0x00 , 0x00 ) , {
	-- 	dimensions_width = 105,
	-- 	dimensions_height = 300,
	-- 	align = 0,
	-- })
	local descText = ui.newTTFLabel({
        text = config_data["desc"],
        size = 18,
        color = ccc3( 0x2c , 0x00 , 0x00 ),
        x = 380,
        y = display.top-170,
        dimensions=cc.size(105,300),
	    align = ui.TEXT_ALIGN_CENTER
	})		
	descText:setAnchorPoint(cc.p(0.5,1))	

	setAnchPos(bg_small , 18 , 120)

	setAnchPos(big_icon , 85 , display.top-200, 0, 1)
	
	setAnchPos(split_sprite , 7 , display.top-155, 0, 1)


	this.layer:addChild(bg_small)
	this.layer:addChild(bg)
	this.layer:addChild(big_icon)
	this.layer:addChild(name_label)
	if lv_label ~= nil then this.layer:addChild(lv_label) end
	this.layer:addChild(split_sprite)
	this.layer:addChild(title)
	this.layer:addChild(descText)

	if effect_label ~= nil then
		--当前效果
		local effect_bg = display.newSprite(PATH .. "effect_bg.png")
		local effect = display.newSprite(PATH .. "cur_pro.png")

		setAnchPos(effect_bg , 40 , display.top-450) --295
		setAnchPos(effect , 45 , display.top-347) --298

		this.layer:addChild(effect_bg)
		this.layer:addChild(effect)
		
		--下一级效果
		effect_bg = display.newSprite(PATH .. "effect_bg.png")
		effect = display.newSprite(PATH .. "next_pro.png")

		setAnchPos(effect_bg , 40 , display.top-520) --225
		setAnchPos(effect , 45 , display.top-512) --228

		this.layer:addChild(effect_bg)
		this.layer:addChild(effect)
		

		this.layer:addChild(effect_label)
		this.layer:addChild(effect_next)
	end

	-- 按钮
	if pid ~= 0 then
		local frontPath
		if params.showType =="info" then
			frontPath = COMMONPATH .. "ok.png"
		else
			frontPath = COMMONPATH .. "use.png"
			
		end
		local useBtn= KNBtn:new(COMMONPATH, { "btn_bg_red.png" , "btn_bg_red_pre.png"} , 166 , 140 , {
			scale = true,
			front = frontPath,
			callback = function()
				popScene()
				if params.propOpt then
					params.propOpt()
				end
			end
		}):getLayer()

		this.layer:addChild(useBtn)
	end
	
	--是否有附加详情说明
	if getConfig("prop", cid, "bagdesc2") then
		local effect_bg = display.newSprite(PATH .. "effect_bg.png")
		local effect = display.newSprite(PATH .. "prop_desc.png")

		setAnchPos(effect_bg , 45 , display.top-450) --295
		setAnchPos(effect , 50 , display.top-447) --298

		this.layer:addChild(effect_bg)
		this.layer:addChild(effect)
		
		--local desc = display.strokeLabel(getConfig("prop", cid, "bagdesc2"), 25, display.top-470, 16, ccc3(0x2c, 0, 0), nil, nil, {align = 0}) --280
		local desc = ui.newTTFLabel({
	        text = getConfig("prop", cid, "bagdesc2"),
	        size = 18,
	        color = ccc3( 0x2c , 0x00 , 0x00 ),
	        x = 50,
	        y = display.top-460,
	        dimensions=cc.size(480,280),
		    --align = ui.TEXT_ALIGN_CENTER
		    align = 0
		})
		desc:setAnchorPoint(cc.p(0,1))
		--setAnchPos(desc, 35, display.top-460 - desc:getContentSize().height) --290
		this.layer:addChild(desc)
		
	end
	
	--如果商城中展示详情，则添加购买按钮
	if params.shopBuyFun then
		local optBtn = KNBtn:new(COMMONPATH , {"btn_bg_red.png", "btn_bg_red_pre.png"} , 166 , 140 , {
			front = COMMONPATH .. "buy.png",
			scale = true,
			callback = function() 
				params.shopBuyFun()
			end
		})

		this.layer:addChild(optBtn:getLayer())
	end


	return this
end

function PropDetail:getLayer()
	return self.layer
end

function PropDetail:getRange()
	local x = self.layer:getPositionX()
	local y = self.layer:getPositionY()
	if self.params["parent"] then
		x = x + self.params["parent"]:getX() + self.params["parent"]:getOffsetX()
		y = y + self.params["parent"]:getY() + self.params["parent"]:getOffsetY()
	end
	return CCRectMake(x,y,self.layer:getContentSize().width,self.layer:getContentSize().height)
end
return PropDetail
