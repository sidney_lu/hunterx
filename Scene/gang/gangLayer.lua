--活动页面
local M = {}
local SCENETYPE
local NOGANG , SEEGANG , MEMBER , RANKING  , RANK , PRAY , APPOINT , APPLY ,
 TASK , SHOP , HALL , DONATE , WARS , WARSMEBER , WARSRANK 
 = 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7 ,
  8 , 9 , 10 , 11 , 12 , 13 , 14 
local PATH = IMG_PATH .. "image/scene/gang/"
local InfoLayer = requires( "Scene.common.infolayer")
local KNBtn = requires( "Common.KNBtn")
local KNMask = requires("Common.KNMask")
local KNCheckBox = requires("Common.KNCheckBox")
local KNBar = requires("Common.KNBar")
local KNRadioGroup = requires( "Common.KNRadioGroup")
local KNSlider = requires("Common.KNSlider")
local KNClock = requires("Common.KNClock")
local baseElement = requires( "Scene.common.baseElement")
local data
local gangInfoTable , noticeText , energyText , tributeText
local GangTitle_config =requires("Config.Gang")
local baseScroll,scrollLayer
function M:new( params )
	echoLog("[ganglayer]","create faith data")
	--DATA_Gang:newTempData()
	--HTTP:call(42040,{},{})
	local this = {}
	setmetatable(this , self)
	self.__index = self
	params = params or {}
	SCENETYPE = params.type or 0 
	echoLog("[ganglayer]",SCENETYPE)
	
	-- 基础层
	this.baseLayer = display.newLayer()
	this.viewLayer = display.newLayer()
	this.gangInfoLayer = nil		--帮派信息
	
	local bg = display.newSprite( COMMONPATH.."dark_bg.png" )
	setAnchPos( bg , 0 , 425 , 0 , 0.5 )
	this.baseLayer.bg = bg
	this.baseLayer:addChild( bg )

	local function backFun()
		KNClock:removeTimeFun( "trends" )
		KNClock:removeTimeFun( "wars" )
		if SCENETYPE == SEEGANG or SCENETYPE == MEMBER or SCENETYPE == RANKING or SCENETYPE == PRAY or SCENETYPE == RANK or SCENETYPE == TASK or SCENETYPE == DONATE then
			switchScene("gang")
		elseif SCENETYPE == APPOINT or  SCENETYPE == APPLY or SCENETYPE == SHOP or SCENETYPE == WARS then
			switchScene("gang")
		elseif SCENETYPE == WARSMEBER  or SCENETYPE == WARSRANK then
			this:createWars()
		else
			switchScene("home")
		end
	end
	if not DATA_Gang:isJoinGang() then
		if SCENETYPE == SEEGANG then
			this:seeGangList( )	--加入查看其它帮派列表
		else
			this:noGang()	--默认没有加入帮派前界面
		end
		-- 显示公用层 底部公用导航以及顶部公用消息
		this.infoLayer = InfoLayer:new("gang", 0, { title_text = PATH.."gang_text2.png", tail_hide = true , closeCallback = backFun })
	else
		-- height + 130是关键，暂时不确定原因
		baseScroll = KNScrollView:new(0, 90 , display.width, display.height - 120 - 90 - 182 + 130, 10 , false )
		scrollLayer = cc.LayerColor:create(ccc4(255,123,123,0))
		scrollLayer:setContentSize(display.width,500)
		baseScroll:addChild(scrollLayer)
		baseScroll:scrollBy({x=0,y=0})
		this.baseLayer:addChild(baseScroll:getLayer())
		this:createHall()		
		this:refreshGangInfo( )
		this.infoLayer = InfoLayer:new("gang", 0 , { title_text = PATH.."gang_text2.png", tail_hide = true , closeCallback = backFun })
	end
	--this.infoLayer:showInfo(1)
	this.baseLayer:addChild( this.infoLayer:getLayer(),20 )	

	return this.baseLayer 
end
function M:showScroll( ... )
	self.baseLayer:addChild(baseScroll:getLayer())
end
function M:hideScroll( ... )
	self.baseLayer:removeChild(baseScroll:getLayer())
end
function M:refreshGangInfo( params )
	params = params or {} 
	local type = params.type or 0
	
	local isRefresh = params.refresh or false	
	
	--是否强制作刷新
	if  self.gangInfoLayer and isRefresh then
		self.gangInfoLayer:removeSelf()
		self.gangInfoLayer = nil
	end
	
	if type == 3 then return end
	
	local HALLPATH = PATH .. "hall/"
	local infoData = DATA_Gang:get( "info" )
	local infoElement = {
			infoData.name ,																											--帮派名称
			"Lv" .. infoData.level ,																									--帮派等级
			infoData.count .. "/" .. infoData.count_max ,																			--人数
			"-" ,																											--帮派ID
			infoData.chief_name ,																								--帮主
			infoData.sum_ability ,																									--战力
			string.convertMoney(infoData.tribute)  ,						--帮贡
			string.convertMoney(infoData.funds) ,								--资金
			}
	--人物信息层
	local layer
	
	local function refreshInfo()
		energyText:setString( infoData.usertribute_v )
		tributeText:setString( infoData.usertribute )
		
		if type == 0 then
			for i = 1 , #gangInfoTable do
				gangInfoTable[i]:setString( infoElement[i] )
			end
		end
	end

	if not self.gangInfoLayer then
		
		self.gangInfoLayer = display.newLayer()
		self.baseLayer:addChild( self.gangInfoLayer , 10 )
		
		layer = display.newLayer()

		local bgPath , bgX = HALLPATH .. "notice_bg.png" , display.height - 120 + 20 - 182/2 
		if type == 0 then
			bgPath = bgPath
			bgX = bgX
			
			--layer:addChild(  display.newSprite( bgPath , display.cx , bgX):align(display.BOTTOM_CENTER) )
			local bg = display.newSprite( bgPath , display.cx , bgX)
			layer:addChild( bg )
			--layer:addChild(  display.newSprite(HALLPATH .. "notice_left.png" , 90 , 650 ) )
			bg:addChild(display.newSprite(HALLPATH .. "notice_left.png" , 90 , 90 ))
			--layer:addChild(  display.newSprite(HALLPATH .. "notice_right.png" , 320 , 650 ) )
			bg:addChild(  display.newSprite(HALLPATH .. "notice_right.png" , 320 , 90 ) )
			
			--帮派基本信息
			local addX , addY 
			gangInfoTable = {}
			for i = 1 , #infoElement do
				addX = 225 + (( i - 1 ) % 2 ) * 230
				--addY = 694 - math.floor( ( i - 1 ) / 2 ) * 38
				addY = 135 - math.floor( ( i - 1 ) / 2 ) * 38
				gangInfoTable[i] = display.strokeLabel( infoElement[i] , addX , addY , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
						dimensions_width = 200,
						dimensions_height = 25,
						align = 0
					})
					
				--layer:addChild( gangInfoTable[i] )
				bg:addChild(gangInfoTable[i])
			end


			
			
	
			
			scrollLayer:setContentSize(display.width,500)

			--layer:addChild(  display.newSprite(HALLPATH .. "my_bg.png" , display.cx , 365):align(display.BOTTOM_CENTER) )
			scrollLayer:addChild(  display.newSprite(HALLPATH .. "my_bg.png" , display.cx , 265))
			--layer:addChild(  display.newSprite(HALLPATH .. "my_total.png" , 90 , 380 ):align(display.BOTTOM_LEFT) )	--我的总帮威
			scrollLayer:addChild(  display.newSprite(HALLPATH .. "my_total.png" , 130 , 265 ))	--我的总帮威
			--layer:addChild(  display.newSprite(HALLPATH .. "my_usable.png" , 250 , 380):align(display.BOTTOM_LEFT) )	--可用帮威
			scrollLayer:addChild(  display.newSprite(HALLPATH .. "my_usable.png" , 290 , 265))	--可用帮威
			
			
			infoData.contribution_max = tonumber(infoData.contribution_max)
			--energyText = display.strokeLabel( string.convertMoney(infoData.contribution_max) , 365 , 265 , 20 , ccc3(112 , 236 , 241) , 2 )--可用荣誉
			energyText = display.strokeLabel( string.convertMoney(infoData.contribution_max) , 205 , 265 , 20 , ccc3(112 , 236 , 241) , 2 )	--可用帮威
			tributeText = display.strokeLabel( string.convertMoney(infoData.contribution)  , 205 , 240 , 20 , ccc3(177 , 245 , 97) , 2 )	--帮威
			scrollLayer:addChild( tributeText ) 
			scrollLayer:addChild( energyText )
			scrollLayer:addChild( KNBtn:new( COMMONPATH , {"tribute.png"} , 175 , 265 , {scale = true , callback = function()
				if SCENETYPE~=HALL then return end
				KNMsg.getInstance():flashShow("你的历史总帮威是" ..infoData.contribution_max ) end}):getLayer() )
			scrollLayer:addChild( KNBtn:new( PATH , {"usertribute_v.png"} , 175 , 240 , {scale = true , callback = function()
				if SCENETYPE~=HALL then return end
				KNMsg.getInstance():flashShow("你的可用帮威是" ..infoData.contribution ) end}):getLayer() )
			
			
			scrollLayer:addChild(  display.newSprite(HALLPATH .. "notice_text_bg.png" , display.cx , 345 ) )
			scrollLayer:addChild(  display.newSprite(HALLPATH .. "notice_title.png" , 40 , 345 ) )
			--生成公告文字内容
			--noticeText = display.strokeLabel( infoData.announcement , 70 , 405 + 75 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {

				-- 	dimensions_width = 330,
				-- 	dimensions_height = 70,
				-- 	align = 0
				-- })
			noticeText = ui.newTTFLabel({
		        --text = "abcdefghijklmnopqrstuvwxyz1234567890123456789",
		        text = "真的是中文才能换名字吗，试试吧，看看",
		        size = 20,
		        color = ccc3( 0x2c , 0x00 , 0x00 ),
		        x = 70,
		        y = 275,
		        align = ui.TEXT_ALIGN_LEFT,
		        --dimensions = cc.size(330,200)
		        dimensions = cc.size(250,200)
			})
			-- noticeText = cc.LabelTTF:create("abcdefghijklmnopqrstuvwxyz1234567890123456789","Arial",20)

			-- noticeText:setDimensions(cc.size(300,display.height))
			noticeText:setAnchorPoint(cc.p(0,0.5))
			scrollLayer:addChild( noticeText )
			--编辑公告
			local isGangMan =  infoData.duty == 100
			local editBtn = KNBtn:new(COMMONPATH , isGangMan and { "btn_ver.png" , "btn_ver_pre.png" } or { "btn_ver_grey.png" } ,
			 415 , 305 , {
				front = HALLPATH .. "edit_notice.png",
--				priority = -131,	
				callback = function()
					if isGangMan and SCENETYPE == HALL then
						local function confirmFun(str)
							HTTP:call("alliance_notice",{ notice = str },{success_callback = 
							function(data)
								infoData = DATA_Gang:get( "info" )
								dump(infoData)
								noticeText:setString( infoData.announcement )
							end})	
						end
						self:inputBox({confirmFun = confirmFun , isFind = false })
					end
				end
			}):getLayer()
			editBtn:setScale(0.8)
			scrollLayer:addChild( editBtn )
			
			
		else
			--修改后无用样式
--			bgPath = HALLPATH .. "notice_bg2.png"
--			bgX = 680
--			layer:addChild(  display.newSprite( bgPath , display.cx , bgX ) )
		end
		
		self.gangInfoLayer:addChild( layer )
	else
		refreshInfo()
	end
	
end
-- --祈福界面
-- function M:createPray()
-- 	SCENETYPE = PRAY
-- 	self:refreshGangInfo( { type = 3 , refresh = true } )
-- 	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_pray_title.png" )
	
-- 	local PRAYPATH = PATH .. "pray/"
-- 	local layer = display.newLayer()
-- 	if self.viewLayer then	
-- 		self.viewLayer:removeSelf()
-- 		self.viewLayer = display.newLayer()
-- 		self.baseLayer:addChild( self.viewLayer )
-- 	end
	
-- 	local data = DATA_Gang:get("pray")
-- 	local layer = display.newLayer()


-- 	layer:addChild(display.newSprite(PRAYPATH .. "bg.png" , display.cx , 109 ):align(display.BOTTOM_CENTER) )
-- 	layer:addChild(display.newSprite(PRAYPATH .. "trends_title.png" , display.cx , 727 ) )
-- 	layer:addChild(display.newSprite(PRAYPATH .. "tip.png" , display.cx , 580) )
-- 	layer:addChild(display.newSprite(PRAYPATH .. "frame.png" , display.cx , 450) )
-- 	layer:addChild(display.newSprite(PRAYPATH .. "cost.png" , 110 , 190) )
-- 	layer:addChild(display.newSprite(COMMONPATH .. "gold.png" , 350 , 190) )
-- 	layer:addChild(display.newSprite(COMMONPATH .. "gold.png" , 170 , 190) )
-- 	layer:addChild(display.newSprite(PRAYPATH .. "surplus.png" , 288 + 25 , 190) )
-- 	layer:addChild(display.newSprite(PRAYPATH .. "award_frame.png" , display.cx , 282) )
-- 	for i = 1 , 4 do
-- 		layer:addChild(display.newSprite(SCENECOMMON .. "skill_frame2.png" , 108 + (i-1) * 87 , 278) )
-- 	end
-- 	local trendsText = {}
-- 	for i = 1 , 3 do
-- 		trendsText[i] = display.strokeLabel( "" , 40 , 708 - i * 25 , 20 ,ccc3(0x2c , 0x00 , 0x00 ) )
-- 		layer:addChild( trendsText[i] )
-- 	end 
	
-- 	local delay = nil
-- 	local function refreshTrends()
-- 		if not delay or delay > 10 then
-- 			delay = 0
-- 			HTTP:call("alliance","cliffordmovement",{ },{ no_loading = true , success_callback =
-- 			function( tempData )
-- 				for i = 1 , #trendsText do
-- 					local curData = tempData.clifford_movement[i] or { content = ""  }
-- 					local str = curData.content 
-- 					trendsText[i]:setString( str )
-- 				end
-- 			end})
-- 		end
-- 		delay = delay + 1
-- 	end
	
-- 	KNClock:addTimeFun( "trends" , refreshTrends )
	
-- 	refreshTrends()
	
	
-- 	local function showAward( index , awards )
-- 		local gold = awards.award_gold
-- 		local silver = awards.award_silver
-- 		layer:addChild( display.newSprite(COMMONPATH .. ( gold == 0 and "silver.png" or "gold.png" ) , 330 , 567 - index * 34 ) )
-- 		layer:addChild( display.strokeLabel( ( gold == 0 and silver or gold)  , 345 ,  557 - index  * 34 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) ) )
-- 	end
	
-- 	for i = 1 , 5 do
-- 		layer:addChild(display.newSprite(IMG_PATH .. "image/scene/battle/hero_info/line_long.png" , display.cx , 552 - i * 34) )
-- 		layer:addChild(display.newSprite( PRAYPATH .. "other_tip.png" , display.cx - 50, 567 - ( i + 1 ) * 34) )
-- 		layer:addChild( display.strokeLabel( 5 - i , 115 + 40  ,  557 - ( i + 1 ) * 34 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) ) )
-- 		showAward( i + 1  , data.clifford_award[ (i + 1) ] )
-- 		if i == 1 then
-- 			layer:addChild(display.newSprite( PRAYPATH .. "highest_tip.png" , display.cx - 50, 567 - i * 34) )
-- 			showAward( i , data.clifford_award[ i ] )
-- 		end
-- 	end
-- 	layer:addChild( display.strokeLabel( data.clifford_money , 187 , 180 , 18 , ccc3( 0x2c , 0x00 , 0x00 ) ) )
-- 	--剩余黄金
-- 	local surplusText = display.strokeLabel( DATA_User:get("gold") , 370 , 180 , 18 , ccc3( 0x2c , 0x00 , 0x00 ) )
-- 	layer:addChild( surplusText ) 
	
	
	
	
	
	
-- 	--动画部分处理
-- 	-- local maskLayer = UICutLayer:create()--WindowLayer:createWindow()	
-- 	local maskLayer = display.newLayer()
-- 	setAnchPos(maskLayer , 0 , 252 , 0 , 0)
-- 	maskLayer:setContentSize(CCSizeMake(480,53))
-- 	layer:addChild( maskLayer )
	
-- 	local prayRecord = {}
-- 	local curGroup = 1
-- 	local isOver = false
-- 	local function createInfo( isFirst )
-- 		if not isFirst then
-- 			for j = 0 , 3 do
-- 				transition.stopTarget( prayRecord[curGroup][j] )
-- 				prayRecord[curGroup][j]:removeSelf()
-- 				prayRecord[curGroup][j] = nil
-- 			end
-- 			prayRecord[curGroup] = nil
			
-- 			prayRecord[curGroup] = display.newSprite( PRAYPATH .. ( data.rands and ("text".. data.rands[curGroup+1]) or ("init" .. curGroup ) )  .. ".png" , 108 + curGroup * 87 , 25)
-- 			maskLayer:addChild( prayRecord[curGroup] )
-- 			curGroup = curGroup + 1
-- 		else
-- 			for i = 0 , 3 do
-- 				if prayRecord[i] then
-- 					prayRecord[i]:removeSelf()
-- 					prayRecord[i] = nil
-- 				end
-- 				prayRecord[i] = display.newSprite( PRAYPATH .. ( data.rands and ("text".. data.rands[i+1]) or ("init" .. i ) )  .. ".png" , 108 + i * 87 , 25)
-- 				maskLayer:addChild( prayRecord[i]  )
-- 			end
-- 		end
		
-- 	end
-- 	createInfo(true)
	
-- --		if isOver and group == curGroup then
-- --			for j = 0 , 3 do
-- --				transition.stopTarget( prayRecord[curGroup][j] )
-- --				if j ~= data.rands[curGroup] then
-- --					prayRecord[curGroup][j]:removeSelf()
-- --					prayRecord[curGroup][j] = nil
-- --				end
-- --			end
-- --			curGroup = curGroup + 1
-- --			curGroup = curGroup>=4 and  4 or curGroup
-- --			prayRecord[curGroup] = prayRecord[curGroup][data.rands[curGroup]]
-- --		end
-- 	local speed = 0.2
-- 	local isData = false		--是否接收到数据
-- 	local downTime = 3	--倒计时
-- 	local function startAction( group )
-- 		if group then
-- 			local addX = 76 + group * 87
-- 			--计算Y坐标
-- 			local function countY( i ) 
-- 				local tempY = 50
-- 				data = DATA_Gang:get("pray")
-- --				data.rands = { 1 , 3 , 1 , 1 }--星照星星
-- 				if data.rands then
-- 					tempY = ( isOver and ( group == curGroup ) and ( i < data.rands[curGroup]) ) and ( i == data.rands[curGroup] and 20 or -60 ) or 50 
-- 				end
-- 				return tempY
-- 			end
			
-- 			local function chekeOver( i )
-- 				setAnchPos( prayRecord[group][i] , 76 + group * 87 , -60 , 0 , 0 )
-- 				if i == 2 then
-- 					startAction(group) 
-- 				end
-- 			end
			
-- 			transition.moveTo( prayRecord[group][0] , { delay = 0	,			time = speed , y = countY(0) , onComplete = function() chekeOver(0)  end })
-- 			transition.moveTo( prayRecord[group][1] , { delay = speed/2 , 		time = speed , y = countY(1) , onComplete = function() chekeOver(1)  end })
-- 			transition.moveTo( prayRecord[group][2] , { delay = speed/2 * 2 ,	time = speed , y = countY(2) , onComplete = function() chekeOver(2)  end })
-- 			transition.moveTo( prayRecord[group][3] , { delay = speed/2 * 3 , 	time = speed , y = countY(3) , onComplete = function() chekeOver(3)  end })
-- 		else
-- 			--初始化动画
-- 			downTime = 3
-- 			for i = 0 , 3 do
-- 				if prayRecord[i] then
-- 					prayRecord[i]:removeSelf()
-- 					prayRecord[i] = nil
-- 				end
-- 				prayRecord[i] = {}
				
-- 				for j = 0 , 3 do
-- 					prayRecord[i][j] = display.newSprite(PRAYPATH .. "text".. j .. ".png" )
-- 					setAnchPos( prayRecord[i][j] , 76 + i * 87 , -60 , 0 , 0 )
-- 					maskLayer:addChild( prayRecord[i][j] )
-- 				end

-- 				startAction(i)
-- 			end
-- 		end
		 
		 
		 
-- 	end
	
	
	
	
-- 	local mask
-- 	local function overAction(  )
-- 		downTime = downTime - 1
-- 		isOver = downTime <= 0
-- 		if ( ( not isOver ) and ( not isData ) ) then
-- 			return
-- 		end
-- 		speed = speed * 2
-- 		data = DATA_Gang:get("pray")
-- 		createInfo()
		
-- 		if curGroup < 4 then
-- 			return
-- 		end
		
-- 		Clock:removeTimeFun( "prayAction" )
	
		
-- 		mask:remove()
-- 		surplusText:setString( DATA_User:get("gold") )	--刷新剩余黄金数量
-- 		self:refreshGangInfo( { type = 3 , refresh = true } )
		
		
-- 		local totalNum = 0
-- 		if table.concat(data.rands) == "0123" then
-- 			totalNum = 5--出现福星高照  取最高奖励数据，1为最高奖励
-- 		else
-- 			for i = 1 , #data.rands do
-- 				if data.rands[i] == 0 then
-- 					totalNum = totalNum + 1
-- 				end
-- 			end
-- 		end
-- 		local awardData = data.clifford_award[ 6 - totalNum ]
-- 		KNMsg.getInstance():flashShow(  "恭喜您获得" .. ( awardData.award_gold == 0 and  awardData.award_silver .. "银两" or awardData.award_gold .. "黄金" )   )
-- 	end
	
-- 	--祈福
-- 	local prayBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" } , 168 , 116 , {
-- 		front = PRAYPATH .. "pray_text.png",
-- --		priority = -131,	
-- 		callback = function()
-- 			if countGold(data.clifford_money ) then
-- 				mask = KNMask:new({ priority = -133 ,	opacity = 0 })
-- 				local scene = display.getRunningScene()
-- 				scene:addChild( mask:getLayer() )
-- 				Clock:addTimeFun("prayAction" , overAction )
				
-- 				speed = 0.2
-- 				curGroup = 0
-- 				downTime = 3
-- 				isOver = false
-- 				isData = false
				
-- 				local tempData = DATA_Gang:get("pray")
-- 				tempData.rands = nil
-- 				DATA_Gang:set_type("pray" , tempData)
				
-- 				startAction()
-- 				HTTP:call("alliance","sendclifford",{ },{success_callback =function() isData = true end})
-- 			end
-- 		end
-- 	}):getLayer()
-- 	layer:addChild( prayBtn )
	
	
-- 	self.viewLayer:addChild(layer)
-- end
--帮派大厅
function M:createHall()
	local HALLPATH = PATH .. "hall/"
	SCENETYPE = HALL
	local layer = display.newLayer()
	if self.viewLayer then	
		self.viewLayer:removeSelf()
		self.viewLayer = nil
		self.viewLayer = display.newLayer()
	end
	--权限
	local privilegeBtn = KNBtn:new( HALLPATH , { "privilege.png" , "privilege_pre.png" } , 15 , 220 , {
		priority = -131,	
		callback = function()
			if SCENETYPE~=HALL then return end
			-- HTTP:call("alliance_get", {},{success_callback = 
			-- function()
			-- 	self:lookPrivilege()
			-- end})
			self:lookPrivilege()
		end
	}):getLayer()
	layer:addChild( privilegeBtn )
	
	local btnConfig = {
		{flag = "donate" },
		{flag = "pray" },
		{flag = "gang_wars" },
		{flag = "task" },
		{flag = "ranking" },
		{flag = "shop" },
	}
	for i = 1 , #btnConfig do
		local tempBtn = KNBtn:new(COMMONPATH , 
		{ "btn_bg_red.png" , "btn_bg_red_pre.png"  , "btn_bg_red2.png"} , 
		74 + math.floor( (i-1)/3 ) * 188 , 150 - (i-1)%3 * 74 ,
		{
			front = HALLPATH .. btnConfig[i].flag .. ".png" , 
			priority = -131,	
			callback = 
			function()
				if SCENETYPE~=HALL then return end
				if btnConfig[i].flag == "task" then
					--HTTP:call("alliance","task",{ },{success_callback =
					HTTP:call(42021,{},{success_callback =
					function()
						SCENETYPE = TASK
						local task = requires("Scene.gang.taskLayer")
						task:new()
						task:createTask(self)
					end})
				elseif btnConfig[i].flag == "donate" then
					--HTTP:call("alliance","task",{ },{success_callback =
					HTTP:call(42040,{},{success_callback =
					function()
						--self:createDonate()
						SCENETYPE = DONATE
						print("DONATE")
						local donate=requires("Scene.gang.donateLayer")
						donate:new()
						donate:createDonate(self)
					end})
				elseif btnConfig[i].flag == "ranking" then
					--HTTP:call("alliance","totalrank",{ },{success_callback = 
					HTTP:call(42011,{ id=1,lv=3 },{success_callback = 
					function()
						--self:totalRank()
						SCENETYPE = RANK
						local rank=requires("Scene.gang.rankLayer")
						rank:new()
						rank:createRank(self)

					end
					})
				elseif btnConfig[i].flag == "shop" then
					-- HTTP:call("alliance","shop",{ },{success_callback = 
					HTTP:call(42001,{},{success_callback = 
					function()
						self:createShop()
					end
					})
				elseif btnConfig[i].flag == "gang_wars" then
					self:createWars()
--					KNMsg.getInstance():flashShow( "帮战正在研发中，预计在9月中下旬开放，敬请期待！" )
				elseif btnConfig[i].flag == "pray" then
					if DATA_Gang:get( "pray" ) then
						SCENETYPE = PRAY
						--self:refreshGangInfo( { type = 3 , refresh = true } )
						--self:createPray()
						local layer=requires("Scene.gang.prayLayer")
						layer:new()
						layer:createPray(self)
					else
					--	HTTP:call("alliance","clifford",{ },{success_callback = 
						HTTP:call(42031,{ },{success_callback = 
						function()
							--self:createPray()
							local layer=requires("Scene.gang.prayLayer")
							layer:new()
							layer:createPray(self)
						end
						})
					end

				end
			
			end
		}):getLayer()
		layer:addChild( tempBtn )
	end
	
	
	
	--self.viewLayer:addChild(layer)
	--self.baseLayer:addChild( self.viewLayer )
	scrollLayer:addChild(layer)
	
	
	
	
	
	
	local btn_images = {"aide.png" , "aide_press.png"}
	local curTalk
	if checkOpened("talk") ~= true then btn_images = {"aide_press.png"} end
	local temp = KNBtn:new( IMG_PATH .. "image/scene/chat/"  , {"talk_flag1.png"} , 410 , 240 , {
		scale = true ,
		front =  IMG_PATH .. "image/scene/chat/talk_flag.png" , 
		callback = function()
			if SCENETYPE~=HALL then return end
			DATA_Info:setIsMsg( false )
			if DATA_Info:getIsOpen( ) then
				local talkLayer = requires("Scene.common.talk")
				curTalk = talkLayer:new( {type = "gang" } )
				local curScene = display.getRunningScene()
				curScene:addChild( curTalk:getLayer()  )
			else
				curTalk:remove()
			end
			
			DATA_Info:setIsOpen(  not DATA_Info:getIsOpen( ) )
		end
	})
	
	temp:getLayer():setScale(0.7)
	layer:addChild(temp:getLayer())
	DATA_Info:addActionBtn( "gang" , temp )
	if CHANNEL_ID == "tmsjIosAppStore" then
		temp:getLayer():setVisible( false )
		temp:setEnable(false)
	end
	
end
--总榜
function M:totalRank( )
	SCENETYPE = RANK
	self.gangInfoLayer:setVisible(false)
	
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_total.png" )

	
	local listConfig =  {{"ability","tab_ability"} ,  {"tribute","tab_tribute"} , {"rank","tab_rank"}  }
	local activity = "ability"
	self:createList( { alonePageNum = 10 , listConfig = listConfig , defaultType = activity , data = DATA_Gang:get( "gang_rank" ) , defaultPage = 1 } )
end
--成员事件
function M:gangMember( )
	SCENETYPE = MEMBER
	self:refreshGangInfo( { type = 3 , refresh = true } )
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_text2.png" )
	--查看帮派成员列表
	if self.viewLayer then	
		--self.viewLayer:removeSelf()
		self.baseLayer:removeChild(self.viewLayer,true)
		self.viewLayer = display.newLayer()
	end
	local baseBg = display.newSprite(COMMONPATH.."dark_bg_long.png")
	setAnchPos( baseBg , display.cx , 0 , 0.5 , 0)
	self.baseLayer:addChild(baseBg)
	
	local data = { event_movement = DATA_Gang:get("event_movement") , info = DATA_Gang:get("list") }
	local listConfig =  {  {"info","tab_info"} , {"event_movement","tab_event_movement"}  } 
	self:createList( { alonePageNum = 10 , listConfig = listConfig , defaultType = "info" , data = data , defaultPage = 1 } )
end
--生成基本弹出背景图
function M:basePopup( titlePath )
	local bg = display.newSprite( IMG_PATH .. "image/scene/mission/wipe_bg.png")
	local addX = 90
	local addY = 324
	
	local titleBg = display.newSprite( IMG_PATH .. "image/scene/mission/title_bg.png")
	setAnchPos(titleBg, addX , addY)
	bg:addChild(titleBg)
	
	local title = display.newSprite( titlePath )
	setAnchPos(title, addX - 24 , addY )
	bg:addChild(title)
	
	return bg
end
--未加 入帮派时的界面
function M:noGang()
	local NOPATH = PATH .. "no_gang/"
	if self.viewLayer then	
		self.viewLayer:removeSelf()
		self.viewLayer = display.newLayer()
	end
	
	local bg = display.newSprite( NOPATH .. "bg.jpg" )
	setAnchPos(bg , display.cx , display.cy , 0.5 , 0.5)
	self.viewLayer:addChild( bg )
	
	--提示背景
	local tipBg = display.newSprite( NOPATH .. "tip_bg.png" )
	setAnchPos(tipBg , display.cx , 180 , 0.5 )
	self.viewLayer:addChild( tipBg )
	--提示文字
	local str = "加入帮会可获额外体力,还能购买宝石和高星级套装,赶快去加入帮会吧！"
	local tipText = display.strokeLabel( str , 50 , 180 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
			dimensions_width = 380,
			dimensions_height = 80,
			align = 0
		})
	setAnchPos(tipText,display.cx,180,0)
	self.viewLayer:addChild( tipText )
	
	
	
	--创建帮派
	local createBtn = KNBtn:new( COMMONPATH, {"btn_bg_red.png","btn_bg_red_pre.png"}, 50, 135, 
			{
				front = NOPATH .. "create_btn.png",
				--callback = createGang
				callback = function (  )
					requires("Scene.gang.gangcreateLayer"):create(self)
				end
				
			}):getLayer()
	self.viewLayer:addChild( createBtn )
	
	
	--加入帮派
	local joinBtn = KNBtn:new( COMMONPATH, {"btn_bg_red.png","btn_bg_red_pre.png"}, 286, 135, 
			{
				front = NOPATH .. "join_btn.png",
				callback = 
				function()
					HTTP:call("alliance_rank",{ name = gangNameStr},{success_callback = 
					function()
						switchScene("gang" , { type = SEEGANG } )
					end})
				end
			}):getLayer()
	self.viewLayer:addChild( joinBtn )
	
	self.baseLayer:addChild( self.viewLayer )	
end
--申请界面
function M:applyFun()
	print("fddfffffffff apply  function")
	SCENETYPE = APPLY
	local app = requires("Scene.gang.applyLayer")
	app:new()
	app:createApply(self)
	-- GLOBAL_INFOLAYER:refreshTitle( PATH .. "apply_title.png")
	-- local data = { applylist = DATA_Gang:get("applylist") }
	-- local listConfig =  {} 
	-- self:createList( { listConfig = listConfig , defaultType = "applylist" , data = data , defaultPage = 1 } )
end

--输入框
function M:inputBox( params )
	params = params or {}
	local confirmFun = params.confirmFun or function()end
	local cancelFun = params.cancelFun or function()end
	local isFind = params.isFind
	local layer = display.newLayer()
	local mask
	
	
	local findBg = display.newSprite( COMMONPATH .. "tip_bg.png" )
	setAnchPos(findBg , display.cx , 337 , 0.5 )
	layer:addChild( findBg )
	
	
	local findTipSp = display.newSprite(PATH .. "gang_list/" .. ( isFind and "find_tip.png" or "input_notice_title.png" ) )
	setAnchPos(findTipSp , 52 , 538 )
	layer:addChild( findTipSp )
	
	if not isFind then
		layer:addChild(display.strokeLabel( "(50个汉字以内)" , 300 , 440 , 18 , ccc3(0x2c,0x00,0x00) )) 
	end
	
	
	--local inputGangName = CCTextFieldTTF:textFieldWithPlaceHolder( isFind and "请输入帮派ID" or "请输入要编辑的内容" , FONT , 20)
	local inputGangName = ccui.TextField:create(isFind and "请输入帮派ID" or "请输入要编辑的内容" , FONT , 20)
	display.align( inputGangName , display.CENTER_LEFT , 0 , 0)
	inputGangName:setColor( ccc3( 0xff , 0xfb , 0xd4 ) )
	--inputGangName:setColorSpaceHolder( ccc3( 0x4d , 0x15 , 0x15 ) )
	
	local inputGangNameMask = UICutLayer:create()--WindowLayer:createWindow()
	inputGangNameMask:setAnchorPoint( ccp(0 , 0.5) )
	inputGangNameMask:setContentSize( CCSizeMake(370 , 28) )
	inputGangNameMask:setPosition( 55 , 490)
	inputGangNameMask:addChild( inputGangName )
	layer:addChild( inputGangNameMask , 10 )
	
	local inputBg = KNBtn:new(PATH .. "no_gang/", {"input_name_bg.png"} , 47 , 465 , {
		priority = -142 , 
		callback = function()
			inputGangName:setAttachWithIME(true)
		end
	}):getLayer()
	layer:addChild(inputBg)
	
	
	local confirmBtn = KNBtn:new(COMMONPATH , { "btn_bg.png" } , 67 , 364 , {
					front =  COMMONPATH .. "confirm.png" ,
					priority = -142,
					scale = true,
					callback =
					function()
						local gangNameStr = inputGangName:getString()
						confirmFun( gangNameStr )
						inputGangName:setDetachWithIME(true)
						mask:remove()
					end
				}):getLayer()
	layer:addChild(confirmBtn)
	local cancelBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" } , 327 , 364 , {
					front = COMMONPATH .. "cancel.png",
					priority = -142,
					scale = true,
					callback = 
					function()
						inputGangName:setDetachWithIME(true)
						mask:remove()
						cancelFun()
					end,
				}):getLayer()
	layer:addChild(cancelBtn)
	
	
	setAnchPos( layer , 0 , -display.height )
	transition.moveTo( layer , { time = 0.5 , easing = "BACKOUT" , y = 0 })
	
	local scene = display.getRunningScene()
	mask = KNMask:new({ item = layer , priority = -141 })
	scene:addChild( mask:getLayer() )
end
--未加入帮派前查看其它帮派列表
function M:seeGangList()
	local listConfig =  { {"list","tab_list"} , {"ranking","tab_ranking"} }
	local activity = "list"
	if SCENETYPE == RANKING then
		activity = "ranking"
	end
	print("seegamelist rank",DATA_Gang:get("rank"))

	self:createList( { alonePageNum = 10 , listConfig = listConfig , defaultType = activity , data = DATA_Gang:get("rank") , defaultPage = 1 } )
	
	--搜索帮会(由于是放在viewlayer之内，所以高度为-20即可，不需要减去title高度)
	local findBtn = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" } , 
		display.width - 120 , display.height - 120 - 22,
		{
			front = PATH .. "gang_list/" .. "find_gang.png" ,
			callback=function()
				local function confirmFun( str )
					if str and str ~= "" then
						--搜索帮派
						HTTP:call("alliance_getallianceinfo",{ name = str },{success_callback = 
						function(resultData)
							if #resultData==1 then
								self:lookInfo( { type = 1 , data = resultData[1] or {} } )
							else
								KNMsg.getInstance():flashShow("帮派不存在，请输入全名")		
							end
						end})
					else
						KNMsg.getInstance():flashShow( "帮派ID不能为空" )
					end
				end
				self:inputBox({confirmFun = confirmFun , isFind = true })
			end
		}):getLayer()
	self.viewLayer:addChild( findBtn )
end
--重置任务
function M:resetLayout( params )
	params = params or {}

	local layer = display.newLayer()
	local mask
	local TASKPATH = PATH .. "task/"
	local curData = DATA_Gang:get("task").task
	local bg = self:basePopup( PATH .. "task_reset_title.png" )
	setAnchPos( bg , display.cx , display.cy + 30 , 0.5 , 0.5 )
	layer:addChild(bg)
	
	layer:addChild( display.newSprite( TASKPATH .. "task_reset_tip.png" , display.cx , 460):align(display.BOTTOM_CENTER))
	layer:addChild( display.newSprite( TASKPATH .. "task_reset_cost.png" , 155 , 400):align(display.BOTTOM_LEFT) )
	layer:addChild( display.newSprite( COMMONPATH .. "gold.png" , 257 , 400):align(display.BOTTOM_LEFT) )
	layer:addChild( display.strokeLabel( curData.task_reset , 292 , 400 , 20 , ccc3(0x2c , 0x00 , 0x00 ) ) )
	
	
	
	local confirmResetBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" , "btn_bg_red2.png" } , 165 , 307 , {
		front = TASKPATH .. "task_confirm_reset.png",
		priority = -143,	
		callback = function()
			if countGold( curData.task_reset ) then
				if curData.reset_state == 1 then
					HTTP:call("alliance","resettask",{ },{success_callback =
					function()
						mask:remove()
						self:createTask()
					end
					 })
				else
					if DATA_Gang:get("info").userstate < 90 then
						KNMsg.getInstance():flashShow( "权限不足无法重置任务！" )
					else
						KNMsg.getInstance():flashShow( "任务已重置，每天只可重置一次！" )
					end
				end
			end
		end
		})
	
	layer:addChild(confirmResetBtn:getLayer())
	
	
	
	--返回按钮
	local cancelBtn = KNBtn:new(COMMONPATH,{"back_img.png","back_img_press.png"},35,573,{
		scale = true,
		priority = -142,	
		callback = function()
			mask:remove()
		end
	})
	layer:addChild(cancelBtn:getLayer())
	
	setAnchPos( layer , -display.width , 0 )
	transition.moveTo( layer , {time = 0.5 , easing = "BACKOUT" , x = 0 })
	local scene = display.getRunningScene()
	mask = KNMask:new({ item = layer , priority = -141 })
	scene:addChild( mask:getLayer() )
end
--生成list列表
function M:createList( params )
	params = params or {}
	local LISTPATH = PATH .. "gang_list/"
	
	
	if self.viewLayer then	
		self.viewLayer:removeSelf()
		self.viewLayer = nil
		self.viewLayer = display.newLayer()
		self.baseLayer:addChild( self.viewLayer )
	end
	-- if SCENETYPE == SEEGANG then
	-- 	self.viewLayer:setPosition(cc.p(0,-130))
	-- end


	
	
	local data , totalPage , curPage , curType , group , pageText , curData , alonePageNum , listConfig , pageBg , rankText 
	local selectNum , optionFun , countNum	--用手选择参战人员
	local scroll = nil
	self.scroll = nil
	listConfig = params.listConfig			--选项按钮
	data = params.data 						--展示的数据
	curType = params.defaultType 			--默认激活table("list" = 未加入帮派，玩家看到的帮派列表)
	curPage = params.defaultPage or 1 		--默认展示页面
	alonePageNum = params.alonePageNum or 0		--单页item个数
	local isPaging = params.alonePageNum and true or false	--是否分页
	local heightType = 0
	
	
	if curType == "wars_member" then
		countNum = 0
		local members = {}
		--确认选择
		local chooseBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" ,"btn_bg_red2.png"} , 
			163  , 108 ,
			{
				priority = -142 ,
				front = COMMONPATH .. "confirm.png" ,
				callback=
				function()
					if table.nums( members ) == 0 then
						KNMsg.getInstance():flashShow( "请选择参战人员！" )					
					else
						self:createWars()
					end
				end
			})
		self.viewLayer:addChild( chooseBtn:getLayer() )
		self.viewLayer:addChild( display.newSprite( PATH .. "wars/selected.png" , 364 , 700):align(display.BOTTOM_LEFT) )
		self.viewLayer:addChild( display.strokeLabel( "请选择要参加帮战的成员" , 20 , 708 , 22 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
		selectNum = display.strokeLabel( countNum , 435 , 708 , 22 , ccc3( 0xff , 0xfc , 0xd3 ) ) 
		self.viewLayer:addChild( selectNum ) 
		
		optionFun = function( tempParams )
			tempParams = tempParams or {}
			local isSelect = tempParams.isSelect
			local uid = tempParams.uid
			
			countNum = countNum + ( isSelect and 1 or -1 )
			members[ uid .. "" ] = isSelect == true and uid or nil
			
			selectNum:setString( countNum )
		end
	end
	
	
	local function refreshData()
		--echoLog("[ganglayer]",refreshData,curType,table.nums(data))
		if curType == "list" then				--帮派列表
			curData = data.top_list  
		-- elseif curType == "ranking" then		--帮派排行
		-- 	curData = data.top_rank
		elseif curType == "apply" then			--申请加入
			curData = data.top_rank
		elseif curType == "info" then			--帮派成员
			curData = data.info
			--heightType = 2
		elseif curType == "wars_member" then	--选择参战成员
			curData = data.info
			heightType = 2
		elseif curType == "event_movement" then	--帮派事件
			curData = data.event_movement
			--heightType = 2
		elseif curType == "applylist" then		--申请列表
			curData = data.applylist
			heightType = 2
		elseif curType == "appoint" then		--帮内认命
			curData = data.info
		elseif curType == "gem" then			--商城宝石
			curData = data.gemconfig
			--heightType = 2
		-- elseif curType == "task" then			--帮派任务
		-- 	curData = data.task.info
		-- 	heightType = 2
		-- 	return
		end
		echoLog("[ganglayer] curType",curType)
		if isPaging then
			totalPage = math.ceil( #curData / alonePageNum )
			pageText:setString( curPage .. "/" .. totalPage )
		else
			curPage = 1
		end
	end
	
	
	if isPaging then
		--页数背景
		pageBg = display.newSprite( COMMONPATH .. "page_bg.png" )
		setAnchPos(pageBg , 240 , 110 , 0.5)
		self.viewLayer:addChild( pageBg )
		--页数文字
		pageText = display.strokeLabel( curPage .. "/" .. 1  , 230 , 117 , 20 , ccc3(0xff,0xfb,0xd4) )
		setAnchPos( pageText , 240, 117, 0.5 )
		self.viewLayer:addChild(pageText)
	else
		totalPage = nil
	end
	refreshData()
	
	local function createList( )
		if scroll then
			scroll:getLayer():removeSelf()
			scroll = nil
		end
		
		refreshData()
		
		local scrollX , scrollY , scrollWidth , scrollHeihgt
		scrollX			= 15
		scrollY			= isPaging and 155 or 105
		scrollWidth		= 450
		--scrollHeihgt	= isPaging and 530 or 580
		scrollHeihgt    = isPaging and display.height - 120 - 94 - 89 or display.height-120-94-39
		if heightType == 1 then
			scrollY 		= 155
			scrollHeihgt 	= 392
		elseif heightType == 2 then
			scrollY 		= 155
			scrollHeihgt 	= 525
			
		end
		print("ganglayer createList",curType,heightType)
		scroll = KNScrollView:new( scrollX , scrollY , scrollWidth , scrollHeihgt , 5 )
		for i = 1 , ( isPaging and alonePageNum or #curData ) do
			local itemData = curData[ ( curPage - 1 ) * alonePageNum + i ]
			if itemData then
				local tempItem
				print("ganglayer tempItem",curType)
				if curType == "list" or curType=="ranking" or curType=="info" or curType == "appoint" then
					print("ganglayer engine",curType)
					--local itemEngin = requires("Scene.gang.items.listItem")
					local itemEngin = requires("Scene.gang.items."..string.lower(curType).. "Item"):new()
					dump(data)
					tempItem = itemEngin:genItemView(( curType == "appoint" and { isGangMan = data.isGangMan , data = itemData , targetIndex = data.targetIndex ,  position = data.position } or  itemData ),self)
				else
					tempItem = self:listCell( { 
							data = ( curType == "appoint" and { isGangMan = data.isGangMan , data = itemData , targetIndex = data.targetIndex ,  position = data.duty } or  itemData ) , 
							type = curType , 
							parent = scroll , 
							index = ( curPage - 1 ) * alonePageNum + i , 
							checkBoxOpt = optionFun , 
							checked = false ,  
						} )
				end
				scroll:addChild(tempItem, tempItem )
			end
		end
		scroll:alignCenter()
		self.viewLayer:addChild( scroll:getLayer() )
		self.scroll = scroll
	end
	
	
	
	local KNRadioGroup = requires( "Common.KNRadioGroup")
	local startX,startY = 10,display.height - 120 - 30
	if heightType == 1 then startX,startY = 10 , 556 end
	if SCENETYPE == RANK then startX,startY = 18 , 690 end
	
	
	
	
	group = KNRadioGroup:new()
	for i = 1, #listConfig do
		local temp = KNBtn:new( COMMONPATH.."tab/", ( ( SCENETYPE == RANK or SCENETYPE == TASK ) and {"long.png","long_select.png"} or {"tab_star_normal.png","tab_star_select.png"} ) , startX , startY , {
			disableWhenChoose = true,
			upSelect = true,
			id = listConfig[i][1],
			front = { LISTPATH..listConfig[i][1]..".png" , LISTPATH..listConfig[i][2]..".png"},
			callback=
			function()
				curType = listConfig[i][1]
				curPage = 1
				print("ganglayer abcdef",curType)
				createList( listConfig[i][1] )
			end
		},group)
		self.viewLayer:addChild(temp:getLayer())
		startX = startX + temp:getWidth() + ( SCENETYPE == RANK and 12 or 4 )
	end
	--group:chooseById( curType , true )	--激活的选项
	createList()
	
	if curType == "gem" then
		--可用帮威
		local infoData = DATA_Gang:get( "info" )
		infoData.usertribute_v = 599
		local str = ( infoData.usertribute_v > 10000 and math.floor(infoData.usertribute_v/10000) .. "万" or infoData.usertribute_v )
		self.viewLayer:addChild( display.newSprite( LISTPATH .. "usable_prestige.png"	 , 300 , display.height - 120 - 20):align(display.BOTTOM_LEFT) )
		local label = display.strokeLabel( str , 400 , display.height - 120 - 20 , 20 , ccc3( 0xdb , 0x03 , 0x03 ))
		--label = display.createStroke(label:getLabel(),2,ccc3( 0xff , 0xff, 0xff))

		self.viewLayer:addChild(label)

		--local testButton =cc.Sprite:create(COMMONPATH.."long_btn.png")
		-- local testButton = cc.LabelTTF:create("abcdefghijklmnopqrstuvwxyz1234567890123456789","Arial",20)
		-- testButton:setPosition(cc.p(display.cx,display.cy))
		-- testButton = display.createStroke(testButton,2,ccc3(0xff,0xff,0x03))
		-- self.viewLayer:addChild(testButton)
	end
	
	if curType == "info" then
		--总帮威
		local infoData = DATA_Gang:get( "info" )
		local str = string.convertMoney(infoData.contribution)
		self.viewLayer:addChild( display.newSprite( LISTPATH ..  "total_prestige.png" , 280 , display.height- 120 - 20):align(display.BOTTOM_LEFT) )
		self.viewLayer:addChild( display.strokeLabel( str , 400 , display.height - 120 - 20 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
	end
	
	if curType == "task"  then
			--重置任务
			local resetBtn = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" ,"long_btn_grey.png"} , 
				350  , 695 ,
				{
					priority = -142 ,
					front = PATH .. "task/" .. "task_reset.png" ,
					callback=
					function()
						if data.task.reset_state == 1 then
							self:resetLayout()
						else
							if DATA_Gang:get("info").userstate < 90 then
								KNMsg.getInstance():flashShow( "权限不足无法重置任务！" )
							else
								KNMsg.getInstance():flashShow( "任务已经重置！" )
							end
						end
					end
				})
			resetBtn:setEnable( data.task.reset_state == 1 )
			self.viewLayer:addChild( resetBtn:getLayer() )
			
		--总帮威
		local infoData = DATA_Gang:get( "info" )
		local str = ( infoData.usertribute > 10000 and math.floor(infoData.usertribute/10000) .. "万" or infoData.usertribute )
		self.viewLayer:addChild( display.newSprite( LISTPATH ..  "total_prestige.png" , 20 , 695):align(display.BOTTOM_LEFT) )
		self.viewLayer:addChild( display.strokeLabel( str , 140 , 695 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
		end
	
	if curType == "appoint" then
		local str = "请选择要任命的" .. data.text
		if data.text == "帮主" then
			str = "请选择要任命的下一任帮主"
		end
		self.viewLayer:addChild( display.strokeLabel( str , 20 , 708 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
	end

	if curType == "applylist" then
			local gangInfoData = DATA_Gang:get("info")
			print(gangInfoData)
			for k,v in pairs(gangInfoData) do
				print("gangInfoData",k,v)
			end
			local isFull = tonumber(gangInfoData.count) < tonumber(gangInfoData.count_max)	--是否达到收人上限
			
			local oneOkBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" ,"btn_bg_red2.png"} , 
				52 , 105 ,
				{
					priority = -142 ,
					front = LISTPATH .. "one_ok.png" ,
					callback=
					function()
						HTTP:call("alliance","agreeonekey",{ },{success_callback = 
						function()
							self:applyFun()
							
						end})
					end
				})
			oneOkBtn:setEnable( isFull )
			self.viewLayer:addChild( oneOkBtn:getLayer() )
			
			local oneNoBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" } , 
				285 , 105 ,
				{
					priority = -142 ,
					front = LISTPATH .. "all_no.png" ,
					callback=
					function()
						HTTP:call("alliance","refuseonekey",{ },{success_callback = 
						function()
							self:applyFun()
						end})
					end
				}):getLayer()
			self.viewLayer:addChild( oneNoBtn )
			
			--设置验证
			local checkingBtn 
			checkingBtn = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" } , 
				358 , 700 ,
				{
					priority = -142 ,
					front = LISTPATH .. ( gangInfoData.state == 1 and "auto_add.png" or "checking_text.png" ),
					callback=
					function()
						HTTP:call("alliance","switchuser",{ id = data.id },{success_callback = 
						function(resultData)
							gangInfoData = DATA_Gang:get("info")
							local str
							if gangInfoData.state == 1 then
								str = "您已将帮会收人的设置修改为 自动加入"
							else
								str = "您已将帮会收人的设置修改为 验证加入"
							end
							KNMsg.getInstance():flashShow( str )
							checkingBtn:setFront( LISTPATH .. ( gangInfoData.state == 1 and "auto_add.png" or "checking_text.png" ) )
						end})
					end
				})
			self.viewLayer:addChild( checkingBtn:getLayer() )
			
			self.viewLayer:addChild(display.newSprite(LISTPATH .. "set_text.png" , 320 , 720 ))
			self.viewLayer:addChild(display.newSprite(LISTPATH .. "member_num.png" , 72 , 720 ))
			self.viewLayer:addChild(display.strokeLabel( gangInfoData.count .. "/" .. gangInfoData.count_max , 125 , 708 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
	end
	

	self.viewLayer:addChild( display.newSprite( COMMONPATH.."tab_line.png"  , 6 , startY - 5 ):align(display.BOTTOM_LEFT) )
	--翻页按钮
	if isPaging then
		local pre = KNBtn:new(COMMONPATH,{"next_big.png"}, 150, 100, {
			scale = true,
			flipX = true,
			callback = function()
				if curPage > 1 then
					curPage = curPage - 1
					createList( curType )
				end
			end
		})
		self.viewLayer:addChild(pre:getLayer())
		local next = KNBtn:new(COMMONPATH,{"next_big.png"}, 285, 100, {
			scale = true,
			callback = function()
				if curPage < totalPage then
					curPage = curPage + 1
					createList( curType )
				end
			end
		})
		self.viewLayer:addChild(next:getLayer())
	end
	
end
--生成列表item
function M:listCell( params )
	params = params or {}
	local type = params.type or 0 
	local data = params.data or {}
	local index = params.index
	local parent = params.parent
	local ITEMPATH = PATH .. "gang_list/"
	
	local layer = display.newLayer()
	--背景
	local bg
	if type == "ranking" or type == "rank" then
		bg = KNBtn:new( COMMONPATH , { "item_bg.png" } ,  0 , 0 , 
			{
				parent = parent ,
				upSelect = true , 
--				priority = -140 , 
				callback=
				function()
					--查看其它帮派信息
					HTTP:call("alliance","getallianceinfo",{ id = data.id },{success_callback = 
					function(resultData)
						self:lookInfo( { type = 0 , data = resultData or {} } )
					end})
				end
			}):getLayer()
		layer:addChild( bg )
	else
		local str = type == "task" and IMG_PATH .. "image/scene/activity/item_bg.png" or COMMONPATH .. "item_bg.png"
		bg = display.newSprite( str )
		setAnchPos(bg , 0 , 0) 
		layer:addChild( bg )
	end
	
	--是否存在复选框
	if params.checkBoxOpt then
		local checkBox
		checkBox = KNCheckBox:new( 377 , 15 , {
										path= COMMONPATH , 
										parent = params["parent"],
										checkBoxOpt = function() 
											params["checkBoxOpt"]( { isSelect = checkBox:isSelect() , uid = data.uid } )
										end,
										file={"checkbox_bg.png","checkbox_choose.png","checkbox_lock.png"}})
		layer:addChild(checkBox:getLayer())
		checkBox:check( params["checked"] )
	end
	
	local titleElement , addX , addY
	
	local function createItem()
		for i = 1 , #titleElement do
			addX = 50 + math.floor( ( i - 1 ) / 3 ) * 260
			addY = 73 - (( i - 1 ) % 3) * 30
			local curTitle = titleElement[i].title
			if 	curTitle ~= "" then
				local tempTitle = display.newSprite( ITEMPATH .. curTitle .. ".png")
				setAnchPos( tempTitle , addX , addY )
				layer:addChild( tempTitle )
				
				local tipText
				addX = addX +  65
				
				if curTitle == "gang_rank" then
					if 	type == "rank" or type == "ranking"then
						if titleElement[i].text<=3 then
							tipText = display.newSprite( IMG_PATH .. "image/scene/ranklist/" .. titleElement[i].text ..".png" )
							setAnchPos( tipText , addX , addY - 10  )
						else
							tipText = display.strokeLabel( titleElement[i].text , addX , addY - 8 , 40 , ccc3( 0xff , 0x00 , 0x00 ) )
						end
					else
						tipText = getImageNum( tonumber( titleElement[i].text ) , COMMONPATH .. "small_num.png" )
						setAnchPos( tipText , addX , addY )
					end
				elseif curTitle == "gang_member" then
					tipText = display.strokeLabel( titleElement[i].text , addX , addY - 8 , 40 , ccc3( 0xff , 0x00 , 0x00 ) )
				else
					tipText = display.strokeLabel( titleElement[i].text , addX+120 , addY , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
							dimensions_width = i<4 and 200 or 80,
							dimensions_height = 25,
							align = 0
						})
				end
				layer:addChild( tipText )
			end
		end
	end
	
	-- if type == "ranking" then
	-- 	titleElement = {
	-- 						{ title = "gang_title" , 	text = data.name  .. "(Lv" .. data.level ..")"} , 										--帮派名
	-- 						{ title = "gang_man_title" ,text = "" .. data.chief_name  } , 							--帮主
	-- 						{ title = "ability_title" ,	text = "  " .. data.sum_ability } , 						--战力
	-- 						{ title = "" , 				text = data.level } , 											--帮派等级
	-- 						{ title = "gang_rank" , 	text = data.top } ,											--排名
	-- 						{ title = "gang_member" , 	text = data.count .."/" .. data.count_max } ,				--成员数
	-- 					}
	-- 	createItem()
	if type == "gem"  then
		local curProp = awardCell( data , { parent = params.parent} ) 
		setAnchPos( curProp , 15 , 30 , 0 , 0 )
		layer:addChild(curProp)
		
		layer:addChild( display.strokeLabel( data.name , 88 , 75 , 18 , ccc3( 0x2c , 0x00 , 0x00 ) ))
		layer:addChild( display.strokeLabel( data.bagdesc , 88 , 50 , 18 , ccc3( 0xac , 0x25 , 0x0f ) ))
		layer:addChild( display.strokeLabel( "帮威：" .. data.tribute , 88 , 25 , 18 , ccc3( 0x2c , 0x00 , 0x00 ) ))
		
		local buyBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
			335 , 8 ,
			{
				parent = params.parent , 
				front = COMMONPATH .. "buy.png" ,
				callback=
				function()
					self:buy( { data = data , index = index - 1 } )
				end
			})
		buyBtn:setEnable( data.state == 1 )
		layer:addChild( buyBtn:getLayer() )
		--开启宝石加成
		local openBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
			237 , 8 ,
			{
				parent = params.parent , 
				front = PATH .. "shop/open_text.png" ,
				callback=
				function()
					local function openFun()
						local isTribute = DATA_Gang:get("info").tribute > data.tribute	--帮贡
						local isTunds = DATA_Gang:get("info").funds > data.funds		--资金
						--local isUserstate = DATA_Gang:get("info").userstate == 100	--是否是帮主
						if isTribute and isTunds   then
							--HTTP:call("alliance_wakeprop",{ propid = index - 1 },{success_callback = 
							HTTP:call("alliance_wakeprop",{ propid = data.cid },{success_callback = 
							function()
								KNMsg.getInstance():flashShow( "开启成功!" )
								self:createShop( )
							end})
						else
							KNMsg.getInstance():flashShow( "资金或帮贡不足" )
						end
					end
					
					KNMsg.getInstance():boxShow( "开启需要花费" .. data.tribute ..  "帮贡和"  ..  data.funds .. "资金，确认开启吗？",{ 
--																	confirmText = SCENECOMMON .. "navigation/na_charge_big.png" , 
																	confirmFun = openFun , 
																	cancelFun = function() end 
																	} )

          
				end
			})
		openBtn:setEnable( data.state == 0 and 
			(DATA_Gang:get("info").duty == 100 
				or DATA_Gang:get("info").duty==95 
				or DATA_Gang:get("info").duty==96))
		layer:addChild( openBtn:getLayer() )
	elseif type == "wars_member" then
		titleElement = {
						{ title = "meber_name" , 	text = data.name } , 						--成员名称
						{ title = "meber_tribute" ,	text = data.tribute } , 					--成员帮威
						{ title = "meber_ability" ,	text = data.ability } , 					--成员战力
						{ title = "meber_title" , 	text = data.title } ,						--成员头衔
					}
		createItem()
	-- elseif type == "info" then
	-- 	titleElement = {
	-- 					{ title = "meber_name" , 	text = data.name } , 						--成员名称
	-- 					{ title = "meber_tribute" ,	text = data.tribute } , 					--成员帮威
	-- 					{ title = "meber_ability" ,	text = data.ability } , 					--成员战力
	-- 					{ title = "meber_title" , 	text = data.title } ,						--成员头衔
	-- 				}
	-- 	createItem()
		
	-- 	--查看其它成员
	-- 	local lookBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
	-- 		347 , 11 ,
	-- 		{
	-- 			parent = params.parent , 
	-- 			front = PATH .. "gang_list/" ..  "look.png" ,
	-- 			callback=
	-- 			function()
	-- 				HTTP:call(20007,{ touid = data.uid },{success_callback = 
	-- 				function()
	-- 					local otherPalyerInfo = requires("Scene.common.otherPlayerInfo")
	-- 					display.getRunningScene():addChild( otherPalyerInfo:new():getLayer() )
	-- 				end})
	-- 			end
	-- 		}):getLayer()
	-- 	layer:addChild( lookBtn )
	-- 	local isOpen = DATA_Gang:get("info").post == 100 --帮主才能踢人
	-- 	--踢出其它成员
	-- 	local removeBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
	-- 		237 , 11 ,
	-- 		{
	-- 			parent = params.parent , 
	-- 			front = PATH .. "gang_list/" ..  ( ( isOpen and data.state ~= 100 ) and "remove.png" or "remove_dis.png") ,
	-- 			callback=
	-- 			function()
	-- 				if data.state ~= 100 then
	-- 					KNMsg.getInstance():boxShow( "你确认要将  " .. data.name .. "  踢出帮会吗？" ,{ 
	-- 																confirmFun = function()
	-- 																	HTTP:call("alliance","excluding",{ touid = data.uid },{ success_callback = 
	-- 																	function() 
	-- 																		self:gangMember()
	-- 																	end })
	-- 																end , 
	-- 																cancelFun = function() end 
	-- 																} )

	-- 				else
	-- 					KNMsg.getInstance():flashShow( "权限不足，无法完成操作" )
	-- 				end
	-- 			end
	-- 		})
	-- 	removeBtn:setEnable( isOpen and data.state ~= 100 )--不可以踢出自己
	-- 	layer:addChild( removeBtn:getLayer() )		
		
		
	elseif type == "event_movement" then
	
		local str = data.event .. "   " .. data.time
		local eventText = display.strokeLabel( str , 30 , 75 , 20 , ccc3( 0xcc , 0x13 , 0x11 ) , nil , nil , {
					dimensions_width = 400 ,
					dimensions_height = 25,
					align = 0
				})
		layer:addChild( eventText )	
		
		str =data.content 	
		local contentText = display.strokeLabel( str , 30 , 10 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
					dimensions_width = 400 ,
					dimensions_height = 65,
					align = 0
				})
		layer:addChild( contentText )		
		
	elseif type == "applylist" then
		titleElement = {
				{ title = "look" } , 							--查看
				{ title = "reject" } , 							--拒绝
				{ title = "agree" } , 							--同意
			}
			
		local str = data.name .. "(Lv" .. data.level .. ")" .. "申请加入帮会"
		local tipText = display.strokeLabel( str , 30 , 67 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
					dimensions_width = 400 ,
					dimensions_height = 25,
					align = 0
				})
		layer:addChild( tipText )
		
		for i = 1 , #titleElement do
			local curFalg = titleElement[i].title
			local tempBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" } , 
				150 + ( i - 1 ) * 100 , 15 ,
				{
					parent = params.parent , 
					front = PATH .. "gang_list/".. curFalg .. ".png" ,
					callback=
					function()
						if curFalg == "look" then
							HTTP:call(20007,{ touid = data.uid },{success_callback = 
								function()
									local otherPalyerInfo = requires("Scene.common.otherPlayerInfo")
									display.getRunningScene():addChild( otherPalyerInfo:new():getLayer() )
								end})
						elseif curFalg == "reject" then
							HTTP:call("alliance","refuse",{ touid = data.uid },{success_callback = 
								function()
									self:applyFun()
								end})
						elseif curFalg == "agree" then
							print("agree",DATA_Gang:get("info").guildId )
							HTTP:call("alliance_agree",{ touid = data.uid,guildid = DATA_Gang:get("info").guildId },{success_callback = 
								function()
									self:applyFun()
								end})
						end
					end
				}):getLayer()
			layer:addChild( tempBtn )
		end
		layer:setContentSize( bg:getContentSize() )
	end
	

	
	layer:setContentSize( bg:getContentSize() )
	return layer
end

--职务Icon
function M:positionCell( params )
	params = params or {}
	local group = params.group	--职位编号
	local index = params.index 	--堂口编号
	local data  = params.data or nil 	--是否有人
	local targetIndex = params.targetIndex --职位索引
	local APPOINTPATH = PATH .. "appoint/"
	local layer = display.newLayer()
	--认命按钮
	if group ~= 1 then
		local isApply = data ~= nil 
		local appointBtn = KNBtn:new( COMMONPATH , 
				{ "btn_bg.png" , "btn_bg_pre.png" } , 
				0 , 0 ,
				{
					priority = -142 ,
					front = PATH .. "appoint/" ..  ( isApply and "retire.png" or "appoint.png" ) ,
					callback=
					function()
						if isApply then
							HTTP:call("alliance","manage",{ touid = data.uid , state = 0 , index = 0 },{success_callback = 
							function()
								self:appointFun()	
							end})
						else
							SCENETYPE = APPOINT
							local listConfig =  { }
							local textConfig = { "帮主" , "副帮主" ,{"青龙堂堂主" , "白虎堂堂主" ,"朱雀堂堂主" ,"玄武堂堂主"}}
							local positionConfig = { 100 , 95 , 90 , 0 }
							self:createList( { alonePageNum = 10 , listConfig = listConfig , defaultType = "appoint" , data = { targetIndex = targetIndex ,  position = positionConfig[group] , info = DATA_Gang:get( "list" ) , 
								text = ( group == 3 and textConfig[group][index] or textConfig[group] )} , defaultPage = 1 } )
						end
					end
				}):getLayer()
		setAnchPos(appointBtn , -41 , 0.5)
		layer:addChild( appointBtn )	
	end
	
	
	--人物角色
	local otherData = nil
	local textData = nil
	if data then
		otherData = { IMG_PATH.."image/scene/common/navigation/level_bg.png" , -17 , 54 } 
		textData = { { data.level  , 18 , ccc3( 0xbf , 0x3a , 0x01 ) , { x = -33 , y = 34	} , nil , 20 } , 
					{ data.name , 18 , ccc3( 0x2c , 0x00 , 0x01 ) , { x = 1 , y = -45	} , nil , 20 } }
	end
	local roleIcon = KNBtn:new( COMMONPATH , { data and "sex".. data.sex .. ".png" or "role_frame.png" } , 7 , 70 ,
		{
			front = data and  COMMONPATH .. "role_frame.png"  or nil,
			other = otherData , 
			text = textData ,
			callback = function()end,
		}):getLayer()
	setAnchPos(roleIcon , 0 - 32  , 70 , 0.5)
	layer:addChild( roleIcon )
	
	--职务背景
	local positionBg = display.newSprite( APPOINTPATH .. "position_bg.png")
	setAnchPos( positionBg , 0 , 170 , 0.5 , 0.5 )
	layer:addChild( positionBg )
	
	local positionElement = {
				{ "gang_man"} ,
				{ "deputy_gang_man" , "deputy_gang_man" } ,
				{ "dragon" ,"tiger" , "bird" , "tortoise" } }
	
	--职务名称
	local position = display.newSprite( APPOINTPATH .. positionElement[group][index] ..  ".png")
	setAnchPos( position , 0 , 170 , 0.5 , 0.5 )
	layer:addChild( position )
	
	
	layer:setContentSize( CCSize( 110 , 190 ) )
	return layer
end
function M:appointDetails( parent )
	SCENETYPE = APPOINT
	local listConfig =  { }
	local textConfig = { "帮主" , "副帮主" ,{"青龙堂堂主" , "白虎堂堂主" ,"朱雀堂堂主" ,"玄武堂堂主"}}
	local positionConfig = { 100 , 95 , 90 , 0 }
	parent:createList( { alonePageNum = 10 , listConfig = listConfig , defaultType = "appoint" , data = { targetIndex = targetIndex ,  position = positionConfig[group] , info = DATA_Gang:get( "list" ) , 
		text = ( group == 3 and textConfig[group][index] or textConfig[group] )} , defaultPage = 1 } )
end
--认命界面
function M:appointFun(  )
							
	SCENETYPE = APPOINT
	
	GLOBAL_INFOLAYER:refreshTitle( PATH .. "appoint/" .. "appoint_title.png")
	
	local app = requires("Scene.gang.appointLayer"):new()
	app:createAppointMaster(self)
	-- if self.viewLayer then	
	-- 	self.viewLayer:removeSelf()
	-- 	self.viewLayer = display.newLayer()
	-- end
	
	-- local layer = display.newLayer()
	
	-- local bg = display.newSprite( COMMONPATH .. "figure_bg.png" )
	-- setAnchPos( bg , display.cx , display.cy + 14 , 0.5 , 0.5)
	-- layer:addChild( bg )
	-- layer:addChild( display.newSprite( PATH .. "appoint/" .. "line1.png" , display.cx , 576):align(display.BOTTOM_CENTER) )
	-- layer:addChild( display.newSprite( PATH .. "appoint/" .. "line2.png" , display.cx , 340):align(display.BOTTOM_CENTER) )
	
	
	-- local titleVein = display.newSprite( PATH .. "appoint/" .."title_vein.png" )
	-- setAnchPos(titleVein , display.cx , 700 , 0.5 )
	-- layer:addChild(titleVein)
	
	-- local roomMan = display.newSprite( PATH .. "appoint/" .. "room_man.png" )
	-- setAnchPos(roomMan , display.cx , 368 , 0.5 )
	-- layer:addChild(roomMan)
	
	-- local infoData = DATA_Gang:get( "list" )
	
	-- local leadTable = { {} , {} , {} }	--查找帮主，副帮主，堂主数据
	-- for i = 1 , #infoData do
	-- 	if infoData[i].duty == 100 then
	-- 		leadTable[1][1] = infoData[i]
	-- 	elseif infoData[i].duty == 95 or infoData[i].duty == 96 then
	-- 		leadTable[2][#leadTable[2]+1] = infoData[i]
	-- 	elseif infoData[i].duty == 81 or infoData[i].duty==82 or infoData[i].duty==83 or infoData[i].duty==84 then
	-- 		leadTable[3][#leadTable[3]+1] = infoData[i]
	-- 	end
	-- end
	
	-- local positionElement = {
	-- 		{ y = 550 , position = 1 , data = { { x = 200 } } } ,
	-- 		{ y = 397 , position = 2 , data = { { x = 112 } , { x = 284 } } } ,
	-- 		{ y = 153 , position = 3 , data = { { x = 36  } , { x = 145 } , { x = 255  } , { x = 365  } } } ,}
			
	
	-- for i = 1 , #positionElement do
	-- 	local position = positionElement[i].position
	-- 	for j = 1 , #positionElement[i].data do
	-- 		local curData = positionElement[i].data[j]
	-- 		--positionCell 含任命按钮
	-- 		dump(leadTable)
	-- 		local temp = self:positionCell( { targetIndex = j , group = position , index = j , data = leadTable[position][j] } )
	-- 		setAnchPos(temp , curData.x + 40 ,positionElement[i].y , 0.5)
	-- 		layer:addChild( temp )	
	-- 	end
	-- end
	
	
	-- local str = "说明：只有帮主可以任命帮会职位，且帮威排名前10才可被任命！"
	-- local tipText = display.strokeLabel( str , 14 , 105 , 16 , ccc3( 0xff , 0xfb , 0xd4 ) , nil , nil , {
	-- 			dimensions_width = 466 ,
	-- 			dimensions_height = 25,
	-- 			align = 0
	-- 		})
	-- layer:addChild( tipText )
	
	-- self.viewLayer:addChild(layer)
	-- self.baseLayer:addChild( self.viewLayer )	
end
--帮派详细信息页面
function M:lookInfo( params )
	params = params or {}
	local data = params.data or {}
	local isNoGang = params.type == 1
	local layer = display.newLayer()
	local mask
	
	local bg = self:basePopup( PATH .. "gang_info.png" )
	setAnchPos( bg , display.cx , display.cy + 30 + 60 , 0.5 , 0.5 )
	layer:addChild(bg)
	
	local subheight = 97
	local infoBg = display.newSprite( PATH .. "info_bg.png" )
	setAnchPos( infoBg , 429/2 , subheight - 8 , 0.5)
	bg:addChild( infoBg )
	
	local infoTitle = display.newSprite( PATH .. "info_title.png" )
	setAnchPos( infoTitle , 429/2 - 105 , subheight )
	bg:addChild( infoTitle )

	
	local elementTable = {
							{ text = data.name .. "  (Lv" .. data.level .. ")" } , 		--帮派等级								--帮派名
							{ text = '<>'} , 											--ID
							{ text = data.count .. "/" .. data.count_max  } ,					--成员数
							{ text = data.sum_ability} , 									--战力
							{ text = data.chief_name } , 								--帮主
	}
	
	for i = 1 , #elementTable do
		local str = elementTable[i].text
		--361 is basepopup height
		local tipText = display.strokeLabel( str , 220 + 100 , 341-30- i * 42 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
					dimensions_width = 200 ,
					dimensions_height = 25,
					align = 0
				})
		--layer:addChild( tipText )
		bg:addChild(tipText)
	end
	
	local applyBtn
	local buttonHeight = 25
	--生成申请加入
	local function createApplyBtn()
		print("func createApplyBtn ")
		if applyBtn then
			applyBtn:getLayer():removeSelf()
			applyBtn = nil
		end
		
		local applyData = DATA_Gang:getApply()
		local isApply = false	--当前帮派是否已经申请
		
		if applyData then
			for key , v in pairs( applyData ) do
				if v.guildId == tonumber( data.guildId ) then
					isApply = true
					break
				end
			end
		end
		
		applyBtn = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" , "long_btn_grey.png"} , 70 , buttonHeight ,
			{
				priority = -142 ,
				front = PATH .. "gang_list/" .. ( isApply and "cancel_apply.png" or "apply.png" ) ,
				callback=
				function()
					if isApply then
						--取消申请
						HTTP:call("alliance_ApplicationCancel",{ id = data.guildId },{success_callback = createApplyBtn})
					else
						if table.nums( applyData ) >= 5 then
							KNMsg.getInstance():flashShow( "同时最多只可向5个帮会发送入帮申请" )
						else
							HTTP:call("alliance_ApplicationApply",{ id = data.guildId },{success_callback = 
								function()
									if ( not DATA_Gang:isJoinGang() ) then
										
										switchScene("gang",nil,function ()
											KNMsg.getInstance():flashShow("申请已提交")
										end) 
									else
										createApplyBtn() 
									end
								end})
						end
					end
				end
			})
		if isApply == true then applyBtn:setStateView(3) end
--		applyBtn:setEnable( not isApply )
		bg:addChild( applyBtn:getLayer() )
	end
	--搜索或者普通查看
	if isNoGang then
		createApplyBtn()
	end
	
	--返回
	local backBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" } , 
		isNoGang and 243 or (429 - 146) / 2 , buttonHeight ,
		{
			priority = -142 ,
			front = COMMONPATH .. "back.png" ,
			callback=
			function()
				mask:remove()
			end
		}):getLayer()
	bg:addChild( backBtn )
	
	
	setAnchPos( layer , -display.width , 0 )
	transition.moveTo( layer , {time = 0.5 , easing = "BACKOUT" , x = 0 })
	local scene = display.getRunningScene()
	layer:setContentSize(bg:getContentSize())
	mask = KNMask:new({ item = layer , priority = -141 })
	scene:addChild( mask:getLayer() )
end
function M:back(  )
	switchScene("gang")
end

--查看权限界面
function M:lookPrivilege( params )
	params = params or {}
	
	local layer = display.newLayer()
	local mask
	local PRIVILEGEPATH = PATH .. "privilege/"
	
	local bg = self:basePopup( PRIVILEGEPATH .. "title.png" )
	setAnchPos( bg , display.cx , display.cy + 30 , 0.5 , 0.5 )
	layer:addChild(bg)
	local jobFlag = DATA_Gang:get("info").duty 
	local jobElement = { ["0"] = "帮众" , ["90"] = "堂主" , ["95"] = "副帮主" , ["100"] = "帮主" ,}
	if jobFlag > 10 then

	else
		jobElement[jobFlag..""] = "帮众"
	end
	local privilegeDescription = ui.newTTFLabel({
				        text = "你是本帮" .. jobElement[ jobFlag .. "" ] .. "，拥有以下权限：",
				        size = 20,
				        color = ccc3( 0x2c , 0x00 , 0x00 ),
				        x = 429/2,
				        y = 290,
				        align = ui.TEXT_ALIGN_CENTER
    				})		
	bg:addChild(privilegeDescription)
	-- bg:addChild( display.strokeLabel( "你是本帮" .. jobElement[ jobFlag .. "" ] .. "，拥有以下权限：" ,
	-- 				 0 , 300 , 20 , ccc3(0x2c , 0x00 , 0x00 ) , nil , nil , {
	-- 				dimensions_width = 480 ,
	-- 				dimensions_height = 24,
	-- 				align = 1
	-- 			}) )
	
	local elementPrivilege = {
								"gang_up" , 										--帮派升级
								"abdicate" , 										--转让帮主
								"gang_appoint" , 									--帮内任命
								"gang_add_member" , 								--帮会收人
								"member_manage" , 									--成员管理
								"quit_gang" , }										--退出帮会
	--权限配置
	local privilegeConfig = {
		["0"] 	= { gang_up = "gang_up" , member_manage = "member_manage" , quit_gang = "quit_gang" } ,
		["90"] 	= { gang_up = "gang_up" , member_manage = "member_manage" , quit_gang = "quit_gang" ,	gang_add_member = "gang_add_member" } , 		
		["95"] 	= { gang_up = "gang_up" , member_manage = "member_manage" , quit_gang = "quit_gang" ,	gang_add_member = "gang_add_member"} , 		
		["100"] = { gang_up = "gang_up" , member_manage = "member_manage" , abdicate = "abdicate" ,		gang_add_member = "gang_add_member" ,  gang_appoint = "gang_appoint" ,  } , }			
		
	local privilegeValue = privilegeConfig[ jobFlag .. "" ]
	
	local isOpen	--是否开启对应功能
	for i = 1 , #elementPrivilege do
		isOpen = privilegeValue[elementPrivilege[i]] and true or false
		local addX , addY = 80 + ( ( i - 1 ) % 2 ) * 170 , 200 - math.floor( ( i - 1 )/2 ) * 73
		local tempBtn = KNBtn:new( COMMONPATH , ( isOpen and { "long_btn.png" , "long_btn_pre.png" } or { "long_btn_grey.png" } ), 
			addX , addY ,
			{
				front = PRIVILEGEPATH .. elementPrivilege[i] .. ".png" ,
				priority = -142,	
				callback=
				function()
					if elementPrivilege[i] == "gang_appoint" then
						if DATA_Gang:get("list") then
							self.gangInfoLayer:setVisible(false)
							mask:remove()
							self:appointFun()
						else 
							HTTP:call("alliance_getmembers",{},{success_callback = function ( )
								self.gangInfoLayer:setVisible(false)
								mask:remove()
								self:appointFun()
							end})
						end
					elseif elementPrivilege[i]=="abdicate" then
						HTTP:call("alliance_getmembers",{},{success_callback=function ( ... )
							self.gangInfoLayer:setVisible(false)
							mask:remove()
							local listConfig =  { }
							local selection = requires("Scene.gang.appointSelectLayer"):new()
							--local layer = selection:createSelectList({ alonePageNum = 10 , listConfig = listConfig , defaultType = "appoint" , data = { targetIndex = targetIndex ,  position = positionConfig[group] , info = DATA_Gang:get( "list" ) , 
							local layer = selection:createSelectList({ alonePageNum = 10 , listConfig = listConfig , defaultType = "appoint" , data = { targetIndex = targetIndex ,  position = 100 , info = DATA_Gang:get( "list" ) , 
						 	--text = ( group <= 3 and textConfig[group][index] or textConfig[group] )} , defaultPage = 1 },
						 	text = GangTitle_config[100],
						 	isGangMan = true},defaultPage = 1},

						 	self)
							
							local KNMask = requires("Common.KNMask")
							self.mask = KNMask:new( { item = layer } )
							local Scene = display.getRunningScene()
							Scene:addChild( self.mask:getLayer() )
								
						end})						
					elseif elementPrivilege[i]=="gang_add_member" then
						HTTP:call("alliance_fresh",{ },{success_callback = 
						function()
							local applylist = DATA_Gang:get("applylist")
							if #applylist == 0 then
								KNMsg.getInstance():flashShow("暂无其他玩家申请")
							else
								self.gangInfoLayer:setVisible(false)
								mask:remove()
								self:applyFun()
							end
						end})	
					elseif elementPrivilege[i] == "member_manage" then
						HTTP:call("alliance_eventmovement",{ },{success_callback =
						function()
							mask:remove()
							self:gangMember()
						end})
					elseif elementPrivilege[i]=="quit_gang" then
						KNMsg.getInstance():boxShow( "退出帮派后,帮威将全部清空,你确认要退出帮会吗？" ,{ 
											confirmFun = function()
												HTTP:call("alliance","excluding",{ touid = DATA_Session:get("uid") },{ success_callback = 
													function() 
														switchScene("gang")
													end })
											end , 
											cancelFun = function() end 
											} )

					elseif elementPrivilege[i]=="gang_up" then
						dump(DATA_Gang:get("gangup"))
						local needReload = DATA_Gang:get("gangup") == nil or DATA_Gang:get("gangup").curlevel ~= DATA_Gang:get("info").level
						print("upgrade            .......",DATA_Gang:get( "gangup" ))
						--if not DATA_Gang:get( "gangup" ) then
						if needReload == true then
							HTTP:call("alliance_escalate",{ level = DATA_Gang:get("info").level },{success_callback = 
							function()
								mask:remove()
								self:gangUpFun()
							end})
						else

							--if DATA_Gang:get("info").level < 5 then
								mask:remove()
								self:gangUpFun()
							-- else
							-- 	KNMsg.getInstance():flashShow( "帮派已经达到5级，不需要再升级了" )
							-- end
						end
					end
				end
			})
		tempBtn:setEnable( isOpen )
		
		bg:addChild( tempBtn:getLayer() )
	end
	
	--返回按钮
	local cancelBtn = KNBtn:new(COMMONPATH,{"back_img.png","back_img_press.png"},35,320,{
		scale = true,
		priority = -142,	
		callback = function()
			mask:remove()
		end
	})
	bg:addChild(cancelBtn:getLayer())
	
	setAnchPos( layer , -display.width , 0 )
	transition.moveTo( layer , {time = 0.5 , easing = "BACKOUT" , x = 0 })
	local scene = display.getRunningScene()
	mask = KNMask:new({ item = layer , priority = -141 })
	scene:addChild( mask:getLayer() )
end
--帮会升级
function M:gangUpFun()
	params = params or {}
	
	local layer = display.newLayer()
	local mask
	local UPPATH = PATH .. "gang_up/"
	local data = DATA_Gang:get( "gangup" )
	
	local bg = self:basePopup( UPPATH .. "gang_up_title.png" )
	setAnchPos( bg , display.cx , display.cy + 30 , 0.5 , 0.5 )
	layer:addChild(bg)
	
	layer:addChild( display.newSprite(UPPATH .. "up_tip.png" , 		display.cx 	, 580):align(display.CENTER))
	layer:addChild( display.newSprite(UPPATH .. "frame.png" , 		display.cx 	, 502):align(display.CENTER))
	layer:addChild( display.newSprite(UPPATH .. "up_cost.png" , 	display.cx 	, 423):align(display.CENTER))
	layer:addChild( display.newSprite(UPPATH .. "frame2.png", 		display.cx 	, 372):align(display.CENTER))
	layer:addChild( display.newSprite(UPPATH .. "left_title.png"  ,	61			, 450):align(display.BOTTOM_LEFT))
	layer:addChild( display.newSprite(UPPATH .. "right_title.png" ,	243			, 524):align(display.BOTTOM_LEFT))
	layer:addChild( display.newSprite(UPPATH .. "bottom_title.png",	61			, 342):align(display.BOTTOM_LEFT))
	
	local curLv = DATA_Gang:get("info").level
	data.infoconfig = data.infoconfig or {}
	data.infoconfig = data.infoconfig or {}
	data.infoconfig[ curLv .. "" ] = data.infoconfig[ curLv .. "" ] or {}
	data.infoconfig[ curLv + 1 .. "" ] = data.infoconfig[  curLv + 1 .. "" ] or {}
	local showElement = { 
							{ now = curLv ,  later = curLv + 1 } , 
							{ now = 30+data.infoconfig[curLv .. ""].power or 0 ,  later = 30 + ( data.infoconfig[ curLv + 1 .. "" ].power or 0 )  } , 
							{ now = data.infoconfig[curLv .. ""].nummax or 0 ,  later = data.infoconfig[ curLv + 1 .. ""].nummax or 0 } , 
							{ now = nil ,  later = nil } , 
							{ now = data.infoconfig[curLv .. ""].task_num or 0  ,  later = data.infoconfig[ curLv + 1 .. "" ].task_num or 0 } , 
						 }
	local addX , addY
	for i = 1 , 5 do
		if showElement[i].now then
			addX , addY = 180 + ( ( i - 1 ) % 2 ) * 180 , 532 - math.floor( ( i - 1 ) / 2 ) * 37
			layer:addChild( display.newSprite(UPPATH .. "up_flag.png"	, addX , addY):align(display.BOTTOM_LEFT))
			addY = addY - 5
			layer:addChild( display.strokeLabel( showElement[i].now 	, addX - 30 , addY , 20 , ccc3(0x2c , 0x00 , 0x00 ) )	)
			layer:addChild( display.strokeLabel( showElement[i].later 	, addX + 35 , addY , 20 , ccc3(0x2c , 0x00 , 0x00 ) )	)
		end
	end
	--data.lvconfig = data.lvconfig or {}
	--data.lvconfig[curLv+1 .. ""] = data.lvconfig[curLv+1 .. ""] or {}
	local curNum = ( data.infoconfig[curLv+1 .. ""].task_exp or 0 )
	local curNum1 = DATA_Gang:get("info").exp
	local curNum2 = ( data.infoconfig[curLv+1 .. ""].task_money or 0 ) 
	local curNum3 = DATA_Gang:get("info").funds
	
	local needElement = {
						banggong = { need = curNum  , 	have = curNum1 } , 
						zijin    = { need = curNum2  , 	have = curNum3 } , 
	}
	layer:addChild( display.strokeLabel( ( curNum > 10000 and ( math.floor(curNum/10000) .. "万" ) or curNum )  		, 122 , 373 , 20 , ccc3(0x2c , 0x00 , 0x00 ) )	)
	layer:addChild( display.strokeLabel( ( curNum2 > 10000 and ( math.floor(curNum2/10000) .. "万" ) or curNum2 ) 	, 122 , 341 , 20 , ccc3(0x2c , 0x00 , 0x00 ) )	)
	
	layer:addChild( display.strokeLabel( ( curNum1 > 10000 and ( math.floor(curNum1/10000) .. "万" ) or curNum1 ) 	, 327 , 373 , 20 , ccc3(0x6c , 0xd9 , 0x1e ) )	)
	layer:addChild( display.strokeLabel( ( curNum3 > 10000 and ( math.floor(curNum3/10000) .. "万" ) or curNum3 )	, 327 , 341 , 20 , ccc3(0x6c , 0xd9 , 0x1e ) )	)
	
	local isUp = true	--是否可升级
	for key , v in pairs(needElement) do
		if v.have < v.need  then
			isUp = false
			break
		end
	end
	
	local prayBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" , "btn_bg_red2.png" } , 165 , 290  , {
	front = UPPATH .. "confirm_up_text.png",
	priority = -133,	
	callback = function()
		HTTP:call("alliance_upgrade",{},{success_callback = 
		function()
			print("alliance_upgrade success")
			KNMsg.getInstance():flashShow( "升级成功" )
			--self:refreshGangInfo( { type = 0 , refresh = true } )
			mask:remove()
--			self:gangUpFun()
		end})	
	end})
	prayBtn:setEnable( isUp )
	layer:addChild( prayBtn:getLayer() )
	
	
	--返回按钮
	local cancelBtn = KNBtn:new(COMMONPATH,{"back_img.png","back_img_press.png"},35,573,{
		scale = true,
		priority = -142,	
		callback = function()
			mask:remove()
			self:refreshGangInfo( { type = 0 , refresh = true } )
		end
	})
	layer:addChild(cancelBtn:getLayer())
	
	setAnchPos( layer , -display.width , 0 )
	transition.moveTo( layer , {time = 0.5 , easing = "BACKOUT" , x = 0 })
	local scene = display.getRunningScene()
	mask = KNMask:new({ item = layer , priority = -132 })
	scene:addChild( mask:getLayer() )
end

--创建商城
function M:createShop()
	
	local SHOPPATH = PATH .. "shop/"
	SCENETYPE = SHOP
	self:refreshGangInfo( { type = 3 , refresh = true } )
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_shop_title.png" )
	if self.viewLayer then	
		--self.viewLayer:removeSelf()
		self.baseLayer:removeChild(self.viewLayer,true)
		self.viewLayer = display.newLayer()
		self.baseLayer:addChild( self.viewLayer )

		self:hideScroll()
	end
	
	
	local listConfig =  { {"gem","tab_gem"} }
	local activity = "gem"
	self:createList( { alonePageNum = 10 , listConfig = listConfig , defaultType = activity , data = DATA_Gang:get( "shop" ) , defaultPage = 1 } )
end

--购买弹出
function M:buy( params )
	params = params or {}
	local data = params.data or {}
	local index = params.index or 0
	local TEMPPATH = IMG_PATH.."image/scene/shop/"
	local layer = display.newLayer()
	local mask
	local baseX , baseY = display.cx , display.cy - 28
	
	layer:addChild( display.newSprite(COMMONPATH .. "tip_bg.png" ,  display.cx + 3 , baseY - 28):align(display.BOTTOM_CENTER) )
	--物品Icon
	layer:addChild( KNBtn:new( SCENECOMMON , { "skill_frame1.png" } , baseX - 148 , baseY + 135 , {  front = getImageByType( data.cid , "s") } ):getLayer())	
	-- 名字
	layer:addChild( display.strokeLabel( data.name , baseX - 75 , baseY + 170 , 20 , ccc3(0x2c , 0x00 , 0x00 ) , nil , nil , { dimensions_width = 130 , dimensions_height = 30 ,align = 0 } ) )
	
	--花费总数
	local valueBg = display.newSprite(TEMPPATH.."value_bg.png")
	valueBg:setScaleY(0.89)
	setAnchPos(valueBg , baseX - 17 , baseY + 130 , 0.5)
	layer:addChild(valueBg)
	
	layer:addChild( display.newSprite( PATH .. "usertribute_v.png" , baseX - 75  , baseY + 140):align(display.BOTTOM_LEFT) )
	

	

	
	
	--购买数量
	layer:addChild( display.newSprite(TEMPPATH.."num_text.png" , baseX + 55  , baseY + 140):align(display.BOTTOM_LEFT) )
	
	--购买数量
	local num = 1 
	layer:addChild(display.newSprite(TEMPPATH.."num_bg.png" , baseX + 116 , baseY + 136):align(display.BOTTOM_LEFT) )
	
	--数量文本
	local numText = display.strokeLabel( 1 .. "" , baseX + 116 , baseY + 136 , 20 , ccc3(0xff , 0xfb , 0xd5 ) , nil , nil , { dimensions_width = 36 , dimensions_height = 30 ,align = 1 } )
	layer:addChild(numText)
	local priceText = display.strokeLabel( data.tribute , baseX - 40 , baseY + 135 , 20 , ccc3(0x2c , 0x00 , 0x00 ) , nil , nil ,{ dimensions_width = 80 , dimensions_height = 30 ,align = 0} )
	layer:addChild(priceText)
	--修改数值
	local function changeValue()
		numText:setString(num)
		priceText:setString( num * data.tribute )
	end
	
	
	
	
	--划动条
	local slider = KNSlider:new( "buy" ,  {
		x = baseX - 106 , 
		y = baseY -53 , 
		minimum = 1 , 
		maximum = 20, 
		value = 1 , 
		callback  = function( _curIndex )
			num = _curIndex
			changeValue()
		end,
		priority = -140
	})
	layer:addChild( slider )
	
	--增减按钮
	local addBtn = KNBtn:new(COMMONPATH,{"next_big.png"} , baseX + 128 , baseY + 77 , {
		scale = true,
		priority = -132 ,
		callback = function()
			if num < 99 then
				num = num + 1
				slider:setValue( num )
				changeValue()
			end
		end
	})
	
	local minusBtn = KNBtn:new(COMMONPATH,{"next_big.png"} , baseX - 165 , baseY + 77 ,{
		scale = true,
		priority = -132 ,
		callback = function()
			if num > 1 then
				num = num - 1
				slider:setValue( num )
				changeValue()
			end
		end
	})
	minusBtn:setFlip(true)
	
	addBtn:getLayer():setScale( 0.9 )
	minusBtn:getLayer():setScale( 0.9 )
	layer:addChild(addBtn:getLayer())
	layer:addChild(minusBtn:getLayer())
	

	

	
	
	--确定，取消按钮
	local ok = KNBtn:new(COMMONPATH,{"btn_bg.png","btn_bg_pre.png"}, baseX - 134 , baseY ,{
		front = COMMONPATH.."ok.png" ,
		scale = true,
		priority = -132,
		callback = function()
			if DATA_Gang:get("info").usertribute_v >= num * data.tribute then
				
				HTTP:call("alliance" , "shopprop" , {
					id = index,
					num = num
				} , {
					success_callback = function()
						mask:remove()
						KNMsg.getInstance():flashShow( "购买成功  " .. "消耗帮威:" .. num * data.tribute .. "  获得" .. data.name .. "X" .. num )
					end
				})
			else
				KNMsg.getInstance():flashShow( "可用帮威不足！" )
			end

		end
	})
	local cancel = KNBtn:new(COMMONPATH,{"btn_bg.png","btn_bg_pre.png"} , baseX + 54 , baseY ,{front = COMMONPATH.."cancel.png",scale = true,priority = -132,callback=
		function()
			mask:remove()
		end})
	layer:addChild(ok:getLayer())
	layer:addChild(cancel:getLayer())
	
	setAnchPos( layer , 0 , -display.height )
	transition.moveTo( layer , { time = 0.5 , easing = "BACKOUT" , y = 0 })
	mask = KNMask:new( { item = layer ,  priority = -131 } )
	local scene = display.getRunningScene()
	scene:addChild( mask:getLayer() )
end
--帮战/帮会战主界面
function M:createWars()
	local WARSPATH = PATH .. "wars/"
	SCENETYPE = WARS
	self:refreshGangInfo( { type = 3 , refresh = true } )
	
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_wars_title.png" )
	
	local layer = display.newLayer()
	if self.viewLayer then	
		--self.viewLayer:removeSelf()
		self.baseLayer:removeChild(self.viewLayer)
		self.viewLayer = nil
		self.viewLayer = display.newLayer()
		self.baseLayer:addChild( self.viewLayer )
		self.viewLayer:addChild( layer )
	end
	
	layer:addChild( display.newSprite( PATH .. "hall/notice_bg2.png" , display.cx , 615):align(display.BOTTOM_CENTER) )	--上背景
	local shandLayer = display.newLayer()
	shandLayer:addChild( display.newSprite( SCENECOMMON .. "prop_bg.png" , display.cx , 617):align(display.BOTTOM_CENTER) )	--阴影
	shandLayer:addChild( display.newSprite( SCENECOMMON .. "prop_bg.png" , display.cx , 617):align(display.BOTTOM_CENTER) )	--阴影
	shandLayer:setScaleX( 1.2 )
	layer:addChild( shandLayer )
	layer:addChild( display.newSprite( PATH .. "appoint/title_vein.png" , display.cx , 705):align(display.BOTTOM_CENTER) )	--上饰纹
	layer:addChild( display.newSprite( WARSPATH .. "wars_info_title.png" , display.cx , 705 ):align(display.BOTTOM_CENTER) )	--帮战信息标题
	
	layer:addChild( display.newSprite( COMMONPATH .. "half_bg.png" , display.cx , 109):align(display.BOTTOM_CENTER) )	--下背景
	layer:addChild( display.newSprite( WARSPATH .. "shade_bg.png" , display.cx , 200):align(display.BOTTOM_CENTER) )	--背景框
	layer:addChild( display.newSprite( PATH .. "appoint/title_vein.png" , display.cx , 570):align(display.BOTTOM_CENTER) )	--下饰纹
	layer:addChild( display.newSprite( WARSPATH .. "wars_rule_title.png" , display.cx , 570 ):align(display.BOTTOM_CENTER) )	--帮战信息标题
	
	layer:addChild( display.newSprite( WARSPATH .. "gang_wars_time.png" , 45, 534):align(display.BOTTOM_LEFT) )	--帮战时间
	layer:addChild( display.newSprite( WARSPATH .. "apply_condition.png" , 45, 475):align(display.BOTTOM_LEFT) )	--服名条件
	layer:addChild( display.newSprite( WARSPATH .. "win_condition.png" , 45, 360):align(display.BOTTOM_LEFT) )	--胜利条件
	
	--榜单按钮
	layer:addChild( KNBtn:new( WARSPATH , { "rank.png" } , 358 , 628 , { 
								scale = true , 
								callback = function()
 			 						HTTP:call("bangzhan","get_rank",{ },{success_callback =
									function()
										 self:createWarsRank()
									end})
								end}):getLayer())	
	--领取奖励
	layer:addChild( KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" , "long_btn_grey.png" } , 340 , 207 , { 
								scale = true , 
								front = COMMONPATH .. "all_get.png" , 
								callback = function()
									 
								end}):getLayer())	
	--参战人员
	layer:addChild( KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" , "btn_bg_red2.png" } , 58 , 115 , { 
								scale = true , 
								front = WARSPATH .. "go_to_war_member.png" , 
								callback = function()
			 						HTTP:call("alliance","eventmovement",{ },{success_callback =
									function()
										self:createWarsMember()
									end})
								end}):getLayer())	
	--参与帮战
	layer:addChild( KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" , "btn_bg_red2.png" } , 272 , 115 , { 
								scale = true , 
								front = WARSPATH .. "go_to_gang_wars.png" , 
								callback = function()
			 						HTTP:call("gangbattle_enter", {wid=1}, {
										success_callback = function(data)
											
											switchScene("war", data)
										end
									})	
								end}):getLayer())	
	--帮战时间
	-- local lblEvent = display.strokeLabel( "每周一、周三、周五、周六晚上19：00开始" , 150 , 506 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
	-- 				dimensions_width = 276 ,
	-- 				dimensions_height = 55,
	-- 				align = 0 
	-- 				}) 
	local lblEvent = ui.newTTFLabel({
		        text = "每周一、周三、周五、周六晚上19：00开始",
		        size = 20,
		        color = ccc3( 0x2c , 0x00 , 0x00 ),
		        x = 150,
		        y = 560,
		        align = ui.TEXT_ALIGN_LEFT,
		        --dimensions = cc.size(330,200)
		        dimensions = cc.size(276,55)
			})
	lblEvent:setAnchorPoint(cc.p(0,1))
	layer:addChild( lblEvent)	
	--报名条件
	local str = "由帮主报名筛选 10帮会成员参与帮战，帮主报名参加，帮主需要在本周第一次帮战开启前20分钟的任意时段报名"
	lblJoinCondition = ui.newTTFLabel({
        text = str,
        size = 20,
        color = ccc3( 0x2c , 0x00 , 0x00 ),
        x = 150,
        y = 500,
        align = ui.TEXT_ALIGN_LEFT,
        --dimensions = cc.size(330,200)
        dimensions = cc.size(276,104)
	})
	lblJoinCondition:setAnchorPoint(cc.p(0,1))
	layer:addChild( lblJoinCondition)	
				
	--胜利条件
	local str = "由帮主报名筛选10名帮会成员参与帮战，帮战胜利所有成员将可获得帮威望，帮会获得大量帮贡和荣誉"
	lblVictoryCondition = ui.newTTFLabel({
        text = str,
        size = 20,
        color = ccc3( 0x2c , 0x00 , 0x00 ),
        x = 150,
        y = 385,
        align = ui.TEXT_ALIGN_LEFT,
        --dimensions = cc.size(330,200)
        dimensions = cc.size(276,104)
	})
	lblVictoryCondition:setAnchorPoint(cc.p(0,1))
	layer:addChild( lblVictoryCondition)
	--战胜消息
	lblVictoryMessage = ui.newTTFLabel({
        text = "10月1日本帮战胜了【很腹黑猥琐】",
        size = 20,
        color = ccc3( 0x2c , 0x00 , 0x00 ),
        x = 40,
        y = 225,
        align = ui.TEXT_ALIGN_LEFT,
        --dimensions = cc.size(330,200)
        dimensions = cc.size(320,25)
	})
	lblVictoryMessage:setAnchorPoint(cc.p(0,0.5))
	layer:addChild( lblVictoryMessage)
	--下期帮战
	lblForwardMessage = ui.newTTFLabel({
        text = "10月6日19:00帮理工对手【王者屌丝逆袭】帮会",
        size = 20,
        color = ccc3( 0x2c , 0x00 , 0x00 ),
        x = 40,
        y = 165,
        align = ui.TEXT_ALIGN_LEFT,
        --dimensions = cc.size(330,200)
        dimensions = cc.size(414,25)
	})
	lblForwardMessage:setAnchorPoint(cc.p(0,0.5))
	layer:addChild(lblForwardMessage )
		
	local warsInfo = {}			
	for i = 1 , 3 do
		warsInfo[i] = display.strokeLabel( "10月1日本帮战胜了【很腹黑猥琐】" , 45 , 700 - i * 24  , 20 , ccc3( 0x2c , 0x00 , 0x00 ))
		layer:addChild( warsInfo[i] )
	end
	
	local totalTime = 60	--刷新总时间
	local coutTime = 0		--计时器
	
	--刷新讯息	
	local function refreshWarsInfo()
		if true then		--帮战时间还没有到则直接返回
			return
		end
		
		coutTime = coutTime + 1
		
		if coutTime > totalTime then
			HTTP:call("alliance","notice",{ notice = str },{success_callback = 
			function(data)
				local infoData = DATA_Gang:get( "wars" )
				for i = 1 , #warsInfo do
					warsInfo[i]:setString( infoData[i] )
				end
			end})	
		end
	end
	
	KNClock:addTimeFun( "wars" , refreshWarsInfo )
end
--参战成员选择
function M:createWarsMember( )
	SCENETYPE = WARSMEBER
	
	self:refreshGangInfo( { type = 3 , refresh = true } )
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_wars_title.png" )
	--查看帮派成员列表
	if self.viewLayer then	
		self.viewLayer:removeSelf()
		self.viewLayer = display.newLayer()
	end
	
	local data = { event_movement = DATA_Gang:get("event_movement") , info = DATA_Gang:get("list") }
	local listConfig =  {} 
	self:createList( { listConfig = listConfig , defaultType = "wars_member" , data = data , defaultPage = 1 } )
end
--帮战榜单界面
function M:createWarsRank( )
	SCENETYPE = WARSRANK
	local WARSPATH = PATH .. "wars/"
	local speedTime = 0.3	--线路运动时间 
	local data = DATA_Gang:get( "warsRank" ) or {}
	
	self:refreshGangInfo( { type = 3 , refresh = true } )
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_wars_title.png" )
	local layer = display.newLayer()
	if self.viewLayer then	
		self.viewLayer:removeSelf()
		self.viewLayer = nil
		self.viewLayer = display.newLayer()
		self.baseLayer:addChild( self.viewLayer )
		self.viewLayer:addChild( layer )
	end
	
	layer:addChild( display.newSprite( WARSPATH .. "wars_rank_title.png" , display.cx , 688):align(display.BOTTOM_CENTER) )
	
	
	layer:addChild( display.newSprite( WARSPATH .. "roadmap.png" , display.cx , 130):align(display.BOTTOM_CENTER) )
	local actionLayer = display.newLayer()	--动画层
	layer:addChild( actionLayer )
	layer:addChild( display.newSprite( WARSPATH .. "first_gang.png" , display.cx + 2 , 364):align(display.BOTTOM_CENTER))
	
	data["1"] =  data["1"] or {}
	data["1"]["1"] = data["1"]["1"] or {}
	layer:addChild( display.strokeLabel( data["1"]["1"].name or "" , display.cx - 60  , 400 , 20 , ccc3( 0xff , 0xe3 , 0x37 ) , nil , nil , {
					dimensions_width = 120,
					dimensions_height = 28,
					align = 1
				}) )
				
				
	local function gangRankCell( params )
		params = params or {}
		local type = params.type
		local data = params.data or {}
		
		local bgBtn = KNBtn:new( WARSPATH , { "rank_item_bg" .. type .. ".png"  } , 0 , 0 , {
								scale = true , 
								callback = function()
--									HTTP:call("alliance","getallianceinfo",{ id = data.id },{success_callback = 
									HTTP:call("alliance","getallianceinfo",{ id = 1 },{success_callback = 
									function(resultData)
										self:lookInfo( { data = resultData or {} } )
									end})
								end} ):getLayer()
		bgBtn:addChild( display.strokeLabel( data.name or "" , 15  , 0 , 18 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
						dimensions_width = 65,
						dimensions_height = 55,
						align = 1
					}) )
		return bgBtn
	end
	
	local addX = { 172 , 115 , 0 }
	local distance = { 115 , 230 , 0 }
	local dataList = { 8 , 4 , 2 }
	local lookBattle = {} 	--查看战斗按钮存储
	
	for t = 1 , 2 do
		local actionCout = 0
		local initY = t==1 and 30 or 714
		local isUp = t==1 and true or false	--是否向上
		local tempY = 0
		for i = 1 , 3 do
			local totalNum = math.floor( 4 / i)
			tempY = tempY + 20 * ( 6 - i )
			local addY = initY + ( isUp and tempY or -tempY )
			local delayValue = ( actionCout * ( speedTime * 2 ) )--动画延迟时间
			
			local curGroupData = data[dataList[i]..""]
			if table.nums( curGroupData ) >= 1 then
				actionCout = actionCout + 1
			end
			
			for j = 1 , totalNum do
				local x = display.cx - addX[i]  + ( j - 1 ) * distance[i] -  95/2
				local index = j + ( t - 1 ) *totalNum 
				local curData = curGroupData[ index .. "" ]
				local item = gangRankCell( { group = i , color = j%2 , type = ( i == 1 and 1 or 2 ) ,  data = curData } )
				setAnchPos( item , x , addY , 0 , 0 )
				layer:addChild( item )
				
				--查看战斗
				local btnOffY = ( i==2 and 60 or 40 )
				local newY = addY - ( isUp and btnOffY or -( btnOffY + 39 ) )
				if i>=2 then
					lookBattle[ t .. ( i - 1 ) .. j ] =  KNBtn:new( PATH .. "wars/" , { "look_battle.png" , "look_battle_pre.png" } , x + 30 , newY , {
										scale = true ,
										id = t .. ( i - 1 ) .. j , 
										callback = function()
											dump( t .. ( i - 1 ) .. j )
										end
									}):getLayer() 
					layer:addChild( lookBattle[ t .. ( i - 1 ) .. j ] )
				end
				
				--横向线条
								
				local color = j%2
				if curData and i ~= 3 then
					local scaleFactor = { 0.7 , 1.3 , 0 }
					local str = ( color == 0 and "left_red.png" or "left_blue.png" )
					local line = display.newSprite( WARSPATH .. str )
					setAnchPos( line , x + 95/2 , addY + 22 , math.abs( color - 1 )  , 0 )
					actionLayer:addChild( line )
					line:setScaleX( 0.1 )
					transition.scaleTo( line , { delay = delayValue , scaleX = scaleFactor[i] , time= speedTime  } )
				end
				--竖向线动画
				if color == 0 or i == 3 then
					local winColor = 1
					newY = addY - ( isUp and -39 or -39 ) 
					local scaleFactor = { 1 , 0.4 , 1 }
					local str = winColor == 0 and "up_red.png" or "up_blue.png"
					local line = display.newSprite( WARSPATH .. str )
					setAnchPos( line , x - ( distance[i] - 95 ) / 2 - 1   , newY   , 0.5  , isUp and 0 or 1 )
					actionLayer:addChild( line )
					line:setScaleY( 0.1 )
					transition.scaleTo( line , { delay = ( i == 3 and delayValue or delayValue + speedTime ), scaleY = scaleFactor[i] , time= speedTime  } )
				end
				
			end
		end
	end
	
	
end

return M