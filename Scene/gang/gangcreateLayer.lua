local M = {}
local KNBtn = requires( "Common.KNBtn")
local KNMask = requires("Common.KNMask")
function M:create( parent )
	local m_nogangPath = IMG_PATH .. "image/scene/gang/no_gang/"
	local createGangLayer = self.createGangLayer
	--由于父节点knmask已经不存在，所以老是出现
	--invalid cobj in function 'lua_cocos2dx_Node_getParent'
	--不确定会不会内存滞留
	if createGangLayer then
		createGangLayer:removeSelf()
		createGangLayer = nil
	end
	createGangLayer = display.newLayer()
	self.createGangLayer = createGangLayer
	local bg =  parent:basePopup( m_nogangPath .. "create_gang_title.png" )
	setAnchPos( bg , display.cx , 440 , 0.5 , 0.5 )
	createGangLayer:addChild(bg)
	
	--帮派名字提示
	bg = display.newSprite( m_nogangPath .. "name_tip.png")
	setAnchPos(bg, display.cx, 570, 0.5 , 0.5 )
	createGangLayer:addChild(bg)
	
	local tempText = display.strokeLabel( "（6个汉字以内）", 330 , 470 , 14 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
		dimensions_width = 120,
		dimensions_height = 25,
		align = 0
	})
	createGangLayer:addChild( tempText )
	
	-- local inputGangName = cc.TextFieldTTF:textFieldWithPlaceHolder("请输入帮派名称" , FONT , 20)
	local inputGangName = ccui.TextField:create("请输入帮派名称" , FONT , 20)
	display.align( inputGangName , display.CENTER_LEFT , 0 , 0)
	inputGangName:setColor( ccc3( 0xff , 0xfb , 0xd4 ) )
	-- inputGangName:setColorSpaceHolder( ccc3( 0x4d , 0x15 , 0x15 ) )
	
	local inputGangNameMask = UICutLayer:create()--WindowLayer:createWindow()
	inputGangNameMask:setAnchorPoint( ccp(0 , 0.5) )
	inputGangNameMask:setContentSize( CCSizeMake(350 , 28) )
	inputGangNameMask:setPosition( 62 , 525 )
	inputGangNameMask:addChild( inputGangName )
	createGangLayer:addChild( inputGangNameMask , 10 )
	
	local inputBg = KNBtn:new( m_nogangPath , {"input_name_bg.png"} , 48 , 500 , {
		priority = -140 , 
		callback = function()
			inputGangName:setAttachWithIME(true)
		end
	}):getLayer()
	createGangLayer:addChild(inputBg)
	
	--创建需求
	bg = display.newSprite( m_nogangPath .. "create_need.png")
	setAnchPos(bg, 60, 450 )
	createGangLayer:addChild(bg)
	
	local isMeet = 0 
	local completeY = 416
	local interval = 30
	local function addConfirmFlag( index )
		isMeet = isMeet + 1
		local completeFalg = display.newSprite( m_nogangPath .. "complete_flag.png")
		setAnchPos( completeFalg , 70, completeY  - ( index - 1 ) *  interval  )
		createGangLayer:addChild( completeFalg )
	end
	
	local needTable = {
						{ curValue = DATA_User:get("lv") , 						maxValue = 15 , 		tip = "1.等级达到15级(" } , 
						{ curValue = DATA_User:get("silver") ,				maxValue = 300000 , 	tip = "2.银两30万("} , 
						{ curValue = DATA_Bag:getTypeCount("prop", "16020") ,	maxValue = 1 , 			tip = "3.建帮派令1个(" },}
						
	for i = 1 , #needTable do
		local str = needTable[i].tip .. needTable[i].curValue .. "/" .. needTable[i].maxValue .. ")"
		local tempText = display.strokeLabel( str, 220 , completeY -  ( i - 1 ) * interval , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
			dimensions_width = 300,
			dimensions_height = 25,
			align = 0
		})
	
		createGangLayer:addChild( tempText )
		
		if needTable[i].curValue >= needTable[i].maxValue then
			addConfirmFlag( i )
			tempText:setColor(  ccc3( 0xef , 0x00 , 0x00 )  )
		end
	end
	
	--返回按钮
	local cancelBtn = KNBtn:new(COMMONPATH,{"back_img.png","back_img_press.png"},35,573,{
		scale = true,
		priority = -131,	
		callback = function()
			inputGangName:setDetachWithIME(true)
			createMask:remove()
			self.createGangLayer = nil
			createGangLayer = nil
			
		end
	})
	createGangLayer:addChild(cancelBtn:getLayer())
	
	--确认创建按钮
	local cancelBtn = KNBtn:new(COMMONPATH,
		isMeet >= 3 and {"btn_bg_red.png","btn_bg_red_pre.png"} or { "btn_bg_red2.png" } , 
		display.cx - 73 , 290 ,
		{
			front = m_nogangPath .. "confirm_create.png",
			priority = -131,	
			callback = function()
				if isMeet >= 3 then
					local gangNameStr = inputGangName:getString()
					if gangNameStr == "" then
						KNMsg.getInstance():flashShow( "帮派名字不能为空" )
						return
					end
					
					if string.len( gangNameStr ) <= 10 then
						local propId = DATA_Bag:cidByData("16020" , "id" )
						HTTP:call("alliance_create",{ name = gangNameStr , propid = propId },{success_callback = 
						function(data)
							inputGangName:setDetachWithIME(true)
							switchScene( "gang" )	
						end})
				
					else
						KNMsg.getInstance():flashShow( "帮派名字过长，请修改！" )
					end
				end
			end
		}):getLayer()
	createGangLayer:addChild(cancelBtn)
	
	local function onTouch(eventType , x , y)
		inputGangName:setDetachWithIME(true)
	end
	createGangLayer:setTouchEnabled( true )
	-- createGangLayer:registerScriptTouchHandler(onTouch , false , -140 , true)



	local listener = cc.EventListenerTouchOneByOne:create()
	listener:setSwallowTouches(false)
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		inputGangName:setDetachWithIME(true)
		return true
	end, cc.Handler.EVENT_TOUCH_BEGAN)
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y
	
	end,cc.Handler.EVENT_TOUCH_MOVED)	
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y
	
	end,cc.Handler.EVENT_TOUCH_ENDED)
	createGangLayer:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, createGangLayer)


	
	local scene = display.getRunningScene()
	createMask = KNMask:new({ item = createGangLayer })
	scene:addChild( createMask:getLayer() )
	
	setAnchPos( createGangLayer , 0 , display.height , 0 , 0 )
	transition.moveTo( createGangLayer , { time = 0.5 , y = 0 , easing = "BACKOUT"})
	return self.createGangLayer
end
return M