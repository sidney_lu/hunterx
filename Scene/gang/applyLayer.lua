local M = {}
local PATH = IMG_PATH .. "image/scene/gang/"
local KNBtn = requires( "Common.KNBtn")
function M:new( params )
	local this = {}
	setmetatable(this , self)
	self.__index = self
end


function M:createApply( parent,params )	
	self.baseLayer = parent.baseLayer
	self.viewLayer = parent.viewLayer
	GLOBAL_INFOLAYER:refreshTitle( PATH .. "apply_title.png")

	self.baseBg = display.newSprite(COMMONPATH.."dark_bg_long.png")
	setAnchPos( self.baseBg , display.cx , 0 , 0.5 , 0)
	self.baseLayer:addChild(self.baseBg)
	local data = { applylist = DATA_Gang:get("applylist") }
	if table.nums(data.applylist) == 0 then
		--空
	else
		local listConfig =  {} 
		self:createList( { listConfig = listConfig , defaultType = "applylist" , data = data , defaultPage = 1 } )
	end
end
--生成list列表
function M:createList( params )
	params = params or {}
	local LISTPATH = PATH .. "gang_list/"
	
	
	if self.viewLayer then	
		--self.viewLayer:removeSelf()
		self.baseLayer:removeChild(self.viewLayer,true)
		self.viewLayer = nil
		self.viewLayer = display.newLayer()
		self.baseLayer:addChild( self.viewLayer )
	end
	-- if SCENETYPE == SEEGANG then
	-- 	self.viewLayer:setPosition(cc.p(0,-130))
	-- end


	
	
	local data , totalPage , curPage , curType , group , pageText , curData , alonePageNum , listConfig , pageBg , rankText 
	local selectNum , optionFun , countNum	--用手选择参战人员
	local scroll = nil
	self.scroll = nil
	listConfig = params.listConfig			--选项按钮
	data = params.data 						--展示的数据
	curType = params.defaultType 			--默认激活table("list" = 未加入帮派，玩家看到的帮派列表)
	curPage = params.defaultPage or 1 		--默认展示页面
	alonePageNum = params.alonePageNum or 0		--单页item个数
	local isPaging = params.alonePageNum and true or false	--是否分页
	local heightType = 0
	
	
	if curType == "wars_member" then
		countNum = 0
		local members = {}
		--确认选择
		local chooseBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" ,"btn_bg_red2.png"} , 
			163  , 108 ,
			{
				priority = -142 ,
				front = COMMONPATH .. "confirm.png" ,
				callback=
				function()
					if table.nums( members ) == 0 then
						KNMsg.getInstance():flashShow( "请选择参战人员！" )					
					else
						self:createWars()
					end
				end
			})
		self.viewLayer:addChild( chooseBtn:getLayer() )
		self.viewLayer:addChild( display.newSprite( PATH .. "wars/selected.png" , 364 , 700):align(display.BOTTOM_LEFT) )
		self.viewLayer:addChild( display.strokeLabel( "请选择要参加帮战的成员" , 20 , 708 , 22 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
		selectNum = display.strokeLabel( countNum , 435 , 708 , 22 , ccc3( 0xff , 0xfc , 0xd3 ) ) 
		self.viewLayer:addChild( selectNum ) 
		
		optionFun = function( tempParams )
			tempParams = tempParams or {}
			local isSelect = tempParams.isSelect
			local uid = tempParams.uid
			
			countNum = countNum + ( isSelect and 1 or -1 )
			members[ uid .. "" ] = isSelect == true and uid or nil
			
			selectNum:setString( countNum )
		end
	end
	
	
	local function refreshData()
	
		curData = data.applylist

		echoLog("[ganglayer] curType",curType)
		if isPaging then
			totalPage = math.ceil( #curData / alonePageNum )
			pageText:setString( curPage .. "/" .. totalPage )
		else
			curPage = 1
		end
	end
	
	
	if isPaging then
		--页数背景
		pageBg = display.newSprite( COMMONPATH .. "page_bg.png" )
		setAnchPos(pageBg , 240 , 110 , 0.5)
		self.viewLayer:addChild( pageBg )
		--页数文字
		pageText = display.strokeLabel( curPage .. "/" .. 1  , 230 , 117 , 20 , ccc3(0xff,0xfb,0xd4) )
		setAnchPos( pageText , 240, 117, 0.5 )
		self.viewLayer:addChild(pageText)
	else
		totalPage = nil
	end
	refreshData()
	
	local function createList( )
		if scroll then
			scroll:getLayer():removeSelf()
			scroll = nil
		end
		
		refreshData()
		
		local scrollX , scrollY , scrollWidth , scrollHeihgt
		scrollX			= 15
		scrollY			= isPaging and 155 or 105
		scrollWidth		= 450
		--scrollHeihgt	= isPaging and 530 or 580
		scrollHeihgt    = isPaging and display.height - 120 - 94 - 89 or display.height-120-94-29
		if heightType == 1 then
			scrollY 		= 155
			scrollHeihgt 	= 392
		elseif heightType == 2 then
			scrollY 		= 155
			scrollHeihgt 	= 525
			
		end
		print("ganglayer createList",curType,heightType)
		scroll = KNScrollView:new( scrollX , scrollY , scrollWidth , scrollHeihgt , 5 )
		for i = 1 , ( isPaging and alonePageNum or #curData ) do
			local itemData = curData[ ( curPage - 1 ) * alonePageNum + i ]
			if itemData then
				local tempItem
	
				local itemEngin = requires("Scene.gang.items.applyListItem"):new()
				tempItem = itemEngin:genItemView(itemData,self)
	
				scroll:addChild(tempItem, tempItem )
			end
		end
		scroll:alignCenter()
		self.viewLayer:addChild( scroll:getLayer() )
		self.scroll = scroll
	end
	
	
	
	local KNRadioGroup = requires( "Common.KNRadioGroup")
	local startX,startY = 10,display.height - 120 - 30
	if heightType == 1 then startX,startY = 10 , 556 end
	if SCENETYPE == RANK then startX,startY = 18 , 690 end
	
	
	
	
	group = KNRadioGroup:new()
	for i = 1, #listConfig do
		local temp = KNBtn:new( COMMONPATH.."tab/", ( ( SCENETYPE == RANK or SCENETYPE == TASK ) and {"long.png","long_select.png"} or {"tab_star_normal.png","tab_star_select.png"} ) , startX , startY , {
			disableWhenChoose = true,
			upSelect = true,
			id = listConfig[i][1],
			front = { LISTPATH..listConfig[i][1]..".png" , LISTPATH..listConfig[i][2]..".png"},
			callback=
			function()
				curType = listConfig[i][1]
				curPage = 1
				print("ganglayer abcdef",curType)
				createList( listConfig[i][1] )
			end
		},group)
		self.viewLayer:addChild(temp:getLayer())
		startX = startX + temp:getWidth() + ( SCENETYPE == RANK and 12 or 4 )
	end
	--group:chooseById( curType , true )	--激活的选项
	createList()
	
	if curType == "gem" then
		--可用帮威
		local infoData = DATA_Gang:get( "info" )
		local str = ( infoData.usertribute_v > 10000 and math.floor(infoData.usertribute_v/10000) .. "万" or infoData.usertribute_v )
		self.viewLayer:addChild( display.newSprite( LISTPATH .. "usable_prestige.png"	 , 300 , 695):align(display.BOTTOM_LEFT) )
		self.viewLayer:addChild( display.strokeLabel( str , 400 , 695 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
	end
	
	if curType == "info" then
		--总帮威
		local infoData = DATA_Gang:get( "info" )
		local str = ( infoData.contribution > 10000 and math.floor(infoData.contribution/10000) .. "万" or infoData.contribution )
		self.viewLayer:addChild( display.newSprite( LISTPATH ..  "total_prestige.png" , 280 , display.height- 120 - 20):align(display.BOTTOM_LEFT) )
		self.viewLayer:addChild( display.strokeLabel( str , 400 , display.height - 120 - 20 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
	end
	
	if curType == "task"  then
			--重置任务
			local resetBtn = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" ,"long_btn_grey.png"} , 
				350  , 695 ,
				{
					priority = -142 ,
					front = PATH .. "task/" .. "task_reset.png" ,
					callback=
					function()
						if data.task.reset_state == 1 then
							self:resetLayout()
						else
							if DATA_Gang:get("info").userstate < 90 then
								KNMsg.getInstance():flashShow( "权限不足无法重置任务！" )
							else
								KNMsg.getInstance():flashShow( "任务已经重置！" )
							end
						end
					end
				})
			resetBtn:setEnable( data.task.reset_state == 1 )
			self.viewLayer:addChild( resetBtn:getLayer() )
			
		--总帮威
		local infoData = DATA_Gang:get( "info" )
		local str = ( infoData.usertribute > 10000 and math.floor(infoData.usertribute/10000) .. "万" or infoData.usertribute )
		self.viewLayer:addChild( display.newSprite( LISTPATH ..  "total_prestige.png" , 20 , 695):align(display.BOTTOM_LEFT) )
		self.viewLayer:addChild( display.strokeLabel( str , 140 , 695 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
		end
	
	if curType == "appoint" then
		local str = "请选择要任命的" .. data.text
		if data.text == "帮主" then
			str = "请选择要任命的下一任帮主"
		end
		self.viewLayer:addChild( display.strokeLabel( str , 20 , 708 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
	end

	if curType == "applylist" then
			local gangInfoData = DATA_Gang:get("info")
			--local isFull = tonumber(gangInfoData.count) < tonumber(gangInfoData.count_max)	--是否达到收人上限
			
			-- local oneOkBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" ,"btn_bg_red2.png"} , 
			-- 	52 , 105 ,
			-- 	{
			-- 		priority = -142 ,
			-- 		front = LISTPATH .. "one_ok.png" ,
			-- 		callback=
			-- 		function()
			-- 			HTTP:call("alliance","agreeonekey",{ },{success_callback = 
			-- 			function()
			-- 				self:applyFun()
							
			-- 			end})
			-- 		end
			-- 	})
			-- oneOkBtn:setEnable( isFull )
			-- self.viewLayer:addChild( oneOkBtn:getLayer() )
			
			-- local oneNoBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" } , 
			-- 	285 , 105 ,
			-- 	{
			-- 		priority = -142 ,
			-- 		front = LISTPATH .. "all_no.png" ,
			-- 		callback=
			-- 		function()
			-- 			HTTP:call("alliance","refuseonekey",{ },{success_callback = 
			-- 			function()
			-- 				self:applyFun()
			-- 			end})
			-- 		end
			-- 	}):getLayer()
			-- self.viewLayer:addChild( oneNoBtn )
			
			--设置验证
			local checkingBtn 
			checkingBtn = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" } , 
				358 , display.height - 120 - 20 ,
				{
					priority = -142 ,
					front = LISTPATH .. ( gangInfoData.state == 1 and "auto_add.png" or "checking_text.png" ),
					callback=
					function()
						HTTP:call("alliance","switchuser",{ id = data.id },{success_callback = 
						function(resultData)
							gangInfoData = DATA_Gang:get("info")
							local str
							if gangInfoData.state == 1 then
								str = "您已将帮会收人的设置修改为 自动加入"
							else
								str = "您已将帮会收人的设置修改为 验证加入"
							end
							KNMsg.getInstance():flashShow( str )
							checkingBtn:setFront( LISTPATH .. ( gangInfoData.state == 1 and "auto_add.png" or "checking_text.png" ) )
						end})
					end
				})
			checkingBtn:setEnable(false)
			self.viewLayer:addChild( checkingBtn:getLayer() )
			
			self.viewLayer:addChild(display.newSprite(LISTPATH .. "set_text.png" , 320 , display.height - 120 ))
			self.viewLayer:addChild(display.newSprite(LISTPATH .. "member_num.png" , 72 , display.height - 120 ))
			self.viewLayer:addChild(display.strokeLabel( gangInfoData.count .. "/" .. gangInfoData.count_max , 125 , display.height - 120 - 12 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
	end
	

	self.viewLayer:addChild( display.newSprite( COMMONPATH.."tab_line.png"  , 6 , display.height - 120 -20 - 5 ):align(display.BOTTOM_LEFT) )
	--翻页按钮
	if isPaging then
		local pre = KNBtn:new(COMMONPATH,{"next_big.png"}, 150, 100, {
			scale = true,
			flipX = true,
			callback = function()
				if curPage > 1 then
					curPage = curPage - 1
					createList( curType )
				end
			end
		})
		self.viewLayer:addChild(pre:getLayer())
		local next = KNBtn:new(COMMONPATH,{"next_big.png"}, 285, 100, {
			scale = true,
			callback = function()
				if curPage < totalPage then
					curPage = curPage + 1
					createList( curType )
				end
			end
		})
		self.viewLayer:addChild(next:getLayer())
	end
	
end
return M