local M = {}
local KNBtn = requires( "Common.KNBtn")
local GangTitle_config = requires("Config.Gang")
function M:new(  )
	local this = {}
	setmetatable(this , self)
	self.__index = self

	return this
end

function M:createAppointMaster( parent )
	self.baseLayer = parent.baseLayer
	--self.viewLayer = parent.viewLayer
	local PATH = IMG_PATH .. "image/scene/gang/"
	-- if self.viewLayer then	
	-- 	self.viewLayer:removeSelf()
	-- 	--self.viewLayer = display.newLayer()
	-- 	self.viewLayer = nil
	-- end
	
	local layer = display.newLayer()

	
	local bg = display.newSprite( COMMONPATH .. "figure_bg.png" )
	bg:setTouchEnabled(true)
	-- bg:addNodeEventListener(cc.NODE_TOUCH_EVENT, function (event)
 --        local x, y, prevX, prevY = event.x, event.y, event.prevX, event.prevY
 --        if event.name == "began" then
 --             print("layer began")
 --        elseif event.name == "moved" then
 --            print("layer moved")
 --        elseif event.name == "ended" then
 --             print("layer ended")
 --        end

 --        return true
 --    end)

    local listener = cc.EventListenerTouchOneByOne:create()
	listener:setSwallowTouches(true)
	listener:registerScriptHandler(function(touch, event)
		-- print("----------EVENT_TOUCH_BEGAN:")
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		-- this.clickFunc("began", x , y)
		print("bdfd bg bg touch began")
		return true
	end, cc.Handler.EVENT_TOUCH_BEGAN)
	bg:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, bg)

	--bg:setTouchSwallowEnabled(true)
	setAnchPos( bg , display.cx , display.cy + 14 , 0.5 , 0.5)
	layer:addChild( bg )
	layer:addChild( display.newSprite( PATH .. "appoint/" .. "line1.png" , display.cx , 576):align(display.BOTTOM_CENTER) )
	layer:addChild( display.newSprite( PATH .. "appoint/" .. "line2.png" , display.cx , 340):align(display.BOTTOM_CENTER) )
	
	
	-- local back_btn = KNBtn:new(COMMONPATH, {"back_img.png" , "back_img_press.png"} , 50 , display.top-88 , {
	-- --	priority = this.params.priority,
	-- 	callback = function()
	-- --		switchScene("home")
	-- 		--parent:back()
	-- 		switchScene("gang")
	-- 	end})
	-- back_btn:setPosition(cc.p(math.random(50,60),500))
	-- bg:addChild(back_btn:getLayer())

	local titleVein = display.newSprite( PATH .. "appoint/" .."title_vein.png" )
	setAnchPos(titleVein , display.cx , 700 , 0.5 )
	layer:addChild(titleVein)
	
	local roomMan = display.newSprite( PATH .. "appoint/" .. "room_man.png" )
	setAnchPos(roomMan , display.cx , 368 , 0.5 )
	layer:addChild(roomMan)
	
	local infoData = DATA_Gang:get( "list" )
	
	local leadTable = { {} , {} , {} }	--查找帮主，副帮主，堂主数据
	for i = 1 , #infoData do
		if infoData[i].duty == 100 then
			leadTable[1][1] = infoData[i]
		elseif infoData[i].duty == 95 then leadTable[2][1] = infoData[i]
		elseif infoData[i].duty == 96 then leadTable[2][2] = infoData[i]
		elseif infoData[i].duty == 81 then leadTable[3][1] = infoData[i]
		elseif infoData[i].duty==82 then leadTable[3][2] = infoData[i]
		elseif infoData[i].duty==83 then leadTable[3][3] = infoData[i]
		elseif infoData[i].duty==84 then leadTable[3][4] = infoData[i]
			
		end
	end
	
	local offsetX = 30
	local positionElement = {
			{ y = 550 , position = 1 , data = { { x = display.cx, duty = 100 } } } ,
			{ y = 397 , position = 2 , data = { { x = 112 + offsetX ,duty = 95} , { x = 284 + offsetX ,duty = 96} } } ,
			{ y = 153 , position = 3 , data = { { x = 36 + offsetX,duty = 81  } , { x = 145 + offsetX,duty=82 } , { x = 255 + offsetX,duty=83  } , { x = 365  + offsetX,duty=84 } } } ,}
			
	self.buttonArray = {
		[1]={data = {}},
		[2]={data = {{},{} }},
		[3]={data = {{}, {}, {}, {}}},
	}
	for i = 1 , #positionElement do
		local position = positionElement[i].position
		for j = 1 , #positionElement[i].data do
			local curData = positionElement[i].data[j]
			--positionCell 含任命按钮
			
			local temp = self:positionCell( parent,{ targetIndex = j , group = position , index = j , data = leadTable[position][j],duty = curData.duty } )
			setAnchPos(temp , curData.x + 40 ,positionElement[i].y , 0.5)
			layer:addChild( temp )	
			self.buttonArray[i][j]= temp
		end
	end
	self.layer = layer
	
	
	local str = "说明：只有帮主可以任命帮会职位，且帮威排名前10才可被任命！"
	-- local tipText = display.strokeLabel( str , 14 , 105 , 16 , ccc3( 0xff , 0xfb , 0xd4 ) , nil , nil , {
	-- 			dimensions_width = 466 ,
	-- 			dimensions_height = 25,
	-- 			align = 0
	-- 		})
	local tipText = ui.newTTFLabel({
				        text = str,
				        size = 16,
				        color = ccc3( 0xff , 0xfb , 0xd4 ),
				        x = 14,
				        y = 105,
				        --align = ui.TEXT_ALIGN_CENTER
    				})		
	tipText:setAnchorPoint(0,0)
	layer:addChild( tipText )
	
	--self.viewLayer:addChild(layer)
	--self.baseLayer:addChild( self.viewLayer )	
	self.baseLayer:addChild(layer)
end
function M:back()
	-- self.baseLayer:removeChild(self.layer,true)
	-- self.layer = nil
	-- self.baseBg=nil
	-- self.titleBg=nil
	--self.selection:close()

	self.mask:remove()
	
end

function M:appointFun(  )
	print("appoinlayer appoint fun selection",self.selection)
	if self.selection then
		self.selection:close()
		self.selection=nil
		self:back()
	end
	-- if self.layer then
	-- 	self:back()
	-- end
	--HTTP:call("alliance_getmembers",{},{success_callback = function ( ... )

		local infoData = DATA_Gang:get( "list" )
		local leadTable = { {} , {} , {} }	--查找帮主，副帮主，堂主数据
		--for i = 1 , #infoData do
		for k,v in pairs(infoData) do
			if v.duty == 100 then
				leadTable[1][1] = v
			elseif v.duty == 95  then
				leadTable[2][1] = v
			elseif v.duty == 96 then leadTable[2][2] = v
			elseif v.duty == 81 then leadTable[3][1] = v
			elseif v.duty==82 then leadTable[3][2] = v
			elseif v.duty==83  then leadTable[3][3] = v
			elseif v.duty==84 then leadTable[3][4] = v
			end
		end

		local offsetX = 30
		local positionElement = {
				{ y = 550 , position = 1 , data = { { x = display.cx, duty = 100 } } } ,
				{ y = 397 , position = 2 , data = { { x = 112 + offsetX ,duty = 95} , { x = 284 + offsetX ,duty = 96} } } ,
				{ y = 153 , position = 3 , data = { { x = 36 + offsetX,duty = 81  } , { x = 145 + offsetX,duty=82 } , { x = 255 + offsetX,duty=83  } , { x = 365  + offsetX,duty=84 } } } ,}
				
		-- self.buttonArray = {
		-- 	[1]={data = {}}
		-- 	[2]={data = {{},{} }
		-- 	[3]={data = {{}, {}, {}, {}}
		-- }
		for i = 1 , #positionElement do
			local position = positionElement[i].position
			for j = 1 , #positionElement[i].data do
				local curData = positionElement[i].data[j]
				-- --positionCell 含任命按钮
				-- dump(leadTable)
				
				-- local temp = self:positionCell( parent,{ targetIndex = j , group = position , index = j , data = leadTable[position][j],duty = curData.duty } )
				-- setAnchPos(temp , curData.x + 40 ,positionElement[i].y , 0.5)
				-- layer:addChild( temp )	
				-- self.buttonArray[i][j]= temp
				self.layer:removeChild(self.buttonArray[i][j],true)
				self.buttonArray[i][j] = self:positionCell( parent,{ targetIndex = j , group = position , index = j , data = leadTable[position][j],duty = curData.duty } )
				setAnchPos(self.buttonArray[i][j] , curData.x + 40 ,positionElement[i].y , 0.5)
				self.layer:addChild(self.buttonArray[i][j])
			end
		end	
	--end})
	
end
--职务Icon
function M:positionCell( parent,params )
	local PATH = IMG_PATH .. "image/scene/gang/"
	params = params or {}
	local group = params.group	--职位编号
	local index = params.index 	--堂口编号
	local data  = params.data or nil 	--是否有人
	local targetIndex = params.targetIndex --职位索引
	local APPOINTPATH = PATH .. "appoint/"
	local layer = display.newLayer()
	--认命按钮
	if group ~= 1 then
		local isApply = data ~= nil 
		local appointBtn = KNBtn:new( COMMONPATH , 
			{ "btn_bg.png" , "btn_bg_pre.png" } , 0 , 0 ,
				{ 	priority = -142 ,
					front = PATH .. "appoint/" ..  ( isApply and "retire.png" or "appoint.png" ) ,
					callback=
					function()
						if isApply then
							HTTP:call("alliance_manage",{ touid = data.uid , duty = 0 },{success_callback = 
							function()
								--self:createAppointMaster(parent)

								self:appointFun()
							end})
						else
							HTTP:call("alliance_getmembers",{},{success_callback = function()
								-- SCENETYPE = APPOINT
								local listConfig =  { }
								--local textConfig = { "帮主" , "副帮主" ,{"青龙堂堂主" , "白虎堂堂主" ,"朱雀堂堂主" ,"玄武堂堂主"}}
								--local positionConfig = { 100 , 95 , 90 , 0 }
								-- parent:createList( { alonePageNum = 10 , listConfig = listConfig , defaultType = "appoint" , data = { targetIndex = targetIndex ,  position = positionConfig[group] , info = DATA_Gang:get( "list" ) , 
								-- 	text = ( group == 3 and textConfig[group][index] or textConfig[group] )} , defaultPage = 1 } )
								self.selection = requires("Scene.gang.appointSelectLayer"):new()


						


								--local layer = selection:createSelectList({ alonePageNum = 10 , listConfig = listConfig , defaultType = "appoint" , data = { targetIndex = targetIndex ,  position = positionConfig[group] , info = DATA_Gang:get( "list" ) , 
								local layer = self.selection:createSelectList({ alonePageNum = 10 , listConfig = listConfig , defaultType = "appoint" , data = { targetIndex = targetIndex ,  position = params.duty , info = DATA_Gang:get( "list" ) , 
							 	--text = ( group <= 3 and textConfig[group][index] or textConfig[group] )} , defaultPage = 1 },
							 	text = GangTitle_config[params.duty]},defaultPage = 1},
							 	self)
								
								local KNMask = requires("Common.KNMask")
								self.mask = KNMask:new( { item = layer } )
								local Scene = display.getRunningScene()
								Scene:addChild( self.mask:getLayer() )
							end})
							
							
						end
					end
				}):getLayer()
		setAnchPos(appointBtn , 110/2-32 ,25, 0.5,0.5)
		layer:addChild( appointBtn )	
	end

	
	--人物角色
	local otherData = nil
	local textData = nil
	if data then
		otherData = { IMG_PATH.."image/scene/common/navigation/level_bg.png" , -17 , 54 } 
		textData = { { data.level  , 18 , ccc3( 0xbf , 0x3a , 0x01 ) , { x = -33 , y = 34	} , nil , 20 } , 
					{ data.name , 18 , ccc3( 0x2c , 0x00 , 0x01 ) , { x = 1 , y = -45	} , nil , 20 } }
	end
	local roleIcon = KNBtn:new( COMMONPATH , { data and "sex".. data.sex .. ".png" or "role_frame.png" } , 7 , 70 ,
	{
		front = data and  COMMONPATH .. "role_frame.png"  or nil,
		other = otherData , 
		text = textData ,
		callback = function()end,
	}):getLayer()
	setAnchPos(roleIcon , 110/2 - 32  , 70 , 0.5)
	layer:addChild( roleIcon )
	
	--职务背景
	local positionBg = display.newSprite( APPOINTPATH .. "position_bg.png")
	setAnchPos( positionBg , 110/2 -32 , 170 , 0.5 , 0.5 )
	layer:addChild( positionBg )
	
	local positionElement = {
				{ "gang_man"} ,
				{ "deputy_gang_man" , "deputy_gang_man" } ,
				{ "dragon" ,"tiger" , "bird" , "tortoise" } }
	
	--职务名称
	local position = display.newSprite( APPOINTPATH .. positionElement[group][index] ..  ".png")
	setAnchPos( position , 110/2 -32 , 170 , 0.5 , 0.5 )
	layer:addChild( position )
	
	
	layer:setContentSize( CCSize( 110 , 190 ) )
	return layer
end

return M