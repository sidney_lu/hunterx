local M = {}
local KNBtn = requires( "Common.KNBtn")
local KNSlider = requires("Common.KNSlider")
local KNRadioGroup = requires( "Common.KNRadioGroup")
local PATH = IMG_PATH .. "image/scene/gang/"
local bangweiLabel,fundsLabel
local moneyText
local flowOffsetY = 120
local costFlag
function M:new( params )
	local this = {}
	setmetatable(this , self)
	self.__index = self

	return this
end

function M:clean( )
	self.txtUserTribute:setString(DATA_Gang:get("info").contribution_max)
	local goldValue = DATA_User:get("gold")
	self.txtGold:setString(string.convertMoney(goldValue))

	self.data = DATA_Gang:get("task")
	-- if data.donate.juangold >0 then
	-- 	self.default = 1
	-- else
	-- 	self.default = 2
	-- end
	--echoLog("[donatelayer]",self.data.donate.juangold,self.data.donate.juansilver)
	self.maxValue = ( self.default == 1 and self.data.donate.juangold or  self.data.donate.juansilver )
	--echoLog("[donatelayer] self.maxValue=",self.default,self.maxValue)
	--display.strokeLabel(  , 415 , 681 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) )
	if self.maxValue>0 then
		self.slider:setValue(1)
		self:changeValue(1)
	else
		self.slider:setValue(0)
		self:changeValue(0)
		self.slider:setEnable(false)
	end
	self.slider:setMaximumValue(self.maxValue)

	self.txtFunds:setString(string.convertMoney(DATA_Gang:get("info").funds))
end
function M:changeDonateSource( default )
	--
	if costFlag then
		--costFlag:removeSelf()
		scrollLayer:removeChild(costFlag,true)
		costFlag = nil
	end
	
	costFlag = display.newSprite( COMMONPATH .. ( self.default == 1 and "gold.png" or "silver.png" )  , 311 , 338 -flowOffsetY):align(display.BOTTOM_LEFT)
	scrollLayer:addChild(costFlag , 50  )
	--
	local num = default == 1 and self.data.donate.juangold or self.data.donate.juansilver
	if self.slider:getValue() < num then
		num =self.slider.getValue()
		echoLog("slider value",self.slider.getValue())
	end

	self:changeValue(num)
end
function M:changeValue(num)
		local ratioData = ( self.default == 1 and self.data.donateinfo.gold or self.data.donateinfo.silver ) 
		local value = math.floor(num * ratioData.tribute)

		bangweiLabel:setString( value )
		moneyText:setString( num )
		value = math.floor(num * ratioData.funds)
		fundsLabel:setString( value )
end
--创建捐献
function M:createDonate(parent,params)
	self.viewLayer = parent.viewLayer
	self.baseLayer = parent.baseLayer
	parent:refreshGangInfo( { type = 3 , refresh = true } )
	
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_donate_title.png" )
	
	--GLOBAL_INFOLAYER:showNavigation(false)
	
	params = params or {}
	self.default = params.default or 1 		--默认捐献类型  1黄金，2银两

	local curType = params.type or "task"
	local TASKPATH = PATH .. "task/"
	local data = DATA_Gang:get("task")
	self.data = data
	if data.donate.juangold == 0 then
		self.default = 2
	end
	
	local layer = display.newLayer()
	if self.viewLayer then	
		
		--self.viewLayer:removeSelf()
		self.baseLayer:removeChild(self.viewLayer,true)
		self.viewLayer = display.newLayer()
		self.viewLayer:addChild( layer )
		self.baseLayer:addChild( self.viewLayer )
		--self.viewLayer:setTag(-100)
		-- local bg=display.newSprite( TASKPATH .."frame_bg.png"  , 20 , 104):align(display.BOTTOM_LEFT)
		-- bg:setTag(-100)
		-- bg:setTouchSwallowEnabled(true)
		-- self.baseLayer:addChild(bg)
	else
		self.baseLayer:addChild(layer)
			
	end
	

	local bg = display.newSprite( TASKPATH .."frame_bg.png"  , 20 , display.height - 120 - 30):align(display.TOP_LEFT)
	layer:addChild( bg )
	
	bg:addChild( display.newSprite( TASKPATH .. "donate_tip.png" , 443 / 2 , 507))
	--帮会当前资金总额
	local fundsOffsetY = 73
	bg:addChild( display.newSprite( PATH .. "pray/award_frame.png"  , 443/2 , 440 -fundsOffsetY):align(display.BOTTOM_CENTER))
	bg:addChild( display.newSprite( TASKPATH .. "fund_text.png"  , 70 , 475 -fundsOffsetY):align(display.BOTTOM_LEFT))
	self.txtFunds = display.strokeLabel(string.convertMoney(DATA_Gang:get("info").funds) , 287 , 475 -fundsOffsetY , 26 , ccc3( 0x2c , 0x00 , 0x00 ))
	bg:addChild( self.txtFunds )

	
	
	local scroll = KNScrollView:new(0, 94 , display.width, display.height - 120 - 94 - 235, 10 , false )
	layer:addChild(scroll:getLayer())
	scrollLayer = cc.LayerColor:create(ccc4(255,123,123,0))
	scrollLayer:setContentSize(display.width,260)
	scroll:addChild(scrollLayer)

	scrollLayer:addChild( display.newSprite( TASKPATH .. "select_donate.png"  , 55 , 336 - flowOffsetY):align(display.BOTTOM_LEFT) )
	--layer:addChild( display.newSprite( TASKPATH .. "select_donate.png"  , 55 , 336):align(display.BOTTOM_LEFT) )
	scrollLayer:addChild( display.newSprite( TASKPATH .. "cost_text.png"  , 245 , 340 - flowOffsetY):align(display.BOTTOM_LEFT) )
	scrollLayer:addChild( display.newSprite( TASKPATH .. "money_bg.png"  , 324 , 338 - flowOffsetY):align(display.BOTTOM_LEFT) )
	scrollLayer:addChild( display.newSprite( TASKPATH .. "get_num.png"  , 55 , 189 - flowOffsetY):align(display.BOTTOM_LEFT) )
	scrollLayer:addChild( display.newSprite( TASKPATH .. "get_num2.png"  , 262 , 189 - flowOffsetY):align(display.BOTTOM_LEFT) )
	
	
	
	local btnFlag  , comboBox  , addBtn , minusBtn = nil , nil ,  nil , nil , nil , nil , nil , nil , nil
	local num ,baseX = 1,display.cx
	--local num  , baseX , baseY = 1, display.cx , display.cy - 28
--	maxValue = ( self.default == 1 and ( DATA_User:get("gold") > data.donate.juangold and data.donate.juangold or DATA_User:get("gold") ) or  ( DATA_User:get("silver") > data.donate.juangold and data.donate.juansilver or DATA_User:get("silver") ) ) 
	self.maxValue = 0
	self.maxValue = ( self.default == 1 and data.donate.juangold or  data.donate.juansilver )
	
	--捐献数值
	moneyText = display.strokeLabel( "" , 287 + 85 , 357 + 46 - 60 -flowOffsetY, 18 , ccc3( 0xf7 , 0xee , 0xc5 ) , nil, nil ,
		{align = 0,size={50,150}})	

	scrollLayer:addChild( moneyText )
	--可获得帮威
	bangweiLabel = display.strokeLabel( "" , display.cx - 137  , 190 -flowOffsetY, 18 , ccc3(0x2c , 0x00 , 0x00 ) , nil , nil ,
	 {
	 	 dimensions_width = 74 , 
	 	 dimensions_height = 23 , 
	 	 align = 1 
	 })
	scrollLayer:addChild( bangweiLabel )
	--可获得资金
	fundsLabel = display.strokeLabel( "" , display.cx + 70  , 190 -flowOffsetY , 18 , ccc3(0x2c , 0x00 , 0x00 ) , nil , nil ,
	 {
	 	 dimensions_width = 74 , 
	 	 dimensions_height = 23 , 
	 	 align = 1 
	 })
	scrollLayer:addChild( fundsLabel )
	
	

	
	--增加按钮
	addBtn = KNBtn:new(COMMONPATH,{"next_big.png"} , baseX + 128 , 249 -flowOffsetY, {
		scale = true,
		priority = -130,
		callback = function()
			if num < self.maxValue then
				num = num + 1
				self.slider:setValue( num )
			end
		end
	})
	--dump( maxValue )
	--划动条
	self.slider = KNSlider:new( "buy" ,  {
										x = 443/2 - 86, 
										y = 280 - flowOffsetY, 
										minimum = 1 , 
										maximum = maxValue , 
										value = 1 , 
										callback  = function( _curIndex )  
											if num <= self.maxValue then
												num =  _curIndex  
												self:changeValue(num) 
											end
										end ,
										priority = -140
										} )
	scrollLayer:addChild( self.slider )
	minusBtn = KNBtn:new(COMMONPATH,{"next_big.png"} , baseX - 165 , 249 -flowOffsetY,{
		scale = true,
		priority = -130,
		callback = function()
			if num > 1 then
				num = num - 1
				self.slider:setValue( num )
			end
		end
	})
	minusBtn:getLayer():setTag(-2)
	minusBtn:setFlip(true)
	
	addBtn:getLayer():setScale( 0.9 )
	minusBtn:getLayer():setScale( 0.9 )
	
	scrollLayer:addChild(addBtn:getLayer())
	scrollLayer:addChild(minusBtn:getLayer())

	local confirmBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" , "btn_bg_red2.png" } , 165 , 126 - flowOffsetY , {
			front = TASKPATH .. "confirm_donate.png",
			priority = -131,	
			callback = function()
				local tempValue = ( ( self.default == 1 ) and data.donate.juangold or data.donate.juansilver )
				
				if tempValue == 0 then

					KNMsg.getInstance():flashShow( "已到捐献上限,请明天再来！" )
				else
					local selfMoney = ( self.default == 1 and  DATA_User:get("gold") or  DATA_User:get("silver") )
					if num>selfMoney then
						local moneyName = ( self.default == 1 ) and  "砖石" or "金币" 
						KNMsg.getInstance():flashShow( moneyName .. "不足！"  )
					else
						--HTTP:call("alliance","senddonate",{ type = ( default == 1 ) and "gold" or "silver" , num = num },{success_callback =
						HTTP:call(42042,{ type = ( self.default == 1 ) and "gold" or "silver" , num = num },{success_callback =
						function(data)
							
					
							KNMsg.getInstance():flashShow("捐献成功,帮威+" .. bangweiLabel:getString() .. ",资金+" .. fundsLabel:getString())
							self:clean()
						end
						 })
					end
				end
			end
			})
	--confirmBtn:setEnable( ( self.default == 1 ) and ( DATA_User:get("gold") > 1 ) or ( DATA_User:get("silver") > 1 ) )
	scrollLayer:addChild( confirmBtn:getLayer() )
	
	local function changeLayout( )
		if costFlag then
			--costFlag:removeSelf()
			scrollLayer:removeChild(costFlag,true)
			costFlag = nil
		end
		
		costFlag = display.newSprite( COMMONPATH .. ( self.default == 1 and "gold.png" or "silver.png" )  , 311 , 338 -flowOffsetY):align(display.BOTTOM_LEFT)
		scrollLayer:addChild(costFlag , 50  )
		
		maxValue = ( self.default == 1 and self.data.donate.juangold or  self.data.donate.juansilver )
		
		-- if maxValue == 0 then--为临时解决单项目捐献
		-- 	maxValue = 0.5
		-- end
		print("self.silder:setmax ",maxValue)
		--self.slider:setMax( maxValue )
		self.slider:setMaximumValue(maxValue)
		if maxValue==0 then
			self.slider:setEnable(false)
		end
		
		
		local tempValue = ( ( self.default == 1 ) and data.donate.juangold or data.donate.juansilver )
		--num = tempValue == 0 and 0 or 1
		return tempValue
	end
	local ret = changeLayout()
	num = ret==0 and 0 or 1


	local KNComboBox = requires("Common.KNComboBox")
	local group = KNRadioGroup:new()
	local items = {}
	for i = 1, 2 do
		items[i] = KNBtn:new( COMMONPATH, { i == 1 and "gold.png" or "silver.png" , "star_select.png"}, 0, 0, {
			id = i,
			noHide = true,
			callback = function()
				
				if comboBox.itemsShow==true then
					comboBox:autoShow()
					if self.default~=i then
						self.default = i
						comboBox:refreshBtn( { front = COMMONPATH .. ( self.default == 1 and "gold.png" or "silver.png" ) })
					-- local ret = changeLayout()
					-- num = ret == 0 and 0 or 1
						echoLog("donate self.default",self.default)
						
						--changeDonateSource  函数会调用changeValue函数
						self:changeDonateSource()
						--self:changeDonateSource(self.default)
						--self:changeValue(num)

					end
				else
					
				end
				-- if self.default==1 then
				-- 	items[1].item[2]:setVisible(true)
				-- 	items[2].item[2]:setVisible(false)--砖石
				-- 	print("砖石 click setVisible")
				-- elseif self.default== 2 then
				-- 	items[1].item[2]:setVisible(true)
				-- 	items[2].item[2]:setVisible(false)--砖石
				-- 	print("钱 click setVisible")
				-- end
				
			end
		},group)
	end
	comboBox = KNComboBox:new(110, 335 -flowOffsetY, {
		dir = COMMONPATH,
		res = {"small_btn_bg.png", "small_btn_bg_pre.png"},
		front = COMMONPATH .. ( self.default == 1 and "gold.png" or "silver.png" ) ,
		bg = TASKPATH.."combo_bg.png",
		up = false,
		offset = 15,
		addX = 12,
		additionHeight = 15 ,
		items = items,
		default = self.default,
		itemsGroup = group
	})
	--self.viewLayer:addChild( comboBox:getLayer() )
	scrollLayer:addChild(comboBox:getLayer())
	
	--总帮威(我的总名望)
	local infoData = DATA_Gang:get( "info" )
	local str = string.convertMoney(infoData.contribution_max)
	bg:addChild( display.newSprite( PATH ..  "usertribute_v.png" , 120 , 602))

	bg:addChild( display.newSprite( PATH ..  "gang_list/total_prestige.png" , 52 , 602))
	echoLog("dfdsfkjshdfkjsdjflksdjflksdjf=",str)
	self.txtUserTribute = display.strokeLabel( str , 140 , 592 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) )
	bg:addChild( self.txtUserTribute )
	
	bg:addChild( display.newSprite( TASKPATH ..  "surplus.png" , 320 , 602))
	bg:addChild( display.newSprite( COMMONPATH ..  "gold.png" , 362 , 602 ))
	
	
	self.txtGold = display.strokeLabel( "0" , 372 , 592 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) )
	bg:addChild( self.txtGold )
	self:clean()-- get default value
	
	--setAnchPos( layer , 0 , -85 , 0 , 0 )
	-- layer:setContentSize(cc.size(display.width,display.height))
	-- layer:setTouchSwallowEnabled(true)
	-- layer:setTag(100000000000)

	bg:addChild( display.newSprite( COMMONPATH.."tab_line.png"  , 443/2, 583))
end



return M