TaskLayer ={}
local PATH = IMG_PATH .. "image/scene/gang/"
local KNBtn = requires( "Common.KNBtn")
function TaskLayer:new( params )
	local this = {}
	setmetatable(this , self)
	self.__index = self
end

function TaskLayer:createTask( parent, params )
	self.parent = parent
	self.baseLayer = parent.baseLayer
	self.viewLayer = parent.viewLayer

	parent:refreshGangInfo( { type = 3 , refresh = true } )
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_task_title.png" )
	params = params or {}
	local default = params.default or 1 		--默认捐献类型  1黄金，2银两
	local curType = params.type or "task"
	local TASKPATH = PATH .. "task/"
	
	-- if self.viewLayer then	
	-- 	self.viewLayer:removeSelf()
	-- 	self.viewLayer = nil
	-- 	self.viewLayer = display.newLayer()
	-- 	self.baseLayer:addChild( self.viewLayer )
	-- end
	
	local listConfig =  { {"task","tab_task"} }
	local activity = curType
	self:createList( { listConfig = {} , defaultType = activity ,
	data = DATA_Gang:get("task") , defaultPage = 1 ,--alonePageNum =5,
	backFun = function() self:createTask(params) end } )
end
function TaskLayer:updateList( )

	
end
function TaskLayer:createScrollByNative( d )
	self.scroll:getLayer():removeSelf()
	self:createScroll(d)
end
function TaskLayer:createScroll( d )
	-- print("TaskLayer createScroll",scroll)
	-- if scroll then
	-- 	scroll:getLayer():removeSelf()
	-- 	scroll = nil
	-- end
	-- print("TaskLayer createScroll",d.task.info)
	-- print("TaskLayer",DATA_Gang:get("task").task.info)
	local curData = d.task.info

	local scrollX , scrollY , scrollWidth , scrollHeihgt
	scrollX			= 15
	scrollY			= isPaging and 155 or 91
	scrollWidth		= 450
	scrollHeihgt	= isPaging and 530 or 580
	
	-- if heightType == 1 then
	-- 	scrollY 		= 155
	-- 	scrollHeihgt 	= 392
	-- elseif heightType == 2 then
	-- 	scrollY 		= 155
	-- 	scrollHeihgt 	= 525
	-- end
	
	local curPage =  1
	local alonePageNum = 5
	local curType = "task"
	local scroll = KNScrollView:new( scrollX , scrollY , scrollWidth , scrollHeihgt , 5 )
	for i = 1 , ( isPaging and alonePageNum or #curData ) do
		local itemData = curData[ ( curPage - 1 ) * alonePageNum + i ]
		if itemData then
			local tempItem = self:listCell( { 
												--data = ( curType == "appoint" and { isGangMan = data.isGangMan , data = itemData , targetIndex = data.targetIndex ,  position = data.position } or  itemData ) , 
												data = itemData ,
												type = curType , 
												parent = scroll , 
												index = ( curPage - 1 ) * alonePageNum + i , 
												checkBoxOpt = optionFun , 
												checked = false ,  
											} )
			scroll:addChild(tempItem, tempItem )
		end
	end
	scroll:alignCenter()
	self.scroll=scroll
	self.viewLayer:addChild( scroll:getLayer() )

end
--生成list列表
function TaskLayer:createList( params )
	params = params or {}
	local LISTPATH = PATH .. "gang_list/"
	
	
	if self.viewLayer then	
		--self.viewLayer:removeSelf()
		self.baseLayer:removeChild(self.viewLayer,true)
		self.viewLayer = nil
		self.viewLayer = display.newLayer()
		self.baseLayer:addChild( self.viewLayer )
		if self.parent.hideScroll then
			self.parent:hideScroll()
		end
	end

	local data , totalPage , curPage , curType , group , pageText , curData , alonePageNum , listConfig , pageBg , rankText 
	local selectNum , optionFun , countNum	--用手选择参战人员
	local scroll = nil

	if table.nums(params.data)==0 then return end
	listConfig = params.listConfig			--选项按钮
	data = params.data 						--展示的数据
	curType = params.defaultType 			--默认激活table
	curPage = params.defaultPage or 1 		--默认展示页面
	alonePageNum = params.alonePageNum or 0		--单页item个数
	local isPaging = params.alonePageNum and true or false	--是否分页
	local heightType = 0
	if not data.task.info then return end

	local function refreshData()
		curData = data.task.info
		heightType = 2
		echoLog("[ganglayer] curType",curType)
		if isPaging then
			totalPage = math.ceil( #curData / alonePageNum )
			pageText:setString( curPage .. "/" .. totalPage )
		else
			curPage = 1
		end
	end
	
	
	if isPaging then
		--页数背景
		pageBg = display.newSprite( COMMONPATH .. "page_bg.png" )
		setAnchPos(pageBg , 240 , 110 , 0.5)
		self.viewLayer:addChild( pageBg )
		--页数文字
		pageText = display.strokeLabel( curPage .. "/" .. 1  , 230 , 117 , 20 , ccc3(0xff,0xfb,0xd4) )
		setAnchPos( pageText , 240, 117, 0.5 )
		self.viewLayer:addChild(pageText)
	else
		totalPage = nil
	end
	refreshData()
	
	local function createList( )
		if scroll then
			scroll:getLayer():removeSelf()
			scroll = nil
		end
		refreshData()
		local scrollX , scrollY , scrollWidth , scrollHeihgt
		scrollX			= 15
		scrollY			= isPaging and 155 or 92
		scrollWidth		= 450
		scrollHeihgt	= isPaging and 530 or 600
		
		if heightType == 1 then
			scrollY 		= 155
			scrollHeihgt 	= 392
		elseif heightType == 2 then
			scrollY 		= 92
			scrollHeihgt 	= 525
		end
		print("TaskLayer scrollHeihgt",scrollY,scrollHeihgt)
		local scroll = KNScrollView:new( scrollX , scrollY , scrollWidth , scrollHeihgt , 5 )
		for i = 1 , ( isPaging and alonePageNum or #curData ) do
			local itemData = curData[ ( curPage - 1 ) * alonePageNum + i ]
			if itemData then
				local tempItem = self:listCell( { 
													--data = ( curType == "appoint" and { isGangMan = data.isGangMan , data = itemData , targetIndex = data.targetIndex ,  position = data.position } or  itemData ) , 
													data = itemData ,
													type = curType , 
													parent = scroll , 
													index = ( curPage - 1 ) * alonePageNum + i , 
													checkBoxOpt = optionFun , 
													checked = false ,  
												} )
				scroll:addChild(tempItem, tempItem )
			end
		end
		scroll:alignCenter()
		self.viewLayer:addChild( scroll:getLayer() )

	end
	
	
	
	local KNRadioGroup = requires( "Common.KNRadioGroup")
	local startX,startY = 10,690
	if heightType == 1 then startX,startY = 10 , 556 end
	if SCENETYPE == RANK then startX,startY = 18 , 690 end
	
	--创建tab listConfig为空则没有tab
	group = KNRadioGroup:new()
	for i = 1, #listConfig do
		local temp = KNBtn:new( COMMONPATH.."tab/", ( ( SCENETYPE == RANK or SCENETYPE == TASK ) and {"long.png","long_select.png"} or {"tab_star_normal.png","tab_star_select.png"} ) , startX , startY , {
			disableWhenChoose = true,
			upSelect = true,
			id = listConfig[i][1],
			front = { LISTPATH..listConfig[i][1]..".png" , LISTPATH..listConfig[i][2]..".png"},
			callback=
			function()
				curType = listConfig[i][1]
				curPage = 1
				createList( listConfig[i][1] )
			end
		},group)
		self.viewLayer:addChild(temp:getLayer())
		startX = startX + temp:getWidth() + ( SCENETYPE == RANK and 12 or 4 )
	end
	group:chooseById( curType , true )	--激活的选项
	--createList()
	self:createScroll(params.data)--by the first time
	
	
	if curType == "task"  then	
			--重置任务
			local resetBtn = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" ,"long_btn_grey.png"} , 
				350  , display.height - 120 -22,
				{
					priority = -142 ,
					front = PATH .. "task/" .. "task_reset.png" ,
					callback=function()
						if data.task.reset_state == 1 then
							--self:resetLayout()
							HTTP:call(42023,{},{success_callback = 
							function()
								--self:createTask(self.parent)
								self:createScrollByNative(DATA_Gang:get("task"))
								self.resetBtn:setEnable(DATA_Gang:get("task").reset_state==1)
							end})
						else
							if DATA_Gang:get("info").userstate < 90 then
								KNMsg.getInstance():flashShow( "权限不足无法重置任务！" )
							else
								KNMsg.getInstance():flashShow( "任务已经重置！" )
							end
						end
					end
				})
			self.resetBtn = resetBtn
			resetBtn:setEnable( data.task.reset_state == 1 )
			self.viewLayer:addChild( resetBtn:getLayer() )
			
		--总帮威
		local infoData = DATA_Gang:get( "info" )
		local str = ( infoData.contribution > 10000 and math.floor(infoData.contribution/10000) .. "万" or infoData.contribution )
		self.viewLayer:addChild( display.newSprite( LISTPATH ..  "total_prestige.png" , 20 , display.height - 120 - 18):align(display.BOTTOM_LEFT) )
		self.viewLayer:addChild( display.strokeLabel( str , 140 , display.height-120-20 , 26 , ccc3( 0xdb , 0x03 , 0x03 ) ) )
		end
	
	

	self.viewLayer:addChild( display.newSprite( COMMONPATH.."tab_line.png"  , 6 , display.height - 120 - 30 - 5 ):align(display.BOTTOM_LEFT) )
	--翻页按钮
	if isPaging then
		local pre = KNBtn:new(COMMONPATH,{"next_big.png"}, 150, 100, {
			scale = true,
			flipX = true,
			callback = function()
				if curPage > 1 then
					curPage = curPage - 1
					createList( curType )
				end
			end
		})
		self.viewLayer:addChild(pre:getLayer())
		local next = KNBtn:new(COMMONPATH,{"next_big.png"}, 285, 100, {
			scale = true,
			callback = function()
				if curPage < totalPage then
					curPage = curPage + 1
					createList( curType )
				end
			end
		})
		self.viewLayer:addChild(next:getLayer())
	end
	
end

--生成列表item
function TaskLayer:listCell( params )
	params = params or {}
	local type = params.type or 0 
	local data = params.data or {}
	
	local index = params.index
	local parent = params.parent
	local ITEMPATH = PATH .. "gang_list/"
	
	local layer = display.newLayer()
	--背景
	local str = type == "task" and IMG_PATH .. "image/scene/activity/item_bg.png" or COMMONPATH .. "item_bg.png"
	local	bg = display.newSprite( str )
	setAnchPos(bg , 0 , 0) 
	layer:addChild( bg )
	
	local titleElement , addX , addY	
	local function createItem()
		for i = 1 , #titleElement do
			addX = 50 + math.floor( ( i - 1 ) / 3 ) * 260
			addY = 73 - (( i - 1 ) % 3) * 30
			local curTitle = titleElement[i].title
			if 	curTitle ~= "" then
				local tempTitle = display.newSprite( ITEMPATH .. curTitle .. ".png")
				setAnchPos( tempTitle , addX , addY )
				layer:addChild( tempTitle )
				
				local tipText
				addX = addX +  65
				
				tipText = display.strokeLabel( titleElement[i].text , addX , addY , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
						dimensions_width = i<4 and 200 or 80,
						dimensions_height = 25,
						align = 0
				})
				
				layer:addChild( tipText )
			end
		end
	end
	
	if type == "task" then
		local TASKPATH = PATH .. "task/"
		layer:addChild( display.newSprite( TASKPATH .. "task_discribe_title.png" , 15 , 130):align(display.BOTTOM_LEFT) )
		layer:addChild( display.newSprite( TASKPATH .. "task_award_title.png" , 15 , 50):align(display.BOTTOM_LEFT) )
		local eventText = display.strokeLabel( data.desc , 185 , 70 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
					dimensions_width = 300,
					dimensions_height = 60,
					align = 0
				})	

		
		layer:addChild( eventText )	
		local awardData = data.award
		for i = 1 , #awardData do 
			local curProp = awardCell( awardData[i] , { parent = parent }) 
			setAnchPos( curProp , 120 + (i-1)*80 , 10 , 0 , 0 )
			layer:addChild(curProp)
		end
		
		local acceptPath , getPath , isAccept , isGet
		if data.state == 0 then			--0未接 
			isAccept = true
			acceptPath = PATH .. "gang_list/" ..  "accept.png"
			isGet = false
			getPath = COMMONPATH .. "get_grey.png"
		elseif data.state == 1 then		--1已接
			isAccept = true
			acceptPath = PATH .. "gang_list/" ..  "execute.png"
			isGet = false
			getPath = COMMONPATH .. "get_grey.png"
		elseif data.state == 2 then		--2完成 
			isAccept = false
			acceptPath = PATH .. "gang_list/" ..  "execute_gray.png"
			isGet = true
			getPath = COMMONPATH .. "get.png"
			layer:addChild( display.newSprite( TASKPATH .. "complete_flag.png" , 23 , 45):align(display.BOTTOM_LEFT) )
		elseif data.state == 3 then		--3已领奖
			isAccept = false
			acceptPath = PATH .. "gang_list/" ..  "execute_gray.png"
			isGet = false
			getPath = COMMONPATH .. "get_complete.png"
			layer:addChild( display.newSprite( TASKPATH .. "complete_flag.png" , 23 , 45):align(display.BOTTOM_LEFT) )
		end
		--接受 执行
		local acceptBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
			360 , 90 ,
			{
				parent = parent , 
				front = acceptPath ,
				callback = function()
					DATA_Mission:setData("task","task_id",data.taskid)
--					1-关卡、2-藏宝楼、3-山神庙、4-狩猎、5-如意阁、6-道具搜集、7-帮派捐献、8-帮派祈福
					data.state = tonumber(data.state)
					data.type = tonumber(data.type)
	
					if data.state == 1 then
						if data.type == 1 then
							DATA_Mission:setByKey("current","map_id",DATA_Mission:get("max","map_id"))
							DATA_Mission:setByKey("current","mission_id",DATA_Mission:get("max","mission_id"))
							if DATA_Mission:haveData(DATA_Mission:get("max","map_id")) then
								switchScene("mission")
							else
								HTTP:call(21001 , {},{success_callback = function()
									switchScene("mission")
								end })
							end
						elseif data.type == 2 then
							--HTTP:call("insequip" , "get",{},{success_callback = function(data)
								switchScene("fb" , { state = "equip" , map = data.current_map } )
							--end })
						elseif data.type == 3 then
							--HTTP:call("penglai_get", {}, {
							--success_callback = function(data)
								switchScene("fb" , { state = "hero", data = data} )
							--end})
						elseif data.type == 4 then
							HTTP:call("inspet_new",{},{success_callback = function()
								switchScene("fb" , { state = "pet"} )
							end })
						elseif data.type == 5 then
							--HTTP:call("insskill" , "get",{},{success_callback = function()
							HTTP:call("insskillget",{},{success_callback = function()
								switchScene("fb" , { state = "skill"} )
							end })
						elseif data.type == 6 then	--暂不做处理 道具搜集
							KNMsg.getInstance():flashShow("陛下，道具搜集暂未施行。")
						elseif data.type == 7 then
							--self:createDonate()
							SCENETYPE = DONATE
							local donate=requires("Scene.gang.donateLayer")
							donate:new()
							donate:createDonate(self.parent)
						elseif data.type == 8 then
							HTTP:call("alliance","clifford",{ },{success_callback = 
							function()
								self:createPray()
							end})
						end
					elseif data.state == 0 then
						-- HTTP:call("alliance","receivetask",{ id = data.id },{success_callback = 
						-- function()
						-- 	self:createTask(self.parent)
						-- end})
						print("tasklayer data.id",data.id)
						print("42024",DATA_Gang.get("task").task.info)
						HTTP:call(42024,{ id = data.id },{success_callback = function()
							--self:createTask(self.parent)
							print("42024",DATA_Gang.get("task"))
							print("42024",DATA_Gang.get("task").task.info)

							self:createScrollByNative(DATA_Gang:get("task"))
						end})
					end
				end
			})
		acceptBtn:setEnable( isAccept )	
		layer:addChild( acceptBtn:getLayer() )
		
		--领奖 未领
		local getBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
			360 , 11 ,
			{
				parent = parent , 
				front = getPath ,
				callback = function()
					-- local items = {}
					-- for i = 1 , #awardData do 
					-- 	table.insert(items,{cid = awardData[i].cid})
					-- end
					
					if #awardData == 0 then KNMsg.getInstance():flashShow( "缺少任务奖励!" ) end
					--目前，最多3个奖励，所以可以这么写
					local itemString1 = awardData[1] and awardData[1].cid or nil
					local num1 = awardData[1].num
					local itemString2 = awardData[2] and awardData[2].cid or nil
					local num2 = awardData[2] and awardData[2].num or nil
					local itemString3 = nil
					local num3 = awardData[3] and awardData[3].num or nil

					echoLog("tasklayer 1, 2",itemString1,itemString2)
					--HTTP:call("alliance","posttask",{ id = data.id },{success_callback = 
					HTTP:call(42022,{taskid=data,item1=itemString1,item2=itemString2,item3=itemString3,num1=num1,num2=num2,num3=num3},{success_callback = 
					function()
						local function inform( data )
							if data.awards then
								local str = ""
								local decollator = "    "	--分割符
								for key , v in pairs( data.awards ) do
									if v.cid == "power"		then str = str .. "体力  +" .. v.num
									elseif v.cid == "gold"		then str = str .. "黄金  +" .. v.num
									elseif v.cid == "silver"		then str = str .. "银币  +" .. v.num
									elseif v.cid == "task_tribute"		then str = str .. "帮威  +" .. v.num
									elseif v.cid == "task_exp"		then str = str .. "帮贡  +" .. v.num
									elseif v.cid == "task_power"		then str = str .. "体力  +" .. v.num
									elseif v.cid == "funds"		then str = str .. "资金  +" .. v.num
									end
									str = str ..  decollator
								end
								
								
								KNMsg.getInstance():flashShow( str )
							end
						end
						inform( {awards = data.award} )
						
						--self:createTask(self.parent)
						
						
						self:createScrollByNative(DATA_Gang:get("task"))
					end})
				end
			})
		getBtn:setEnable( isGet )
		layer:addChild( getBtn:getLayer() )
	end
	

	
	layer:setContentSize( bg:getContentSize() )
	return layer
end

return TaskLayer