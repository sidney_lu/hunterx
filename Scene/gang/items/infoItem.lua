local M = {}
local KNBtn = requires( "Common.KNBtn")
local gameTitle_config = requires("Config.Gang")
function M:new( ... )
	local this = {}
	setmetatable(this , self)
	self.__index = self
	return this
end
function M:genItemView( data,parent )
	local layer = display.newLayer()
	local m_gangItemPath = IMG_PATH .. "image/scene/gang/gang_list/"
	
	bg = display.newSprite( COMMONPATH .. "item_bg.png" )
	setAnchPos(bg , 0 , 0) 
	layer:addChild( bg )
	
	local m_gangItemPath = IMG_PATH .. "image/scene/gang/gang_list/"
	titleElement = {
					{ title = "meber_name" , 	text = data.name } , 						--成员名称
					{ title = "meber_tribute" ,	text = data.tribute } , 					--成员帮威
					{ title = "meber_ability" ,	text = data.ability } , 					--成员战力
					{ title = "meber_title" , 	text = gameTitle_config[data.duty] } ,						--成员头衔
				}
	for i = 1 , #titleElement do
		addX = 50 + math.floor( ( i - 1 ) / 3 ) * 230
		addY = 73 - (( i - 1 ) % 3) * 30
		local curTitle = titleElement[i].title
		if 	curTitle ~= "" then
			local tempTitle = display.newSprite( m_gangItemPath .. curTitle .. ".png")
			setAnchPos( tempTitle , addX , addY )
			layer:addChild( tempTitle )
			
			local tipText
			addX = addX +  60
			
		
			tipText = display.strokeLabel( titleElement[i].text , addX+120 , addY , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
					dimensions_width = 200 ,
					dimensions_height = 25,
					align = 0
				})
	
			layer:addChild( tipText )
		end
	end
	
	local isLook = true
	if data.uid ==  DATA_Session:get("uid") then
		isLook =false
	end
	--查看其它成员 ,自己不能查看
	local lookBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
		347 , 11 ,
		{
			--parent = params.parent , 
			front = m_gangItemPath .. (isLook and "look.png" or "look_dis.png"),
			callback=
			function()
				HTTP:call(20007,{ touid = data.uid },{success_callback = 
				function()
					local otherPalyerInfo = requires("Scene.common.otherPlayerInfo")
					display.getRunningScene():addChild( otherPalyerInfo:new():getLayer() )
				end})
			end
		})
	lookBtn:setEnable(isLook)
	layer:addChild( lookBtn:getLayer() )
	local isOpen = DATA_Gang:get("info").duty == 100 or DATA_Gang:get("info").duty == 90 --帮主或者副帮主才能踢人
	--踢出其它成员
	local removeBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
		237 , 11 ,
		{
			--parent = params.parent , 
			front = m_gangItemPath ..  ( ( isOpen and data.state ~= 100 ) and "remove.png" or "remove_dis.png") ,
			callback = function()
				if data.state ~= 100 then
					KNMsg.getInstance():boxShow( "你确认要将  " .. data.name .. "  踢出帮会吗？" ,{ 
									confirmFun = function()
										HTTP:call("alliance_excluding",{ touid = data.uid },{ success_callback = 
										function() 
											parent:gangMember()
										end })
									end , 
									cancelFun = function() end 
									} )

				else
					KNMsg.getInstance():flashShow( "权限不足，无法完成操作" )
				end
			end
		})
	if  data.uid ==  DATA_Session:get("uid")  then --不可以踢出自己
		removeBtn:setEnable(false)
	else
		removeBtn:setEnable( isOpen and data.state ~= 100 )--如果是帮主 更不可以踢
	end
	layer:addChild( removeBtn:getLayer() )	

	layer:setContentSize( bg:getContentSize() )
	return layer
end
return M