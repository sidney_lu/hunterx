local M = {}
local KNBtn = requires( "Common.KNBtn")
function M:new(  )
	local this = {}
	setmetatable(this , self)
	self.__index = self
	return this
end
function M:genItemView( data )

	local layer = display.newLayer()
	local str = COMMONPATH .. "item_bg.png"
	local m_gangItemPath = IMG_PATH .. "image/scene/gang/gang_list/"
	self:preLoadFrames(m_gangItemPath.."apply.png",m_gangItemPath.."apply.png")
	self:preLoadFrames(m_gangItemPath.."cancel_apply.png",m_gangItemPath.."cancel_apply.png")
	
	print("gang list item common paty=",COMMONPATH,m_gangItemPath)
	bg = display.newSprite( str )
	setAnchPos(bg , 0 , 0) 
	layer:addChild( bg )
	local titleElement = {
						{ title = "gang_title" , 	text = data.name } , 										--帮派名
						{ title = "gang_man_title" ,text = data.chief_name } , 							--帮主
						{ title = "ability_title" ,	text = "  " .. data.sum_ability } , 						--战力
						{ title = "gang_member" , 	text = data.count .."/" .. data.count_max } ,				--成员数
					}
	for i = 1 , #titleElement do
		print("for",i,titleElement[i].text)

		addX = 50 + math.floor( ( i - 1 ) / 3 ) * 260
		addY = 73 - (( i - 1 ) % 3) * 30
		print(addX,addY)
		local curTitle = titleElement[i].title
		if 	curTitle ~= "" then
			local tempTitle = display.newSprite( m_gangItemPath .. curTitle .. ".png")
			setAnchPos( tempTitle , addX , addY )
			layer:addChild( tempTitle )
			
			addX = addX +  60
			
			
			local tipText
			if titleElement[i].title == "gang_member" then
				tipText = display.strokeLabel( titleElement[i].text , addX , addY - 4 , 28 , ccc3( 0xff , 0x00 , 0x00 ) )
			
			else
				tipText = display.strokeLabel( titleElement[i].text , addX+120 , addY , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
						--dimensions_width = i<4 and 200 or 80,
						dimensions_width = 200,
						dimensions_height = 25,
						align = 0
				})
			end
	
			layer:addChild( tipText )
		end
	end

	self:createApplyBtn(layer,data,m_gangItemPath)

	layer:setContentSize( bg:getContentSize() )
	return layer
end

function M:createApplyBtn( layer,data ,m_gangItemPath )
	--apply button
	-- if self.applyBtn then
	-- 	self.applyBtn:getLayer():removeSelf()
	-- 	self.applyBtn = nil
	-- end
	

	
	
	self.applyBtn = KNBtn:new( COMMONPATH , { "long_btn.png" , "long_btn_pre.png" , "long_btn_grey.png"} ,
	 	addX - 64 , addY - 55,
	{
		priority = -142 ,
		front = m_gangItemPath .. ( isApply and "cancel_apply.png" or "apply.png" ) ,
		callback=
		function()
			local applyData = DATA_Gang:getApply()
			local isApply = false	--当前帮派是否已经申请
			if applyData then
				for key , v in pairs( applyData ) do
					if v.guildId == tonumber( data.guildId ) then
						isApply = true
						break
					end
				end
			end
			if isApply then
				--取消申请
				HTTP:call("alliance_ApplicationCancel",{ id = data.guildId },{success_callback = function ( )
					self.applyBtn.item["front"]:setSpriteFrame(display.newSpriteFrame(m_gangItemPath.."apply.png"))
					self.applyBtn:setStateView(1)
				end })
			else
				if table.nums( applyData ) >= 5 then
					KNMsg.getInstance():flashShow( "同时最多只可向5个帮会发送入帮申请" )
				else
					HTTP:call("alliance_ApplicationApply",{ id = data.guildId },{success_callback = 
						function()
							print("ganglayer ffffffffffffcallback",DATA_Gang:isJoinGang())
							if ( DATA_Gang:isJoinGang() ) then
								switchScene("gang") 
							else
								self.applyBtn.item["front"]:setSpriteFrame(display.newSpriteFrame(m_gangItemPath.."cancel_apply.png"))				
								self.applyBtn:setStateView(3)
							end
						end})
				end
			end
		end
	})

 	local appHistory = DATA_Gang:get("applyData")
 	for k,v in pairs(appHistory) do
 		if v.guildId == tonumber(data.guildId) then
 			self.applyBtn.item["front"]:setSpriteFrame(display.newSpriteFrame(m_gangItemPath.."cancel_apply.png"))				
 			self.applyBtn:setStateView(3)
 		end
 	end
--		applyBtn:setEnable( not isApply )
	layer:addChild( self.applyBtn:getLayer() )
end

function M:preLoadFrames( imageUri ,imageName)
	--local texture=CCTextureCache:sharedTextureCache():addImage(CCFileUtils:sharedFileUtils():getCachePath().."..\\res\\"..imageUri)
	local texture=cc.Director:getInstance():getTextureCache():addImage(imageUri)
    local texSize=texture:getContentSize()
    local texRect = cc.rect(0, 0, texSize.width, texSize.height);
    local frame=cc.SpriteFrame:createWithTexture(texture,texRect)
	cc.SpriteFrameCache:getInstance():addSpriteFrame(frame,imageName);
end
return M