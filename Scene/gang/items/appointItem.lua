local M = {}
local KNBtn = requires( "Common.KNBtn")
local GangTitle_config = requires("Config.Gang")
function M:new( ... )
	local this = {}
	setmetatable(this , self)
	self.__index = self
	return this
end

function M:genItemView( data,parent )
	local layer = display.newLayer()
	local bg = display.newSprite( COMMONPATH .. "item_bg.png" )
	setAnchPos(bg , 0 , 0) 
	layer:addChild( bg )

	local tempData = data.data
	local m_gangPath = IMG_PATH.."image/scene/gang/gang_list/"
	titleElement = {
						{ title = "gang_title" , 	text = tempData.name } , 										--帮派名
						{ title = "meber_tribute" ,	text = tempData.tribute  } , 									--帮威
						{ title = "meber_ability" ,	text = tempData.ability } , 									--战力
						{ title = "meber_title" , 	text = GangTitle_config[tempData.duty] } ,										--头衔
					}
	for i = 1 , #titleElement do
		addX = 50 + math.floor( ( i - 1 ) / 3 ) * 210
		addY = 73 - (( i - 1 ) % 3) * 30
		local curTitle = titleElement[i].title
		if 	curTitle ~= "" then
			local tempTitle = display.newSprite( m_gangPath .. curTitle .. ".png")
			setAnchPos( tempTitle , addX , addY )
			layer:addChild( tempTitle )
			
			local tipText
			addX = addX +  65
			
		
			tipText = display.strokeLabel( titleElement[i].text , addX+120 , addY , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
					dimensions_width = 200,
					dimensions_height = 25,
					align = 0
				})
		
			layer:addChild( tipText )
		end
	end

	--查看其它成员
	local appointBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
		335 , 17 ,
		{
			--parent = parent , 
			front = IMG_PATH.."image/scene/gang/appoint/appoint.png" ,
			callback=
			function()
				HTTP:call("alliance_manage",{ touid = tempData.uid , duty = data.position , index = data.targetIndex },{success_callback = 
				function()
					if data.isGangMan then
						switchScene("gang")
					else
						parent:appointFun()	
					end
				end})
			end
		})
	if data.isGangMan then
		if tempData.duty == 100 then
			appointBtn:setEnable( false )
		else
			appointBtn:setEnable( true )
		end
	else
		appointBtn:setEnable( tempData.duty <=10 )
	end

	layer:setContentSize(bg:getContentSize())
	layer:addChild( appointBtn:getLayer() )

	return layer
end
return M