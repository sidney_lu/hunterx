--20150707 帮会排名
local M = {}
local KNBtn = requires( "Common.KNBtn")
function M:new( ... )
	local this = {}
	setmetatable(this , self)
	self.__index = self
	return this
end

--必须有 1)parent.scroll   超出scroll范围的部分点击无效
--       2)parent:lookInfo 显示帮会详细信息
function M:genItemView( data ,parent)
	local ITEMPATH = IMG_PATH .. "image/scene/gang/gang_list/"
	local layer = display.newLayer()
	bg = KNBtn:new( COMMONPATH , { "item_bg.png" } ,  0 , 0 , 
	{
		--parent = parent ,
		upSelect = true , 
--				priority = -140 , 
		callback=
		function(event)
			if parent then
				if parent.scroll.validRect:containsPoint(cc.p(event.x,event.y))==false then return end

				--查看其它帮派信息
				HTTP:call("alliance_getallianceinfo",{ id = data.guildId },{success_callback = 
				function(resultData)
						parent:lookInfo( { type = 0 , data = resultData[1] or {} } )
				
				end})
			end
		end
	}):getLayer()
	layer:addChild( bg )

	titleElement = {
		{ title = "gang_title" , 	text = data.name  .. "(Lv" .. data.level ..")"} , 										--帮派名
		{ title = "gang_man_title" ,text = "" .. data.chief_name  } , 							--帮主
		{ title = "ability_title" ,	text = "  " .. data.sum_ability } , 						--战力
		{ title = "" , 				text = data.level } , 											--帮派等级
		{ title = "gang_rank" , 	text = data.top } ,											--排名
		{ title = "gang_member" , 	text = data.count .."/" .. data.count_max } ,				--成员数
	}
	for i = 1 , #titleElement do
		addX = 50 + math.floor( ( i - 1 ) / 3 ) * 260
		addY = 73 - (( i - 1 ) % 3) * 30
		local curTitle = titleElement[i].title
		if 	curTitle ~= "" then
			local tempTitle = display.newSprite( ITEMPATH .. curTitle .. ".png")
			setAnchPos( tempTitle , addX , addY )
			layer:addChild( tempTitle )
			
			local tipText
			addX = addX +  65
			
			if curTitle == "gang_rank"  then
				if tonumber(titleElement[i].text)<=3 then
					tipText = display.newSprite( IMG_PATH .. "image/scene/ranklist/" .. titleElement[i].text ..".png" )
					setAnchPos( tipText , addX + 3 , addY - 10  )
				else
					-- tipText = display.strokeLabel( tostring(titleElement[i].text) , addX + 55, addY , 32 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
					-- 	dimensions_width = 80,
					-- 	dimensions_height = 50,
					-- 	align = 0
					-- })	
					tipText = ui.newTTFLabel({
				        text = tostring(titleElement[i].text),
				        size = 36,
				        color = ccc3( 0x2c , 0x00 , 0x00 ),
				        x = addX + 31,
				        y = addY,
				        align = ui.TEXT_ALIGN_CENTER
    				})		
					tipText:setAnchorPoint(cc.p(0.5,0))		
				end
			elseif curTitle == "gang_member" then
				tipText = display.strokeLabel( titleElement[i].text , addX , addY - 4 , 28 , ccc3( 0xff , 0x00 , 0x00 ) )
			else

				tipText = display.strokeLabel( tostring(titleElement[i].text) , addX + 65, addY , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
						dimensions_width = 120,

						dimensions_height = 25,
						align = 0
					})
			end
			layer:addChild( tipText )
		end
	end

	layer:setContentSize( bg:getContentSize() )
	return layer
end
return M
