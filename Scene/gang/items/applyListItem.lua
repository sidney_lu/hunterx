local M = {}
local KNBtn = requires( "Common.KNBtn")
function M:new( ... )
	local this = {}
	setmetatable(this , self)
	self.__index = self
	return this
end

function M:genItemView( data,parent )
	local layer = display.newLayer()
	local bg = display.newSprite( COMMONPATH .. "item_bg.png" )
	local m_gangPath = IMG_PATH.."image/scene/gang/gang_list/"
	setAnchPos(bg , 0 , 0) 
	layer:addChild( bg )

	titleElement = {
				{ title = "look" } , 							--查看
				{ title = "reject" } , 							--拒绝
				{ title = "agree" } , 							--同意
			}
			
	--local str = data.name .. "(Lv" .. data.level .. ")" .. "申请加入帮会"
	-- local tipText = display.strokeLabel( str , 30 , 67 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
	-- 			dimensions_width = 400 ,
	-- 			dimensions_height = 25,
	-- 			align = 0
	-- 		})
	tipText = ui.newTTFLabel({
				        text = data.name .. " (Lv" .. data.level .. ")" .. " 申请加入帮会",
				        size = 26,
				        color = ccc3( 0x2c , 0x00 , 0x00 ),
				        x = 30,
				        y = 67,
				        align = ui.TEXT_ALIGN_LEFT
    				})	
    tipText:setAnchorPoint(cc.p(0,0))	
	bg:addChild( tipText )
	
	for i = 1 , #titleElement do
		local curFalg = titleElement[i].title
		local tempBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" } , 
			150 + ( i - 1 ) * 100 , 15 ,
			{
				--parent = params.parent , 
				front = m_gangPath.. curFalg .. ".png" ,
				callback=
				function()
					if curFalg == "look" then
						HTTP:call(20007,{ touid = data.uid },{success_callback = 
							function()
								local otherPalyerInfo = requires("Scene.common.otherPlayerInfo")
								display.getRunningScene():addChild( otherPalyerInfo:new():getLayer() )
							end})
					elseif curFalg == "reject" then
						HTTP:call("alliance_refuse",{ touid = data.uid },{success_callback = 
							function()
								--self:applyFun()
								parent:createApply(parent)--刷新界面
							end})
					elseif curFalg == "agree" then
						HTTP:call("alliance_agree",{ touid = data.uid,guildid = DATA_Gang:get("info").guildId },{success_callback = 
							function()
								--self:applyFun()
								parent:createApply(parent)--刷新界面
							end})
					end
				end
			}):getLayer()
		layer:addChild( tempBtn )
	end

	layer:setContentSize(bg:getContentSize())
	return layer
end
return M