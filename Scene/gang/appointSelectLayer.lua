local M = {}
local KNBtn = requires( "Common.KNBtn")
function M:new( ... )
	local this = {}
	setmetatable(this , self)
	self.__index = self

	return this
end
function M:close(  )
	self.baseBg:removeSelf()
	self.titleBg:removeSelf()
	self.baseLayer:removeSelf()
	self.baseLayer = nil
	self.baseBg = nil
	self.titleBg = nil
end
function M:createSelectList( params ,parent)
	-- self.viewLayer = parent.viewLayer
	-- self.baseLayer = parent.viewLayer
	self.baseBg = display.newSprite(COMMONPATH.."dark_bg_long.png")
	setAnchPos( self.baseBg , display.cx , 0 , 0.5 , 0)
	parent.baseLayer:addChild(self.baseBg)
	self.titleBg = display.newSprite(IMG_PATH.."image/scene/common/title_0.png")
	setAnchPos(self.titleBg,display.cx,display.height+27,0.5,1)
	parent.baseLayer:addChild(self.titleBg)
	local titleText = display.newSprite(IMG_PATH.."image/scene/gang/appoint/appoint_title.png")
	self.titleBg:addChild(titleText)
	titleText:setPosition(cc.p(display.cx,58))

	params = params or {}

	local LISTPATH = IMG_PATH .. "image/scene/gang/gang_list/"
	
	
	-- if self.viewLayer then	
	-- 	self.viewLayer:removeSelf()
	-- 	self.viewLayer = nil
	-- 	self.viewLayer = display.newLayer()
	-- 	self.baseLayer:addChild( self.viewLayer )
	-- end
	-- if SCENETYPE == SEEGANG then
	-- 	self.viewLayer:setPosition(cc.p(0,-130))
	-- end

	self.baseLayer = display.newLayer()
	self.viewLayer = display.newLayer()
	self.baseLayer:addChild(self.viewLayer)
	
	local back_btn = KNBtn:new(COMMONPATH, {"back_img.png" , "back_img_press.png"} , 50 , display.top-88 , {
	--	priority = this.params.priority,
		callback = function()
	--		switchScene("home")
			self:close()
			parent:back()
		end})
	back_btn:setPosition(cc.p(math.random(50,60),display.top-86))
	self.viewLayer:addChild(back_btn:getLayer())
	
	local data , totalPage , curPage , curType , group , pageText , curData , alonePageNum , listConfig , pageBg , rankText 
	local selectNum , optionFun , countNum	--用手选择参战人员
	local scroll = nil
	self.scroll = nil
	listConfig = params.listConfig			--选项按钮
	data = params.data 						--展示的数据
	curType = params.defaultType 			--默认激活table("list" = 未加入帮派，玩家看到的帮派列表)
	curPage = params.defaultPage or 1 		--默认展示页面
	alonePageNum = params.alonePageNum or 0		--单页item个数
	local isPaging = params.alonePageNum and true or false	--是否分页
	local heightType = 0
	
	
	if curType == "wars_member" then
		countNum = 0
		local members = {}
		--确认选择
		local chooseBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" ,"btn_bg_red2.png"} , 
			163  , 108 ,
			{
				priority = -142 ,
				front = COMMONPATH .. "confirm.png" ,
				callback=
				function()
					if table.nums( members ) == 0 then
						KNMsg.getInstance():flashShow( "请选择参战人员！" )					
					else
						self:createWars()
					end
				end
			})
		self.viewLayer:addChild( chooseBtn:getLayer() )
		self.viewLayer:addChild( display.newSprite( PATH .. "wars/selected.png" , 364 , 700):align(display.BOTTOM_LEFT) )
		self.viewLayer:addChild( display.strokeLabel( "请选择要参加帮战的成员" , 20 , 708 , 22 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
		selectNum = display.strokeLabel( countNum , 435 , 708 , 22 , ccc3( 0xff , 0xfc , 0xd3 ) ) 
		self.viewLayer:addChild( selectNum ) 
		
		optionFun = function( tempParams )
			tempParams = tempParams or {}
			local isSelect = tempParams.isSelect
			local uid = tempParams.uid
			
			countNum = countNum + ( isSelect and 1 or -1 )
			members[ uid .. "" ] = isSelect == true and uid or nil
			
			selectNum:setString( countNum )
		end
	end
	
	
	local function refreshData()
		--echoLog("[ganglayer]",refreshData,curType,table.nums(data))
		if curType == "list" then				--帮派列表
			curData = data.top_list  
		elseif curType == "ranking" then		--帮派排行
			curData = data.top_rank
		elseif curType == "apply" then			--申请加入
			curData = data.top_rank
		elseif curType == "info" then			--帮派成员
			curData = data.info
			--heightType = 2
		elseif curType == "wars_member" then	--选择参战成员
			curData = data.info
			heightType = 2
		elseif curType == "event_movement" then	--帮派事件
			curData = data.event_movement
			--heightType = 2
		elseif curType == "applylist" then		--申请列表
			curData = data.applylist
			heightType = 2
		elseif curType == "appoint" then		--帮内认命
			curData = data.info
		elseif curType == "gem" then			--商城宝石
			curData = data.gemconfig
			heightType = 2
		elseif curType == "task" then			--帮派任务
			curData = data.task.info
			heightType = 2
			return
		end
		echoLog("[ganglayer] curType",curType)
		if isPaging then
			totalPage = math.ceil( #curData / alonePageNum )
			pageText:setString( curPage .. "/" .. totalPage )
		else
			curPage = 1
		end
	end
	
	
	if isPaging then
		--页数背景
		pageBg = display.newSprite( COMMONPATH .. "page_bg.png" )
		setAnchPos(pageBg , 240 , 40 , 0.5)
		self.viewLayer:addChild( pageBg )
		--页数文字
		pageText = display.strokeLabel( curPage .. "/" .. 1  , 230 , 117 , 20 , ccc3(0xff,0xfb,0xd4) )
		setAnchPos( pageText , 240, 47, 0.5 )
		self.viewLayer:addChild(pageText)
	else
		totalPage = nil
	end
	refreshData()
	
	local function createList( )
		if scroll then
			scroll:getLayer():removeSelf()
			scroll = nil
		end
		
		refreshData()
		
		local scrollX , scrollY , scrollWidth , scrollHeihgt = 15,80,450,display.height - 120 - 10 - 80
		-- scrollX			= 15
		-- scrollY			= isPaging and 155 or 105
		-- scrollWidth		= 450
		-- --scrollHeihgt	= isPaging and 530 or 580
		-- scrollHeihgt    = isPaging and display.height - 120 - 94 - 89 or display.height-120-94-39
		-- if heightType == 1 then
		-- 	scrollY 		= 155
		-- 	scrollHeihgt 	= 392
		-- elseif heightType == 2 then
		-- 	scrollY 		= 155
		-- 	scrollHeihgt 	= 525
			
		-- end
		scroll = KNScrollView:new( scrollX , scrollY , scrollWidth , scrollHeihgt , 5 )
		for i = 1 , ( isPaging and alonePageNum or #curData ) do
			local itemData = curData[ ( curPage - 1 ) * alonePageNum + i ]
			if itemData then
				local tempItem
	
			
				local itemEngin = requires("Scene.gang.items."..string.lower(curType).. "Item"):new()
				if data.isGangMan==nil or itemData.duty~=100 then --如果是任命帮主，不显示帮主自己

					tempItem = itemEngin:genItemView(( curType == "appoint" and { isGangMan = data.isGangMan , data = itemData , targetIndex = data.targetIndex ,  position = data.position } or  itemData ),parent)
					scroll:addChild(tempItem, tempItem )
				end
			end
		end
		scroll:alignCenter()
		self.viewLayer:addChild( scroll:getLayer() )
		self.scroll = scroll
	end
	
	
	
	local KNRadioGroup = requires( "Common.KNRadioGroup")
	local startX,startY = 10,display.height - 120 - 30
	if heightType == 1 then startX,startY = 10 , 556 end
	if SCENETYPE == RANK then startX,startY = 18 , 690 end
	
	
	
	
	group = KNRadioGroup:new()
	for i = 1, #listConfig do
		local temp = KNBtn:new( COMMONPATH.."tab/", ( ( SCENETYPE == RANK or SCENETYPE == TASK ) and {"long.png","long_select.png"} or {"tab_star_normal.png","tab_star_select.png"} ) , startX , startY , {
			disableWhenChoose = true,
			upSelect = true,
			id = listConfig[i][1],
			front = { LISTPATH..listConfig[i][1]..".png" , LISTPATH..listConfig[i][2]..".png"},
			callback=
			function()
				curType = listConfig[i][1]
				curPage = 1
				createList( listConfig[i][1] )
			end
		},group)
		self.viewLayer:addChild(temp:getLayer())
		startX = startX + temp:getWidth() + ( SCENETYPE == RANK and 12 or 4 )
	end
	--group:chooseById( curType , true )	--激活的选项
	if #curData == 0 then
	else
		createList()
	end
	
	
	
	if curType == "appoint" then
		local str = "请选择要任命的" .. data.text
		if data.text == "帮主" then
			str = "请选择要任命的下一任帮主"
		end
		self.viewLayer:addChild( display.strokeLabel( str , 20 , display.height - 120 +10 , 20 , ccc3( 0xff , 0xfc , 0xd3 ) ) )
	end

	
	

	self.viewLayer:addChild( display.newSprite( COMMONPATH.."tab_line.png"  , 6 , display.height - 120 - 10 ):align(display.BOTTOM_LEFT) )
	--翻页按钮
	if isPaging then
		local pre = KNBtn:new(COMMONPATH,{"next_big.png"}, 150, 30, {
			scale = true,
			flipX = true,
			callback = function()
				if curPage > 1 then
					curPage = curPage - 1
					createList( curType )
				end
			end
		})
		self.viewLayer:addChild(pre:getLayer())
		local next = KNBtn:new(COMMONPATH,{"next_big.png"}, 285, 30, {
			scale = true,
			callback = function()
				if curPage < totalPage then
					curPage = curPage + 1
					createList( curType )
				end
			end
		})
		self.viewLayer:addChild(next:getLayer())
	end

	return self.baseLayer
end
return M