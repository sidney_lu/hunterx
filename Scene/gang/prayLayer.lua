local PrayLayer ={}
local KNClock = requires("Common.KNClock")
local KNBtn = requires( "Common.KNBtn")
local KNMask = requires("Common.KNMask")
local PATH = IMG_PATH .. "image/scene/gang/"
local PRAYPATH = PATH .. "pray/"
local curGroup = 1
local isOver = false
local speed = 0.2
local KNCardpopup = requires("Common.KNCardpopup")
local dropsCount = 0
function PrayLayer:new()
	local this = {}
	setmetatable(this , self)
	self.__index = self
end
--祈福界面
function PrayLayer:createPray(parent,params)
	--SCENETYPE = PRAY
	parent:refreshGangInfo( { type = 3 , refresh = true } )
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_pray_title.png" )
	--GLOBAL_INFOLAYER:showNavigation(false)
	self.baseLayer = parent.baseLayer
	self.viewLayer = parent.viewLayer

	local layer = display.newLayer()
	if self.viewLayer then	
		self.baseLayer:removeChild(self.viewLayer,true)
		--self.viewLayer:removeSelf()
		self.viewLayer = display.newLayer()
		self.baseLayer:addChild( self.viewLayer )
	end
	
	local data = DATA_Gang:get("pray")
	local layer = display.newLayer()
	setAnchPos(layer,display.cx, display.cy,0.5,0.5)

	layer:addChild(display.newSprite(PRAYPATH .. "bg.png" , display.cx ,  display.cy))
	layer:addChild(display.newSprite(PRAYPATH .. "trends_title.png" , display.cx , 727 ) )
	layer:addChild(display.newSprite(PRAYPATH .. "tip.png" , display.cx , 580) )
	layer:addChild(display.newSprite(PRAYPATH .. "frame.png" , display.cx , 450) )
	layer:addChild(display.newSprite(PRAYPATH .. "cost.png" , 110 , 190) )
	--layer:addChild(display.newSprite(COMMONPATH .. "gold.png" , 350 , 190) )
	--layer:addChild(display.newSprite(COMMONPATH .. "gold.png" , 170 , 190) )
	layer:addChild(display.newSprite(PATH .. "usertribute_v.png" , 350 , 190) )
	layer:addChild(display.newSprite(PATH .. "usertribute_v.png" , 170 , 190) )
	layer:addChild(display.newSprite(PRAYPATH .. "surplus.png" , 288 + 25 , 190) )
	layer:addChild(display.newSprite(PRAYPATH .. "award_frame.png" , display.cx , 282) )
	for i = 1 , 4 do
		layer:addChild(display.newSprite(SCENECOMMON .. "skill_frame2.png" , 108 + (i-1) * 87 , 278) )
	end
	local trendsText = {}
	for i = 1 , 3 do
		trendsText[i] = display.strokeLabel( "" , 40 , 708 - i * 25 , 20 ,ccc3(0x2c , 0x00 , 0x00 ) )
		layer:addChild( trendsText[i] )
	end 
	
	local delay = nil
	local function refreshTrends()
		if not delay or delay > 10 then
			delay = 0
			--HTTP:call("alliance","cliffordmovement",{ },{ no_loading = true , success_callback =
			HTTP:call(42033,{ },{ no_loading = true , success_callback =
			function( tempData )
				for i = 1 , #trendsText do
					local curData = tempData.clifford_movement[i] or { content = ""  }
					local str = curData.content 
					trendsText[i]:setString( str )
				end
			end})
		end
		delay = delay + 1
	end
	
	--KNClock:addTimeFun( "trends" , refreshTrends )
	Clock:addTimeFun("trends",refreshTrends)
	--refreshTrends()
	--echoLog("[PrayLayer]","refreshTrends")
	
	local function showAward( index , awards )
		local gold = awards.award_gold
		local silver = awards.award_silver
		layer:addChild( display.newSprite(COMMONPATH .. ( gold == 0 and "silver.png" or "gold.png" ) , 330 , 567 - index * 34 ) )
		layer:addChild( display.strokeLabel( ( gold == 0 and silver or gold)  , 345 ,  557 - index  * 34 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) ) )
	end
	
	for i = 1 , 5 do
		layer:addChild(display.newSprite(IMG_PATH .. "image/scene/battle/hero_info/line_long.png" , display.cx , 552 - i * 34) )
		layer:addChild(display.newSprite( PRAYPATH .. "other_tip.png" , display.cx - 50, 567 - ( i + 1 ) * 34) )
		layer:addChild( display.strokeLabel( 5 - i , 115 + 40  ,  557 - ( i + 1 ) * 34 , 20 , ccc3( 0x2c , 0x00 , 0x00 ) ) )
		showAward( i + 1  , data.clifford_award[ (i + 1) ] )
		if i == 1 then
			layer:addChild(display.newSprite( PRAYPATH .. "highest_tip.png" , display.cx - 50, 567 - i * 34) )
			showAward( i , data.clifford_award[ i ] )
		end
	end
	layer:addChild( display.strokeLabel( string.convertMoney(data.clifford_money) , 187 , 180 , 18 , ccc3( 0x2c , 0x00 , 0x00 ) ) )
	--剩余黄金
	local gangInfo = DATA_Gang:get( "info" )
	local surplusText = display.strokeLabel( string.convertMoney(gangInfo.contribution) , 370 , 180 , 18 , ccc3( 0x2c , 0x00 , 0x00 ) )
	layer:addChild( surplusText ) 
	
	--setAnchPos(layer,0,-100)
	
	
	
	
	--动画部分处理
	--local maskLayer = UICutLayer:createChip(480,53)--WindowLayer:createWindow()	

	local maskLayer = display.newLayer()
	--setAnchPos(maskLayer , 0 , 252 , 0 , 0)
	maskLayer:setContentSize(CCSizeMake(480,53))
	--layer:addChild( maskLayer )
	self.maskLayer = maskLayer
	local clipping = display.newClippingRectangleNode(cc.rect(0, 0, 480, 53))

	clipping:addChild(maskLayer)
	setAnchPos(clipping , 0 , 252 , 0 , 0)
	layer:addChild(clipping)

	local prayRecord = {}


	local function createInfo( isFirst )
		if not isFirst then
			for j = 0 , 3 do
				transition.stopTarget( prayRecord[curGroup][j] )
				prayRecord[curGroup][j]:removeSelf()
				prayRecord[curGroup][j] = nil
			end
			prayRecord[curGroup] = nil
			local imgPath = PRAYPATH .. ( data.rands and ("text".. data.rands[curGroup+1]) or ("init" .. curGroup ) )  .. ".png"
			prayRecord[curGroup] = display.newSprite( imgPath  , 108 + curGroup * 87 , 25)
			maskLayer:addChild( prayRecord[curGroup] )
			curGroup = curGroup + 1
		else
			for i = 0 , 3 do
				if prayRecord[i] then
					prayRecord[i]:removeSelf()
					prayRecord[i] = nil
				end
				prayRecord[i] = display.newSprite( PRAYPATH .. ( data.rands and ("text".. data.rands[i+1]) or ("init" .. i ) )  .. ".png" , 108 + i * 87 , 25)
				maskLayer:addChild( prayRecord[i]  )
			end
		end
		
	end
	createInfo(true)
	self.prayRecord = prayRecord
--		if isOver and group == curGroup then
--			for j = 0 , 3 do
--				transition.stopTarget( prayRecord[curGroup][j] )
--				if j ~= data.rands[curGroup] then
--					prayRecord[curGroup][j]:removeSelf()
--					prayRecord[curGroup][j] = nil
--				end
--			end
--			curGroup = curGroup + 1
--			curGroup = curGroup>=4 and  4 or curGroup
--			prayRecord[curGroup] = prayRecord[curGroup][data.rands[curGroup]]
--		end

	local isData = false		--是否接收到数据
	local downTime = 3	--倒计时
	
	
	
	
	
	local mask
	local function overAction(  )
		downTime = downTime - 1
		isOver = downTime <= 0
		if ( ( not isOver ) and ( not isData ) ) then
			return
		end
		--echoLog("[PrayLayer]","isOver")
		speed = speed * 2
		data = DATA_Gang:get("pray")
		--print(data.rands)
		createInfo()
		--echoLog("[PrayLayer]","createInfo")
		if curGroup < 4 then
			return
		end
		
		Clock:removeTimeFun( "prayAction" )
	
		
		mask:remove()
		--echoLog("[PrayLayer]","remove")
		--surplusText:setString( string.convertMoney(DATA_User:get("gold")))	--刷新剩余黄金数量
		local gangInfo = DATA_Gang:get( "info" )
		surplusText:setString(string.convertMoney(gangInfo.contribution))	--刷新剩余黄金数量
		--parent:refreshGangInfo( { type = 3 , refresh = true } )
		
		--echoLog("[PrayLayer]","refreshGangInfo")
		local totalNum = 0
		if table.concat(data.rands) == "0123" then
			totalNum = 5--出现福星高照  取最高奖励数据，1为最高奖励
		else
			for i = 1 , #data.rands do
				if data.rands[i] == 0 then
					totalNum = totalNum + 1
				end
			end
		end
		--echoLog("[PrayLayer]","awardData totalNum",totalNum)
		local awardData = data.clifford_award[ 6 - totalNum ]
		-- dump(data.awards)
		-- data.awards.drop = {}
		-- data.awards.drop[1] = {cid=9001}
		-- data.awards.drop[2] = {cid=9001}
		dropsCount = table.nums(data.awards.drop)
		if dropsCount>0 then
			
			self:showCards(data.awards.drop,awardData)
			
			-- local scene = display.getRunningScene()
			-- local card_mask = KNMask:new({priority = -133})
			-- local card_popup = KNCardpopup:new(9001 ,
			-- 		 function() 
			-- 		 	--callback(index) 
			-- 		 	scene:removeChild(card_mask:getLayer())
			-- 		 	KNMsg.getInstance():flashShow(  "恭喜您获得" .. ( awardData.award_gold == 0 and  awardData.award_silver .. "银两" or awardData.award_gold .. "砖石" )   )
			-- 		 end , 
			-- 		 {
			-- 			-- init_x = card_x - 196,
			-- 			-- init_y = card_y - 211,
			-- 			-- end_x = card_x - 196,
			-- 			-- end_y = card_y - 211 - 230 ,
			-- 			-- top_tips = top_tips,

			-- 		})
			-- scene:addChild(card_mask:getLayer() , 100)
			-- card_mask:getLayer():addChild( card_popup:play() )
		else
			KNMsg.getInstance():flashShow(  "恭喜您获得" .. ( awardData.award_gold == 0 and  awardData.award_silver .. "银两" or awardData.award_gold .. "砖石" )   )
		end
		
		
	end


	
	--祈福
	local prayBtn = KNBtn:new( COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" } , 168 , 116 , {
		front = PRAYPATH .. "pray_text.png",
--		priority = -131,	
		callback = function()
			if countGold(data.clifford_money ) then
				mask = KNMask:new({ priority = -133 ,	opacity = 0 })
				local scene = display.getRunningScene()
				scene:addChild( mask:getLayer() )
				
				speed = 0.2
				curGroup = 0
				downTime = 3
				isOver = false
				isData = false
				
				--HTTP:call("alliance","sendclifford",{ },{
				HTTP:call(42032,{ },{
					success_callback =function(data) 
						isData = true
						Clock:addTimeFun("prayAction" , overAction )
						--local tempData = DATA_Gang:get("pray")
						--tempData.rands = nil
						--DATA_Gang:set_type("pray" , tempData)
						self:startAction()

						
					end
					,error_callback=function ( data )
						KNMsg.getInstance():flashShow(data.msg)
						mask:remove()
					end
				})
			else
				--KNMsg.getInstance():flashShow("缺少金币")
			end
		end
	}):getLayer()
	layer:addChild( prayBtn )
	
	
	self.viewLayer:addChild(layer)
end

function PrayLayer:showCards(items, awardData )
	local scene = display.getRunningScene()
	local card_mask = KNMask:new({priority = -133})
	local card_popup = KNCardpopup:new(items[dropsCount].cid ,
		 function() 
		 	--callback(index) 
		 	scene:removeChild(card_mask:getLayer())
		 	dropsCount = dropsCount - 1
		 	if dropsCount ==0 then
		 		KNMsg.getInstance():flashShow(  "恭喜您获得" .. ( awardData.award_gold == 0 and  awardData.award_silver .. "银两" or awardData.award_gold .. "砖石" )   )
		 	else
		 		self:showCards(items,awardData)
		 	end
		 end , 
		 {
			-- init_x = card_x - 196,
			-- init_y = card_y - 211,
			-- end_x = card_x - 196,
			-- end_y = card_y - 211 - 230 ,
			-- top_tips = top_tips,

		})
	scene:addChild(card_mask:getLayer() , 100)
	card_mask:getLayer():addChild( card_popup:play() )
end
function PrayLayer:startAction( group )
	local prayRecord = self.prayRecord
	if group then
		local addX = 76 + group * 87
		--计算Y坐标
		local function countY( i ) 
			print("countY")
			local tempY = 50
			data = DATA_Gang:get("pray")
			--echoLog("[PrayLayer]",data.rands)
--				data.rands = { 1 , 3 , 1 , 1 }--星照星星
			if data.rands then
				tempY = ( isOver and ( group == curGroup ) and ( i < data.rands[curGroup]) ) and ( i == data.rands[curGroup] and 20 or -60 ) or 50 
			end
			print("countY",tempY)
			return tempY
		end
		
		local function chekeOver( i )
			print("checkover")
			setAnchPos( prayRecord[group][i] , 76 + group * 87 , -60 , 0 , 0 )
			if i == 2 then
				self:startAction(group) 
			end
		end
		--echoLog("PrayLayer","startAction prayRecord=",prayRecord,group,prayRecord[group])
		transition.moveTo( prayRecord[group][0] , { delay = 0	,			time = speed , y = countY(0) , onComplete = function() chekeOver(0)  end })
		--echoLog("PrayLayer","startAction prayRecord=1")
		transition.moveTo( prayRecord[group][1] , { delay = speed/2 , 		time = speed , y = countY(1) , onComplete = function() chekeOver(1)  end })
		--echoLog("PrayLayer","startAction prayRecord=2")
		transition.moveTo( prayRecord[group][2] , { delay = speed/2 * 2 ,	time = speed , y = countY(2) , onComplete = function() chekeOver(2)  end })
		transition.moveTo( prayRecord[group][3] , { delay = speed/2 * 3 , 	time = speed , y = countY(3) , onComplete = function() chekeOver(3)  end })
	else
		--初始化动画
		downTime = 3
		for i = 0 , 3 do
			if prayRecord[i] then
				prayRecord[i]:removeSelf()
				--self.maskLayer:removeChild(prayRecord[i],true)
				prayRecord[i] = nil
			end
			prayRecord[i] = {}
			
			for j = 0 , 3 do
				prayRecord[i][j] = display.newSprite(PRAYPATH .. "text".. j .. ".png" )
				setAnchPos( prayRecord[i][j] , 76 + i * 87 , -60 , 0 , 0 )
				self.maskLayer:addChild( prayRecord[i][j] )
			end

			self:startAction(i)
		end
	end
	 
	 
	 
end
return PrayLayer