RankLayer ={}
local PATH = IMG_PATH .. "image/scene/gang/"
local KNBtn = requires( "Common.KNBtn")
local pagingOffsetY = 0
function RankLayer:new( params )
	local this = {}
	setmetatable(this , self)
	self.__index = self
end

function RankLayer:createRank( parent, params )
	--self.gangInfoLayer:setVisible(false)
	self.parent = parent
	self.baseLayer = parent.baseLayer
	self.baseLayer.bg:setPosition(cc.p(0,display.cy))

	self.viewLayer = parent.viewLayer
	parent.gangInfoLayer:setVisible(false)
	GLOBAL_INFOLAYER:refreshTitle( PATH.."gang_total.png" )
	--GLOBAL_INFOLAYER:showNavigation(false)
	local listConfig =  {{"ability","tab_ability"} ,  {"tribute","tab_tribute"} , {"rank","tab_rank"}  }
	local activity = "ability"
	self:createList( { alonePageNum = 4 , listConfig = listConfig , defaultType = activity ,
	 data = DATA_Gang:get( "gang_rank" ) , defaultPage = 1 } )


end
--生成list列表
function RankLayer:createList( params )
	params = params or {}
	local LISTPATH = PATH .. "gang_list/"
	
	if self.viewLayer then	
		--self.viewLayer:removeSelf()
		self.baseLayer:removeChild(self.viewLayer,true)
		self.viewLayer = nil
		self.viewLayer = display.newLayer()
		self.baseLayer:addChild( self.viewLayer )
		if self.parent and self.parent.hideScroll then
			self.parent:hideScroll()
		end
	end
	--self.viewLayer:setPosition(cc.p(0,-140))

	print("RankLayer data=",params.data)
	print("RankLayer data=",DATA_Gang:get( "gang_rank" ))
	local data , totalPage , curPage , curType , group , pageText , curData , alonePageNum , listConfig , pageBg , rankText 
	local selectNum , optionFun , countNum	--用手选择参战人员
	local scroll = nil

	listConfig = params.listConfig			--选项按钮
	data = params.data 						--展示的数据
	curType = params.defaultType 			--默认激活table
	self.curType = curType
	curPage = params.defaultPage or 1 		--默认展示页面
	self.curPage = curPage
	alonePageNum = params.alonePageNum or 0		--单页item个数
	self.alonePageNum = alonePageNum
	local isPaging = params.alonePageNum and true or false	--是否分页
	local heightType = 0

	
	--刷新当前数据
	local function showMyRankText()
		local textElement = { ability = "战力排行:" , rank = "帮会排行:" ,tribute = "帮威排行:"  }
		echoLog("[ganglayer]","showRank",curType,table.nums(data),table.nums(data[curType]))
		local str = textElement[curType .. ""] .. data[ curType .. "_top" ] 
		if not rankText then
			rankText = display.strokeLabel( str , 10 , 95  + pagingOffsetY, 18 , ccc3(0xff,0xfb,0xd4), nil, nil, {
				dimensions_width = 130,
				dimensions_height = 50
			})
			self.viewLayer:addChild( rankText )
		end
		rankText:setString( str )
	end
	local function refreshData()
		--elseif curType == "rank" then			--帮会总榜
		--print("RankLayer refreshData data is nil?",data)
		--dump(data)
			curData = data.ability
			showMyRankText()

		echoLog("[ganglayer] curType",curType,"curData count",#curData)
		if isPaging then
			totalPage = math.ceil( #curData / alonePageNum )
			pageText:setString( curPage .. "/" .. totalPage )
		else
			curPage = 1
		end
	end
	
	
	if isPaging then
		--页数背景
		pageBg = display.newSprite( COMMONPATH .. "page_bg.png" )
		setAnchPos(pageBg , 240 , 110 +pagingOffsetY, 0.5)
		self.viewLayer:addChild( pageBg )
		--页数文字
		pageText = display.strokeLabel( curPage .. "/" .. 1  , 230 , 117, 20 , ccc3(0xff,0xfb,0xd4) )
		setAnchPos( pageText , 240, 117 + pagingOffsetY, 0.5 )
		self.viewLayer:addChild(pageText)
	else
		totalPage = nil
	end
	print("RankLayer refreshData",data)
	refreshData()
	
	-- local function createList( )
	-- 	if scroll then
	-- 		scroll:getLayer():removeSelf()
	-- 		scroll = nil
	-- 	end
		
	-- 	refreshData()
		
	-- 	local scrollX , scrollY , scrollWidth , scrollHeihgt
	-- 	scrollX			= 15
	-- 	scrollY			= isPaging and 155 or 105
	-- 	scrollWidth		= 450
	-- 	scrollHeihgt	= isPaging and 530 or 580
		
	-- 	if heightType == 1 then
	-- 		scrollY 		= 155
	-- 		scrollHeihgt 	= 392
	-- 	elseif heightType == 2 then
	-- 		scrollY 		= 155
	-- 		scrollHeihgt 	= 525
	-- 	end
		
	-- 	scroll = KNScrollView:new( scrollX , scrollY , scrollWidth , scrollHeihgt , 5 )
	-- 	for i = 1 , ( isPaging and alonePageNum or #curData ) do
	-- 		local itemData = curData[ ( curPage - 1 ) * alonePageNum + i ]
	-- 		if itemData then
	-- 			local tempItem = self:listCell( { 
	-- 												data = ( curType == "appoint" and { isGangMan = data.isGangMan , data = itemData , targetIndex = data.targetIndex ,  position = data.position } or  itemData ) , 
	-- 												type = curType , 
	-- 												parent = scroll , 
	-- 												index = ( curPage - 1 ) * alonePageNum + i , 
	-- 												checkBoxOpt = optionFun , 
	-- 												checked = false ,  
	-- 											} )
	-- 			scroll:addChild(tempItem, tempItem )
	-- 		end
	-- 	end
	-- 	scroll:alignCenter()
	-- 	self.viewLayer:addChild( scroll:getLayer() )
	-- end
	
	
	
	local KNRadioGroup = requires( "Common.KNRadioGroup")
	local startX,startY = 10,690
	if heightType == 1 then startX,startY = 10 , 556 end
	if SCENETYPE == RANK then startX,startY = 18 , display.height - 120 - 30 end
	
	
	
	
	group = KNRadioGroup:new()
	for i = 1, #listConfig do
		local temp = KNBtn:new( COMMONPATH.."tab/", ( ( SCENETYPE == RANK or SCENETYPE == TASK ) and {"long.png","long_select.png"} or {"tab_star_normal.png","tab_star_select.png"} ) , startX , startY , {
			disableWhenChoose = true,
			upSelect = true,
			id = listConfig[i][1],
			front = { LISTPATH..listConfig[i][1]..".png" , LISTPATH..listConfig[i][2]..".png"},
			callback=
			function()
				curType = listConfig[i][1]
				self.curType = curType
				curPage = 1
				curData = data[listConfig[i][1]]
				--self:createList( listConfig[i][1] )
				print("xxxxxxxx",listConfig[i][1],#curData)
				totalPage = math.ceil( #curData / alonePageNum )
				pageText:setString( curPage .. "/" .. totalPage )
				self.curPage = curPage

				showMyRankText()
				self:createScrollByNative(curData)
			end
		},group)
		self.viewLayer:addChild(temp:getLayer())
		startX = startX + temp:getWidth() + ( SCENETYPE == RANK and 12 or 4 )
	end
	--group:chooseById( curType , true )	--激活的选项
	

	--createList()
	print("data before scroll", data,curData)
	print("data before scroll", DATA_Gang:get( "gang_rank" ))
	self:createScroll(curData)
	
	

	self.viewLayer:addChild( display.newSprite( COMMONPATH.."tab_line.png"  , 6 , startY - 5 ):align(display.BOTTOM_LEFT) )
	print("rlayer isPaging",isPaging)
	--翻页按钮
	if isPaging then
		local pre = KNBtn:new(COMMONPATH,{"next_big.png"}, 150, 100 + pagingOffsetY, {
			scale = true,
			flipX = true,
			callback = function()
				if curPage > 1 then
					curPage = curPage - 1
					self.curPage = curPage
					--self:createList( curType )
					--totalPage = math.ceil( #curData / alonePageNum )
					pageText:setString( curPage .. "/" .. totalPage )
					self:createScrollByNative(curData)
				end
			end
		})
		self.viewLayer:addChild(pre:getLayer())
		local next = KNBtn:new(COMMONPATH,{"next_big.png"}, 285, 100 + pagingOffsetY, {
			scale = true,
			callback = function()
				if curPage < totalPage then
					curPage = curPage + 1
					self.curPage = curPage
					--self:createList( curType )
					pageText:setString( curPage .. "/" .. totalPage )
					self:createScrollByNative(curData)
				end
			end
		})
		self.viewLayer:addChild(next:getLayer())
	end
	
end


function RankLayer:createScrollByNative( d )


	if self.scroll then
		self.scroll:getLayer():removeSelf()
	end
	self:createScroll(d)
end
function RankLayer:createScroll( d )
	--print("RankLayer",DATA_Gang:get("task").task.info)
	--local curData = d.ability
	local localData = d
	local scrollX , scrollY , scrollWidth , scrollHeihgt
	scrollX			= 15
	--scrollY			= isPaging and 155 or 105
	scrollY = 155
	scrollWidth		= 450
	--scrollHeihgt	= isPaging and display.height - 120 - 40 - 94 or display.height - 120  - 94 - 100
	scrollHeihgt = display.height - 120 - 155 - 30
	
	-- if heightType == 1 then
	-- 	scrollY 		= 155
	-- 	scrollHeihgt 	= 392
	-- elseif heightType == 2 then
	-- 	scrollY 		= 155
	-- 	scrollHeihgt 	= 525
	-- end
	local curPage = self.curPage
	local alonePageNum = self.alonePageNum
	local scroll = KNScrollView:new( scrollX , scrollY , scrollWidth , scrollHeihgt , 5 )
	--for i = 1 , ( isPaging and alonePageNum or #localData ) do
	for i = 1 , alonePageNum do
		-- print("RankLayer list i=",i,"curPage",curPage)
		-- dump(localData)
		local itemData = localData[ ( curPage - 1 ) * alonePageNum + i ]
		--print("RankLayer itemdata",itemData)
		if itemData then
			local tempItem = self:listCell( { 
												--data = ( curType == "appoint" and { isGangMan = data.isGangMan , data = itemData , targetIndex = data.targetIndex ,  position = data.position } or  itemData ) , 
												type = self.curType,
												data = itemData ,
												parent = scroll , 
												index = ( curPage - 1 ) * alonePageNum + i , 
												checkBoxOpt = optionFun , 
												checked = false ,  
											} )
			scroll:addChild(tempItem, tempItem )
		end
	end
	scroll:alignCenter()
	self.scroll=scroll
	self.baseLayer:addChild( scroll:getLayer() )

end

--生成列表item
function RankLayer:listCell( params )
	print("RankLayer listCell type,",params.type)
	params = params or {}
	local type = params.type or 0 
	local data = params.data or {}
	local index = params.index
	local parent = params.parent
	local ITEMPATH = PATH .. "gang_list/"
	
	local layer = display.newLayer()
	--背景
	local bg
	if type == "ranking" or type == "rank" then
		bg = KNBtn:new( COMMONPATH , { "item_bg.png" } ,  0 , 0 , 
			{
				parent = parent ,--超出不会点击
				upSelect = true , 
	--				priority = -140 , 
				callback=
				function()
					--查看其它帮派信息
					HTTP:call("alliance_getallianceinfo",{ id = data.guildId },{success_callback = 
					function(resultData)
						if #resultData==1 then
							if self.parent and self.parent.lookInfo then
							
								self.parent:lookInfo( {data = resultData[1]} )

							end
						end
					end})
				end
			}):getLayer()
		layer:addChild( bg )
	else
		local str = type == "task" and IMG_PATH .. "image/scene/activity/item_bg.png" or COMMONPATH .. "item_bg.png"
		bg = display.newSprite( str )
		setAnchPos(bg , 0 , 0) 
		layer:addChild( bg )
	end
	
	
	local titleElement , addX , addY
	
	local function createItem()
		for i = 1 , #titleElement do
			addX = 50 + math.floor( ( i - 1 ) / 3 ) * 260
			addY = 73 - (( i - 1 ) % 3) * 30
			local curTitle = titleElement[i].title
			if 	curTitle ~= "" then
				local tempTitle = display.newSprite( ITEMPATH .. curTitle .. ".png")
				setAnchPos( tempTitle , addX , addY )
				layer:addChild( tempTitle )
				
				local tipText
				addX = addX +  65
				if curTitle == "gang_rank" then
					if 	type == "rank" or type == "ranking"then
						if titleElement[i].text<=3 then
							tipText = display.newSprite( IMG_PATH .. "image/scene/ranklist/" .. titleElement[i].text ..".png" )
							setAnchPos( tipText , addX , addY - 10  )
						else
							tipText = display.strokeLabel( titleElement[i].text , addX , addY - 8 , 40 , ccc3( 0xff , 0x00 , 0x00 ) )
						end
					else
						tipText = getImageNum( tonumber( titleElement[i].text ) , COMMONPATH .. "small_num.png" )
						setAnchPos( tipText , addX , addY )
					end
				elseif curTitle == "gang_member" then
					tipText = display.strokeLabel( titleElement[i].text , addX + 40, addY , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
							dimensions_width = i<4 and 200 or 80,
							dimensions_height = 25,
							align = 0
						})
				else
					tipText = display.strokeLabel( titleElement[i].text , addX+120 , addY , 20 , ccc3( 0x2c , 0x00 , 0x00 ) , nil , nil , {
							dimensions_width = i<4 and 200 or 80,
							dimensions_height = 25,
							align = 0
						})
				end
				layer:addChild( tipText )
			end
		end
	end
	
	
		
	if self.curType == "ability"  then	
		titleElement = {
							{ title = "meber_name" ,	text = data.nickname  } , 										--帮主
							{ title = "gang_lv" ,		text = data.characterLevel } , 											--等级
							{ title = "meber_ability" ,	text = data.ability } ,										--战力
							{ title = "gang_rank" , 	text = data.top } , 										--排行
						}
		createItem()
	
		--查看其它成员
		local lookBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
			335 , 17 ,
			{
				parent = params.parent , 
				front = PATH .. "gang_list/look.png" ,
				callback=
				function()
					HTTP:call(20007,{ touid = data.characterId },{success_callback = 
					function()
						local otherPalyerInfo = requires("Scene.common.otherPlayerInfo")
						display.getRunningScene():addChild( otherPalyerInfo:new():getLayer() )
					end})
				end
			}):getLayer()
		layer:addChild( lookBtn )
	
	elseif self.curType == "rank" then
		titleElement = {
							{ title = "gang_title" , 	text = data.name  .. "(Lv" .. data.level ..")"} , 										--帮派名
							{ title = "gang_man_title" ,text = data.chief_name  } , 							--帮主
							{ title = "ability_title" ,	text = "  " .. data.sum_ability } , 						--战力
							{ title = "" , 				text = data.level } , 											--帮派等级
							{ title = "gang_rank" , 	text = data.top } ,											--排名
							{ title = "gang_member" , 	text = data.count .."/" .. data.count_max } ,				--成员数
						}
		createItem()
	elseif self.curType == "tribute" then
		titleElement = {
							{ title = "meber_name" ,	text = data.nickname  } , 										--帮主
							{ title = "gang_lv" ,		text = data.characterLevel } , 											--等级
							{ title = "meber_tribute" ,	text = data.contribution } ,										--帮威
							{ title = "gang_rank" , 	text = data.top } , 										--排行
						}
		createItem()
		
		--查看其它成员
		local lookBtn = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
			335 , 17 ,
			{
				parent = params.parent , 
				front = PATH .. "gang_list/look.png" ,
				callback=
				function()
					HTTP:call(20007,{ touid = data.characterId },{success_callback = 
					function()
						local otherPalyerInfo = requires("Scene.common.otherPlayerInfo")
						display.getRunningScene():addChild( otherPalyerInfo:new():getLayer() )
					end})
				end
			}):getLayer()
		layer:addChild( lookBtn )
	end
	

	
	layer:setContentSize( bg:getContentSize() )
	return layer
end


return RankLayer