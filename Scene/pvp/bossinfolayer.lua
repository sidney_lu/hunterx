local M = {}
local KNBtn = require("Common.KNBtn")
local Progress = require("Common.KNProgress")
function M:create( param )
	local this = {}
	setmetatable(this , self)
	self.__index = self
	this.rootLayer = cc.Layer:create()
	-- local bg = display.newSprite(IMG_PATH.. "common/dark_bg.png")
	-- this.rootLayer:addChild(bg)
	dump(param,nil,5)
	local cur = param.data.received
	local curTime
	if cur==0 then

	else
		curTime = param.data.config[cur]
	end
	local bosshp = param.boss_curhp
	local bosshp_max = 2000000
	local label = cc.LabelTTF:create("开始/结束" , FONT , 20)
	label:setPosition(cc.p(137,200))
	label:setColor(ccc3( 0xff , 0x55 ,0xf3 ))
	this.rootLayer:addChild(label)
	self.countDown = cc.LabelTTF:create(curTime.start_time.. " / "..curTime.end_time,FONT,20)
	self.countDown:setPosition(cc.p(187,200))
	self.countDown:setAnchorPoint(cc.p(0,0.5))
	this.rootLayer:addChild(self.countDown)

	local labelBlood = cc.LabelTTF:create("Boss血量",FONT,20)
	labelBlood:setPosition(cc.p(137,170))
	this.rootLayer:addChild(labelBlood)
	local path = IMG_PATH.."image/scene/fb/"
	self.bloodBar = Progress:new(path,{"luck_bg.png","luck_fro.png"},
	 187, 155,{
		showText = true,
		cur = bosshp,
		max = bosshp_max
	})
	this.rootLayer:addChild(self.bloodBar:getLayer())
	--玩家目前伤害
	local btnList = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
			137 , 120 ,
			{
				--parent = params.parent , 
				--front = PATH .. "shop/open_text.png" ,
				callback=function()
					self:openList()
				end
			})
	this.rootLayer:addChild(btnList:getLayer())
	local btnFight = KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
			237 , 120 ,
			{
				--parent = params.parent , 
				--front = PATH .. "shop/open_text.png" ,
				callback=function()
					self:startFight()
				end
			})
	this.rootLayer:addChild(btnFight:getLayer())
	return this
end
function M:openList( ... )
	HTTP:call("boss_ranking_list", {} , {success_callback = 
	function(data)
		pushScene("pvp",{key="bossrank",data=data})
	end})
end
function M:startFight( ... )
	HTTP:call("boss_kill", {} , {success_callback = 
	function(data)
		DATA_Battle:setMod("boss")
		DATA_Battle:setAct("execute")--???
		DATA_Battle:set( data)

		--结束画面用数据
		DATA_Result:set( data)

		switchScene("battle")
	end})
end
function M:getLayer()
	return self.rootLayer
end
return M