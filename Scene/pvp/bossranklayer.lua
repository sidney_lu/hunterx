local M = {}
local InfoLayer = requires("Scene.common.infolayer")
local KNBtn = requires( "Common.KNBtn")
function M:create( param )
	local this = {}
	setmetatable(this , self)
	self.__index = self

	this.layer = display.newLayer()
	local bg = display.newSprite(COMMONPATH.."dark_bg.png")
	setAnchPos(bg,0,80)
	this.layer:addChild(bg)

	dump(param)
	---------------------------------------
	local data = {}
	data[1]={name="bb",lv=1,atk=10,rank=1}
	data[2]={name="bb",lv=1,atk=10,rank=1}
	data[3]={name="bb",lv=1,atk=10,rank=1}
	data[4]={name="bb",lv=1,atk=10,rank=1}
	data[5]={name="bb",lv=1,atk=10,rank=1}
	this:initList(data)
	---------------------------------------

	local retButton =  KNBtn:new( COMMONPATH , { "btn_bg.png" , "btn_bg_pre.png" , "btn_bg_dis.png"} , 
	display.cx , 100 ,
	{
		callback=function()
			popScene()
		end
	})
	this.layer:addChild(retButton:getLayer())
	if this.info then
		this.layer:removeChild(this.info:getLayer(), true)
	end
	this.info = InfoLayer:new("fb", 0, {tail_hide = true, title_text = title, 
		closeCallback = handler(self,self.func)})
	this.layer:addChild(this.info:getLayer(),10)

	
	return this
end
function M:func( ... )
	popScene()
end
function M:initList( list )
	local scroll = KNScrollView:new(30,135, 440, display.height-110-125, 5)
	
	-- if self.data["data"]["list"] then
	-- 	self.data["data"]["list"] = self:convertMaxList(self.data["data"]["list"],self.data["data"]["maxlist"])
	-- end
	-- local my_rank = nil
	-- if isset(self.data["data"],my_rank) then
	-- 	my_rank = {{rank = self.data["data"]["my_rank"], name = DATA_User:get("name"), lv = DATA_User:get("lv")}}
	-- end
	-- local list = {
	-- 	{self.data["data"]["rank"],TOPTEN}, --topTen
	-- 	{self.data["data"]["enermy"],ENEMY},
	-- 	{self.data["data"]["list"],FIGHT},
	-- 	{my_rank,SELF},
	-- 	{self.data["data"]["next"],BACK},
	-- }
	local count, my = 0, 0
	for i = 1, #list do
		testlog("bossrank loop")
			local item = self:createItem(list[i])
			scroll:addChild(item,item)
			--for j = 1, #list[i][1]	do
				-- local item = self:createItem(list[i][2],scroll,list[i][1][j])
				-- setAnchPos(item,0, 300)
				-- scroll:addChild(item, item)		
				-- if list[i][2] ~= SELF then
				-- 	count = count + 1
					
				-- 	if list[i][1][j].isme then
				-- 		my = count
				-- 	end
				-- else
				-- 	my = count
				-- end
			--end
	
	end
	scroll:alignCenter()
	self.layer:addChild(scroll:getLayer())


end
function M:createItem( obj )
	local layer = display.newLayer()
	local path = IMG_PATH.."image/scene/athletics/"
	local str = path.."rank_bg.png"

	
	local itemBg = display.newSprite(str)
	setAnchPos(itemBg)
	layer:addChild(itemBg)

	--local icon = display.newSprite()
	local lblName = cc.LabelTTF:create(obj.name,FONT,26)
	lblName:setAnchorPoint(cc.p(0,0.5))
	lblName:setPosition(20,70)
	itemBg:addChild(lblName)

	local lblAttack = cc.LabelTTF:create(obj.atk,FONT,18)
	lblAttack:setAnchorPoint(cc.p(0,0.5))
	lblAttack:setPosition(20,30)
	itemBg:addChild(lblAttack)


	--重要
	layer:setContentSize( itemBg:getContentSize() )
	return layer
end
function M:getLayer(  )
	return self.layer
end
return M