--[[

首页场景

]]


collectgarbage("setpause"  ,  100)
collectgarbage("setstepmul"  ,  5000)


-- [[ 包含各种 Layer ]]
local PVPLayer = requires("Scene.pvp.pvplayer")



local M = {}

function M:create(params)
	local scene = display.newScene("pvp")
	if params.key == "bossrank" then
		scene:addChild(require("Scene.pvp.bossranklayer"):create(params):getLayer())
	else
		---------------插入layer---------------------
		scene:addChild(PVPLayer:new(params):getLayer())
		---------------------------------------------
	end

	return scene
end

return M
