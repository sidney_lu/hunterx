local PATH = IMG_PATH .. "image/scene/pay/"
local KNLoading = requires("Common.KNLoading")


--[[一条数据]]
local PayItem = {
	layer,
	params,
}

function PayItem:new(data , params)
	local this = {}
	setmetatable(this,self)
	self.__index  = self

	this.params = params or {}

	this.layer = display.newLayer()

	
	local bg = display.newSprite( PATH .. "bar.png" )
	setAnchPos(bg , 0 , 0)
	this.layer:addChild(bg)


	-- local chong = display.newSprite( PATH .. "chong_" .. data.rmb .. ".png" )
	-- setAnchPos(chong , 30 , 20)
	-- this.layer:addChild(chong)

	-- local gold = display.newSprite( PATH .. "gold.png" )
	-- setAnchPos(gold , 200 , 20)
	-- this.layer:addChild(gold)

	-- local get = display.newSprite( PATH .. "get_" .. data.rmb .. ".png" )
	-- setAnchPos(get , 265 , 22)
	-- this.layer:addChild(get)

	-- if data.extra then
	-- 	local extra = display.newSprite( PATH .. "extra_" .. data.rmb .. ".png" )
	-- 	setAnchPos(extra , 360 , 15)
	-- 	this.layer:addChild(extra)
	-- end

	local icon = display.newSprite(PATH.."yuanbao.png")
	this.layer:addChild(icon)
	icon:setPosition(cc.p(50,35))
	-- 显示信息
	local label = display.strokeLabel( data.name , 80 , 25 , 25 ,
	 ccc3( 0xff , 0xff , 0xff )) 
	this.layer:addChild(label)

	this.layer:setContentSize( CCSizeMake( 436 , 80 ) )

	local moneyBg = display.newSprite(PATH.."01.png")
	moneyBg:setPosition(cc.p(350,35))
	this.layer:addChild(moneyBg)
	local moneyValue = ui.newTTFLabel({
        text = "10",
        size = 26,
        color = ccc3( 0x2c , 0x00 , 0x00 ),
        x = 40,
        y = 30,
        align = ui.TEXT_ALIGN_LEFT
	})	
	moneyBg:addChild(moneyValue)
	local moneyText = display.newSprite(PATH.."renminbi.png")
	moneyBg:addChild(moneyText)
	moneyText:setPosition(cc.p(90,30))
	-- 设置可点击
	local init_y = 0
	--[[
	this.layer:registerScriptTouchHandler(function( type , x , y )
		if type == CCTOUCHBEGAN then
			local range = this:getRange()
			if y < this.params.parent:getY() + this.params.parent:getHeight() and y > this.params.parent:getY()  then
				if this:getRange():containsPoint( ccp(x , y) ) then
					init_y = y
					return true
				end
			end

			return false
		elseif type == CCTOUCHMOVED then
		elseif type == CCTOUCHENDED then
			if this:getRange():containsPoint( ccp(x , y) ) then
				if math.abs(y - init_y) < 30 then
						print("pay id:", data.id)
						local function CallBack(order)
							-- 购买成功
							-- 调用服务端接口获取
							HTTP:call(40009 , {order=order, id=data.id} , {
								success_callback = function(d)
									KNMsg:getInstance():flashShow(d.msg)
								end,
								error_callback = function(d)
									KNMsg:getInstance():flashShow(d.msg)
								end
							})
						end
						if INIT_FUNCTION.platform == 1 then --android
							local className = "com.payment.commplatform.NdPlatformUtil"
							local javaMethodSig = "(II)V"  -- 字符串需要加分号
							local rec, uid = luaj.callStaticMethod(className, "payForProduce", {
								data.id,
								CallBack
								}, javaMethodSig)
						else
							CallBack("test0001")
						end
						
						-- HTTP:call("iapppay","token",{
						-- 	num = data.num
						-- } , {
						-- 	success_callback = function(response)
						-- 	dump(response)
						-- 	dump(data.num)
						-- 	    if device.platform == "ios" then
						-- 	    	if CHANNEL_ID == "91" then
						-- 	    	    -- IOS 91平台
						-- 				LuaCall91PlatForm:getInstance():buy(response["exorderno"] , response["waresid"] , data.num .. "两黄金" , response["price"] , 1)
						-- 			elseif CHANNEL_ID == "appFameOfficial" then
						-- 				--ios 云顶sdk平台正版
						-- 				local load = KNLoading:new()
		 --                                display.getRunningScene():addChild(load:getLayer())
						-- 			    LuaCallAppFameSDKOfficial:getInstance():buy(response["price"] , response["exorderno"],
		 --                    				function() 
		 --                    					load:remove() 
		 --                    				end ,
		 --                    				function(response)
		 --                    					load:remove()
		 --                    					local orderNo = response["orderNo"]
		 --                    					local receiptString = response["receiptString"]
											
		 --                    					HTTP:call("pay", "callback", { orderNo = orderNo , receiptString = receiptString } , {
		 --                    						requestUrl = CONFIG_PAY_URL .. "?from=appFameOfficial",
		 --                    						success_callback = function()
						-- 							-- this:selectActivity( curName )
						-- 						end
						-- 						})

		 --                    			end) 
						-- 			else
						-- 			end

						-- 	    else
									
						-- 	    end
						-- 	end
						-- })
				end
			end
		end

		return true
	end)]]

	this.layer:setTouchEnabled(true)

	local listener = cc.EventListenerTouchOneByOne:create()
	listener:setSwallowTouches(false)
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y

		local range = this:getRange()
		if y < this.params.parent:getY() + this.params.parent:getHeight() and y > this.params.parent:getY()  then
			if this:getRange():containsPoint( ccp(x , y) ) then
				init_y = y
				return true
			end
		end
		return false
	end, cc.Handler.EVENT_TOUCH_BEGAN)
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y
	
	end,cc.Handler.EVENT_TOUCH_MOVED)	
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		if this:getRange():containsPoint( ccp(x , y) ) then
			if math.abs(y - init_y) < 30 then
				print("pay id:", data.id)
				local function CallBack(order)
					-- 购买成功
					-- 调用服务端接口获取
					HTTP:call(40009 , {order=order, id=data.id} , {
						success_callback = function(d)
							KNMsg:getInstance():flashShow(d.msg)
						end,
						error_callback = function(d)
							KNMsg:getInstance():flashShow(d.msg)
						end
					})
				end
				if INIT_FUNCTION.platform == 1 then --android
					local className = "com.payment.commplatform.NdPlatformUtil"
					local javaMethodSig = "(II)V"  -- 字符串需要加分号
					local rec, uid = luaj.callStaticMethod(className, "payForProduce", {
						data.id,
						CallBack
						}, javaMethodSig)
				else
					CallBack("test0001")
				end
			end
		end	
	end,cc.Handler.EVENT_TOUCH_ENDED)
	this.layer:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, this.layer)


	return this
end

function PayItem:getLayer()
	return self.layer
end

--获取所有父组件，取得按钮的绝对位置
function PayItem:getRange()
	local x = self.layer:getPositionX()
	local y = self.layer:getPositionY()

	local parent = self.layer:getParent()
	if parent then
		x = x + parent:getPositionX()
		y = y + parent:getPositionY()
		while parent:getParent() do
			parent = parent:getParent()
			x = x + parent:getPositionX()
			y = y + parent:getPositionY()
		end
	end
	return CCRectMake(x,y,self.layer:getContentSize().width,self.layer:getContentSize().height)
end


return PayItem