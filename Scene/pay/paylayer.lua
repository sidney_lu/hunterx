local PATH = IMG_PATH .. "image/scene/pay/"

local InfoLayer = requires( "Scene.common.infolayer")

local KNBar = requires("Common.KNBar")
local PayItem = requires( "Scene.pay.item")
local KNBtn = requires("Common.KNBtn")
local vipinfo = requires("Config.vip")
local PayLayer = {
	layer,
	infolayer,
}

function PayLayer:new(args)

	local this = {}
	setmetatable(this , self)
	self.__index = self

	args = args or {}
	testlog("PayLayer new",args,args.isPush)

	-- 基础层
	this.layer   = display.newLayer()

	-- 背景
	--local bg = display.newSprite(COMMONPATH .. "mid_bg.png")
	local bg = display.newSprite(PATH.."bg.png")
	setAnchPos(bg , 0 , 0)						-- 70 是底部公用导航栏的高度
	this.layer:addChild(bg)
	
	local vipBg = display.newSprite(IMG_PATH.."image/scene/gang/hall/notice_bg2.png")
	this.layer:addChild(vipBg)
	vipBg:setPosition(cc.p(display.cx,display.top-150))
	local goVip = KNBtn:new( PATH , { "anniu.png" ,"anniu.png"}, 290 , 40 ,
	{
		priority = -130,
		front = PATH .. "vip_privilege.png" ,
		callback = 
		function()
			--switchScene("vip")
			pushScene("vip")
		end
	}):getLayer()
	--this.layer:addChild( goVip )
	vipBg:addChild(goVip)


	vipBg:addChild( display.newSprite( PATH .. "vip_tip.png" , 23 , 10):align(display.BOTTOM_LEFT) )	--充值额度越高，越实惠
	--local vipinfo = DATA_Vip:get( "vipinfo" )

	local vipexp = DATA_User:get("vipexp")
	local curVipLv =  DATA_User:get("vip")
	local nextVipLv = curVipLv + 1
	nextVipLv = nextVipLv > 9 and 9 or nextVipLv
	local needexp = vipinfo[nextVipLv].gold
	local costValue = needexp - vipexp
	--20150917 Sidney add vip等级-----------------
	--local nextGold = needexp - vipexp--到下级还需要充值多少
	local highLv = 9	--定义最高vip等级
	
	local tempBar = KNBar:new("exp_general" , 91 , 53 , { maxValue = needexp , curValue = vipexp , color = ccc3(255, 255, 255) })
	tempBar:setIsShowText( nextVipLv <= highLv )
	vipBg:addChild( tempBar )
	vipBg:addChild( display.newSprite( IMG_PATH.."image/scene/vip/cur_vip.png" , 18 ,  38):align(display.BOTTOM_LEFT) )	-- 当前vip等级 654
	vipBg:addChild( display.newSprite( IMG_PATH .. "image/scene/vip/cur_vip_title.png" , 113 ,  68 ):align(display.BOTTOM_LEFT) )	-- 当前vip等级694
	--------------------------------------

	-- local str
	-- if curVipLv <= 9 then
	-- 	str = "您是VIP-" .. curVipLv .. "再充" .. costValue .. "元成为VIP" .. ( nextVipLv )
	-- else
	-- 	str = "您是VIP-9"
	-- end
	-- this.layer:addChild( display.strokeLabel( str , 33 , display.top-150 , 18 , ccc3( 255 , 0 , 255 ) ) )

	local post_data = {}
    HTTP:call(40008 , post_data , {
			success_callback = function(data)
				print("-------success_callback:----------", #data)
				for k,v in pairs(data) do
					print(k,v)
				end
				local scroll = KNScrollView:new(20 , 85 , 436 , display.top-295 , 0 , false)
				for i = 1 , #data do
					local item = PayItem:new( data[i] , {
						parent = scroll
					})
					scroll:addChild(item:getLayer() , item)
				end
				this.layer:addChild( scroll:getLayer())
			end
		}
	)
-- if CHANNEL_ID == "appFameOfficial" or CHANNEL_ID == "tmsjIosAppStore" then
-- 		 data = {
-- 				{ rmb = 518 , num = 5180 , extra = true },
-- 				{ rmb = 308 , num = 3080 , extra = true },
-- 				{ rmb = 208 , num = 2080 , extra = true },
-- 				{ rmb = 108 , num = 1080 , extra = true },
-- 				{ rmb = 60 , num = 600 , extra = true },
-- 				{ rmb = 30 , num = 300 },
-- 				{ rmb = 6 , num = 60 },
--               }
--     else
--     	 data = {
--     	 		{ rmb = 5000, num = 50000, extra = true },
-- 				{ rmb = 500 , num = 5000 , extra = true },
-- 				{ rmb = 300 , num = 3000 , extra = true },
-- 				{ rmb = 200 , num = 2000 , extra = true },
-- 				{ rmb = 100 , num = 1000 , extra = true },
-- 				{ rmb = 50 , num = 500 , extra = true },
-- 				{ rmb = 30 , num = 300 },
-- 				{ rmb = 20 , num = 200 },
-- 				{ rmb = 10 , num = 100 },
-- 				-- { rmb = 1 , num = 10 },
-- 			}
--     end

	

	this.infolayer = InfoLayer:new("pay" , 0 , {tail_hide = true, title_text = PATH .. "title.png" , closeCallback = function()
		--Wolf：判断pay界面是否从home进入，是则返回home，否则返回shop
		if InfoLayer.isEnterFromHome   then
			switchScene("home")
		else
			switchScene("shop")
		end
	end, isPush=args.isPush})
	this.layer:addChild(this.infolayer:getLayer(),1)

--	-- 弹窗
--	if CHANNEL_ID ~= "uc" then
--		local handle
--		handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
--			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(handle)
--			handle = nil
--			
--			local msg = "【温馨提示】\n1，大家在本轮测试的充值，公测时将三倍返还\n2，本轮测试时间截止8月15日"
--			if CHANNEL_ID == "cmge" then
--				msg = "【温馨提示】\n大家在本轮测试的充值，公测时将三倍返还\n进入支付页面后，请重新填写充值金额"
--			end
--			KNMsg.getInstance():boxShow(msg , {
--				confirmFun = function() end,
--				align = 0
--			})
--		end , 0.02 , false)
--	end
--	

	return this.layer 
end


function PayLayer:getLayer()
	return self.layer
end

return PayLayer
