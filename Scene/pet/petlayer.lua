local PATH = "image/scene/pet/"
local SCENECOMMON = "image/scene/common/"
--[[英雄模块，首页点击英雄图标进入]]
local InfoLayer = require("Scene.common.infolayer")
local petInfo = require("Scene.pet.petinfo")
local KNBtn = require("Common.KNBtn")
local KNRadioGroup = require("Common.KNRadioGroup")
local SelectList = require("Scene.common.selectlist")
local wearEquipCell = require("Scene.hero.wearEquipCell")
--local Pet_Config = require("Config.Pet")
local Pet_Config = require("Config.Fengshen")
local Config_PetStage = require("Config.petstageconfig")

local PetLayer = {
	baseLayer,
	layer,
	infolayer,
	chooseLayer,
	petinfoLayer,
	iconGroup,
	hatch_btn,
}

function PetLayer:new(args)
	local this = {}
	setmetatable(this,self)
	self.__index  = self

	args = args or {}
	self.showType = args.showType
	self.heroData = args.heroData
	self.s3 = args.s3
	this.baseLayer = display.newLayer()
	this.layer = display.newLayer()

	this.viewLayer = display.newLayer()
	this.chooseLayer = display.newLayer()


	-- 背景
	local bg = display.newSprite(COMMONPATH .. "dark_bg.png")
	bg:setScaleY(display.height/698)
	setAnchPos(bg , 0 , 80)						-- 70 是底部公用导航栏的高度

	this.baseLayer:addChild(bg)

	local pet_bg = display.newSprite(PATH .. "pet_bg.png")
	setAnchPos(pet_bg , 15 , display.height - 100 - 633) --再减去pet_bg高度633
	this.viewLayer:addChild(pet_bg)

	--宠物排序规则	
	local function petSort(l,r) --自定义顺序
		local sortValueL = 0
		local sortValueR = 0
		--若是上阵宠物，其权值最大 
		if l == DATA_Pet:getFighting()..""	then
			sortValueL = sortValueL + 999
		elseif r == DATA_Pet:getFighting().."" then
			sortValueR = sortValueR + 999
		else
		--首先按照星级排序，然后再加等级
			local starL = getConfig("pet",DATA_Bag:get("pet",l,"cid"),"star")
			local starR	= getConfig("pet",DATA_Bag:get("pet",r,"cid"),"star")
			sortValueL = sortValueL + starL
			sortValueR = sortValueR + starR
		end
			
		return sortValueL > sortValueR
	end

	if self.showType == "select" then
		this.infolayer = InfoLayer:new("pet" , 0 , {tail_hide = true , title_text = PATH .. "title.png",
			isPush=true})
	else
		this.infolayer = InfoLayer:new("pet" , 0 , {tail_hide = true , title_text = PATH .. "title.png"})
	end
	
    --宠物选择按钮
    local function createChoose(index)
	    --这里根据排序规则生成键值的序列
	    local choose_index = index or 1
	    local keyList = getSortList(DATA_Bag:get("pet"),petSort)
	    print("[petlayer] keylist"..#keyList)
    	--local scroll = KNScrollView:new( 10 , 123 , 100 , 520 , 3)
    	--[[
    		--110 title  --
    		--100  hero   --
    		--x   scroll --
    		--90  bar    --
    	]]
    	local scroll = KNScrollView:new( 23 , 90 , 100 , display.height - 110 - 90 - 100 , 3)
    	local detailScroll 
    	detailScroll = KNScrollView:new(120, display.height - 100 - 400, 325, 400, 0, true, 1, {
    	--detailScroll = KNScrollView:new(120, 370, 325, 400, 0, true, 1, {
	    	page_callback = function()

			    this.iconGroup:chooseByIndex(detailScroll:getCurIndex())

			    scroll:setIndex(detailScroll:getCurIndex(), true)
			    this:createInfo(this.iconGroup:getId())
			    this:updateHero(this.iconGroup:getId())
	    	end
    	})

    	
    	
		this.iconGroup = KNRadioGroup:new()
		--20150925 add by sidney 默认为0
		--this.iconGroup:chooseByIndex(0)
		local state
		local x , y = 25 , 630
		for i = 1 , DATA_Bag:count("pet")  do
			local front , disable , bg
			local other = {}
			local temp_pet_info = DATA_Bag:get("pet" , keyList[i])

			if i <= DATA_Bag:count("pet") then
				front = getImageByType(temp_pet_info["cid"] , "s")
				disable = false
				bg = "small_photo_bg.png"
				if DATA_Pet:getFighting() == temp_pet_info["id"] then --判断是否是上阵宠物
					other[#other + 1] = {PATH .. "pet_state_fighting.png" , -10 , 40}
				end
				
			else
				bg = "small_photo_bg2.png"
				disable = true
			end

			other[#other + 1] = { COMMONPATH .. "equip_lv_bg.png" , 45 , 55 }
			
			-- 计算等级信息
			local lv , cur_exp , max_exp = DATA_Pet:calcExp(temp_pet_info.exp)
			local textData = {lv , 16 , ccc3( 0xff , 0xff , 0xff ) , { x = 25 , y = 30	} , nil , 20 }
			local temp
			temp = KNBtn:new(COMMONPATH , {bg,"select1.png"} , x , y  , {
				id = temp_pet_info["id"],
				front = front,
				noHide = true,
				frontScale = { 1 , -2 , 5 },
				parent = scroll,
				selectOffset = { 0 , 1 },
				selectZOrder = 15,
				other = other,
				disable = disable,
				upSelect = true,
				text = textData,
				callback = function()
					--self:updateHero(i)--if needed

					detailScroll:setIndex(i)
					this:createInfo(temp:getId())
					this:updateHero(temp:getId())
				end,
			} , this.iconGroup)
			scroll:addChild(temp:getLayer() , temp)
			--dump(temp_pet_info)
			--testlog("pet layer left list",self.s3 and self.s3.eye or 0,i)
			if (args.gid and temp_pet_info["id"] == args.gid)
				or (self.s3 and self.s3 == temp_pet_info.id) then
				choose_index = i
				this.iconGroup:chooseByIndex(choose_index)
				detailScroll:setIndex(choose_index, true, 0)
			end
		end
		scroll:alignCenter()

		this.layer:addChild(this.chooseLayer)
		
		this.layer:addChild(scroll:getLayer())


		-- 新手引导
		local guide_step = KNGuide:getStep()
		if guide_step == 402 then
			local btn = scroll:getItems(1)
			local btn_range = btn:getRange()

			KNGuide:show( btn:getLayer() , {
				mask_clickable = true,
				x = btn_range:getMinX(),
				y = btn_range:getMinY(),
				selectList = true,
				callback = function()
					KNGuide:show( this.hatch_btn:getLayer() )
				end
			})
		end
		
		--这里是滑动的宠物界面local pet
		for i = 1 , DATA_Bag:count("pet")  do
			local temp_pet_info = DATA_Bag:get("pet" , keyList[i])
			
			local pet = petInfo:new(0 , 0 , 325 , 355 , { id = temp_pet_info["id"], parent = detailScroll})
			detailScroll:addChild(pet:getLayer(), pet)
		end
		detailScroll:alignCenter()
		this.layer:addChild(detailScroll:getLayer())
    end
	
	this.spHero = this:createHero()
	if this.spHero then
		this.layer:addChild(this.spHero:getLayer())
	end

	data={}
	data["3"]={}
	data["3"]["cid"]=2101
	data["3"]["id"]=3
	--data["0"]["stage"]=8
	data["3"]["exp"]=300 
	data["3"]["lv"]=2
	data["3"]["skill"]={}
	data["1"]={}
	data["1"].cid=2301
	data["1"].id=1
	--data["1"].stage=108
	data["1"].exp=500
	data["1"].lv=4
	data["1"].skill={}
	data["2"] = {cid=2501,id=2,stage=1,exp=350,lv=5}
	data["2"].skill = {}
	DATA_Bag:set("pet",data)
	DATA_Bag:update("prop","6",{type="faction",num=5,cid=16020,id=6})
	--16020
	

	DATA_Pet:init()
	if DATA_Bag:count("pet") > 0 then
		createChoose()
		this:createInfo(args.gid or this.iconGroup:getId())
		this:updateHero(args.gid or this.iconGroup:getId())
	end

	this.layer:addChild(this:createProps())
	
	
	
	this.baseLayer:addChild(this.infolayer:getLayer(),1)
	this.viewLayer:addChild(this.layer)
	this.baseLayer:addChild(this.viewLayer)	
	return this.baseLayer 
end


   
--宠物信息界面 
function PetLayer:createInfo( id )
	testlog("PetLayer createInfo by id",id)
	if self.petinfoLayer then
		self.layer:removeChild(self.petinfoLayer,true)
	end
	self.petinfoLayer = display.newLayer()


	local pet_data = DATA_Bag:get("pet", id)
	local cid = pet_data.cid
	local config_data = Pet_Config[tostring(cid)]
	--local config_data = getConfig("pet",cid)
	
	local desc = createLabel({str = config_data.desc, color = ccc3(0x2c , 0x00 , 0x00),
	 width = 270})
	
	setAnchPos(desc , 148 , display.height - 150 - 353 ,0,1)
	self.petinfoLayer:addChild(desc)
	-- 技能文字(主动和被动技能)
	-- local tiansheng_bg = display.newSprite(  PATH .. "skill_type_bg.png" )
	-- self.petinfoLayer:addChild( tiansheng_bg )
	-- setAnchPos(tiansheng_bg , 108 , 295 )

	-- local tiansheng = display.newSprite(  PATH .. "tiansheng.png" )
	-- self.petinfoLayer:addChild( tiansheng )
	-- setAnchPos(tiansheng , 128 , 308 )

	-- local xuexi_bg = display.newSprite(  PATH .. "skill_type_bg.png" )
	-- self.petinfoLayer:addChild( xuexi_bg )
	-- setAnchPos(xuexi_bg , 108 , 200 )

	-- local xuexi = display.newSprite(  PATH .. "xuexi.png" )
	-- self.petinfoLayer:addChild( xuexi )
	-- setAnchPos(xuexi , 128 , 213 )
	--local conf = config_data.star + 10 * DATA_Bag:get("pet", id, "stage")

	
	local skills_stars = {
		[1] = 1,
		[2] = 3,
		[3] = 4,
		[4] = 2,
		[5] = 4,
		[6] = 5,
	}

	--local skill_lv  = math.floor(pet_data.lv / 10) + 1
	local skill_lv  = 1
	--local pet_skills = pet_data.skill
	local pet_skills = {config_data.skill_k}
	local x, y = 190, 300
	--主动技能和被动技能
	-- for i = 1, 6 do
	-- 	local num = i <= 3 and i or i - 3 
	-- 	local kind = i <= 3 and "a" or "p"
	-- 	local temp_id = kind .. num
	-- 	local pet_skill_cid = pet_skills[temp_id]
	-- 	local lock
	-- 	local text , front
	-- 	local pet_skill_config

	-- 	if not isset(pet_skills , temp_id) then
	-- 		lock = true
	-- 	else
	-- 		pet_skill_config = getConfig("petskill" , pet_skill_cid .. "" )
	-- 		front = getImageByType(pet_skill_cid , "s")
	-- 		text = { { pet_skill_config.name , 14 , ccc3(0x2c, 0, 0) , ccp(0, -45)} }
	-- 	end

	-- 	local skill = KNBtn:new(SCENECOMMON, {lock and "skill_frame4.png" or "skill_frame2.png"}, x, y, {
	-- 		text = text,
	-- 		scale = true,
	-- 		front = front,
	-- 		callback = function()
	-- 			if lock then
	-- 				KNMsg.getInstance():flashShow("需" .. skills_stars[i] .. "星及以上幻兽才能开启此技能位")
	-- 				return false
	-- 			end

	-- 			local skill_data = pet_skill_config
	-- 			skill_data["cid"] = pet_skill_cid
	-- 			skill_data["id"] = id
	-- 			skill_data["stage"] = pet_data.stage

	-- 			pushScene("detail" , {
	-- 				detail = "petskill",
	-- 				data = skill_data,
	-- 				pet_data = pet_data,
	-- 			})
	-- 		end
	-- 	})	
	-- 	self.petinfoLayer:addChild(skill:getLayer())

	-- 	-- 技能等级
	-- 	if not lock then
	-- 		local lv_bg_png = "skill_lv_bg.png"
	-- 		local lv_bg_x = x + 49
	-- 		local lv_bg_y = y + 52

	-- 		local lv_bg = display.newSprite(COMMONPATH .. lv_bg_png)
	-- 		setAnchPos(lv_bg , lv_bg_x , lv_bg_y)
	-- 		self.petinfoLayer:addChild(lv_bg , 10)

	-- 		local lv = CCLabelTTF:create(skill_lv , FONT , 16)
	-- 		lv:setColor( ccc3( 0xff , 0xff , 0xff ) )
	-- 		setAnchPos(lv , lv_bg_x + 11 , lv_bg_y + 2 , 0.5)
	-- 		self.petinfoLayer:addChild(lv , 11)
	-- 	end

		
	-- 	x = x + skill:getWidth() * 1.4
	-- 	if i % 3 == 0 then
	-- 		x = 190
	-- 		y = 205
	-- 	end
	-- end


	-- 操作按钮
	if DATA_Bag:count("pet") > 0 then
		-- local btn_img = {"btn_bg_red.png", "btn_bg_red_pre.png"}
		-- local btn_front = COMMONPATH .. "uppet.png" 
		-- local stage = DATA_Bag:get("pet", id, "stage")
		-- local btn = KNBtn:new(COMMONPATH , btn_img , 130 , 135 , {
		-- 	front = btn_front,
		-- 	callback = function()
		-- 		KNMsg.getInstance():flashShow("进化功能暂未实现，敬请期待")
		-- 		return 
		-- 		-- if config_data["star"] < 3 then
		-- 		-- 	KNMsg.getInstance():flashShow("只有三星以上幻兽能进化")
		-- 		-- 	return false
		-- 		-- elseif not isset(Config_PetStage , tostring(stage + 1)) then
		-- 		-- 	KNMsg.getInstance():flashShow("幻兽已进化到最高阶")
		-- 		-- 	return false
		-- 		-- end
		-- 		-- switchScene("petupdata",self.iconGroup:getId())
		-- 	end
		-- })	
		-- self.petinfoLayer:addChild(btn:getLayer())

		-- 新手引导
		if KNGuide:getStep() == 1201 then KNGuide:show( btn:getLayer() ) end


		-- 出战
		-- local btn_img = {"btn_bg_red.png","btn_bg_red_pre.png"}
		-- local btn_front = COMMONPATH .. "fight.png"
		-- if DATA_Pet:getFighting() == id then
		-- 	btn_img = {"btn_bg_red2.png"}
		-- 	btn_front = COMMONPATH .. "fight_grey.png"
		-- end
		-- local btn = KNBtn:new(COMMONPATH , btn_img , 280 , 135 , {
		-- 	scale = true,
		-- 	front = btn_front,
		-- 	callback = function() 
		-- 		KNMsg.getInstance():flashShow("出战暂未实现，敬请期待")
		-- 		return
		-- 		-- if DATA_Pet:getFighting() == id then
		-- 		-- 	KNMsg.getInstance():flashShow("该幻兽已出战")
		-- 		-- 	return
		-- 		-- end

		-- 		-- HTTP:call("pet" , "seton" , {
		-- 		-- 	id = id
		-- 		-- } , {
		-- 		-- 	success_callback = function()
		-- 		-- 		switchScene("pet")
		-- 		-- 	end
		-- 		-- })
		-- 	end
		-- })	
		-- self.petinfoLayer:addChild(btn:getLayer())
		
		--卸下（退却）
		if self.s3 and self.showType == "select" then
			local btn_img = {"btn_bg_red.png", "btn_bg_red_pre.png"}
			local btn_front = PATH .. "btn_remove.png" 
			local btnRemove = KNBtn:new(COMMONPATH , btn_img , 130 , 115 , {
				front = btn_front,
				callback = function()
					-- if config_data["star"] < 3 then
					-- 	KNMsg.getInstance():flashShow("只有三星以上幻兽能进化")
					-- 	return false
					-- elseif not isset(Config_PetStage , tostring(stage + 1)) then
					-- 	KNMsg.getInstance():flashShow("幻兽已进化到最高阶")
					-- 	return false
					-- end
					-- switchScene("petupdata",self.iconGroup:getId())
				end
			})	
		end
			
		--封神(切换)
		local btn_img = {"btn_bg_red.png","btn_bg_red_pre.png"}
		if self.showType == "select" then
			btn_front = PATH .. "btn_add.png"
		else
			btn_front = PATH .. "refresh_skill.png"
			
		end
		
		
		local btnFengshen = KNBtn:new(COMMONPATH , btn_img , 280 , 115 , {
			scale = true,
			front = btn_front,
			callback = function() 
				
				-- HTTP:call("hero_changegods" , {
				-- 	godid = pet_data.id,eye=self.heroData.eye
				-- } , {
				-- 	success_callback = function()
				-- 		--switchScene("pet")
				-- 		popScene()
				-- 	end
				-- })
				--假设切换成功
				local heros = DATA_Formation:get_OnList()
				local tempEquipData = heros[self.heroData.eye]
				tempEquipData.s3=pet_data.id
				local heros = DATA_Formation:get_OnList()
				local tempEquipData = heros[self.heroData.eye]
				dump(tempEquipData)
				popScene()
			end
		})	
		self.petinfoLayer:addChild(btnFengshen:getLayer())

		-- 新手引导
		if KNGuide:getStep() == 401 then KNGuide:show( btn:getLayer() ) end
	end

--
--
--
--	local wearEquipCell = requires(IMG_PATH , "GameLuaScript/Scene/hero/wearEquipCell")
--	-- 天生技能
--	local skillConfig = requires(IMG_PATH , "GameLuaScript/Config/Skill")
--	local tempData = {
--		cid = pet_data["skill_k"],
--		id = 0,
--		lv = pet_data["skill_lv"],
--	}
--	local equipSeatCell = wearEquipCell:new( 
--		360 ,
--		505 , 
--		tempData , 
--		function()
--			--详情
--			local skill_data = getConfig("petskill" , pet_data["skill_k"])
--			skill_data["cid"] = pet_data["skill_k"]
--			skill_data["lv"] = pet_data["skill_lv"]
--
--			pushScene("detail" , {
--				detail = "skill",
--				data = skill_data,
--				skillSeat = 1,
--				defaultSkill = true,
--			})
--		end ,
--		layer
--	)
--	self.petinfoLayer:addChild( equipSeatCell:getLayer() )
--
--	local skillConfig = requires(IMG_PATH, "GameLuaScript/Config/PetSkill")[ tempData.cid.."" ][tempData.lv..""]
--	--生成技能名称
--	local skill_name = CCLabelTTF:create(skillConfig.name , FONT , 18)
--	skill_name:setColor( ccc3( 0x2c , 0x00 , 0x00 ) )
--	setAnchPos(skill_name , 393 , 482 , 0.5)
--
--	self.petinfoLayer:addChild( skill_name )
--	
--
--	
--	local skill_opened = checkOpened("skill")
--	for i = 1, 2 do
--		local tempData = petSkillInfo["s"..( i + 1 ) ] or "技能"
--		--技能位
--		local equipSeatCell
--		local skillX = 335 - 105 * (i - 1)
--		local equipSeatCell = wearEquipCell:new( 
--					skillX ,
--					200 , 
--					tempData , 
--					function()
--						-- 判断等级开放
--						local check_result = checkOpened("skill")
--						if check_result ~= true then
--							KNMsg:getInstance():flashShow(check_result)
--							return
--						end
--									
--						if type( tempData ) ~= "string" then
--							--详情
--							local DATA = DATA_Bag
--							local type = "skill"
--							local detail
--							if DATA:haveData(tempData["id"] , type) then
--								pushScene("detail" , {
--									detail = "skill",
--									id = tempData["id"],
--									petID = id , 
--									skillSeat = "s".. ( i + 1 ).. "" , 
--								})
--							else
--								HTTP:call(type , "get" , {
--									id = tempData["id"]
--								} , {
--									success_callback = function()
--										pushScene("detail" , {
--											detail = "skill",
--											id = tempData["id"],
--											petID = id , 
--											skillSeat = "s".. ( i + 1 ).. "" , 
--										})
--									end
--								})
--							end
--						else
--							--列表
--							local list
--							list = SelectList:new("skill",self.viewLayer,display.newSprite(COMMONPATH.."title/skill_text.png"),{ btn_opt = "ok.png",target = true, equipType = "petskill" , seatID = i + 1 ,
--									y = 85 ,
--									showTitle = true , 
--									optCallback = function()
--										list:destroy()
--										local targetId = list:getCurItem():getId()
--										--请求换更换宠物技能
--										HTTP:call("skill" , "petskill_dress" , {
--											id = id ,
--											skill_id = targetId ,
--											pos = "s"..( i + 1 )
--										} , {
--											success_callback = function()
--												--刷新数据
--												switchScene("pet")
--											end
--										})
--									end
--									})
--							self.baseLayer:addChild(list:getLayer() , 2)
--						end
--					end ,
--					layer,
--					skill_opened
--				)
--		self.petinfoLayer:addChild(equipSeatCell:getLayer())
--		
--		if tempData.cid  then
--			local skillConfig = requires(IMG_PATH, "GameLuaScript/Config/PetSkill")[ tempData.cid.."" ][tempData.lv..""]
--			--生成技能名称
--			local skill_name = CCLabelTTF:create(skillConfig.name , FONT , 18)
--			skill_name:setColor( ccc3( 0x2c , 0x00 , 0x00 ) )
--			setAnchPos(skill_name , 393 , skillX - 23 , 0.5)
--
--			self.petinfoLayer:addChild( skill_name )
--		end
--	end
	
	self.layer:addChild(self.petinfoLayer)
end
function PetLayer:updateHero( petid )
	testlog("PetLayer updateHero by petid",petid,self.showType,self.spHero)
	if self.showType ~= "select" and self.spHero then
		self.spHero:getLayer():setVisible(false)
		local heros = DATA_Formation:get_OnList()
		local pet_data = DATA_Bag:get("pet", petid)

		for k,v in pairs(heros) do
			if v.s3 == petid then
				local hero_data = DATA_Bag:get("general" , v.gid )
				local frontPath = IMG_PATH .. "image/hero/"..getImageByType(hero_data.cid,"s",true)
				self.spHero:setFront(frontPath)			
				self.spHero:getLayer():setVisible(true)
			end
		end
		

	end
end
function PetLayer:createHero(  )
	--local heroPath = COMMONPATH.."info_pre.png"
	local heroPath = IMG_PATH.."image/hero/s_general1101.png"
	
			--print("cid",cid,getImageByType(cid , "s" , true))
	--logo_bg = display.newSprite(COMMONPATH .. "small_photo_bg.png")
	--setAnchPos(logo_bg , init_x , _init_y - (i - 1) * margin_y )
			
	local tipFlag --= self:coutUpTip( hero["gid"] )
	--local btn = KNBtn:new(IMG_PATH .. "image/hero" , {getImageByType(cid , "s" , true)} ,
	local btn = KNBtn:new("" , {heroPath} ,
	 34 , display.height-201 , {
		other = ( tipFlag and { COMMONPATH .. "up_flag.png" , 40 , 0 } or nil )  ,
		id = i,
		scale = true,
		callback = function()
			-- --self.scroll:setIndex(i)
			-- local index = 0
			-- local item
			-- for k,v in pairs(heros) do
			-- 	index = index + 1
			-- 	if k==i then
			-- 		v.scrollIndex = index
			-- 		v.key = k
			-- 		item = v
			-- 	end
			-- end
			-- self.scroll:setIndex(item.scrollIndex,false,10)
			-- self:showOneHero( item.key )
		end
	})
	btn:getLayer():setScale(1.10)
	local frontPath
	if self.showType~="select" then 
		frontPath = ""
		btn:getLayer():setVisible(false)
	else
		frontPath = IMG_PATH .. "image/hero/"..getImageByType(self.heroData.cid , "s" , true)
		btn:getLayer():setVisible(true)
	end
	btn:setFront(frontPath)

	return btn
end
function PetLayer:createProps()
	local props = display.newLayer()
	local path = PATH	
	--宠物按钮的操作，


	-- 孵化（召唤）
	local btn_img = {"hatch.png"}
	if checkOpened("hatch") ~= true then
		btn_img = {"hatch_grey.png"}
	end
	local btn = KNBtn:new(PATH , btn_img , 15 , display.height - 220 , {
		callback = function()
			-- -- 判断等级开
			-- local check_result = checkOpened("hatch")
			-- if check_result ~= true then
			-- 	KNMsg:getInstance():flashShow(check_result)
			-- 	return
			-- end

			-- switchScene("incubation")
		end
	})
	--props:addChild(btn:getLayer())

	self.hatch_btn = btn

	return props
end
return PetLayer
