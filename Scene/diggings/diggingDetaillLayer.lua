local DiggingDetailLayer = {}

function DiggingsLayer:detailInfo(index, pos)
	local layer = display.newLayer()
	local mask
	index = index or 1
	
	local bg = display.newSprite(IMG_PATH.."image/scene/mission/wipe_bg.png")
	setAnchPos(bg, 240, 425, 0.5, 0.5)
	layer:addChild(bg)
	
	bg = display.newSprite(IMG_PATH.."image/scene/mission/title_bg.png")
	setAnchPos(bg, 250, 570, 0.5)
	layer:addChild(bg)
	
	bg = display.newSprite(PATH.."detail.png")
	setAnchPos(bg, 250, 570, 0.5)
	layer:addChild(bg)
	
	bg = display.newSprite(PATH..index.."_small.png")
	setAnchPos(bg, 240, 440, 0.5)
	layer:addChild(bg)
	
	if self.detail[index]["list"][pos].guard_uid  ~= 0 then
		local icon = display.newSprite(PATH.."player_pro.png")
		setAnchPos(icon, 310, 530)
		layer:addChild(icon)
	end
	
	bg = display.newSprite(PATH.."produce_text.png")
	setAnchPos(bg, 300, 560)
	layer:addChild(bg)
	
	layer:addChild(createLabel({str = silverCfg[index][self.data.last_lv == 0 and DATA_User:get("lv") or self.data.last_lv]["num"].."银/分 ", size = 18, color = ccc3(0x2c, 0, 0), x = 350, y = 560, width = 400}))
	
	
	--详情信息
	bg = display.newSprite(PATH.."cur_have.png")
	setAnchPos(bg, 130, 430)
	layer:addChild(bg)
	
	layer:addChild(createLabel({str = self.detail[index]["list"][pos].name.." Lv"..self.detail[index]["list"][pos].lv, size = 20, color = ccc3(0x2c, 0, 0), x = 230, y = 430, width = 400}))
	
	bg = display.newSprite(PATH.."cur_have_time.png")
	setAnchPos(bg, 130, 400)
	layer:addChild(bg)
	
	local haveTime = createLabel({str = timeConvert(self.detail[index]["list"][pos].sec_count), size = 20, color = ccc3(0x2c, 0, 0), x = 230, y = 400, width = 400})
	if self.detail[index]["list"][pos].touid ~= 0 then
		Clock:addTimeFun("haveTime", function()
			self.detail[index]["list"][pos].sec_count =	self.detail[index]["list"][pos].sec_count + 1 
			haveTime:setString(timeConvert(self.detail[index]["list"][pos].sec_count))
		end)
	end
	layer:addChild(haveTime)
	
	bg = display.newSprite(PATH.."cur_pro.png")
	setAnchPos(bg, 130, 370)
	layer:addChild(bg)
	
	local pro_name, time 
	if self.detail[index]["list"][pos].sys_guard > 0 then
		time = self.detail[index]["list"][pos].sys_guard
	end
	
	if self.detail[index]["list"][pos].guard_man ~= "" then
		pro_name = self.detail[index]["list"][pos].guard_man
		time = 0
	else
		pro_name = "无"	
		time = 0
	end
	layer:addChild(createLabel({str = pro_name, size = 20, color = ccc3(0x2c, 0, 0), x = 240, y = 370}))
	
	bg = display.newSprite(PATH.."cur_pro_time.png")
	setAnchPos(bg, 130, 340)
	layer:addChild(bg)
	
	local restTime = createLabel({str = timeConvert(time), size = 20, color = ccc3(0x2c, 0, 0), x = 260, y = 338, width = 400})
	if self.detail[index]["list"][pos].sys_guard > 0 then
		local icon = display.newSprite(PATH.."sys_pro.png")
		setAnchPos(icon, 280, 530)
		layer:addChild(icon)
		
		Clock:addTimeFun("restTime", function()
			self.detail[index]["list"][pos].sys_guard =self.detail[index]["list"][pos].sys_guard - 1 
			if self.detail[index]["list"][pos].sys_guard > 0 then
				restTime:setString(timeConvert(self.detail[index]["list"][pos].sys_guard))
			else
				Clock:removeFunc("restTime")
				Clock:removeFunc("haveTime")
				 HTTP:call("mining", "get_list", {index}, {
					success_callback = function(data)
						self.detail[index] = data
						mask:remove()
						self:detailInfo(index, pos)
					end
					})
			end
		end)
	end
	layer:addChild(restTime)
	
	layer:addChild(createLabel({str = "抢夺成功后，此矿山会替换你当前的矿山 ", color = ccc3(255,0,0), size = 18, x = 70, y = 255, width = 400}))
	
	
	local back = KNBtn:new(COMMONPATH, {"back_img.png", "back_img_press.png"}, 40, 540, {
		priority = -131,
		callback = function()
			mask:remove()
		end
	})
	layer:addChild(back:getLayer())
	
	local btnBg, front, canLook
	if self.detail[index]["list"][pos]["touid"] == 0 then
		btnBg = {"btn_bg_red2.png"}
		front = PATH.."look_grey.png"
	else
		btnBg = {"btn_bg_red.png", "btn_bg_red_pre.png"}	
		front = PATH.."look.png"
		canLook = true
	end
	
	local look = KNBtn:new(COMMONPATH, btnBg, 70, 280, {
		priority = -131,
		front = front,
		callback = function()
			if canLook then
				HTTP:call(20007,{ touid = self.detail[index]["list"][pos].guard_uid ~= 0 and self.detail[index]["list"][pos].guard_uid or self.detail[index]["list"][pos].uid },{success_callback = 
					function()
						local otherPalyerInfo = requires("Scene.common.otherPlayerInfo")
						display.getRunningScene():addChild( otherPalyerInfo:new():getLayer() )
					end})
			end
		end
	})
	layer:addChild(look:getLayer())
	
	local fight = KNBtn:new(COMMONPATH, {"btn_bg_red.png", "btn_bg_red_pre.png"}, 260, 280, {
		priority = -131,
		front = PATH.."fight.png",
		callback = function()
			--SOCKET:getInstance("battle"):call("mining" , "execute" , "execute" , {type = index, target = self.detail[index]["list"][pos]["uid"]} )
			HTTP:call(61004,{mine_id=self.detail[index]["list"][pos].mine_id, touid = self.detail[index]["list"][pos]["touid"]},{success_callback = function ( data )
				--隐藏该界面
				mask:remove()
				self:updateSpecMyself(pos)			
			end})
		end
	})
	layer:addChild(fight:getLayer())
	
	mask = KNMask:new({item = layer})
	self.layer:addChild(mask:getLayer())	
end

return DiggingDetailLayer