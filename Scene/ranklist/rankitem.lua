local PATH = IMG_PATH.."image/scene/ranklist/"
local SCENECOMMON = IMG_PATH.."image/scene/common/"
local KNBtn = requires("Common.KNBtn")
local RankItem = { layer } 

function RankItem:new(kind,x, y, pos, parent)
	local this = {} 
	setmetatable(this,self)
	self.__index = self
	
	this.layer = display.newLayer()
	
	local bg = display.newSprite(COMMONPATH.."item_bg.png")
	setAnchPos(bg)
	this.layer:addChild(bg)
	
	this.layer:setContentSize(bg:getContentSize())
	setAnchPos(this.layer, x, y)
	
	local text,y,x
	if pos < 4 then
		text = display.newSprite(PATH..pos..".png")
		x = 390
		y = 22
	else
		text = display.strokeLabel(pos.."", 0, 0, 36, ccc3(0xed, 0x17, 0x16))
		x = 390
		y = 30
	end
	setAnchPos(text, x, y, 0.5)
	this.layer:addChild(text)
	
--	local icon = display.newSprite(SCENECOMMON.."/logo.png")
--		setAnchPos(icon,17,24)
--	this.layer:addChild(icon)
--	
--	icon = display.newSprite(SCENECOMMON.."navigation/logo_bg.png")
--	setAnchPos(icon,15,20)
--	this.layer:addChild(icon)
	local headPath = DATA_User:getHeadPath(DATA_Rank:get(kind, "list",pos))
	print("dfasdfdsf",headPath)
	local icon = KNBtn:new("" , { headPath} , 17 , 24 , {
				front = COMMONPATH.."role_frame.png",
				scale = true , 
				parent = parent,
				callback = function()
					local uid = DATA_Rank:get(kind, "list",pos, "uid")
					if not uid  then
						switchScene("userinfo")
					else
						if tonumber( uid ) ~=  tonumber( DATA_Session:get("uid") )  then
							HTTP:call(20007,{ touid = uid },{success_callback = 
								function()
									local otherPalyerInfo = requires("Scene.common.otherPlayerInfo")
									display.getRunningScene():addChild( otherPalyerInfo:new():getLayer() )
								end})
						end
					end
				end
			}):getLayer()
	this.layer:addChild(icon)
	
	
	local name = display.strokeLabel(DATA_Rank:get(kind, "list",pos, "name"), 100, 10, 20,nil, nil, nil, {
		dimensions_width = 130,
		dimensions_height = 30
	})
	setAnchPos(name, 150, 40, 0.5)

	this.layer:addChild(name)
	
	local str
	if kind == "level" then
		str = "等级:"
	elseif kind == "athletics" then
		str = "排名:"
	else
		str = "战力:"
	end
	name = display.strokeLabel(str..DATA_Rank:get(kind, "list", pos, "num"), 220, 45, 24,ccc3(0x2c,0x00,0x00))
	this.layer:addChild(name)
	
	return this

end

function RankItem:getLayer()
	return self.layer
end

function RankItem:getHeight()
	return self.layer:getContentSize().height
end

return RankItem