--[[

登录框

]]


local KNBtn = requires( "Common.KNBtn")
local KNInputText = requires("Common.KNInputText")

local M = {}

local layer
local isReg 
local loginbox_layer

local userinfo_file_path = "userinfo.txt"
if device.platform == "windows" then
	userinfo_file_path = "../userinfo.txt"
end

function M:_91_login( params )
	layer = display.newLayer()

	requires("Config.version")

	
	local bg = display.newSprite("image/scene/login/bg.png")
	setAnchPos(bg , 0 , 0)
	layer:addChild( bg )



	local version_label = CCLabelTTF:create(VERSION , FONT , 18)
	setAnchPos(version_label , 10 , 10)
	version_label:setColor( ccc3( 0x4d , 0x15 , 0x15 ) )
	layer:addChild( version_label )

	local luaj
	if INIT_FUNCTION.platform == 1 then
		luaj = require("framework.luaj")
	end
	local loginBtn
	loginBtn = KNBtn:new("image/scene/login", {"select_btn.png" , "select_btn_pre.png"} , display.cx - 100 , 70 , {
		front = "image/scene/login/login.png" , 
		callback = function()
			local className = "com.payment.commplatform.NdPlatformUtil"
			local javaMethodSig = "()Ljava/lang/String;"  -- 字符串需要加分号
			local ok, ret = luaj.callStaticMethod(className, "isLogin", args, javaMethodSig)
			if not ok then
			    print("luaj error:", ret)
			else
			    print("ret:", ret)
			end
			--DATA_Session:set({ uid = result["uid"] , sid = result["sid"] })

			-- 	LuaCall91PlatForm:getInstance():login(function(response)
			    
			--     local loginUin = response["loginUin"]
			--     local sessionId = response["sessionId"]
			--     local nickName = response["nickName"]

			-- 	if not sessionId or not loginUin then
			-- 		KNMsg.getInstance():flashShow("登录失败，请重新登录")
			-- 		return
			-- 	end

			-- 	-- 发请求
			-- 	local post_data = {
			-- 		platform = "91" ,
			-- 		_91_sid = sessionId ,
			-- 		_91_uin = loginUin,
			-- 		channel = CHANNEL_ID,
			-- 	}
			-- 	for k , v in pairs(device.infos) do
			-- 		post_data[k] = v
			-- 	end
			-- 	HTTP:call("verify" , "check" , post_data)
			-- end)
		end
	}):getLayer()
	layer:addChild(loginBtn)

	return layer
end

function M:create( params )
	if CHANNEL_ID == "91" then return M:_91_login( params ) end
	
	print("-----------------M:create-----------------------")
	isReg = params.isReg or false	--是否是注册
	
	loginbox_layer = nil

	layer = display.newLayer()
	package.loaded["Config.version"] = nil
	-- for k,v in pairs(package.loaded) do
	-- 	print(k,v)
	-- 	-- io.writefile("/Users/looffer/crababy/workspace/self/FantasyHero/fantasyhero/writablePath/loaded.txt", k .. "\n", "a+")
	-- end
	print("login box login box login box package.path:", package.path)
	local version = require("Config.version")
	print("version string",version.string())
	local ver_str = version.string()
	-- print("current version:", ver_str)	

	local bg = display.newSprite("image/scene/login/bg.png")
	bg:setPosition(display.width/2, display.height/2)
	layer:addChild( bg )

	local logo = display.newSprite("image/scene/login/logo.png")
	setAnchPos(logo,10,display.height-97)
	layer:addChild(logo)

	local version_label = CCLabelTTF:create(ver_str , FONT , 18)
	setAnchPos(version_label , 10 , 10)
	version_label:setColor( ccc3( 0x4d , 0x15 , 0x15 ) )
	layer:addChild( version_label )

	if CHANNEL_ID == "test" then
		local test_label = CCLabelTTF:create("骚年，你运行的是测试版本，有问题请重新安装" , FONT , 18)
		setAnchPos(test_label , 10 , display.height - 30)
		test_label:setColor( ccc3( 0x4d , 0x15 , 0x15 ) )
		layer:addChild( test_label )
	end
	
	local userinfo_file_path = "userinfo.txt"
	if device.platform == "windows" then
		userinfo_file_path = "../userinfo.txt"
	end
	local username = KNFileManager.readfile(userinfo_file_path , "name" , "=")
	local password = KNFileManager.readfile(userinfo_file_path , "pwd" , "=")
	
	if username and username ~= "" then
		M:showLoginBox()--显示登录按钮
	else
		M:gameSelect()--登陆选择
	end

	return layer
end

local function test_power()
	-- 发请求
	local post_data = {
		user = "hyd001" ,
		pwd = "123456" ,
		channel = CHANNEL_ID,
	}

	HTTP:call(10002 , post_data , {
			success_callback = function(d)
			   -- 进行下一步
			   -- 发请求
				local post_data = {
					serverid = 1 ,
					channel = CHANNEL_ID,
				}
				
				
				HTTP:call(10003 ,  post_data , {
					success_callback = function()
						CONFIG_HOST = "http://127.0.0.1:9998"
						-- 往游戏服务器发送第一个请求
						local post_data = {
							channel = CHANNEL_ID,
						}
						HTTP:call(20001 ,  post_data , {
							test_power()
						})
					end
				})
			end,
			error_callback = function(d)
			end
		})

end
 function M:isRightEmail(str)
     if string.len(str or "") < 6 then return false end
     local b,e = string.find(str or "", '@')
     local bstr = ""
     local estr = ""
     if b then
         bstr = string.sub(str, 1, b-1)
         estr = string.sub(str, e+1, -1)
     else
         return false
     end
 
     -- check the string before '@'
     local p1,p2 = string.find(bstr, "[%w_]+")
     if (p1 ~= 1) or (p2 ~= string.len(bstr)) then return false end
     
     -- check the string after '@'
     if string.find(estr, "^[%.]+") then return false end
     if string.find(estr, "%.[%.]+") then return false end
     if string.find(estr, "@") then return false end
     if string.find(estr, "[%.]+$") then return false end
 
     _,count = string.gsub(estr, "%.", "")
     if (count < 1 ) or (count > 3) then
         return false
     end
 
     return true
 end
function M:showLoginBox()
	local box_bg , username_textfield , effBg , 
		password_textfield ,  login_btn , send_btn , backBtn ,registerBtn 

	

	--[[登录按钮]]
	local login_callback = function()
		
		-- if test_power then
		-- 	test_power()
		-- 	return
		-- end

		audio.stopMusic( false )
		
		echoLog("Login" , "Click Login Button11")
		
		local open_id = username_textfield:getText()
		open_id = string.trim( open_id )

		local password = password_textfield:getText()
		password = string.trim( password )
		--************************************************
		if CHANNEL_ID=="xx" then
			open_id="abcd"
			password="123456789"
			KNFileManager.updatafile(userinfo_file_path , "name" , "=" , open_id)
			KNFileManager.updatafile(userinfo_file_path , "pwd" , "=" , password)
			local servers={}
			local result={servers=servers,default_server_id=1}
			table.insert(servers,{id=1,name="火影村",status=2})
			switchScene("servers",result)
			return
		end
		--*************************************************
		if open_id == "" then
			-- 错误提示
			KNMsg:getInstance():flashShow("请输入ID")
			return
		end

		if password == "" then
			-- 错误提示
			KNMsg:getInstance():flashShow("请输入密码")
			return
		end
		if self:isRightEmail(open_id)==false then
			if string.valid(open_id) == false then
				-- 错误提示
				KNMsg:getInstance():flashShow("用户名非法")
				return
			elseif string.valid(password)==false then
				KNMsg:getInstance():flashShow("密码只能是数字或者字母")
				return
			end
		elseif string.valid(password) == false then
			KNMsg:getInstance():flashShow("密码只能是数字或者字母")
			return 
		end

		-- 发请求
		local post_data = {
			user = open_id ,
			pwd = password ,
			channel = CHANNEL_ID,
		}
		
		device.infos = {}
		-- device.infos.udid = "3fb1c779-de35-32ff-98d8-1a30e19bf59a"
		-- device.infos.mac = "BD:5E:16:AF:57:F0"
		-- device.infos.device = "GT-N7000"
		-- device.infos.netWorkName = "T-Mobile"
		-- device.infos.msi = "998707864986066"
		-- device.infos.memory = "740"
		-- device.infos.netWorkType = "0"
		-- device.infos.netWork = "WIFI"
		-- device.infos.display = "1280x720"
		-- device.infos.version = "2.3.4"
		
		-- for k , v in pairs(device.infos) do
		-- 	post_data[k] = v
		-- end
		local mid = 10001
		if isReg == false then mid = 10002 end
		HTTP:call(mid , post_data , {
			success_callback = function(d)
				print("---------------------login success!")
				KNFileManager.updatafile(userinfo_file_path , "name" , "=" , open_id)
				KNFileManager.updatafile(userinfo_file_path , "pwd" , "=" , password)
			end,
			error_callback = function(d)
				switchScene("login" , nil , function()
					KNMsg.getInstance():flashShow("[" .. d.code .. "]" .. d.msg)
				end)
			end
		})
	end


	if loginbox_layer ~= nil then
		loginbox_layer:removeSelf()
	end

	loginbox_layer = display.newLayer()

	-- 输入框背景
	box_bg = display.newSprite(IMG_PATH.."image/scene/login/box_bg.png", display.cx, 165)
	loginbox_layer:addChild(box_bg)
	local username = KNFileManager.readfile(userinfo_file_path , "name" , "=")
	local password = KNFileManager.readfile(userinfo_file_path , "pwd" , "=")
	print('---------------username_textfield-----------------')
	-- 用户名输入框
	username_textfield = ui.newEditBox({
        image = IMG_PATH.."image/common/editbox.png",
        size = CCSize(170, 35),
        x = display.cx,
        y = 185,
        listener = function(event, editbox)
            if event == "began" then
                printf("editBox1 event began : text = %s", editbox:getText())
            elseif event == "ended" then
                printf("editBox1 event ended : %s", editbox:getText())
            elseif event == "return" then
                printf("editBox1 event return : %s", editbox:getText())
            elseif event == "changed" then
                printf("editBox1 event changed : %s", editbox:getText())
            else
                printf("EditBox event %s", tostring(event))
            end
        end
        })
	layer:addChild(username_textfield,11)
	username_textfield:setText(( ( username and username ~= "" ) and username or nil ) )
	username_textfield:setFontColor(ccc3(0, 0, 0))

	-- 密码输入框
	password_textfield = ui.newEditBox({
        image = IMG_PATH.."image/common/editbox.png",
        size = CCSize(170, 35),
        x = display.cx,
        y = 145,
        listener = function(event, editbox)
            if event == "began" then
                printf("editBox1 event began : text = %s", editbox:getText())
            elseif event == "ended" then
                printf("editBox1 event ended : %s", editbox:getText())
            elseif event == "return" then
                printf("editBox1 event return : %s", editbox:getText())
            elseif event == "changed" then
                printf("editBox1 event changed : %s", editbox:getText())
            else
                printf("EditBox event %s", tostring(event))
            end
        end
        })
	password_textfield:setInputFlag(kEditBoxInputFlagPassword)
	layer:addChild(password_textfield,11)
	password_textfield:setText((( password and password ~= "" ) and password or nil))
	password_textfield:setFontColor(ccc3(0, 0, 0))
	
	-- 开始按钮
	--send_btn = KNBtn:new("image/scene/login" , {"send_btn.png" , "send_btn_pre.png"} , 390 , 455 , {
	--	callback = 
	--	function()
	--		isReg = false
	--		login_callback()
	--	end
	--})
	--loginbox_layer:addChild(send_btn:getLayer())
	
	-- 注册按钮
	registerBtn = KNBtn:new(IMG_PATH.."image/scene/login" , {"register.png" , "register_pre.png"} , display.cx + 120 , 125 , {
		callback =
		function()
			isReg = true
			--switchScene("fixed")
			login_callback()
		end
	})
	loginbox_layer:addChild(registerBtn:getLayer())
	
	-- 返回按钮
	--[[backBtn = KNBtn:new("image/scene/login" , {"back.png" , "back_pre.png"} , 10 , 460 , {
		callback = 
		function()
			--changeStauts()
		end
	}):getLayer()
	loginbox_layer:addChild( backBtn )
	--]]

	-- 登录按钮
	login_btn = KNButton:new("login" , 0 , display.cx , 20 ,
		function()
			isReg = false
			login_callback()
		end 
	 , 1 , { noDisable = true })
	setAnchPos(login_btn , display.cx - 120 , 20 , 0.5)
	loginbox_layer:addChild( login_btn )
	
	setAnchPos( loginbox_layer , 0 , -display.cy )
	transition.moveTo(loginbox_layer , {time = 0.5 , y = 0 , easing = "BACKOUT" })
	layer:addChild( loginbox_layer , 10)
end
--登陆选择
function M:gameSelect()
	local loginBtn , fastGameBtn
	loginBtn = KNBtn:new("image/scene/login", {"select_btn.png" , "select_btn_pre.png"} , 42 , 70 , {
		front = "image/scene/login/login.png" ,
		callback = function()
			-- fastGameBtn:removeSelf()
			fastGameBtn:removeSelf()
			fastGameBtn = nil

			-- loginBtn:removeSelf()
			loginBtn:removeSelf()
			loginBtn = nil

			M:showLoginBox()
		end
	}):getLayer()
	layer:addChild(loginBtn)
	
	fastGameBtn = KNBtn:new("image/scene/login", {"select_btn.png" , "select_btn_pre.png"} , 264 , 70 , {
		front = "image/scene/login/fast_game.png" ,
		callback = function()
			if true then
				KNMsg.getInstance():flashShow("不可快速游戏，请使用帐号登录")
				return
			end
			local open_id = ""
			local password = ""

			if device.infos["msi"] and type(device.infos["msi"]) == "string" and string.len(device.infos["msi"]) >= 10 then
				open_id = device.infos["msi"]
			elseif device.infos["mac"] and type(device.infos["mac"]) == "string" and string.len(device.infos["mac"]) >= 10 then
				open_id = string.gsub(device.infos["mac"] , ":" , "")
			end

			if string.len(open_id) < 10 then
				KNMsg.getInstance():flashShow("不可快速游戏，请使用帐号登录")
				return
			end

			password = tostring( math.random(10000 , 999999) )
			local fast_login_sign = nil
			if CMD5 ~= nil then
				fast_login_sign = MD5(open_id .. CHANNEL_ID)
			end

			-- 发请求
			local post_data = {
				open_id = open_id ,
				pwd = password ,
				channel = CHANNEL_ID,
				fast_login_sign = fast_login_sign,
				reg = 2 ,
			}
			for k , v in pairs(device.infos) do
				post_data[k] = v
			end
			
			HTTP:call("verify" , "check" , post_data , {
				success_callback = function(d)
					KNFileManager.updatafile(userinfo_file_path , "name" , "=" , open_id)
					if d["result"] and d["result"]["pwd"] then
						password = d["result"]["pwd"]
					end
					KNFileManager.updatafile(userinfo_file_path , "pwd" , "=" , password)
				end
			})
		end
	}):getLayer()
	layer:addChild(fastGameBtn)

end


return M
