--[[

登录场景

]]


collectgarbage("setpause" , 100)
collectgarbage("setstepmul" , 5000)


-- [[ 包含各种 Layer ]]
local loginbox_layer = requires("Scene.login.loginbox")



local M = {}

function M:create( params )
	requires("Common.KNClock"):removeTimeFun()
	params = params or {}
	--[[数据初始化]]
	requires( "Network.commonActions"):init()
	local rec, isenable
	local scene = display.newScene("login")
	if INIT_FUNCTION.platform == 1 then 		-- Android
		-- 读取JNI，查看是否播放音乐
		-- local className = "com.payment.commplatform.NdPlatformUtil"
		-- local javaMethodSig = "()I"  -- 字符串需要加分号
		-- rec, isenable = luaj.callStaticMethod(className, "isMusicEnabled", {}, javaMethodSig)
		-- if isenable ==  1 then
		-- 	KNFileManager.updatafile("savefile.txt" , "sound" , "=", "1")
		-- 	KNFileManager.updatafile("savefile.txt" , "audio" , "=", "1")
		-- else
		-- 	KNFileManager.updatafile("savefile.txt" , "sound" , "=", "0")
		-- 	KNFileManager.updatafile("savefile.txt" , "audio" , "=", "0")
		-- end
	end
	
	local playmusic = KNFileManager.readfile("savefile.txt" , "sound" , "=")
	if  playmusic==nil or playmusic== "1" then
		i7PlayMusic( IMG_PATH .. "sound/login.mp3")
	else
		audio.stopMusic( false )
		isMusicAllowed = false
		-- audio.disable()
	end
	--CHECK = os.time()..'check'
	CCLuaLog("login M:create params music:"..playmusic)
	---------------插入layer---------------------
	scene:addChild( loginbox_layer:create( params ) )
	---------------------------------------------

	return scene
end

return M
