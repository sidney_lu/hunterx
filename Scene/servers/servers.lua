--[[

选区

]]


local KNBtn = requires( "Common.KNBtn")
local KNMask = requires("Common.KNMask")
local KNRadioGroup = requires("Common.KNRadioGroup")
local M = {}

local select_server
local PATH = IMG_PATH .. "image/scene/servers/"
function M:create( params )
	local default_server_id = params.default_server_id
	local servers = params.servers

	local default_server , tempDefault_server
	local server_name_label
	local curState
	local servercount = table.nums(servers)
	-- 获取默认选区
	for k,v in pairs(servers) do
		if v.id == default_server_id then
			default_server = v
			break
		end
	end
	tempDefault_server = default_server
	select_server = default_server

	local layer = display.newLayer()
	--背景
	layer:addChild( display.newSprite(IMG_PATH .. "image/scene/login/bg.png" , display.cx , display.cy ) )
	
	local logo =display.newSprite(IMG_PATH .. "image/scene/login/logo.png")
	setAnchPos(logo,10,display.height-10 - 87)
	layer:addChild(logo)

	local function setCurState()
		if default_server == nil then return end
		if curState then
			-- curState:removeSelf()
			curState:removeSelf()
			curState = nil
		end
		curState = display.newSprite( PATH .. default_server.status .. ".png" )
		setAnchPos(curState , display.cx + 30 , 138)
		layer:addChild( curState )
	end
	
	
	local function createList()
		if servercount == 0 then return end
		local listLayer = display.newLayer()
		local mask
		local scroll = KNScrollView:new( 30 , display.cy - 110 , 420 , 400 , 5 , false , 0 , { priority = -133 } )
		local group = KNRadioGroup:new()
		-- 服务器列表背景
		listLayer:addChild( display.newScale9Sprite( PATH .. "server_bg.png" , display.cx , display.cy+70, CCSize(423, 500) ) )
		
		local function createTitle( name )
			local tempTitle = display.newSprite( PATH .. name .. ".png" , display.cx , 0):align(display.BOTTOM_CENTER)
			tempTitle:addChild( display.newSprite( PATH .. "line.png" , tempTitle:getContentSize().width/2 , -5)
				:align(display.BOTTOM_CENTER))
			return tempTitle
		end

		local function createItem( itemData , itemGroup )
			return KNBtn:new( PATH , { "item_bg.png"  , "select.png" } , 0 , 0 , {
				parent = scroll ,
				priority = -132 ,
				noHide = true ,
				upSelect = true ,
				selectZOrder = 1 ,
				text = { { itemData.name , 14 , ccc3( 0xff , 0xfa , 0xd4 ) , { x = 25 , y = 0 }  , true , 20 } }  ,
				other = { PATH .. itemData.status .. ".png"  , 140 , 14 , -2 } ,
				callback = function()
					default_server = itemData
				end
			} , itemGroup or nil )
		end
		
		scroll:addChild( createTitle("recently_title") )
		
		
		local function createItemGroup( _data , isGroup )
			local lastData = _data or {}
			local num = table.nums(lastData)
			for i = 1 , math.ceil( num/2 ) do
				local itemLayer = display.newLayer()
				itemLayer:setContentSize( CCSize( 420 , 57 ) )
				for j = 1 , 2 do
					local index = ( i - 1 ) * 2 + j
					if lastData[index] then
						local curItem = createItem( lastData[index] , isGroup and group or nil )
						setAnchPos( curItem:getLayer() , ( j == 1 and 0 or 210 ), 0 )
						itemLayer:addChild( curItem:getLayer() )
						
						if default_server.id == lastData[index].id then
							group:chooseBtn( curItem , true )
						end 
					end
				end
				scroll:addChild( itemLayer , itemLayer )
			end
		end
		local lastData = {}
		lastData[1] = default_server
		
		createItemGroup( lastData , true )
		scroll:addChild( createTitle("all_title") )
		createItemGroup( servers , true )	--所有服务器列表
		
		scroll:alignCenter()
		listLayer:addChild( scroll:getLayer() )
		
		
		--确定
		listLayer:addChild( KNBtn:new(COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" } , 70 , display.cy-150 , {
					front =  COMMONPATH .. "confirm.png" ,
					priority = -142,
					scale = true,
					callback = function()
						mask:remove()
						server_name_label:setString( default_server.name )
						select_server = default_server
						tempDefault_server = default_server
						
						setCurState()
					end
				}):getLayer() )
		--取消
		listLayer:addChild( KNBtn:new(COMMONPATH , { "btn_bg_red.png" , "btn_bg_red_pre.png" } , 260 , display.cy-150 , {
					front =  COMMONPATH .. "cancel.png" ,
					priority = -142,
					scale = true,
					callback = function()
						default_server = tempDefault_server
						mask:remove()
					end
				}):getLayer() )
	
		mask = KNMask:new( { item = listLayer } )
		setAnchPos( listLayer , 0  , display.height )
		transition.moveTo( listLayer , { time = 0.5 , y = 0 , easing = "BACKOUT" })
		
		layer:addChild( mask:getLayer() )
	end

	local serverBtn = KNBtn:new( PATH , { "small.png" } , display.cx-210 , 120 , { 
		text = { { "点击选区" , 20 , ccc3( 0x29 , 0xbc , 0xce ) , { x = 316 , y = 0 }  , true , 20 } } ,
		callback = createList 
	})
	
	layer:addChild(  serverBtn:getLayer() )
	
	if default_server then
		--设置当前服务器状态
		setCurState()

		server_name_label = CCLabelTTF:create(default_server.name , FONT , 24)
		setAnchPos(server_name_label , display.cx - 150 , 140)
		server_name_label:setColor( ccc3( 0xff , 0xff , 0xff ) )
		layer:addChild( server_name_label )
	end


	local login_callback = function()
		if CHANNEL_ID=="xx" then
			switchScene("home")
			return
		end
		if servercount == 0 then return end
		-- 获取下发的登录服务器
		CONFIG_HOST = default_server.host
		CONFIG_SOCKET_HOST = default_server.socket
		CONFIG_SOCKET_PORT = default_server.port
		if default_server.payurl ~= nil and default_server.payurl ~= "" then
			CONFIG_PAY_URL = default_server.payurl
		end

		-- 发请求
		local post_data = {
			serverid = select_server.id ,
			channel = CHANNEL_ID,
		}
		-- for k , v in pairs(device.infos) do
		-- 	post_data[k] = v
		-- end
		
		HTTP:call(10003 ,  post_data , {
			success_callback = function()
				CONFIG_HOST = "http://"..select_server.ip..":"..select_server.port
				DATA_Session:set({ select_server = select_server })
				print("[http.............]"..DATA_Session:get("sid").."|uid="..DATA_Session:get("uid"))
				-- 往游戏服务器发送第一个请求
				local post_data = {
					channel = CHANNEL_ID,
				}
				HTTP:call(20001 ,  post_data , {
				})
			end
		})
	end


	-- 登录按钮
	local login_btn = KNButton:new("login" , 0 , display.cx , 20 , login_callback , 1 , { noDisable = true })
	setAnchPos(login_btn , display.cx - 120 , 20 , 0.5)
	layer:addChild( login_btn )
	

	return layer
end



return M
