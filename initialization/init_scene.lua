--[[

初始化场景

]]


collectgarbage("setpause"  ,  100)
collectgarbage("setstepmul"  ,  5000)

local initCopyLayer = require "initialization.init_copyfiles"

local M = {}
local scene
-- 初始升级界面
function M:create()
	print("--------------init_scene:create!")
	scene = CCScene:create()
	scene.name = "init"

	require("Config.channel")
	if CHANNEL_ID ~= "xx" then
		print("---1")
		local layer = CCLayer:create()

		local bg = CCSprite:create("image/scene/login/bg.png")

		bg:setPosition(INIT_FUNCTION.cx, INIT_FUNCTION.cy)
		layer:addChild( bg )

		local logo = CCSprite:create("image/scene/login/logo.png")
		logo:setPosition(10 + 223/2, display.height-87/2-10)
		layer:addChild(logo)
		scene:addChild(layer)

		local handle
		handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(handle)
			handle = nil
			M:flash()
		end , 2 , false)
	else
		print("---2")
		M:flash()
	end
	

	return scene
end

function M:flash()
	-- local interface = UpdataRes:getInstance()
	-- local platform_type = interface:get_type()
	if INIT_FUNCTION.platform == 0 then --win
		M:go()
		return
	end

	local layer = CCLayer:create()
	local bg = CCSprite:create("image/scene/login/bg.png")
	
	bg:setPosition(INIT_FUNCTION.cx, INIT_FUNCTION.cy)
	layer:addChild( bg )

	local logo = CCSprite:create("image/scene/login/logo.png")
	logo:setPosition(10 + 223/2,display.height-87/2-10)
	scene:addChild(layer)

	local scheduler = require(cc.PACKAGE_NAME .. ".scheduler")
	scheduler.performWithDelayGlobal(function()
		M:go()
	end, 1)		

	-- local handle
	-- handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
	-- 	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(handle)
	-- 	handle = nil
	-- 	M:go()
	-- end , 2 , false)
end
-- 显示升级界面
function M:go()
	print("-------------------[显示升级界面]")
	INIT_FUNCTION.update = initCopyLayer.new():addTo(scene)
	-- scene:addChild(INIT_FUNCTION.update)	
end

return M