--[[

文件初始化

]]

require("Config.channel")
require("Common.KNMsg")

local luaj
if INIT_FUNCTION.platform == 1 then
	luaj = require("framework.luaj")
end
-- local initCopyLayer = {
-- 	layer,
-- 	viewLayer,
-- 	tipsLayer
-- }

local initCopyLayer = class("initCopyLayer", function()
	return display.newNode()
end)


-- 接口
-- local interface_java = UpdataRes:getInstance()		-- android
-- local platform_type = interface_java:get_type()
local interface_oc
--if INIT_FUNCTION.platform == 2 then 				-- ios
--	interface_oc = UpdateDataOC:getInstance()
--end


local uniq_files_name = "uniq"
local update_url = ""

function initCopyLayer:ctor()
	print("-----------initCopyLayer:ctor")
	self.layer = display.newLayer():addTo(self)

	local bg = CCSprite:create("image/scene/login/bg.png")
	bg:setPosition(INIT_FUNCTION.cx, INIT_FUNCTION.cy)
	self.layer:addChild( bg )
	local logo = CCSprite:create("image/scene/login/logo.png")
	logo:setPosition(10 + 223/2 ,display.height - 87/2 -10)
	self.layer:addChild(logo)
	-- 可能需要初始化第三方SDK
	-- self:init()

	-- self:go()
	self:checkToUpdate()
end

function initCopyLayer:getLayer()
	return self.layer
end

function initCopyLayer:test()
	local base_url = "http://115.28.128.208:8088/update/version.php" --?channel=xx&channel_group=eagle&version=14041501&
	local callback = function(response_string)
		print(response_string)
	end	
	i7cc.networks:get(base_url, {channel="xx", channel_group="eagle", version=14041501}, callback)

	-- local scheduler = require(cc.PACKAGE_NAME .. ".scheduler")
	-- scheduler.performWithDelayGlobal(function()
	-- 	KNMsg.getInstance():flashShow("网络异常，检查新版本失败！")
	-- end, 2)	
end

function initCopyLayer:checkToUpdate()
	if self.viewLayer then
		self.viewLayer:removeSelf()
		self.viewLayer = nil
	end
	self.viewLayer = display.newLayer():addTo(self.layer)
	local offsetY = 50
	-- shadow
	display.newSprite("image/box.png", display.cx, offsetY+52)
		:align(display.CENTER)
		:addTo(self.viewLayer)
	-- loading bar
	display.newSprite("image/loading.png", display.cx-130, offsetY+52)
		:align(display.CENTER)
		:addTo(self.viewLayer)
		:runAction(CCRepeatForever:create( CCRotateBy:create(0.5 , 180) ))		
	-- text
	ui.newTTFLabel({
		x = display.cx+30,
		y = 100,
		text = "正在检查新版本,请稍候...",
		font = "Thonburi",
		size = 22,
		color = cc.c3b(255, 255, 255), -- 使用纯红色
		align = ui.TEXT_ALIGN_CENTER -- 文字内部居中对齐		
	}):addTo(self.viewLayer)

	---------------------------------------------------
	---------------------------------------------------
	-- local versionPath
	-- if device.platform == "ios" then
	-- 	print("[[[[[[[[ios]]]]]]]]")

	-- elseif device.platform == "android" then
	-- 	print("[[[[[[[[android]]]]]]]]")
	-- 	-- versionPath = device.writablePath .. "src." .."Config.version"
	-- 	versionPath = device.writablePath .. "src/Config/version.lua"
	-- else
	-- 	print("[[[[[[[[simulator]]]]]]]]")
	-- 	-- versionPath = device.writablePath .. "writablePath.src." .."Config.version"
	-- 	versionPath = device.writablePath .. "writablePath/src/Config/version.lua"
	-- end	

	-- local version
	-- local pathlist = cc.FileUtils:getInstance():getSearchPaths()
	-- for i,v in ipairs(pathlist) do
	-- 	print(i,v)
	-- 	versionPath = v .. "Config/version.lua"
	-- 	if io.exists(versionPath) then
	-- 		print("---exist: yes")
	-- 		print("versionPath:", versionPath)
	-- 		version = loadfile(versionPath)
	-- 		break
	-- 	end	
	-- end
	
	-- if io.exists(versionPath) then
	-- 	print("---exist: yes")
	-- 	version = require(versionPath)
	-- else
	-- 	print("---exist: no")
	-- 	local pathlist = cc.FileUtils:getInstance():getSearchPaths()
	-- 	for i,v in ipairs(pathlist) do
	-- 		print(i,v)
	-- 	end
	-- 	version = require("Config.version")
	-- end
	-- if not version then
	-- 	version = require("Config.version")
	-- end
	local version = require("Config.version")
	local ver_digit = version.onlyDigit()
	print("current version:", ver_digit)

	local base_url = CONFIG_UPDATA_URL .. "/version.php" --?channel=xx&channel_group=eagle&version=14041501&
	local callback = function(response)
		print(response)
		if type(response) ~= "string" or response == "" then
			print("---network error!")
			KNMsg.getInstance():flashShow("网络异常，检查新版本失败！")
			self:go()
			return			
		end		
		--[[
			version=15041101
			gamefiles/src/Config/version.lua	393		
		--]]
		local rows = i7Split(response , "\n")
		local version_data = i7Split(rows[1] , "=")
		if #version_data ~= 2 then
			print("# Get Version Error..")
			KNMsg.getInstance():flashShow("网络异常，检查新版本失败！")
			self:go()
			return
		end
		print("# New Version: " .. version_data[2]) --15041101

		if version_data[2] == "0" then --version=0
			self:go()
			return
		end		
		-- 显示升级界面
		self:updateNewVersion(version_data[2] , rows)		
	end
	--i7cc.networks:get(base_url, {channel=CHANNEL_ID, channel_group=CHANNEL_GROUP, version=ver_digit}, callback)	
	--暂时不需要升级，直接开始
	self:go()
end

function initCopyLayer:updateNewVersion(new_version , rows)
	if self.viewLayer then
		self.viewLayer:removeSelf()
		self.viewLayer = nil
	end

	self.viewLayer = display.newLayer():addTo(self.layer)
	--add tip label
	ui.newTTFLabel({
		x = display.cx,
		y = 90,
		text = "发现新版本 " .. "v" .. new_version,
		font = "Thonburi",
		size = 25,
		color = cc.c3b(255, 255, 255), 
		align = ui.TEXT_ALIGN_CENTER -- 文字内部居中对齐		
	}):addTo(self.viewLayer)
	--add progress bar
	local bg = CCSprite:create("image/scene/updata/bar_1.png")
	INIT_FUNCTION.setAnchPos(bg , 67 , 136 , 0 , 0.5)
	self.viewLayer:addChild(bg)

    local bg1 = CCSprite:create("image/scene/updata/bar_0.png")
	INIT_FUNCTION.setAnchPos(bg1 , 67 , 136 , 0 , 0.5)
	self.viewLayer:addChild(bg1)
	bg1:setTextureRect(CCRectMake(0,0,1,39))
	--add file size label
	local updata_font_max = CCLabelTTF:create( "111" , "Thonburi" , 20 )
	INIT_FUNCTION.setAnchPos(updata_font_max , 410 , 93 , 1 , 0)
	updata_font_max:setColor(ccc3(158,41,78))
	self.viewLayer:addChild(updata_font_max)

	local update_filename = CCLabelTTF:create( "222" , "Thonburi" , 20 )
	INIT_FUNCTION.setAnchPos(update_filename , 70 , 93)
	update_filename:setColor(ccc3(158,41,78))
	self.viewLayer:addChild(update_filename)

	-- 按钮
	local oy = 50
	local btn_bg = CCSprite:create("image/scene/updata/button.png")
	btn_bg:setPosition(INIT_FUNCTION.cx, oy)
	self.viewLayer:addChild(btn_bg)

	local btn_grey_bg = CCSprite:create("image/scene/updata/button_grey.png")
	btn_grey_bg:setPosition(INIT_FUNCTION.cx, oy)
	self.viewLayer:addChild(btn_grey_bg)
	btn_grey_bg:setVisible(false)
	
	local font_img = CCSprite:create("image/scene/updata/font.png")
	font_img:setPosition(INIT_FUNCTION.cx, oy)
	self.viewLayer:addChild(font_img)

	local btn_rect = CCRectMake(INIT_FUNCTION.cx-btn_bg:getContentSize().width/2 , 60-btn_bg:getContentSize().height/2 , 
		btn_bg:getContentSize().width , btn_bg:getContentSize().height)
	local btn_callback = nil
	-- 按钮是否正在执行
	local btn_progress = false	
	-- 计算总大小
	local filesize_max = 0
	for i = 2 , #rows do
		local temp = i7Split(rows[i] , "\t")
		if #temp >= 2 then
			filesize_max = filesize_max + tonumber(temp[2]) / 1024
		end
	end
	filesize_max = string.format("%.2f" , filesize_max)
	filesize_max = tonumber(filesize_max)
	updata_font_max:setString("0/" .. filesize_max .. "k")	

	-- 更新进度条
	local filesize_cur = 0
	local function updateBar(cur)
		cur = string.format("%.2f" , cur)
		cur = tonumber(cur)

		if type(cur) ~= "number" then return end

		if cur >= filesize_max then
			cur = filesize_max
		end
		
		updata_font_max:setString(cur .. "/" .. filesize_max .. "K")
		
		local width = math.floor( cur * (346 / (filesize_max)) )
		if width > 346 then width = 346 end
		if width <= 0 then width = 1 end
		bg1:setTextureRect( CCRectMake(0 , 0 , width , 39) )
	end

	--[[ 更新文件 ]]
	-- local url = "http://115.28.128.208:8088/update"
	local update_url = CONFIG_UPDATA_URL
	local function updateOneFile(url , path , name , filesize , callback)
		local show_name = string.gsub(name , "%.lua" , "")
		update_filename:setString( show_name )

		local httpGet_callback = function(response, http_code)
			-- 网络请求超时
			if http_code == -28 or type(response) ~= "string" or string.len(response) ~= tonumber(filesize) then
				if http_code == -28 then
					self:showTips("下载文件超时\n请点击按钮重试")
				else
					self:showTips("下载文件异常\n请点击按钮重试")
				end

				btn_grey_bg:setVisible(false)
				btn_bg:setVisible(true)
				btn_progress = false

				btn_callback = function()
					self:hideTips()
					-- 重新开始下载
					updateOneFile(url , path , name , filesize , callback)
				end
				return
			end
-- 
			-- 有一些特殊文件，不更新
			if name == "uniq.lua" or name == "channel.lua" then
				callback(0)
				return
			end

			-- local full_path = IMG_PATH .. path .. name --/mnt/sdcard/scripts/Scene/test.lua
			local full_path
			local writePath
			if device.platform == "ios" then
				print("[[[[[[[[ios]]]]]]]]")
				full_path = device.writablePath .. path .. name
				writePath = device.writablePath
			elseif device.platform == "android" then
				print("[[[[[[[[android]]]]]]]]")
				full_path = device.writablePath .. path .. name
				writePath = device.writablePath
			else
				print("[[[[[[[[simulator]]]]]]]]")
				full_path = device.writablePath .. "writablePath/" .. path .. name
				writePath = device.writablePath .. "writablePath/"
			end

			print("[update]full_path:", full_path)

			-- create dir
			local temp = i7Split(path , "/")	--path: scripts/Scene/
			local dir = ""
			for i = 1 , #temp do

				if temp[i] ~= "" then
					dir = dir .. temp[i] .. "/"
					local temp_fp = io.open(writePath .. dir , "r")
					if temp_fp then --check if the dir exists
					    	io.close(temp_fp)
					else --not exist, then create one
						local lfs = require("lfs")
						lfs.mkdir(writePath .. dir)					
						-- if INIT_FUNCTION.platform == 2 then
						--       UpdateDataOC:getInstance():createDir(IMG_PATH .. dir)
						-- else
						--       os.execute("mkdir \"" .. IMG_PATH .. dir .. "\"")
						-- end
					end
				end
			end
		    	-- write file
			local fp = io.open(full_path , "w")
			if fp then
				fp:write(response)
				fp:close()
			end
			callback(http_code)
		end --httpGet_callback

		print("[url:]", url)
		i7cc.networks:get(url, nil, httpGet_callback, {
				timeout = math.max( math.ceil(filesize / 1024) , 15),
				progress_callback = function(total , now)
					updateBar(filesize_cur + (now / 1024) )
				end
		})
	end --updateOneFile

	--[[ 开始更新文件 ]]
	local function beginUpdateFile()
		local index = 2 	-- 从第二个文件开始
		local version_path
		local version_url
		local version_file_size

		-- version=14122903
		-- gamefiles/scripts/Scene/test.lua	13
		-- gamefiles/scripts/Config/version.lua	393
		-- gamefiles/scripts/Scene/battle/scene.lua	1777
		-- gamefiles/scripts/Scene/home/homelayer.lua	18989

		local function go()
			print("dfklsdjflksdjflsdjfklsf ------ go")
			print("rows[index]:", rows[index])
			if type(rows[index]) == "string" and rows[index] ~= "" then
				local url_data = i7Split(rows[index] , "\t")
-- 
				if #url_data < 2 then
					return false
				end

				local full_url = url_data[1]	--gamefiles/scripts/Scene/test.lua
				local temp = i7Split(full_url , '/') --{gamefiles, scripts, Scene, test.lua}
				local path = ""
				local name = temp[#temp] --test.lua
				for i = 2 , #temp - 1 do
					path = path .. temp[i] .. "/" --scripts/Scene/
				end

				if name == "version.lua" then
					version_path = path --scripts/Scene/
					version_url = full_url --gamefiles/scripts/Scene/test.lua
					version_file_size = url_data[2]		--13

					-- 继续执行下一个
					index = index + 1
					go()
					return true
				end
				--http://255.255.255.255:8088/update/gamefiles/src/Config/version.lua
				print("[1/update_url:]", update_url)
				updateOneFile(update_url .. "/" .. full_url, path, name, url_data[2], 
					function(http_code)
						-- print("--1:", http_code)
						if http_code ~= 200 then
							return
						end
						-- print("--2")
						-- 更新界面
						filesize_cur = filesize_cur + url_data[2] / 1024
						updateBar(filesize_cur)

						-- 继续执行下一个
						index = index + 1
						go()
					end
				)
			else
				-- 最后更新版本文件
				print("最后更新版本文件",version_path,"|",version_url,"|",version_file_size)
				if version_path and version_url and version_file_size then
					print("[2/update_url:]", update_url)
					updateOneFile(
						update_url .. "/" .. version_url, 
						version_path, 
						"version.lua", 
						version_file_size, 
						function(http_code)
							self:go()
						end
					)
				end
			end
		end --local function go
		-- 开始
		go()
	end --beginUpdateFile	

	-- 按钮点击事件
	btn_callback = function()
		print("-----btn pressed!")
		beginUpdateFile()
	end

	self.viewLayer:setTouchEnabled(true)
	local listener = cc.EventListenerTouchOneByOne:create()
	listener:setSwallowTouches(false)
	listener:registerScriptHandler(function(touch, event)
		local location = touch:getLocation()  
		local x, y = location.x, location.y
		if btn_progress == false then
			if btn_rect:containsPoint( ccp(x, y) ) then
				btn_progress = true
				btn_grey_bg:setVisible(true)
				btn_bg:setVisible(false)
				btn_callback()
			end
		end		
		return true
	end, cc.Handler.EVENT_TOUCH_BEGAN)
	self.viewLayer:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, self.viewLayer)		
end

-- 进入主界面
function initCopyLayer:go()
	print("Version Check Completed!")


	local scheduler = require(cc.PACKAGE_NAME .. ".scheduler")
	scheduler.performWithDelayGlobal(function()
		game_start()
	end, 1)		

	-- local handle
	-- handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
	-- 	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(handle)
	-- 	handle = nil
	-- 	game_start()
	-- end , 0.01 , false)
	
end



-- 初始化
function initCopyLayer:init()
	--[[	
	if CHANNEL_ID == "uc" then
		UCGameSDK:getInstance():setLogLevel(2)
		UCGameSDK:getInstance():setOrientation(0)
		UCGameSDK:getInstance():initSDK(false , 2 , 27820 , 518084 , 2126 , "安卓大话水浒" , true , true)
	end
	]]
end


-- show tips
function initCopyLayer:showTips(msg)
	if self.tipsLayer then
		self.tipsLayer:removeSelf()
		self.tipsLayer = nil
	end

	self.tipsLayer = CCSprite:create("image/common/prompt_bg.png")
	INIT_FUNCTION.setAnchPos(self.tipsLayer , INIT_FUNCTION.cx , INIT_FUNCTION.cy , 0.5)

	local textField = CCLabelTTF:create(msg , "Thonburi" , 20)
	textField:setColor(ccc3( 0xff , 0xff , 0xff ))
	textField:setAnchorPoint( ccp( 0 , 1 ) )

	if textField:getContentSize().width > 360 then
		textField:setDimensions(360 , 70)
		textField:setPosition( ccp( ( self.tipsLayer:getContentSize().width - textField:getContentSize().width ) / 2 , self.tipsLayer:getContentSize().height / 1.5 + 10 ) )
	else
		textField:setPosition( ccp( ( self.tipsLayer:getContentSize().width - textField:getContentSize().width ) / 2 , self.tipsLayer:getContentSize().height / 1.5 - 8 ) )
	end

	self.tipsLayer:addChild(textField)

	self.viewLayer:addChild(self.tipsLayer)
end

function initCopyLayer:hideTips()
	if self.tipsLayer then
		self.tipsLayer:removeSelf()
		self.tipsLayer = nil
	end
end

-- 拷贝文件到SD卡
function initCopyLayer:copyfiles(params)
	CCLuaLog("# Check Copy Files...")
	if self.viewLayer then
		self.viewLayer:removeSelf()
		self.viewLayer = nil
	end


	-- 需要拷贝文件
	self.viewLayer = CCLayer:create()

	local loading_box = CCSprite:create("image/box.png")
	INIT_FUNCTION.setAnchPos( loading_box , INIT_FUNCTION.cx , 50 , 0.5 , 0 )
	self.viewLayer:addChild( loading_box )
--	
--	local loading = CCSprite:create("image/loading.png")
--	INIT_FUNCTION.setAnchPos( loading , INIT_FUNCTION.cx - 120 , 102 , 0.5 , 0.5 )
--	self.viewLayer:addChild( loading )
--
--	local action = CCRepeatForever:create( CCRotateBy:create(0.5 , 180) )
--	loading:runAction(action)

	local label = CCLabelTTF:create("正在初始化资源,请稍候...可能需要1-2分钟" , "Thonburi" , 16)
	INIT_FUNCTION.setAnchPos( label , INIT_FUNCTION.cx , 75, 0.5 )
	label:setHorizontalAlignment(0)
	label:setColor( ccc3( 0xff , 0xff , 0xff ) )
	self.viewLayer:addChild(label )
	
	refreshCopy(0)
--
--	-- 开始部署文件(需要过一定时间，让主进程可以渲染页面)
	CCLuaLog("# Start Copy Files...")
	if INIT_FUNCTION.platform == 1 then -- android
		--interface_java:copydata("" , IMG_PATH , function(_ , _)end) 
		-- 调用 Java 方法需要的参数
		local args = {
			"",
			IMG_PATH
		}
		-- Java 类的名称
		local className = "com.eagle.UtilityJni"
		local javaMethodSig = "(Ljava/lang/String;Ljava/lang/String;)V"
		-- 调用 Java 方法
		luaj.callStaticMethod(className, "copyAssets",args,javaMethodSig)
		local handle
		handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
			local args = {}
			local sig = "()I"
			local ok, per = luaj.callStaticMethod("com.eagle.UtilityJni", "getcopyPer", args, sig)
			CCLuaLog("getcopyPer:"..per)
			refreshCopy(per)
			if per<100 then
				return
			end
	
			CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(handle)
			handle = nil
	
			--android关闭在线升级
			self:checkUpdate()
			--self:go()
		end , 0.55 , false)
	elseif INIT_FUNCTION.platform == 2 then
		--interface_oc:copyAssets()
		--local handle
		--handle = CCDirector:sharedDirector():getScheduler():scheduleScriptFunc(function()
		--	
		--	if interface_oc:get_copy() ~= 1 then
		--		return
		--	end
	    --
		--	CCLuaLog("# Finish Copy Files...")
		--	CCDirector:sharedDirector():getScheduler():unscheduleScriptEntry(handle)
		--	handle = nil
	
			self:checkUpdate()
		--end , 0.05 , false)
	end

	self.layer:addChild(self.viewLayer)

end

return initCopyLayer
