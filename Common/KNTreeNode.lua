local M = {
	node,
	nodeText,
	nodeIcon,
	level,
	children
}
function M:new( nodeText,nodeIcon )
	local this = {}
	setmetatable(this,self)
	self.__index  = self
	-- body
	self.children = {}
	self.level = 1
	self.node = cc.Node:create()

	local text = CCLabelTTF:create(nodeText,FONT,24)
	return self.node
end
function M:findNode( parent, node )
	
	for k,v in pairs(parent.children) do
		if v==node then
			return true,{v.level,k}
		end

		local ret,data = v:findNode(v,node)
		if ret == true then
			return true,data
		end
	end

	return false
end
function M:addNode( node )
	node.level = self.level + 1
	table.insert(self.children,node)
end

function M:clearChildren( ... )
	for k ,v in pairs(self.children) do
		self.children:removeSelf()
	end
	self.children = {}
end
function M:getLayer(  )
	-- body
end
return M