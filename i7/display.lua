--- create a tiled CCSpriteBatchNode
-- @param __fileName the first parameter for display.newSprite
-- @param __texture texture(plist) image filename, __fileName must be a part of the texture.
-- @param __size the tiled node size, use cc.size create it please.
-- @param __hPadding horizontal padding, it will display 1 px gap on moveing the node, set padding for fix it.
-- @param __vPadding vertical padding.
-- @return a CCSpriteBatchNode
--[[
function display.newTiledBatchNode(__fileName, __texture, __size, __hPadding, __vPadding)
	__size = __size or cc.size(display.width, display.height)
	__hPadding = __hPadding or 0
	__vPadding = __vPadding or 0
	local __sprite = display.newSprite(__fileName)
	local __sliceSize = __sprite:getContentSize()
	__sliceSize.width = __sliceSize.width - __hPadding
	__sliceSize.height = __sliceSize.height - __vPadding
	local __xRepeat = math.ceil(__size.width/__sliceSize.width)
	local __yRepeat = math.ceil(__size.height/__sliceSize.height)
	-- how maney sprites we need to fill in tiled node?
	local __capacity = __xRepeat * __yRepeat
	local __batch = display.newBatchNode(__texture, __capacity)
	local __newSize = cc.size(0,0)
	--printf("newTileNode xRepeat:%u, yRepeat:%u", __xRepeat, __yRepeat)
	for y=0,__yRepeat-1 do
		for x=0,__xRepeat-1 do
			__newSize.width = __newSize.width + __sliceSize.width
			__sprite = display.newSprite(__fileName)
			  :align(display.LEFT_BOTTOM,x*__sliceSize.width, y*__sliceSize.height)
			  :addTo(__batch)
			  --print("newTileNode:", x*__sliceSize.width, y*__sliceSize.height)
		end
		__newSize.height = __newSize.height + __sliceSize.height
	end
	__batch:setContentSize(__newSize)
	return __batch, __newSize.width, __newSize.height
end

--- create a masked sprite
function display.newMaskedSprite(__mask, __pic)
	local __mb = ccBlendFunc:new()
	__mb.src = GL_ONE
	__mb.dst = GL_ZERO

	local __pb = ccBlendFunc:new()
	__pb.src = GL_DST_ALPHA
	__pb.dst = GL_ZERO

	local __maskSprite = display.newSprite(__mask):align(display.LEFT_BOTTOM, 0, 0)
	__maskSprite:setBlendFunc(__mb)

	local __picSprite = display.newSprite(__pic):align(display.LEFT_BOTTOM, 0, 0)
	__picSprite:setBlendFunc(__pb)

	local __maskSize = __maskSprite:getContentSize()
	local __canva = CCRenderTexture:create(__maskSize.width,__maskSize.height)
	__canva:begin()
	__maskSprite:visit()
	__picSprite:visit()
	__canva:endToLua()

	local __resultSprite = CCSpriteExtend.extend(
		CCSprite:createWithTexture(
		  __canva:getSprite():getTexture()
		)):flipY(true)
	return __resultSprite
end
--]]
function display.newSpriteWithFrame(frame, x, y)
	local sprite = CCSprite:createWithSpriteFrame(frame)
	-- if sprite then CCSpriteExtend.extend(sprite) end
	if x and y then sprite:setPosition(x, y) end
	return sprite
end

--[[--

Creates multiple frames by one image.

### Example:

    -- create array of CCSpriteFrame [walk0001.png -> walk0020.png]
    local frames = display.newFramesWithImage("run.png", 6)

### Parameters:

-   string **image**

-   int **num**

-   [_optional bool **isReversed**_]

### Returns:

-   table

]]
function display.newFramesWithImage(image, num)
	if io.exists(image) then
	elseif string.find(image, "image") == 1 then
	else
		local array = string.split(image, "/")
		local str = ""
		for i = 5 ,table.getn(array) do
			if i == table.getn(array) then
				str = str ..array[i]
			else
				str = str ..array[i].."/"
			end
		end
		image = str
	end

	local frames = {}

	local texture = cc.Director:getInstance():getTextureCache():addImage( image )
	local size = texture:getContentSize()

	--每张图的宽度和高度
	if type(num) == "table" then
		local column,row = num.column,num.row
		local frameWidth = size.width / column
		local frameHeight = size.height / row
		for i=1 , row do 
			for j=1, column do
				local rect = CCRectMake(frameWidth * ( j - 1 ) , frameHeight * (i-1) , frameWidth , frameHeight)
				local frame = cc.SpriteFrame:createWithTexture(texture , rect)
				frames[#frames + 1] = frame
			end
		end
	else
		local frameWidth = size.width / num
		local frameHeight = size.height

		for i = 1 , num do
			local rect = CCRectMake(frameWidth * ( i - 1 ) , 0 , frameWidth , frameHeight)
			local frame = cc.SpriteFrame:createWithTexture(texture , rect)
			frames[#frames + 1] = frame
		end
	end

	return frames
end

function display.newAnimate(animation)
	return CCAnimate:create(animation)
end

function display.playFrames(x , y , frames , time , param)
    if type(param) ~= "table" then param = {} end

    local sprite = display.newSpriteWithFrame(frames[1] , x , y)
    local animation = display.newAnimation(frames, time)

    -- 翻转
    if param.angle ~= nil then
        sprite:setVisible(false)
        transition.rotateTo(sprite , {
            rotate = param.angle,
            time = 0.001,
            onComplete = function()
                sprite:setVisible(true)
            end
        })
    end

    if param.forever then
        transition.playAnimationForever(sprite, animation)
    else
        local animate = display.newAnimate(animation)
        
        if param.reverse then
            animate = animate:reverse()
        end

        -- sprite:runAction( animate )
        transition.execute(sprite , animate , param)
    end

    return sprite
end

function display.wrapString( string )
	-- body
end

--[[ 描边文字 strokeSize 则为不描边]]
function display.strokeLabel(str , x , y , fontSize , fontColor , strokeSize , strokeColor , param)
    param = param or {}

    local node = display.newNode()
    if fontSize == nil then fontSize = 0 end
    local label = CCLabelTTF:create(str , FONT ,  fontSize);
    label:setAnchorPoint(ccp(0 , 0))
    label:setPosition( ccp(x , y) )
    if fontColor ~= nil then
        label:setColor( fontColor )
    else
	    label:setColor( ccc3(0x2c , 0x00 , 0x00) )
    end
    --强制作设置颜色
    -- label:setColor( ccc3(0x2c , 0x00 , 0x00) )
    -- 设置文本的宽度和高度
    if param.dimensions_width and param.dimensions_height then
        --label:setDimensions(param.dimensions_width , param.dimensions_height)
        --echoLog("[display strokelabel]dimensions_width=",param.dimensions_width)
        label:setDimensions(param.dimensions_width , param.dimensions_height)
    end
    if param.size then
    	--echoLog("[display strokelabel]size=",unpack(param.size))
    	label:setContentSize(unpack(param.size))
    end
    -- 文字对齐方式
    if param.align then
    	--echoLog("[display strokelabel]align=",param.align)
        label:setHorizontalAlignment(param.align)
        if param.align==0 then
        	label:setAnchorPoint(ccp(0.5,0))
        end
    end
    -- 描边
--    local stroke = nil
--    if strokeSize ~= nil and strokeSize > 0 then
--        -- if strokeSize == nil then strokeSize = 2 end
--        if strokeColor == nil then strokeColor = ccc3(0 , 0 , 0) end
--        stroke = display.createStroke(label , strokeSize , strokeColor)
--
--        node:addChild(stroke)
--    end
    node:addChild(label)
    node:setContentSize(label:getContentSize())

    function node:getLabel()
        return label
    end

--    function node:getStroke()
--        return stroke
--    end
	--单独设置字体颜色
	function node:setColor( _color )
		label:setColor( _color )
	end
	function node:getDimensions()
		return label:getDimensions()
	end
	function node:getContentSize()
		return label:getContentSize()
	end
	function node:setString(str)
		label:setString( str )
--		node:removeChild(stroke,true)
--		stroke = display.createStroke(label , strokeSize , strokeColor)
--		node:addChild(stroke,-1)
	end

    function node:getString()
        return label:getString()
    end
	
	function node:setPosition(pos,p)
		if p then
			label:setPosition(pos,p)
		else
			label:setPosition(pos)
		end
	end
	
	function node:setAnchPos(anch)
		label:setAnchorPoint(anch)
	end
	
	function node:setDimensions(width,height)
		label:setDimensions(width,height)
	end
    return node
end

--[[描边]]
function display.createStroke(label , size , color)
    --local label_size = label:getTexture():getContentSize()
    local label_size = {width = 250,height =50}
    local x = label_size.width * 2 + size * 2
    local y = label_size.height * 2 + size * 2


    local originalPosX , originalPosY = label:getPosition()
    local originalColor = label:getColor()

    local rt = CCRenderTexture:create(x, y)
    label:setColor(color)       -- ccc3数据

    local originalBlend = label:getBlendFunc()
    -- local bf = BlendFunc:new()

    -- bf.src = 768
    -- bf.dst = 1

    label:setBlendFunc({src=768,dst=1})
    local center = ccp(label_size.width / 1 + size , label_size.height / 1 + size)

    rt:begin()
    for i = 0 , 360 , 15 do
        local _x = center.x + math.sin(math.rad(i)) * size
        local _y = center.y + math.cos(math.rad(i)) * size
        label:setPosition(ccp(_x , _y))
        label:visit()
    end
    rt:endToLua()

    label:setPosition(originalPosX , originalPosY)
    label:setColor(originalColor)
    label:setBlendFunc(originalBlend)

    --[[
    local rtX = originalPosX - size
    local rtY = originalPosY - size
    ]]

    rt:setPosition(originalPosX , originalPosY)

    return rt
end






