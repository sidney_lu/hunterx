--[[

跳跃

]]
function transition.jumpTo(target, args)
    local tx, ty = target:getPosition()
    local x = args.x or tx
    local y = args.y or ty
    local height = args.height or 30
    local jumps = args.jumps or 1
    local action = CCJumpTo:create(args.time, ccp(x, y) , height , jumps)
    return transition.execute(target, action, args)
end


--[[

旋转变形

]]
function transition.skewTo(target, args)
    local tx, ty = target:getPosition()
    local x = args.x or tx
    local y = args.y or ty
    local action = CCSkewTo:create(args.time, x, y)
    return transition.execute(target, action, args)
end

--[[

闪烁

]]
function transition.blink(target, args)
    local action = CCBlink:create(args.time, args.num)
    return transition.execute(target, action, args)
end


--[[

震动

]]
function transition.shake(target , args)
    local target = target or display.getRunningScene()
    args = args or {}
    local time = args.time or 0.01
    local scaleValue = args.initScale or 1
    local addScale = args.addScale or 0.05
    local delay = args.delay or 0.01
    local x = args.x or 5
    local y = args.y or 5
    local onComplete = args.onComplete or nil
    
    transition.moveBy(target , {time = time , x = x , y = y})
    if not args.no_scale then transition.scaleTo(target, {time = time , scale = scaleValue + addScale }) end

    transition.moveBy(target , {delay = time + delay , time = time , x = 0 - x , y = 0 - y , onComplete = onComplete })
    if not args.no_scale then transition.scaleTo(target , {delay = time + delay , time = time , scale = scaleValue }) end
end

--[[

RGB通道变化
要恢复原来颜色的话，RGB都要等于255

** example **
    transition.tintTo( target , {
        time = 1,
        r = 255,
        g = 0,
        b = 0,
    })
]]
function transition.tintTo(target , args)
    local action
   if args then
         action = CCTintTo:create(args.time , args.r , args.g , args.b)
     else
        action =  CCTintTo:create(0,255,255,255 )
     end
    return transition.execute(target, action, args)
end


--[[

对一个CCNode里的所有子节点，播放同一个动画

]]
function transition.playSprites(target , actionName , args)
    local sprites = getAllSprites( target )
    if #sprites == 0 then return end

    local first_complete = true -- 保证 onComplete 只执行一次
    for i = 1 , #sprites do
        if first_complete == true and args.onComplete then
            first_complete = false
        else
            args.onComplete = nil
        end

        transition[actionName]( sprites[i] , args)
    end
end






