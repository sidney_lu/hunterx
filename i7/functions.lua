
--if the table has the pass value,
function table.hasValue(t,value)
	local has = false
	for k,v in pairs(t) do
		if v == value then
			has = true
			break
		end
	end
	return has
end

--[[
join a string by string.
@param table array
@param string delimiter
@return table
]]--
function string.join(array, delimiter)
	if (delimiter=='') then return false end
	local str = '';
	for k , v in pairs(array) do
		str = str .. v .. delimiter
	end
	str = string.sub(str , 0,string.len(str) - string.len(delimiter) )
	return str
end

-- 判断字符串只包含字母，数字
function string.valid(str)
	local pos = string.len(str)
	while pos > 0 do
		local b = string.byte(str, pos)
		if (b>=48 and b<=57) or (b>=65 and b<=90) or (b>=97 and b<=122) then
		else
			return false
		end
		pos = pos - 1
	end
	return true
end
-- 20150716 addby sidney
function string.convertMoney( goldValue )
	return (goldValue>10000) and math.floor(goldValue/10000) .. "万" or goldValue
end
--[[--

Split a string by string.

@param string str
@param string delimiter
@return table

]]
function i7Split(str, delimiter)
	if (delimiter=='') then return false end
	local pos,arr = 0, {}
	-- for each divider found
	for st,sp in function() return string.find(str, delimiter, pos, true) end do
		table.insert(arr, string.sub(str, pos, st - 1))
		pos = sp + 1
	end
	table.insert(arr, string.sub(str, pos))
	return arr
end


function i7PlaySound(file)
	if isSoundAllowed then
		audio.playSound(file)
	end
end

function i7PlayMusic(file)
	print("-----i7PlayMusic:", isMusicAllowed)
	if isMusicAllowed then
		print("---play")
		audio.stopMusic()
		audio.playMusic(file)
	end
end




