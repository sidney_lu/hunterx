local i7cc = {}
i7cc.networks = {}

function i7cc.print()
	print("--------i7cc!")
end 

function i7cc.networks:get(url, url_params, callback, params)
	params = checktable(params)
	local timeout = params.timeout or 15
	--utils
	------------------------------------------------------------------------------
	local function build_params(data)
		local str = ""
		if type(data) ~= "table" then return "" end
		for k , v in pairs(data) do
			str = str .. k .. "=" .. v .. "&"
		end
		str = "?" .. str
		return str
	end

	function onRequestFinished(event)
		local ok = (event.name == "completed")
		local request = event.request

		-- for k,v in pairs(event) do
		-- 	print(k,v)
		-- end
	        	if event.name == "progress" then
	        		-- print("event.total , event.dltotal:", event.total , event.dltotal)
	        		if params.progress_callback then
				params.progress_callback(event.total, event.dltotal)
			end
			return
		end

		if not ok then
			-- 请求失败，显示错误代码和错误消息
			print(request:getErrorCode(), request:getErrorMessage())
			return
		end

		local code = request:getResponseStatusCode()
		if code ~= 200 then
			-- 请求结束，但没有返回 200 响应代码
			print(code)
			return
		end

		-- 请求成功，显示服务端返回的内容
		local response = request:getResponseString()
		-- print(response)
		callback(response, code)
	end
	------------------------------------------------------------------------------
	if url_params then
		url = url .. build_params(url_params) --setup url
	end
	print("@[i7cc.networks:get]>>url:", url)
	local request = network.createHTTPRequest(onRequestFinished, url, "GET")
	request:setTimeout(timeout)
	request:start()
end


return i7cc

