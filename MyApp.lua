
require("config")
require("cocos.init")
require("framework.init")
require("deprecated")
require("config2")

require("initialization.init_functions")
require("Config.channel")

require("i7.init")
i7cc = require("i7cc")

IMG_PATH = ""

requires = require

function game_start()
	-- 设置图片质量
	local cclog = function(...)
		print(string.format(...))
	end

	require("Config.conf") 			-- 配置文件
	require("Common.CommonFunction")

	-- 引入框架
	-- requires("framework.init")
	-- requires("Config.channel")			-- 渠道号(一定要放在框架引入后面)


	-- 游戏入口 - 后续可以去掉很多
	require("Data.Session")
	require("Network.http")
	require("Network.socket")
	

	require("Common.CommonFunction")
	require("SwitchScene")


	-- 常用组件
	require("Common.KNButton")
	require("Common.KNBtn")
	require("Common.KNMsg")
	require("Common.KNScrollView")

	-- 全局引导
	require("Common.KNGuide")
	--全局时钟
	require("Common.KNClock"):new()

	require("Common.KNFileManager")

	----------------
	


	local isReal = KNFileManager.isExist("savefile.txt")
	if not isReal then
		KNFileManager.updatafile("savefile.txt" , "sound" , "=" , 1)
		KNFileManager.updatafile("savefile.txt" , "audio" , "=" , 1)
	end	


	
	--创建登录场景
	switchScene("login" , nil, function()
		if ERROR_CODE ~= nil and ERROR_CODE ~= 0 and ERROR_MSG ~= nil and ERROR_MSG ~= "" then
			KNMsg.getInstance():flashShow(ERROR_MSG)
			ERROR_MSG = ""
			ERROR_CODE = 0
		end
	end)
end

local MyApp = class("MyApp", cc.mvc.AppBase)

function MyApp:ctor()
	MyApp.super.ctor(self)
end

function MyApp:run()
	cc.FileUtils:getInstance():addSearchPath("res/")
	
	-- Begin
	xpcall(function()
		require("Config.channel")
		-- 设置SD卡路径
		-- if INIT_FUNCTION.platform == 0 or INIT_FUNCTION.platform == 3 then 			-- windows
		-- 	print("------platform 0/3")
		-- 	IMG_PATH = ""
		-- elseif INIT_FUNCTION.platform == 1 then 		-- Android
		-- 	print("------platform Android")
		-- 	-- IMG_PATH = "/mnt/sdcard/eagle/" .. CHANNEL_ID .. "/"
		-- 	local serpath = device.writablePath .. "src"
		-- 	print("-----writablePath:", serpath)
		-- 	cc.FileUtils:getInstance():addSearchPath(serpath, true)
		-- 	IMG_PATH = ""
		-- elseif INIT_FUNCTION.platform == 2 then 		-- IOS
		-- 	print("------platform iOS")
		-- 	--IMG_PATH = CCFileUtils:sharedFileUtils():getWritablePath()
		-- 	local serpath = device.writablePath
		-- 	cc.FileUtils:getInstance():addSearchPath(serpath, true)
		-- 	IMG_PATH = ""
		-- else
		-- 	print("------simulator")
		-- 	print("device.writablePath:", device.writablePath)
		-- 	cc.FileUtils:getInstance():addSearchPath("writablePath/src", true)
		-- 	-- cc.FileUtils:getInstance():addSearchPath("writablePath/src", true)
		-- 	IMG_PATH = ""
		-- end
		
		if device.platform == "ios" then
			print("[[[[[[[[ios]]]]]]]]")
			--IMG_PATH = CCFileUtils:sharedFileUtils():getWritablePath()
			local serpath = device.writablePath .. "src"
			print("-----writablePath:", serpath)
			cc.FileUtils:getInstance():addSearchPath(serpath, true)
			IMG_PATH = ""
		elseif device.platform == "android" then
			print("[[[[[[[[android]]]]]]]]")
			-- IMG_PATH = "/mnt/sdcard/eagle/" .. CHANNEL_ID .. "/"
			local serpath = device.writablePath .. "src"
			print("-----writablePath:", serpath)
			cc.FileUtils:getInstance():addSearchPath(serpath, true)
			IMG_PATH = ""
		else
			print("[[[[[[[[simulator]]]]]]]]")
			print("device.writablePath:", device.writablePath)
			cc.FileUtils:getInstance():addSearchPath("writablePath/src", true)
			-- cc.FileUtils:getInstance():addSearchPath("writablePath/src", true)
			IMG_PATH = ""
		end

		-- 非 windows 平台，则要走以下逻辑 (初始化 && 在线升级)
		local scene = require "initialization.init_scene"
		-- CCDirector:sharedDirector():runWithScene(scene:create())


		display.replaceScene(scene:create(), "crossFade", 0.3)
		
	end , __G__TRACKBACK__)    	
end

function MyApp:preLoadFrames( imageUri ,imageName)
	--local texture=CCTextureCache:sharedTextureCache():addImage(CCFileUtils:sharedFileUtils():getCachePath().."..\\res\\"..imageUri)
	local texture=cc.Director:getInstance():getTextureCache():addImage(imageUri)
    local texSize=texture:getContentSize()
    local texRect = cc.rect(0, 0, texSize.width, texSize.height);
    local frame=cc.SpriteFrame:createWithTexture(texture,texRect)
	cc.SpriteFrameCache:getInstance():addSpriteFrame(frame,imageName);
end

return MyApp
